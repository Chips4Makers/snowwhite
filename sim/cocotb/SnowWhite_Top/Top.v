module Top(
    input TCK,
    input TMS,
    input TDI,
    output TDO,
    input TRST_N,
    inout IO0,
    inout IO1,
    inout IO2,
    inout IO3,
    inout IO4,
    inout IO5,
    inout IO6,
    inout IO7,
    inout IO8,
    inout IO9,
    inout IO10,
    inout IO11,
    inout IO12,
    inout IO13,
    inout IO14,
    inout IO15,
    inout IO16,
    inout IO17,
    inout IO18,
    inout IO19,
    inout IO20,
    inout IO21,
    inout IO22,
    inout IO23,
    inout IO24,
    inout IO25,
    inout IO26,
    inout IO27,
    inout IO28,
    inout IO29,
    inout IO30,
    inout IO31,
    input RX,
    output TX,
    input sys_clk,
    input sys_rst
);

initial begin
    $dumpfile("test.vcd");
    $dumpvars;
end

SnowWhite_Top top(
    .*
);

endmodule // Top
