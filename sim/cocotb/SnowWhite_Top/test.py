import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps

@cocotb.test()
def test01_resetrun(dut):
    "Basic test; reset and run for a few cycles"

    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.sys_clk, clk_period).start())

    dut.sys_rst = 1

    yield Timer(int(1.75*clk_period))

    dut.sys_rst = 0

    yield Timer(5*clk_period)
