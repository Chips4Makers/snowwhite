-- The JTAG interface for SnowWhite

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.c4m_jtag.ALL;

-- TODO: Implement boundary scan for IOs with only input or output functionality

entity SnowWhite_JTAG is
  port (
    Clock:      in std_logic;

    -- The TAP signals
    TCK:        in std_logic;
    TMS:        in std_logic;
    TDI:        in std_logic;
    TDO:        out std_logic;
    TRST_N:     in std_logic;

    -- The I/O access ports
    CORE_IN:    out std_logic_vector(33 downto 0);
    CORE_EN:    in std_logic_vector(33 downto 0);
    CORE_OUT:   in std_logic_vector(33 downto 0);

    -- The pad connections
    PAD_IN:     in std_logic_vector(33 downto 0);
    PAD_EN:     out std_logic_vector(33 downto 0);
    PAD_OUT:    out std_logic_vector(33 downto 0);

    -- WB interface
    WB_Adr_o:   out std_logic_vector(31 downto 0);
    WB_Dat_o:   out std_logic_vector(31 downto 0);
    WB_Dat_i:   in std_logic_vector(31 downto 0);
    WB_WE_o:    out std_logic;
    WB_Stb_o:   out std_logic;
    WB_Cyc_o:   out std_logic;
    WB_Ack_i:   in std_logic;
    WB_Err_i:   in std_logic
  );
end SnowWhite_JTAG;

architecture rtl of SnowWhite_JTAG is
  signal TAP_STATE:     TAPSTATE_TYPE;
  signal DRSTATE:       std_logic;
  signal TAP_TDO:       std_logic;
  signal TAP_TDO_EN:    std_logic;
  constant IR_WIDTH:    integer := 3;
  constant IOS:         integer := 32 + 2; -- 32 GPIOs + UART
  signal IR:            std_logic_vector(IR_WIDTH-1 downto 0);
  constant CMD_MEMADDR: std_logic_vector(IR_WIDTH-1 downto 0) := "011";
  constant CMD_MEMRD:   std_logic_vector(IR_WIDTH-1 downto 0) := "101";
  constant CMD_MEMWR:   std_logic_vector(IR_WIDTH-1 downto 0) := "110";
  signal ADDR_SR:       unsigned(31 downto 0);
  signal ADDR_WR:       unsigned(31 downto 0);
  signal DATA_SR:       std_logic_vector(31 downto 0);
  signal DATA_RD:       std_logic_vector(31 downto 0);
  signal DATA_WR:       std_logic_vector(31 downto 0);
  signal ISADDRCMD:     boolean;
  signal ISREADCMD:     boolean;
  signal ISWRITECMD:    boolean;
  signal ISMEMCMD:      boolean;

  signal do_read:       boolean;
  signal done_read:     boolean;
  signal do_write:      boolean;
  signal done_write:    boolean;
  signal JTAG_Stb:      std_logic;
begin
  -- The TAP controller
  JTAG: component c4m_jtag_tap_controller
    generic map (
      IR_WIDTH => IR_WIDTH,
      IOS => IOS,
      VERSION => "0101"
    )
    port map (
      TCK => TCK,
      TMS => TMS,
      TDI => TDI,
      TDO => TAP_TDO,
      TDO_EN => TAP_TDO_EN,
      TRST_N => TRST_N,
      STATE => TAP_STATE,
      NEXT_STATE => open,
      DRSTATE => DRSTATE,
      IR => IR,
      CORE_IN => CORE_IN,
      CORE_EN => CORE_EN,
      CORE_OUT => CORE_OUT,
      PAD_IN => PAD_IN,
      PAD_EN => PAD_EN,
      PAD_OUT => PAD_OUT
    );

  -- Handle the memory access.
  -- Currently only handle read or write in 32 bits at a time. Always an update
  -- has to happen after every 32 bits more bits will be lost, less bits will
  -- result with old data in memory be partly shifted.
  -- For CMD_MEMWR also the data in the location will be read and shifted out.
  -- TODO: Allow streaming of longer length than 32 bits
  ISADDRCMD <= DRSTATE = '1' and IR = CMD_MEMADDR;
  ISREADCMD <= DRSTATE = '1' and (IR = CMD_MEMRD or IR = CMD_MEMWR);
  ISWRITECMD <= DRSTATE = '1' and (IR = CMD_MEMWR);
  ISMEMCMD <= ISREADCMD or ISWRITECMD;

  -- Select the right TDO signals, local ones have priority over JTAG one
  TDO <= ADDR_SR(0) when ISADDRCMD and TAP_STATE = Shift else
         DATA_SR(0) when ISMEMCMD and TAP_STATE = Shift else
         TAP_TDO;
  TDO_CHECK: process(all)
  begin
    if (TAP_STATE = Shift) and (ISADDRCMD or ISMEMCMD) then
      assert TAP_TDO_EN = '0'
        report "TDO driver conflict between JTAG and command"
        severity ERROR;
    end if;
  end process;
  
  process (TCK, Clock, TRST_N)
  begin
    if TRST_N = '0' then
      ADDR_SR <= (others => '0');
      DATA_SR <= (others => '0');

      do_read <= false;
      done_read <= false;
      do_write <= false;
      done_write <= false;
      JTAG_Stb <= '0';
    else
      if rising_edge(TCK) then
        -- Shift address register
        if ISADDRCMD then
          if TAP_STATE = Shift then
            ADDR_SR(30 downto 0) <= ADDR_SR(31 downto 1);
            ADDR_SR(31) <= TDI;
          elsif TAP_STATE = Update then
            do_read <= true; -- Start a read cycle
          end if;
        end if;

        -- Shift data register
        if ISMEMCMD and TAP_STATE = Shift then
          DATA_SR(30 downto 0) <= DATA_SR(31 downto 1);
          DATA_SR(31) <= TDI;
        end if;

        -- When read put data in DATA_SR during Capture
        if ISREADCMD and TAP_STATE = Capture then
          DATA_SR <= DATA_RD;
        end if;

        -- Increase address when in Update and there was a write and/or a read
        if ISMEMCMD and TAP_STATE = Update then
          if ISWRITECMD then
            do_write <= true;
            ADDR_WR <= ADDR_SR;
            DATA_WR <= DATA_SR;
          end if;
          ADDR_SR <= ADDR_SR + 1;
          do_read <= true;
        end if;

        if do_read and done_read then
          do_read <= false;
        end if;
        if do_write and done_write then
          do_write <= false;
        end if;
      end if;

      if rising_edge(Clock) then
        -- Do read or write cycle, do first read cycle
        -- TODO: See if we can get rid of extra TCK cycles for handling JTAG_Stb
        if do_read then
          if (JTAG_Stb = '0') and not done_read then
            JTAG_Stb <= '1';
          elsif WB_Ack_i = '1' or WB_Err_i = '1' then
            done_read <= true;
            if WB_Ack_i = '1' then
              DATA_RD <= WB_Dat_i;
            else
              DATA_RD <= (others => 'X');
            end if;
            JTAG_Stb <= '0';
          end if;
        elsif do_write then
          if (JTAG_Stb = '0') and not done_write then
            JTAG_Stb <= '1';
          elsif WB_Ack_i = '1' or WB_Err_i = '1' then
            done_write <= true;
            JTAG_Stb <= '0';
          end if;
        end if;

        if done_read and not do_read then
          done_read <= false;
        end if;
        if done_write and not do_write then
          done_write <= false;
        end if;
      end if;
    end if;
  end process;

  WB_Adr_o <= std_logic_vector(ADDR_SR) when do_read else
              std_logic_vector(ADDR_WR);
  WB_Dat_o <= DATA_WR when do_write else
              (others => 'X');
  WB_WE_o <= '1' when (not do_read) and do_write else
             '0';
  WB_Cyc_o <= '1' when do_read or do_write else
              '0';
  WB_Stb_o <= JTAG_Stb;
end rtl;
