from migen import *

from misoc.cores.uart import RS232PHY, UART
from misoc.interconnect import wishbone, csr_bus, wishbone2csr
from misoc.interconnect.csr import *

from SnowWhite_SRAM import SnowWhite_SRAM
from SnowWhite_IO import *

class PicoRV32_WB(Module):
    "The PicoRV32 core used for SnowWhite"

    def __init__(self):
        self.bus = bus = wishbone.Interface(data_width=32, address_width=32)
        self.trap = trap = Signal()
        self.irq = irq = Signal(32)
        self.eoi = eoi = Signal(32)
        self.mem_instr = mem_instr = Signal()

        ##

        picorv32 = Instance(
            "picorv32_wb",
            p_BARREL_SHIFTER = 1,
            p_ENABLE_FAST_MUL = 1,
            p_ENABLE_DIV = 1,
            p_ENABLE_IRQ = 1,
            i_wb_rst_i = ResetSignal(),
            i_wb_clk_i = ClockSignal(),
            o_wbm_adr_o = bus.adr,
            o_wbm_dat_o = bus.dat_w,
            i_wbm_dat_i = bus.dat_r,
            o_wbm_we_o = bus.we,
            o_wbm_sel_o = bus.sel,
            o_wbm_stb_o = bus.stb,
            i_wbm_ack_i = bus.ack,
            o_wbm_cyc_o = bus.cyc,
            o_trap = trap,
            i_irq = irq,
            o_eoi = eoi,
            o_mem_instr = mem_instr,
        )
        self.specials += picorv32


class IO(Module, AutoCSR):
    "The JTAG module for SnowShite; the IOs are memory mapped using MiSoC CSRs"

    def __init__(self):
        self.ios = set()

        # The master wishbone bus driven by JTAG
        self.bus = bus = wishbone.Interface(data_width=32, address_width=32)

        self.TX = TX = Signal()
        self.RX = RX = Signal()

        ##

        # The IO signals for the JTAG interface
        PAD_IN = Signal(34)
        PAD_EN = Signal(34)
        PAD_OUT = Signal(34)
        CORE_IN = Signal(34)
        CORE_OUT = Signal(34)
        CORE_EN = Signal(34)

        # The JTAG IOs
        self.submodules.IO_TCK = IO_TCK = SnowWhite_In("TCK")
        self.ios |= IO_TCK.ios
        self.submodules.IO_TMS = IO_TMS = SnowWhite_In("TMS")
        self.ios |= IO_TMS.ios
        self.submodules.IO_TDI = IO_TDI = SnowWhite_In("TDI")
        self.ios |= IO_TDI.ios
        self.submodules.IO_TDO = IO_TDO = SnowWhite_Out("TDO")
        self.ios |= IO_TDO.ios
        self.submodules.IO_TRST_N = IO_TRST_N = SnowWhite_In("TRST_N")
        self.ios |= IO_TRST_N.ios

        # The 32 GPIOs
        IO_GPIOs = [SnowWhite_InOut("IO{}".format(i)) for i in range(32)]
        self.submodules += IO_GPIOs
        for i, io in enumerate(IO_GPIOs):
            self.ios |= io.ios
            self.comb += [
                PAD_IN[i].eq(io.CoreIn),
                io.CoreOut.eq(PAD_OUT[i]),
                io.CoreOutEn.eq(PAD_EN[i]),
            ]

        self._GPIO_IN = GPIO_IN = CSRStatus(32)
        self._GPIO_OUT = GPIO_OUT = CSRStorage(32)
        self._GPIO_EN = GPIO_EN = CSRStorage(32)
        self.comb += [
            GPIO_IN.status.eq(CORE_IN[0:32]),
            CORE_OUT[0:32].eq(GPIO_OUT.storage),
            CORE_EN[0:32].eq(GPIO_EN.storage),
        ]

        # The UART GPIOs
        self.submodules.IO_RX = IO_RX = SnowWhite_In("RX")
        self.ios |= IO_RX.ios
        self.submodules.IO_TX = IO_TX = SnowWhite_Out("TX")
        self.ios |= IO_TX.ios
        self.comb += [
            PAD_IN[32].eq(IO_RX.CoreIn),
            RX.eq(CORE_IN[32]),
            IO_TX.CoreOut.eq(PAD_OUT[33]),
            CORE_OUT[33].eq(TX),
            CORE_EN[33].eq(1),
        ]


        jtag = Instance(
            "SnowWhite_JTAG",
            i_Clock = ClockSignal(),
            i_TCK = IO_TCK.CoreIn,
            i_TMS = IO_TMS.CoreIn,
            i_TDI = IO_TDI.CoreIn,
            o_TDO = IO_TDO.CoreOut,
            i_TRST_N = IO_TRST_N.CoreIn,
            o_CORE_IN = CORE_IN,
            i_CORE_EN = CORE_EN,
            i_CORE_OUT = CORE_OUT,
            i_PAD_IN = PAD_IN,
            o_PAD_EN = PAD_EN,
            o_PAD_OUT = PAD_OUT,
            o_WB_Adr_o = bus.adr,
            o_WB_Dat_o = bus.dat_w,
            i_WB_Dat_i = bus.dat_r,
            o_WB_WE_o = bus.we,
            o_WB_Stb_o = bus.stb,
            o_WB_Cyc_o = bus.cyc,
            i_WB_Ack_i = bus.ack,
            i_WB_Err_i = bus.err,
        )
        self.specials += jtag


class SnowWhite_Top(Module):
    """The top common design for SnowWhite
    
    Parameters:
    - clk_frequency: the clock frequency; default = 10MHz
    - baud_rate: the start-up baudrate; will need right clock frequency specified;
                 default=115200

    Attributes:
    - ios: Set of all the chips attributes
    """

    def __init__(self, clk_frequency=10e6, baudrate=115200):
        self.ios = set()

        ##

        self.submodules.picorv32 = picorv32 = PicoRV32_WB()
        self.submodules.memory = mem = SnowWhite_SRAM()
        if hasattr(mem, "ios"):
            self.ios |= mem.ios

        self.submodules.io = io = IO()
        self.ios |= io.ios

        self.rxtx = rxtx = Record([("rx", 1), ("tx", 1)], "uart")
        self.comb += [
            rxtx.rx.eq(io.RX),
            io.TX.eq(rxtx.tx),
        ]
        phy = RS232PHY(rxtx, clk_frequency, baudrate)
        self.submodules.uart_phy = phy
        self.submodules.uart = UART(phy)

        bus = wishbone.Interface(data_width=32, address_width=32)
        csrbus = wishbone.Interface(data_width=32, address_width=32)
        self.submodules.decoder = wishbone.Decoder(bus, [
            (lambda a: a[31] == 0, mem.bus),
            (lambda a: a[31] == 1, csrbus),
        ])
        self.submodules.arbiter = wishbone.Arbiter([picorv32.bus, io.bus], bus)


        # CSR
        # Currently page size is hardcoded to 512 bytes
        # Make the addres 14 bits wide
        self.submodules.wishbone2csr = wishbone2csr.WB2CSR(
            bus_wishbone=csrbus,
            bus_csr=csr_bus.Interface(32, 32)
        )
        self.submodules.csrbankarray = csr_bus.CSRBankArray(
            self, self.get_csr_dev_address,
            data_width=32, address_width=32
        )
        self.submodules.csrcon = csr_bus.Interconnect(
            self.wishbone2csr.csr, self.csrbankarray.get_buses()
        )

    def get_csr_dev_address(self, name, memory):
        if not hasattr(self, "_csr_index"):
            # Put CSR in top half of memory; 9 bits are inside page
            self._csr_index = 2**(23 - 1)
        else:
            self._csr_index += 1
        return self._csr_index


if __name__ == "__main__":
    from migen.fhdl.verilog import convert

    top = SnowWhite_Top()
    convert(top, top.ios, name="SnowWhite_Top").write("SnowWhite_Top.v")
