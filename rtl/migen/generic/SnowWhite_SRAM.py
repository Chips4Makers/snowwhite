# This class is put in separate file so it can be overloaded with a hardware implementation
from migen import *

from misoc.interconnect import wishbone

class SnowWhite_SRAM(wishbone.SRAM):
    """SnowWhite_SRAM is the class to represent the on-chip SRAM on a SnowWhite core.
    It can be overloaded with a specific hardware implementation.

    It should the following attributes:
    - size: The size in bits of the SRAM
    - bus: the wh-shbone slace bus; it has to have certain sizes:
        address_width: 32, data_width: 32, sel: 4

    Generic implementation uses the wishbone.SRAM class"""

    def __init__(self):
        self.size = 6*1024
        wishbone.SRAM.__init__(self,
            self.size, bus=wishbone.Interface(data_width=32, address_width=32)
        )
