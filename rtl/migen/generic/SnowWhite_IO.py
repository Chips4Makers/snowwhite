"""Generic implementation of IO cells.
This is to be used for simuation, for hardware implementation this module needs to be overloaded"""
from migen import *


class SnowWhite_In(Module):
    """The input module.

    parameters:
    - netname: the name of the net of the pad; this can be used by the overloaded module
        to set different characteristics for different nets

    attributes:
    - netname: the net name given as parameter
    - ios: the signals to be treated as port of top level verilog
    - CoreIn: the input signal to the core"""

    def __init__(self, netname):
        self.netname = netname
        Ext = Signal(name=netname)
        self.ios = {Ext}
        self.CoreIn = CoreIn = Signal()

        ##

        self.comb += CoreIn.eq(Ext)


class SnowWhite_Out(Module):
    """The output module.

    paramters:
    - netname: the name of the net of the pad; this can be used by the overloaded module
        to set different characteristics for different nets

    attributes:
    - netname: the net name given as parameter
    - ios: the signals to be treated as port of top level verilog
    - CoreOut: the output signal from the core"""

    def __init__(self, netname):
        self.netname = netname
        Ext = Signal(name=netname)
        self.ios = {Ext}
        self.CoreOut = CoreOut = Signal()

        ##

        self.comb += Ext.eq(CoreOut)


class SnowWhite_InOut(Module):
    """The input/output module.

    parameters:
    - netname: the name of the net of the pad; this can be used by the overloaded module
        to set different characteristics for different nets

    attributes:
    - netname: the net name given as parameter
    - ios: the signals to be treated as port of top level verilog
    - CoreIn: the input signal to the core
    - CoreOut: the output signal from the core
    - CoreOutEn: the output enable signal from the core"""

    def __init__(self, netname):
        self.netname = netname
        Ext = Signal(name=netname)
        self.ios = {Ext}
        self.CoreIn = CoreIn = Signal()
        self.CoreOut = CoreOut = Signal()
        self.CoreOutEn = CoreOutEn = Signal()

        ##

        ts = TSTriple()
        self.specials += ts.get_tristate(Ext)
        self.comb += [
            CoreIn.eq(ts.i),
            ts.o.eq(CoreOut),
            ts.oe.eq(CoreOutEn),
        ]


#TODO: pull-up, pull-down on external signal; Schmitt trigger implementation.
