from migen import *

from misoc.interconnect import wishbone

from tm035ssram1024x8 import TM035SSRAM1024X8


class SnowWhite_SRAM(Module):
    """The module providing on-chip SRAM using the C035G 1024x8 SRAM macro"""

    def __init__(self):
        self.size = 4*1024
        self.bus = bus = wishbone.Interface(data_width=32, address_width=32)

        ##

        # TODO: Look if we can implement clock gating for the SRAM block

        # self.submodules.ram1 = ram1 = TM035SSRAM1024X8(name="RAM1")
        # self.comb += [
        #     ram1.A.eq(bus.adr[0:10]),
        #     ram1.CE.eq(ClockSignal()),
        #     ram1.OEB.eq(0),
        #     # Only write when in a Wishbone cycle, otherwise do read
        #     ram1.WEB.eq((~bus.we) & bus.cyc & bus.stb & bus.sel[0]),
        #     ram1.DIN.eq(bus.dat_w[0:8]),
        #     bus.dat_r[0:8].eq(ram1.DOUT),
        # ]

        # self.submodules.ram2 = ram2 = TM035SSRAM1024X8(name="RAM2")
        # self.comb += [
        #     ram2.A.eq(bus.adr[0:10]),
        #     ram2.CE.eq(ClockSignal()),
        #     ram2.OEB.eq(0),
        #     # Only write when in a Wishbone cycle, otherwise do read
        #     ram2.WEB.eq((~bus.we) & bus.cyc & bus.stb & bus.sel[1]),
        #     ram2.DIN.eq(bus.dat_w[8:16]),
        #     bus.dat_r[8:16].eq(ram2.DOUT),
        # ]

        # self.submodules.ram3 = ram3 = TM035SSRAM1024X8(name="RAM3")
        # self.comb += [
        #     ram3.A.eq(bus.adr[0:10]),
        #     ram3.CE.eq(ClockSignal()),
        #     ram3.OEB.eq(0),
        #     # Only write when in a Wishbone cycle, otherwise do read
        #     ram3.WEB.eq((~bus.we) & bus.cyc & bus.stb & bus.sel[2]),
        #     ram3.DIN.eq(bus.dat_w[16:24]),
        #     bus.dat_r[16:24].eq(ram3.DOUT),
        # ]

        # self.submodules.ram4 = ram4 = TM035SSRAM1024X8(name="RAM4")
        # self.comb += [
        #     ram4.A.eq(bus.adr[0:10]),
        #     ram4.CE.eq(ClockSignal()),
        #     ram4.OEB.eq(0),
        #     # Only write when in a Wishbone cycle, otherwise do read
        #     ram4.WEB.eq((~bus.we) & bus.cyc & bus.stb & bus.sel[3]),
        #     ram4.DIN.eq(bus.dat_w[24:32]),
        #     bus.dat_r[24:32].eq(ram4.DOUT),
        # ]

        A = Signal(10)
        WEB = Signal(4)
        DIN = Signal(32)
        DOUT = Signal(32)
        self.ios = {A, WEB, DIN, DOUT}
        self.comb += [
            A.eq(bus.adr[0:10]),
            WEB.eq(Cat(
                (~bus.we) & bus.cyc & bus.stb & bus.sel[0],
                (~bus.we) & bus.cyc & bus.stb & bus.sel[1],
                (~bus.we) & bus.cyc & bus.stb & bus.sel[2],
                (~bus.we) & bus.cyc & bus.stb & bus.sel[3],
            )),
            DIN.eq(bus.dat_w),
            bus.dat_r.eq(DOUT),
        ]
        self.sync += [
            bus.ack.eq(bus.cyc & bus.stb),
        ]

if __name__ == "__main__":
    from migen.fhdl.verilog import convert

    sram = SnowWhite_SRAM()
    bus = sram.bus
    ios = {
        bus.adr, bus.dat_w, bus.dat_r, bus.sel, bus.cyc, bus.stb, bus.ack, bus.we,
    }
    convert(sram, ios, name="SnowWhite_SRAM").write("SnowWhite_SRAM.v")
