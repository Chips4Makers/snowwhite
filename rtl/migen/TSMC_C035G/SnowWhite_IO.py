"""Implementation of the SnowWhite IO using the TSMC tph035pnv3 library"""
from migen import *

from tph035pnv3 import *


class SnowWhite_In(Module):
    def __init__(self, netname):
        self.netname = netname
        # Ext = Signal(name=netname)
        self.CoreIn = CoreIn = Signal(name=netname)
        self.ios = {CoreIn}

        ##

        # self.submodules.io = io = PDDWUWSW0204DGZ(name="IO_"+netname)
        # self.comb += [
        #     io.I.eq(0),
        #     io.DS.eq(0),
        #     io.OEN.eq(1),
        #     io.IE.eq(1),
        #     io.SE.eq(0),
        #     CoreIn.eq(io.C),
        #     io.PE.eq(0),
        #     io.PS.eq(0),
        #     io.PAD.eq(Ext),
        # ]

        # TODO: Store net and handle two different drive strengths


class SnowWhite_Out(Module):
    def __init__(self, netname):
        self.netname = netname
        # Ext = Signal(name=netname)
        self.CoreOut = CoreOut = Signal(name=netname)
        self.ios = {CoreOut}

        ##

        # self.submodules.io = io = PDDWUWSW0204DGZ(name="IO_"+netname)
        # self.comb += [
        #     io.I.eq(CoreOut),
        #     io.DS.eq(0),
        #     io.OEN.eq(0),
        #     io.IE.eq(0),
        #     io.SE.eq(0),
        #     io.PE.eq(0),
        #     io.PS.eq(0),
        #     io.PAD.eq(Ext),
        # ]

        # TODO: Store net and handle two different drive strengths


class SnowWhite_InOut(Module):
    def __init__(self, netname):
        self.netname = netname
        self.Ext = Ext = Signal(name=netname)
        self.CoreIn = CoreIn = Signal(name=netname+"_In")
        self.CoreOut = CoreOut = Signal(name=netname+"_Out")
        self.CoreOutEn = CoreOutEn = Signal(name=netname+"_En")
        self.ios = {CoreIn, CoreOut, CoreOutEn}

        ##
        
        # self.submodules.io = io = PDDWUWSW0204DGZ(name="IO_"+netname)
        # self.comb += [
        #     io.I.eq(CoreOut),
        #     io.DS.eq(0),
        #     io.OEN.eq(~CoreOutEn),
        #     io.IE.eq(1),
        #     io.SE.eq(0),
        #     CoreIn.eq(io.C),
        #     io.PE.eq(0),
        #     io.PS.eq(0),
        #     io.PAD.eq(Ext),
        #     io.DS.eq(0),
        # ]

        
        # TODO: Store net and handle two different drive strengths
