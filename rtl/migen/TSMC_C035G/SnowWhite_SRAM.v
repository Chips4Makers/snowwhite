/* Machine-generated using Migen */
module SnowWhite_SRAM(
	input [31:0] bus_adr,
	input [31:0] bus_dat_w,
	output reg [31:0] bus_dat_r,
	input [3:0] bus_sel,
	input bus_cyc,
	input bus_stb,
	output reg bus_ack,
	input bus_we,
	input sys_clk,
	input sys_rst
);

wire ram1_CE;
wire ram1_OEB;
wire ram1_WEB;
wire [9:0] ram1_A;
wire [7:0] ram1_DIN;
wire [7:0] ram1_DOUT;
wire ram2_CE;
wire ram2_OEB;
wire ram2_WEB;
wire [9:0] ram2_A;
wire [7:0] ram2_DIN;
wire [7:0] ram2_DOUT;
wire ram3_CE;
wire ram3_OEB;
wire ram3_WEB;
wire [9:0] ram3_A;
wire [7:0] ram3_DIN;
wire [7:0] ram3_DOUT;
wire ram4_CE;
wire ram4_OEB;
wire ram4_WEB;
wire [9:0] ram4_A;
wire [7:0] ram4_DIN;
wire [7:0] ram4_DOUT;

// synthesis translate_off
reg dummy_s;
initial dummy_s <= 1'd0;
// synthesis translate_on

assign ram1_A = bus_adr[9:0];
assign ram1_CE = sys_clk;
assign ram1_OEB = 1'd0;
assign ram1_WEB = ((((~bus_we) & bus_cyc) & bus_stb) & bus_sel[0]);
assign ram1_DIN = bus_dat_w[7:0];
assign ram2_A = bus_adr[9:0];
assign ram2_CE = sys_clk;
assign ram2_OEB = 1'd0;
assign ram2_WEB = ((((~bus_we) & bus_cyc) & bus_stb) & bus_sel[1]);
assign ram2_DIN = bus_dat_w[15:8];
assign ram3_A = bus_adr[9:0];
assign ram3_CE = sys_clk;
assign ram3_OEB = 1'd0;
assign ram3_WEB = ((((~bus_we) & bus_cyc) & bus_stb) & bus_sel[2]);
assign ram3_DIN = bus_dat_w[23:16];
assign ram4_A = bus_adr[9:0];
assign ram4_CE = sys_clk;
assign ram4_OEB = 1'd0;
assign ram4_WEB = ((((~bus_we) & bus_cyc) & bus_stb) & bus_sel[3]);
assign ram4_DIN = bus_dat_w[31:24];

// synthesis translate_off
reg dummy_d;
// synthesis translate_on
always @(*) begin
	bus_dat_r <= 32'd0;
	bus_dat_r[7:0] <= ram1_DOUT;
	bus_dat_r[15:8] <= ram2_DOUT;
	bus_dat_r[23:16] <= ram3_DOUT;
	bus_dat_r[31:24] <= ram4_DOUT;
// synthesis translate_off
	dummy_d <= dummy_s;
// synthesis translate_on
end

always @(posedge sys_clk) begin
	bus_ack <= (bus_cyc & bus_stb);
	if (sys_rst) begin
		bus_ack <= 1'd0;
	end
end

TM035SSRAM1024X8 TM035SSRAM1024X8(
	.A(ram1_A),
	.CE(ram1_CE),
	.DIN(ram1_DIN),
	.OEB(ram1_OEB),
	.WEB(ram1_WEB),
	.DOUT(ram1_DOUT)
);

TM035SSRAM1024X8 TM035SSRAM1024X8_1(
	.A(ram2_A),
	.CE(ram2_CE),
	.DIN(ram2_DIN),
	.OEB(ram2_OEB),
	.WEB(ram2_WEB),
	.DOUT(ram2_DOUT)
);

TM035SSRAM1024X8 TM035SSRAM1024X8_2(
	.A(ram3_A),
	.CE(ram3_CE),
	.DIN(ram3_DIN),
	.OEB(ram3_OEB),
	.WEB(ram3_WEB),
	.DOUT(ram3_DOUT)
);

TM035SSRAM1024X8 TM035SSRAM1024X8_3(
	.A(ram4_A),
	.CE(ram4_CE),
	.DIN(ram4_DIN),
	.OEB(ram4_OEB),
	.WEB(ram4_WEB),
	.DOUT(ram4_DOUT)
);

endmodule
