"""Migen wrapper for the tph035pnv3 IO library"""
from migen import *


class PDDWUWSW0204DGZ(Module):
    def __init__(self, name=None):
        name = "PDDWUWSW0204DGZ" if name is None else name
        self.name = name
        self.I = I = Signal()
        self.OEN = OEN = Signal()
        self.PAD = PAD = Signal()
        self.C = C = Signal()
        self.PS = PS = Signal()
        self.PE = PE = Signal()
        self.IE = IE = Signal()
        self.DS = DS = Signal()
        self.SE = SE = Signal()

        ##

        io = Instance(
            "PDDWUWSW0204DGZ",
            name = name,
            i_I = I,
            i_OEN = OEN,
            io_PAD = PAD,
            o_C = C,
            i_PS = PS,
            i_PE = PE,
            i_IE = IE,
            i_DS = DS,
            i_SE = SE,
        )
        self.specials += io


# TODO: Other IO cells
