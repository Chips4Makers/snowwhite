"""Migen wrapper for the TM035SSRAM1024x8 SRAM macro"""
from migen import *


class TM035SSRAM1024X8(Module):
    def __init__(self, name=None):
        name = "TM035SSRAM1024X8" if name is None else name
        self.name = name
        self.CE = CE = Signal()
        self.OEB = OEB = Signal()
        self.WEB = WEB = Signal()
        self.A = A = Signal(10)
        self.DIN = DIN = Signal(8)
        self.DOUT = DOUT = Signal(8)

        ##

        sram = Instance(
            "TM035SSRAM1024X8",
            name = name,
            i_CE = CE,
            i_OEB = OEB,
            i_WEB = WEB,
            i_A = A,
            i_DIN = DIN,
            o_DOUT = DOUT,
        )
        self.specials += sram
