# Description

This is the code for the Chips4Makers test tape-outs.

# Configuration

Tools and paths need to be configured in Makefile.conf in top directory.

# Building, simulating, ...

Currently no top Makefile is present so one has to go to different subdirectories
to perform actions.

* rtl/verilog  
  Run make here to generate verilog code from migen and to convert VHDL code of the JTAG interface to verilog. The generated top cell is a generic one.
* sim/cocotb/SnowWhite_Top  
  Run make here to run a short testbench.
* designs/SnowWhiteI
  * rtl/verilog  
    Run make here to generate verilog code for top cell. This is generating code using the TSMC library cells.
  * coriolis  
    Run make here to synthesize the code to a .blif file.  
    ioring.def is currently converted manually to .gds using klayout and with intermediate text GDSII files.
* designs/SnowWhiteII
  * There is a top Makefile. Type `make all` in directory to make everything; `make clean` to clean up after yourself. The SRAM5V block can't be fully rebuilt as resulting gds from OpenRAM was manually fixed before tape-out. Issues have been made on OpenRAM github repo so the need for these manual fixes can be fixed.

# Dependencies

Here is a list of tools needed to build all the files:

* rtl: Migen/MiSoC, yosys with verific plugin  
  [Migen](https://m-labs.hk/migen/index.html) ([github](https://github.com/m-labs/migen)) and MiSoC ([github](https://github.com/m-labs/misoc)) are used to write RTL code. A python environment with these libraries installed is thus necessary.  
  For conversion of VHDL to verilog yosys with the proprietary verific plugin is used.
* sim/cocotb: cocotb & Modelsim  
  For simulation [cocotb](http://potential.ventures/cocotb/) ([github](https://github.com/potentialventures/cocotb)) is used combined with Mentor Modelsim for mixed language simulation. The Modelsim delivered with the Quartus toolsuite is used; this is a 32 bit version and therefor also a 32 bit python is needed.
* designs/SnowWhiteI: TSMC libraries  
  * tcb773pdgo standard cell library
  * tph035pnv3 IO library
  * tm035ssram1024x8 SRAM macro
* designs/SnowWhiteI: TSMC libraries  
  * tph035pnv3 IO library
  * qflow 1.3 with osu035_redm4 technology  
    code found on [qflow gitlab repo](https://gitlab.com/Chips4Makers/qflow) under the [SnowWhiteII tag](https://gitlab.com/Chips4Makers/qflow))
  * klayout for scripting
* .def to .gds: klayout
  klayout is used to convert .def files to .gds files
