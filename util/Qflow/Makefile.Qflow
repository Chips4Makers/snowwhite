# This is makefile to include in a Makefile in a directory where one wants
# to do physical implementation of a design using qflow.
# Before including this file the QFLOWTOP and QFLOWTECH Makefile variable has to be
# set to tell the top cell and the qflow technology for Qflow
# In the directory where the Makefile resides where this file is included has to contain
# the following files/directories:
# - source/$(QFLOWTOP).v: source Verilog code
# - project_vars.sh: The qflow settings for the tools
# - config (optional): Files inside this directory are copied to layout/ directory
#   before place and route is run. I allows for example to give the pin arrangement
#   or a custom aspect ratio by providing own .par file.

ifeq ($(QFLOWTOP),)
    $(error QFLOWTOP needs to be defined before including Makefile.Qflow)
endif
ifeq ($(QFLOWTECH),)
    $(error QFLOWTECH needs to be defined before including Makefile.Qflow)
endif

.PHONY: synth pnr gdsii signoff lvs drc
synth: synthesis/$(QFLOWTOP).blif
pnr: layout/$(QFLOWTOP).def
gdsii: layout/$(QFLOWTOP).gds
signoff: lvs drc
drc: log/drc.log
lvs: log/lvs.log

synthesis/$(QFLOWTOP).blif: source/$(QFLOWTOP).v
	rm -fr synthesis layout
	mkdir synthesis layout
	qflow -T $(QFLOWTECH) synthesize $(QFLOWTOP)

layout/$(QFLOWTOP).def: synthesis/$(QFLOWTOP).blif
	rm -fr layout
	mkdir layout
	if [ -d config ]; then cp config/* layout; fi
	qflow place $(QFLOWTOP)
	qflow sta $(QFLOWTOP)
	qflow route $(QFLOWTOP)
	qflow backanno $(QFLOWTOP)

layout/$(QFLOWTOP).gds: layout/$(QFLOWTOP).def
	qflow migrate $(QFLOWTOP)
	qflow gdsii $(QFLOWTOP)

log/drc.log: layout/$(QFLOWTOP).gds
	qflow drc $(QFLOWTOP)

log/lvs.log: layout/$(QFLOWTOP).gds
	qflow lvs $(QFLOWTOP)

clean::
	rm -fr log synthesis layout source/*.blif source/*.ys qflow_exec.sh qflow_vars.sh $(QFLOWTOP).gds
