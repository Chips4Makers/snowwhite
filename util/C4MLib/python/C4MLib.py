import sys, os

__all__= [
    "get_indices", "get_shapes", "insert_viarect", "insert_ring", "insert_viaring",
    "CellAdder", "IORing", "IO_Ring_dims", "IO_TRI5VT_dims", "config_io",
]

try:
    import pya
except ImportError:
    print("C4MLib needs to be used inside a klayout script")
    sys.exit(20)

# Get some environment variables for dependencies
c4mlibdir = os.environ["C4MLIBDIR"]
tsmc_tpb035v = os.environ["TSMC_TPB035V"]
tsmc_tph035pnv3 = os.environ["TSMC_TPH035PNV3"]


# Some drawing support functions
def get_indices(tech_info, layout, cell, layer_names):
    a = []
    for name in layer_names:
        layer_num = getattr(tech_info, name)
        if isinstance(layer_num, int):
            layer_sub = 0
        else:
            layer_num, layer_sub = layer_num

        a.append((name, layout.layer(layer_num, layer_sub)))

    return dict(a)

def get_shapes(tech_info, layout, cell, layer_names):
    a = []
    for name in layer_names:
        layer_num = getattr(tech_info, name)
        if isinstance(layer_num, int):
            layer_sub = 0
        else:
            layer_num, layer_sub = layer_num

        idx = layout.layer(layer_num, layer_sub)
        a.append((name, cell.shapes(idx)))

    return dict(a)

def insert_viarect(shapes, left, bottom, right, top, size, pitch, enclosure):
  """Write an array of vias enclosed in a rectangle"""

  width = right - left
  nx = (width - 2*enclosure - size)/pitch + 1
  dx = (width - size - (nx-1)*pitch)/2 # Center array
  height = top - bottom
  ny = (height - 2*enclosure - size)/pitch + 1
  dy = (height - size - (ny-1)*pitch)/2 # Center array
  for i_x in xrange(nx): # TODO(?): replace with array instance
    for i_y in xrange(ny):
      via_left = left + dx + i_x*pitch
      via_right = via_left + size
      via_bottom = bottom + dy + i_y*pitch
      via_top = via_bottom + size
      shapes.insert_box(pya.Box(via_left, via_bottom, via_right, via_top))

def insert_ring(shapes, left, bottom, right, top,  width):
    """Write a ring, coordinates are outer dimensions"""

    x_left_in = left + width
    x_left_out = left
    x_right_in = right - width
    x_right_out = right
    y_bottom_in = bottom + width
    y_bottom_out = bottom
    y_top_in = top - width
    y_top_out = top

    points = [
      pya.Point(x_left_out, y_bottom_out),
      pya.Point(x_left_out, y_top_out),
      pya.Point(x_right_out, y_top_out),
      pya.Point(x_right_out, y_bottom_out),
      pya.Point(x_left_in, y_bottom_out),
      pya.Point(x_left_in, y_bottom_in),
      pya.Point(x_right_in, y_bottom_in),
      pya.Point(x_right_in, y_top_in),
      pya.Point(x_left_in, y_top_in),
      pya.Point(x_left_in, y_bottom_out),
    ]
    shapes.insert_simple_polygon(pya.SimplePolygon(points))

def insert_viaring(shapes, left, bottom, right, top, width, size, pitch, enclosure):
    """Write a ring of vias, coordinates are outer dimensions"""
    
    insert_viarect(shapes, left, bottom, left+width, top, size, pitch, enclosure)
    insert_viarect(shapes, right-width, bottom, right, top, size, pitch, enclosure)
    insert_viarect(shapes, left+width, bottom, right-width, bottom+width, size, pitch, enclosure)
    insert_viarect(shapes, left+width, top-width, right-width, top, size, pitch, enclosure)


class CellAdder:
    """Class to add cell to target layout on first use"""

    def __init__(self, libname, filename, target_layout, aliases=None, tech=None, firstuse_cb=None):
        self.lib = lib = pya.Library()
        lib.register(libname)
        self.lib_layout = lay = lib.layout()
        if tech is None:
            lay.read(filename)
        else:
            lay.read(filename, tech.load_layout_options)
        self.target_layout = target_layout
        self.aliases = aliases
        self.firstuse_cb = firstuse_cb

        self._cells = {}

    def __getitem__(self, name):
        if name in self._cells:
            return self._cells[name]
        else:
            alias = name if self.aliases is None else self.aliases[name]

            lib_idx = self.lib_layout.cell(alias).cell_index()
            target_idx = self.target_layout.add_lib_cell(self.lib, lib_idx)
            if self.firstuse_cb is not None:
                self.firstuse_cb(self.lib_layout, lib_idx, self.target_layout, target_idx)

            self._cells[name] = target_idx
            return target_idx


IO_TRI5VT_dims = { # In DbU
    "height": 120000L,
    "I_x": 25000L,
    "I_width": 2800L,
    "I_height": 1000L,
    "C_x": 65650L,
    "C_width": 2800L,
    "C_height": 1000L,
    "OEN_x": 2375L,
    "OEN_width": 2800L,
    "OEN_height": 1000L,
    "IE_x": 76200L,
    "IE_width": 2800L,
    "IE_height": 1000L,
    "SE_x": 72600L,
    "SE_width": 2800L,
    "SE_height": 1000L,
    "PS_x": 48900L,
    "PS_width": 2800L,
    "PS_height": 1000L,
    "PE_x": 52500L,
    "PE_width": 2800L,
    "PE_height": 1000L,
    "DS_x": 43200L,
    "DS_width": 2800L,
    "DS_height": 1000L,
}

# IO Ring dimension
um = 1000L

pad_height = 73
pad_width = 80
io_width = 80
io_height = 120
io_top = pad_height + io_height
filler_width = 11 # The width of filler needed between supply and IO
corner_width = 130

cell_width = cell_height = 2*pad_height + 2*corner_width + 28*io_width + 2*filler_width

dc_dist = 5
dc_width = 30
dc_space = 1

dc_left = io_top + dc_dist
dc_right = cell_width - dc_left
dc_bottom = io_top + dc_dist
dc_top = cell_height - dc_bottom

IO_Ring_dims = { # In um
    "pad_height": pad_height,
    "pad_width": pad_width,
    "io_width": io_width,
    "io_height": io_height,
    "io_top": io_top,
    "filler_width": filler_width,
    "corner_width": corner_width,
    "ioring_width": cell_width,
    "ioring_height": cell_height,
    "dc_dist": dc_dist,
    "dc_width": dc_width,
    "dc_space": dc_space,
    "dc_left": dc_left,
    "dc_right": dc_right,
    "dc_bottom": dc_bottom,
    "dc_top": dc_top,
}

def IORing(tech, tech_info, layout, cell_name, break_top=False, delete_pdiff=True):
    """Create an IO Ring in the given cell"""

    def firstuse(lib_layout, lib_cell_idx, target_layout, target_cell_idx):
        layer_num = tech_info.PDiff
        if isinstance(layer_num, int):
            layer_sub = 0
        else:
            layer_num, layer_sub = layer_num

        layer_idx = lib_layout.layer(layer_num, layer_sub)
        shapes = lib_layout.cell(lib_cell_idx).shapes(layer_idx)
        for shape in shapes.each(): # TODO: Can't this be done in one operation ?
            print(shape)
            shapes.erase(shape)

        layer_idx = target_layout.layer(layer_num, layer_sub)
        shapes = target_layout.cell(target_cell_idx).shapes(layer_idx)
        for shape in shapes.each(): # TODO: Can't this be done in one operation ?
            print(shape)
            shapes.erase(shape)

    # Prepare the library to add cells to the layout.
    BP_cells = CellAdder(
        "tpb035v", 
        "{}/digital/Back_End/gds/tpb035v_260a/wb/4lm/4M_2X1N/tpb035v.gds".format(tsmc_tpb035v),
        layout,
        aliases = {"PAD": "PAD80L"},
        tech = tech,
        firstuse_cb = firstuse if delete_pdiff else None,
    )
    C4M_cells = CellAdder(
        "c4m_cells_ioring",
        "{}/layout/C4MLib.oas.gz".format(c4mlibdir),
        layout,
        tech = tech,
    )
    IO_cells = CellAdder(
        "ph035pnv3",
        "{}/digital/Back_End/gds/tph035pnv3_260a/mt/4lm/tph035pnv3.gds".format(tsmc_tph035pnv3),
        layout,
        aliases = {
            "TRI0204": "PDDWUWHWSW0204CDG",
            "TRI0408": "PDDWUWHWSW0408CDG",
            "TRI0812": "PDDWUWHWSW0812CDG",
            "TRI1216": "PDDWUWHWSW1216CDG",
            "TRI5VT0204": "PDDWUWSW0204DGZ",
            "TRI5VT0408": "PDDWUWSW0408DGZ",
            "TRI5VT0812": "PDDWUWSW0812DGZ",
            "TRI5VT1216": "PDDWUWSW1216DGZ",
            "VDDCORE": "PVDD1CDG",
            "VDDIO": "PVDD2CDG",
            "VSSCORE": "PVSS1CDG",
            "VSSIO": "PVSS2CDG",
            "CORNER": "PCORNER",
            "FILLER1": "PFILLER1",
            "FILLER5": "PFILLER5",
            "FILLER10": "PFILLER10",
            "FILLER20": "PFILLER20",
            "END": "PENDCAP",
        },
        tech = tech,
        firstuse_cb = firstuse if delete_pdiff else None,
    )

    cell = layout.create_cell(cell_name)

    # We do the IO coordinates in um and then multiply it with um
    um = 1000L


    # Get the used shapes for inserting
    shapes = get_shapes(tech_info, layout, cell, [
        "Metal1", "Via12", "Metal2", "Via23", "Metal3", "Via34", "Metal4",
        "Boundary",
    ])


    def conn_vsscore(angle, x, y):
        vsscore_pins_x = ((7, 27.4), (29.8, 50.2), (52.6, 73))
        for x1, x2 in vsscore_pins_x:
            if angle == 0:
                left = long((x + x1)*um)
                right = long((x + x2)*um)
                bottom = long(y*um)
                top = long((dc_bottom + dc_width)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                bottom = long(dc_bottom*um)
                insert_viarect(
                    shapes["Via12"], left, bottom, right, top,
                    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure
                )
            elif angle == 1:
                left = long((dc_right - dc_width)*um)
                right = long(x*um)
                bottom = long((y + x1)*um)
                top = long((y + x2)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                right = long(dc_right*um)
                insert_viarect(
                    shapes["Via12"], left, bottom, right, top,
                    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure
                )
            elif angle == 2:
                left = long((x - x2)*um)
                right = long((x - x1)*um)
                bottom = long((dc_top - dc_width)*um)
                top = long(y*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                top = long(dc_top*um)
                insert_viarect(
                    shapes["Via12"], left, bottom, right, top,
                    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure
                )
            elif angle == 3:
                left = long(x*um)
                right = long((dc_left + dc_width)*um)
                bottom = long((y - x2)*um)
                top = long((y - x1)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                left = long(dc_left*um)
                insert_viarect(
                    shapes["Via12"], left, bottom, right, top,
                    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure
                )
            else:
                raise ValueError()

    def conn_vddcore(angle, x, y):
        vddcore_pins_x = ((7, 38), (42, 73))
        for x1, x2 in vddcore_pins_x:
            if angle == 0:
                left = long((x + x1)*um)
                right = long((x + x2)*um)
                bottom = long(y*um)
                top = long((dc_bottom + dc_width)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                bottom = long(dc_bottom*um)
                insert_viarect(
                    shapes["Via23"], left, bottom, right, top,
                    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure
                )
                shapes["Metal3"].insert_box(pya.Box(left, bottom, right, top))
                insert_viarect(
                    shapes["Via34"], left, bottom, right, top,
                    tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure
                )
            elif angle == 1:
                left = long((dc_right - dc_width)*um)
                right = long(x*um)
                bottom = long((y + x1)*um)
                top = long((y + x2)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                right = long(dc_right*um)
                insert_viarect(
                    shapes["Via23"], left, bottom, right, top,
                    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure
                )
                shapes["Metal3"].insert_box(pya.Box(left, bottom, right, top))
                insert_viarect(
                    shapes["Via34"], left, bottom, right, top,
                    tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure
                )
            elif angle == 2:
                left = long((x - x2)*um)
                right = long((x - x1)*um)
                bottom = long((dc_top - dc_width)*um)
                top = long(y*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                top = long(dc_top*um)
                insert_viarect(
                    shapes["Via23"], left, bottom, right, top,
                    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure
                )
                shapes["Metal3"].insert_box(pya.Box(left, bottom, right, top))
                insert_viarect(
                    shapes["Via34"], left, bottom, right, top,
                    tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure
                )
            elif angle == 3:
                left = long(x*um)
                right = long((dc_left + dc_width)*um)
                bottom = long((y - x2)*um)
                top = long((y - x1)*um)
                shapes["Metal2"].insert_box(pya.Box(left, bottom, right, top))
                left = long(dc_left*um)
                insert_viarect(
                    shapes["Via23"], left, bottom, right, top,
                    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure
                )
                shapes["Metal3"].insert_box(pya.Box(left, bottom, right, top))
                insert_viarect(
                    shapes["Via34"], left, bottom, right, top,
                    tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure
                )
            else:
                raise ValueError()

    # Draw the DC ring
    insert_ring(shapes["Metal1"], dc_left*um, dc_bottom*um, dc_right*um, dc_top*um, dc_width*um) # VSS
    insert_ring(shapes["Metal4"], dc_left*um, dc_bottom*um, dc_right*um, dc_top*um, dc_width*um) # VDD


    # Instantiate the IO cells and connect to the DC ring where appropriate
    x = y = pad_height
    cell.insert(pya.CellInstArray(IO_cells["CORNER"], pya.Trans(x*um, y*um)))
    x += corner_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_RIGHT"], pya.Trans((x-80)*um, y*um)))
    # VSSCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSCORE"], pya.Trans(x*um, y*um)))
    conn_vsscore(0, x, y + io_height)
    x += io_width
    # VDDCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDCORE"], pya.Trans(x*um, y*um)))
    conn_vddcore(0, x, y + io_height)
    x += io_width
    # FILLER cells
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans((x+10)*um, y*um)))
    x += filler_width
    # 24 IO cells
    for _ in range(24):
        cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(x*um, y*um)))
        cell.insert(pya.CellInstArray(IO_cells["TRI5VT1216"], pya.Trans(x*um, y*um)))
        x += io_width
    # FILLER cell
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans((x+10)*um, y*um)))
    x += filler_width
    # VDDIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDIO"], pya.Trans(x*um, y*um)))
    x += io_width
    # VSSIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSIO"], pya.Trans(x*um, y*um)))
    x += io_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_LEFT"], pya.Trans(x*um, y*um)))

    x = cell_width - pad_height
    y = pad_height
    angle = 1
    mirr = False
    cell.insert(pya.CellInstArray(IO_cells["CORNER"], pya.Trans(angle, mirr, x*um, y*um)))
    y += corner_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_RIGHT"], pya.Trans(angle, mirr, x*um, (y-80)*um)))
    # VSSCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vsscore(angle, x - io_height, y)
    y += io_width
    # VDDCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vddcore(angle, x - io_height, y)
    y += io_width
    # FILLER cell
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, x*um, (y+10)*um)))
    y += filler_width
    # 24 IO cells
    for _ in range(24):
        cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
        cell.insert(pya.CellInstArray(IO_cells["TRI5VT1216"], pya.Trans(angle, mirr, x*um, y*um)))
        y += io_width
    # FILLER cell
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, x*um, (y+10)*um)))
    y += filler_width
    # VDDIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDIO"], pya.Trans(angle, mirr, x*um, y*um)))
    y += io_width
    # VSSIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSIO"], pya.Trans(angle, mirr, x*um, y*um)))
    y += io_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_LEFT"], pya.Trans(angle, mirr, x*um, y*um)))

    x = cell_width - pad_height
    y = cell_width - pad_height
    angle = 2
    mirr = False
    cell.insert(pya.CellInstArray(IO_cells["CORNER"], pya.Trans(angle, mirr, x*um, y*um)))
    x -= corner_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_RIGHT"], pya.Trans(angle, mirr, (x+80)*um, y*um)))
    # VSSCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vsscore(angle, x, y - io_height)
    x -= io_width
    # VDDCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vddcore(angle, x, y - io_height)
    x -= io_width
    if not break_top:
        # FILLER cell
        cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
        cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, (x-10)*um, y*um)))
        x -= filler_width
        # 24 IO cells
        for _ in range(24):
            cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
            cell.insert(pya.CellInstArray(IO_cells["TRI5VT1216"], pya.Trans(angle, mirr, x*um, y*um)))
            x -= io_width
        # FILLER cell
        cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
        cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, (x-10)*um, y*um)))
        x -= filler_width
    else:
        # ENDCAP
        cell.insert(pya.CellInstArray(IO_cells["END"], pya.Trans(angle, mirr, x*um, y*um)))
        x -= filler_width
        for _ in range(24):
            cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
            x -= io_width
        x -= filler_width
        cell.insert(pya.CellInstArray(IO_cells["END"], pya.Trans(angle-2, not mirr, x*um, y*um)))
    # VDDIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDIO"], pya.Trans(angle, mirr, x*um, y*um)))
    x -= io_width
    # VSSIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSIO"], pya.Trans(angle, mirr, x*um, y*um)))
    x -= io_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_LEFT"], pya.Trans(angle, mirr, x*um, y*um)))

    x = pad_height
    y = cell_width - pad_height
    angle = 3
    mirr = False
    cell.insert(pya.CellInstArray(IO_cells["CORNER"], pya.Trans(angle, mirr, x*um, y*um)))
    y -= corner_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_RIGHT"], pya.Trans(angle, mirr, x*um, (y+80)*um)))
    # VSSCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vsscore(angle, x + io_height, y)
    y -= io_width
    # VDDCORE
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDCORE"], pya.Trans(angle, mirr, x*um, y*um)))
    conn_vddcore(angle, x + io_height, y)
    y -= io_width
    # FILLER cell
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, x*um, (y-10)*um)))
    y -= filler_width
    # 24 IO cells
    for _ in range(24):
        cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
        cell.insert(pya.CellInstArray(IO_cells["TRI5VT1216"], pya.Trans(angle, mirr, x*um, y*um)))
        y -= io_width
    # FILLER cell
    cell.insert(pya.CellInstArray(IO_cells["FILLER10"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["FILLER1"], pya.Trans(angle, mirr, x*um, (y-10)*um)))
    y -= filler_width
    # VDDIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VDDIO"], pya.Trans(angle, mirr, x*um, y*um)))
    y -= io_width
    # VSSIO
    cell.insert(pya.CellInstArray(BP_cells["PAD"], pya.Trans(angle, mirr, x*um, y*um)))
    cell.insert(pya.CellInstArray(IO_cells["VSSIO"], pya.Trans(angle, mirr, x*um, y*um)))
    y -= io_width
    # Extra bond pad
    cell.insert(pya.CellInstArray(C4M_cells["PAD80_LEFT"], pya.Trans(angle, mirr, x*um, y*um)))

    cell.insert(pya.CellInstArray(C4M_cells["C4MLogo"], pya.Trans(1*um, 1*um)))


    # Draw boundary
    x = y = cell_width
    shapes["Boundary"].insert_box(pya.Box(0, 0, x*um, y*um))


    # Add antenna diode to the IO cell(s)
    io_cell = layout.cell("PDDWUWSW1216DGZ")

    io_shapes = get_shapes(tech_info, layout, io_cell, [
        "Diffusion", "NPlus", "Contact",
    ])
    left = IO_TRI5VT_dims["I_x"]
    right = left + IO_TRI5VT_dims["I_width"]
    top = IO_TRI5VT_dims["height"]
    bottom = top - IO_TRI5VT_dims["I_height"]
    io_shapes["Diffusion"].insert_box(pya.Box(left, bottom, right, top))
    insert_viarect(io_shapes["Contact"], left, bottom, right, top, 400L, 800L, 250L)
    encl = tech_info.NPlus_Diffusion_Enclosure
    left = left - encl
    right = right + encl
    bottom = bottom - encl
    top = top + encl
    io_shapes["NPlus"].insert_box(pya.Box(left, bottom, right, top))

    left = IO_TRI5VT_dims["OEN_x"]
    right = left + IO_TRI5VT_dims["OEN_width"]
    top = IO_TRI5VT_dims["height"]
    bottom = top - IO_TRI5VT_dims["OEN_height"]
    io_shapes["Diffusion"].insert_box(pya.Box(left, bottom, right, top))
    insert_viarect(io_shapes["Contact"], left, bottom, right, top, 400L, 800L, 250L)
    encl = tech_info.NPlus_Diffusion_Enclosure
    left = left - encl
    right = right + encl
    bottom = bottom - encl
    top = top + encl
    io_shapes["NPlus"].insert_box(pya.Box(left, bottom, right, top))


    return cell
    
def config_io(num, side, pin_spec, shapes): # num starts from 0, non-DC io on given side
    first_io = IO_Ring_dims["pad_height"] + IO_Ring_dims["corner_width"] + 2*IO_Ring_dims["io_width"] + IO_Ring_dims["filler_width"]
    if side == "bottom":
        io_y = (IO_Ring_dims["pad_height"] + IO_Ring_dims["io_height"])*um
        io_x = (first_io + num*IO_Ring_dims["io_width"])*um

        for name, value in pin_spec:
            assert (value in (0, 1))

            x = IO_TRI5VT_dims[name + "_x"]
            width = IO_TRI5VT_dims[name + "_width"]
            height = IO_TRI5VT_dims[name + "_height"]

            left = io_x + x
            right = left + width
            top = IO_Ring_dims["dc_bottom"]*um
            bottom = io_y

            m_shapes = shapes["Metal1" if value == 0 else "Metal4"]
            m_shapes.insert(pya.Box(left, bottom, right, top))
    elif side == "right":
        io_x = (IO_Ring_dims["ioring_width"] - IO_Ring_dims["pad_height"] - IO_Ring_dims["io_height"])*um
        io_y = (first_io + num*IO_Ring_dims["io_width"])*um

        for name, value in pin_spec:
            assert (value in (0, 1))

            x = IO_TRI5VT_dims[name + "_x"]
            width = IO_TRI5VT_dims[name + "_width"]
            height = IO_TRI5VT_dims[name + "_height"]

            left = IO_Ring_dims["dc_right"]*um
            right = io_x
            bottom = io_y + x
            top = bottom + width

            m_shapes = shapes["Metal1" if value == 0 else "Metal4"]
            m_shapes.insert(pya.Box(left, bottom, right, top))
    elif side == "top":
        io_y = (IO_Ring_dims["ioring_height"] - IO_Ring_dims["pad_height"] - IO_Ring_dims["io_height"])*um
        io_x = (first_io + (num + 1)*IO_Ring_dims["io_width"])*um # Add extra width to get right side of cell

        for name, value in pin_spec:
            assert (value in (0, 1))

            x = IO_TRI5VT_dims[name + "_x"]
            width = IO_TRI5VT_dims[name + "_width"]
            height = IO_TRI5VT_dims[name + "_height"]

            right = io_x - x
            left = right - width
            bottom = IO_Ring_dims["dc_top"]*um
            top = io_y

            m_shapes = shapes["Metal1" if value == 0 else "Metal4"]
            m_shapes.insert(pya.Box(left, bottom, right, top))
    elif side == "left":
        io_x = (IO_Ring_dims["pad_height"] + IO_Ring_dims["io_height"])*um
        io_y = (first_io + (num + 1)*IO_Ring_dims["io_width"])*um # Add extra width to get top of cell

        for name, value in pin_spec:
            assert (value in (0, 1))

            x = IO_TRI5VT_dims[name + "_x"]
            width = IO_TRI5VT_dims[name + "_width"]
            height = IO_TRI5VT_dims[name + "_height"]

            right = IO_Ring_dims["dc_left"]*um
            left = io_x
            top = io_y - x
            bottom = top - width

            m_shapes = shapes["Metal1" if value == 0 else "Metal4"]
            m_shapes.insert(pya.Box(left, bottom, right, top))
    else:
        raise("side {} not supported".format(side))

