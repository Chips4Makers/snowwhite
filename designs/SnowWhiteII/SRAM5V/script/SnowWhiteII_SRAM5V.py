print("Begin")


import pya
# Get the technogy information
import TSMC_CL035G as ti

TText = pya.Shape.TText
TTextRef = pya.Shape.TTextRef

tech = pya.Technology.technology_by_name("TSMC_CL035")

lib_SRAM = pya.Library()
lib_SRAM.register("SRAM1024x8")
lay_SRAM = lib_SRAM.layout()
lay_SRAM.read("../RAM/priv/SRAM1024x8_manualconnect.gds", tech.load_layout_options)
cell_SRAM = lay_SRAM.cell("SRAM1024x8_manualconnect")

lib_IORing = pya.Library()
lib_IORing.register("IORing")
lay_IORing = lib_IORing.layout()
lay_IORing.read("../../common/script/SnowWhiteII_IORing.oas.gz", tech.load_layout_options)
io_pdiff_idx = lay_IORing.find_layer(ti.PDiff, 0)
if io_pdiff_idx is not None:
  lay_IORing.delete_layer(io_pdiff_idx)
cell_IORing = lay_IORing.cell("SnowWhiteII_IORing")


lay_SWII_SRAM5V = pya.Layout()
top_cell = lay_SWII_SRAM5V.create_cell("SnowWhiteII_SRAM5V")

idx_SRAM = lay_SWII_SRAM5V.add_lib_cell(lib_SRAM, cell_SRAM.cell_index())
SRAM_x = 1050000
SRAM_y = 700000
inst_SRAM = pya.CellInstArray(idx_SRAM, pya.Trans(SRAM_x, SRAM_y))
top_cell.insert(inst_SRAM)

idx_IORing = lay_SWII_SRAM5V.add_lib_cell(lib_IORing, cell_IORing.cell_index())
inst_IORing = pya.CellInstArray(idx_IORing, pya.Trans(0,0))
top_cell.insert(inst_IORing)

pins = {
  "clk0": ("input", "bottom", 5),
  "csb0": ("input", "left", 4),
  "web0": ("input", "left", 3),
  "ADDR0[0]": ("input", "bottom", 6),
  "ADDR0[1]": ("input", "bottom", 7),
  "ADDR0[2]": ("input", "bottom", 8),
  "ADDR0[9]": ("input", "top", 46),
  "ADDR0[8]": ("input", "top", 45),
  "ADDR0[7]": ("input", "top", 44),
  "ADDR0[6]": ("input", "top", 43),
  "ADDR0[5]": ("input", "top", 42),
  "ADDR0[4]": ("input", "top", 41),
  "ADDR0[3]": ("input", "top", 40),
  "DOUT0[0]": ("output", "bottom", 9),
  "DIN0[0]": ("input", "bottom", 10),
  "DOUT0[1]": ("output", "bottom", 11),
  "DIN0[1]": ("input", "bottom", 12),
  "DOUT0[2]": ("output", "bottom", 13),
  "DIN0[2]": ("input", "bottom", 14),
  "DOUT0[3]": ("output", "bottom", 15),
  "DIN0[3]": ("input", "bottom", 16),
  "DOUT0[4]": ("output", "bottom", 17),
  "DIN0[4]": ("input", "bottom", 18),
  "DOUT0[5]": ("output", "bottom", 19),
  "DIN0[5]": ("input", "bottom", 20),
  "DOUT0[6]": ("output", "bottom", 21),
  "DIN0[6]": ("input", "bottom", 28),
  "DOUT0[7]": ("output", "bottom", 29),
  "DIN0[7]": ("input", "bottom", 30),
}

iopads = [
  ("left", 1260000),
  ("left", 10600000),
  ("left", 860000),
  ("left", 660000),
  ("left", 460000),
  ("left", 260000),
  ("bottom", 260000),
  ("bottom", 460000),
  ("bottom", 660000),
  ("bottom", 860000),
  ("bottom", 1060000),
  ("bottom", 1260000),
  ("bottom", 1460000),
  ("bottom", 1660000),
  ("bottom", 1860000),
  ("bottom", 2060000),
  ("bottom", 2260000),
  ("bottom", 2460000),
  ("right", 260000),
  ("right", 460000),
  ("right", 660000),
  ("right", 860000),
  ("right", 1060000),
  ("right", 1260000),
  ("right", 1460000),
  ("right", 1660000),
  ("right", 1860000),
  ("right", 2060000),
  ("right", 2260000),
  ("right", 2460000),
  ("top", 2460000),
  ("top", 2260000),
  ("top", 2060000),
  ("top", 1860000),
  ("top", 1660000),
  ("top", 1460000),
  ("top", 1260000),
  ("top", 1060000),
  ("top", 860000),
  ("top", 660000),
  ("top", 460000),
  ("top", 260000),
  ("left", 2460000),
  ("left", 2260000),
  ("left", 2060000),
  ("left", 1860000),
  ("left", 1660000),
  ("left", 1460000),
]

SRAM_m2pin_idx = lay_SRAM.layer(ti.Metal2_Pin, 0)

top_m1_idx = lay_SWII_SRAM5V.layer(ti.Metal1, 0)
top_via12_idx = lay_SWII_SRAM5V.layer(ti.Via12,0)
top_m2_idx = lay_SWII_SRAM5V.layer(ti.Metal2,0)
top_via23_idx = lay_SWII_SRAM5V.layer(ti.Via23,0)
top_m3_idx = lay_SWII_SRAM5V.layer(ti.Metal3,0)
top_via34_idx = lay_SWII_SRAM5V.layer(ti.Via34,0)
top_m4_idx = lay_SWII_SRAM5V.layer(ti.Metal4,0)

top_m1_shapes = top_cell.shapes(top_m1_idx)
top_via12_shapes = top_cell.shapes(top_via12_idx)
top_m2_shapes = top_cell.shapes(top_m2_idx)
top_via23_shapes = top_cell.shapes(top_via23_idx)
top_m3_shapes = top_cell.shapes(top_m3_idx)
top_via34_shapes = top_cell.shapes(top_via34_idx)
top_m4_shapes = top_cell.shapes(top_m4_idx)

tracks_left = tracks_bottom = tracks_right = tracks_top = 0

io_width = 80000L
io_height = 120000L
ioring_width = 193000L

I_x = 25000L
I_width = 2800L
I_height = 1000L

C_x = 65650L
C_width = 2800L

OEN_x = 2375L
OEN_width = 2800L

IE_x = 76200L
IE_width = 2800L

SE_x = 72600L
SE_width = 2800L

PS_x = 48900L
PS_width = 2800L

PE_x = 52500L
PE_width = 2800L

DS_x = 43200L
DS_width = 2800L

pin_width = 900L # Make width wide enough so a via fits in

track_width = 5000L
track_space = 1000L
track_pitch = track_width + track_space

x_track1_left = 1000000L
x_track1_right = 1800000L
y_track1_bottom = 650000L
y_track1_top = 2150000L

die_width = 2800000L
die_height = 2800000L

def insert_viarect(shapes, left, bottom, right, top, size, pitch, enclosure):
  """Write an array of vias enclosed in a rectangle"""

  width = right - left
  nx = (width - 2*enclosure - size)/pitch + 1
  dx = (width - size - (nx-1)*pitch)/2 # Center array
  height = top - bottom
  ny = (height - 2*enclosure - size)/pitch + 1
  dy = (height - size - (ny-1)*pitch)/2 # Center array
  for i_x in xrange(nx): # TODO(?): replace with array instance
    for i_y in xrange(ny):
      via_left = left + dx + i_x*pitch
      via_right = via_left + size
      via_bottom = bottom + dy + i_y*pitch
      via_top = via_bottom + size
      shapes.insert_box(pya.Box(via_left, via_bottom, via_right, via_top))

def insert_ring(shapes, left, bottom, right, top,  width):
    """Write a ring, coordinates are outer dimensions"""

    x_left_in = left + width
    x_left_out = left
    x_right_in = right - width
    x_right_out = right
    y_bottom_in = bottom + width
    y_bottom_out = bottom
    y_top_in = top - width
    y_top_out = top

    points = [
      pya.Point(x_left_out, y_bottom_out),
      pya.Point(x_left_out, y_top_out),
      pya.Point(x_right_out, y_top_out),
      pya.Point(x_right_out, y_bottom_out),
      pya.Point(x_left_in, y_bottom_out),
      pya.Point(x_left_in, y_bottom_in),
      pya.Point(x_right_in, y_bottom_in),
      pya.Point(x_right_in, y_top_in),
      pya.Point(x_left_in, y_top_in),
      pya.Point(x_left_in, y_bottom_out),
    ]
    shapes.insert_simple_polygon(pya.SimplePolygon(points))


def connect_iopin(side, offset, pin_x, pin_width, val):
  if side == "left":
    left = ioring_width
    right = dc_left
    top = offset + io_width - pin_x
    bottom = top - pin_width
  elif side == "bottom":
    left = offset + pin_x
    right = left + pin_width
    bottom = ioring_width
    top = dc_bottom
  elif side == "right":
    left = dc_right
    right = die_width - ioring_width
    bottom = offset + pin_x
    top = bottom + pin_width
  elif side == "top":
    right = offset + io_width - pin_x
    left = right - pin_width
    bottom = dc_top
    top = die_height - ioring_width
  else:
    raise Exception("Unsupport side {}".format(side))

  if val == 0:
    top_m1_shapes.insert_box(pya.Box(left, bottom, right, top))
  elif val == 1:
    top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
  else:
    raise Exception("Unsupported val {}".format(val))

def connect_pin(shape):
  """Connect a pin of the block to an IO cell"""

  global tracks_left, tracks_bottom, tracks_right, tracks_top

  pin_dir, pin_side, pin_nr = pins[shape.text_string]
  io_side, io_offset = iopads[pin_nr - 1]

  if pin_dir == "input":
    iopin_x = C_x
    iopin_width = C_width
    connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
    connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
    connect_iopin(io_side, io_offset, I_x, I_width, 0)
    connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
    connect_iopin(io_side, io_offset, PS_x, PS_width, 1)
    connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
    connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
  else:
    assert(pin_dir == "output")
    iopin_x = I_x
    iopin_width = I_width
    connect_iopin(io_side, io_offset, OEN_x, OEN_width, 0)
    connect_iopin(io_side, io_offset, IE_x, IE_width, 0)
    connect_iopin(io_side, io_offset, DS_x, DS_width, 1)
    connect_iopin(io_side, io_offset, PS_x, PS_width, 0)
    connect_iopin(io_side, io_offset, PE_x, PE_width, 0)
    connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
    # Draw antenna diode to avoid antenna violations


  if pin_side == "left":
    bottom = SRAM_y + shape.text_pos.y - pin_width/2
    top = bottom + pin_width
    left = x_track1_left - tracks_left*track_pitch - track_width
    right = SRAM_x + shape.text_pos.x
    #top_m3_shapes.insert_box(pya.Box(left, bottom, right, top))
    top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_left = tracks_left + 1

    if io_side == "left":
      right2 = left + track_width
      left2 = ioring_width
      top2 = io_offset + io_width - iopin_x
      bottom2 = top2 - iopin_width
      #insert_viarect(
      #  top_via23_shapes, left, bottom, right2, top, ti,
      #  ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      #)
      top_m2_shapes.insert_box(
        pya.Box(left, min(bottom, bottom2), right2, max(top, top2))
      )
      insert_viarect(
        top_via23_shapes, left, bottom2, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    else:
      raise Exception("Unsupported io_side {} for left pin_side".format(io_side))
  elif pin_side == "bottom":
    top = SRAM_y + shape.text_pos.y
    bottom = y_track1_bottom - tracks_bottom*track_pitch - track_width
    left = SRAM_x + shape.text_pos.x - pin_width/2
    right = left + pin_width
    top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_bottom = tracks_bottom + 1

    if io_side == "bottom":
      top2 = bottom + track_width
      bottom2 = ioring_width
      left2 = io_offset + iopin_x
      right2 = left2 + iopin_width
      insert_viarect(
        top_via23_shapes, left, bottom, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left, left2), bottom, max(right, right2), top2)
      )
      insert_viarect(
        top_via23_shapes, left2, bottom, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    elif io_side == "left":
      bottom2 = bottom
      top2 = bottom2 + track_width
      right2 = right
      left2 = x_track1_left - tracks_left*track_pitch - track_width
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_left = tracks_left + 1

      right3 = left2 + track_width
      left3 = ioring_width
      top3 = io_offset + io_width - iopin_x
      bottom3 = top3 - iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom2, right3, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left2, min(bottom2, bottom3), right3, max(top2, top3))
      )
      insert_viarect(
        top_via23_shapes, left2, bottom3, right3, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    elif io_side == "right":
      bottom2 = bottom
      top2 = bottom2 + track_width
      right2 = x_track1_right + tracks_right*track_pitch + track_width
      left2 = left
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_right = tracks_right + 1

      right3 = die_width - ioring_width
      left3 = right2 - track_width
      bottom3 = io_offset + iopin_x
      top3 = bottom3 + iopin_width
      insert_viarect(
        top_via23_shapes, left3, bottom2, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left3, min(bottom2, bottom3), right2, max(top2, top3))
      )
      insert_viarect(
        top_via23_shapes, left3, bottom3, right2, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    else:
      print("Unsupported io_side {} for bottom pin_side".format(io_side))
      #raise Exception("Unsupported io_side {} for bottom pin_side".format(io_side))
  elif pin_side == "right":
    bottom = SRAM_y + shape.text_pos.y - pin_width/2
    top = bottom + pin_width
    left = SRAM_x + shape.text_pos.x
    right = x_track1_right + tracks_right*track_pitch + track_width
    top_m3_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_right = tracks_right + 1

    if io_side == "right":
      left2 = right - track_width
      right2 = die_width - ioring_width
      bottom2 = io_offset + iopin_x
      top2 = bottom2 + iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left2, min(bottom, bottom2), right, max(top, top2))
      )
      insert_viarect(
        top_via23_shapes, left2, bottom2, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    elif io_side == "bottom":
      left2 = right - track_width
      right2 = right
      top2 = top
      bottom2 = y_track1_bottom - tracks_bottom*track_pitch - track_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_bottom = tracks_bottom + 1
      
      top3 = bottom2 + track_width
      bottom3 = ioring_width
      left3 = io_offset + iopin_x
      right3 = left3 + iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom2, right2, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left2, left3), bottom2, max(right2, right3), top3)
      )
      insert_viarect(
        top_via23_shapes, left3, bottom2, right3, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    elif io_side == "top":
      left2 = right - track_width
      right2 = right
      bottom2 = bottom
      top2 = y_track1_top + tracks_top*track_pitch + track_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Ensclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_top = tracks_top + 1

      bottom3 = top2 - track_width
      top3 = die_height - ioring_width
      right3 = io_offset + io_width - iopin_x
      left3 = right3 - iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom3, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left2, left3), bottom3, max(right2, right3), top2)
      )
      insert_viarect(
        top_via23_shapes, left3, bottom3, right3, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    else:
      raise Exception("Unsupported io_side {} for right pin_side".format(io_side))
  elif pin_side == "top":
    left = SRAM_x + shape.text_pos.x - pin_width/2
    right = left + pin_width
    bottom = SRAM_y + shape.text_pos.y
    top = y_track1_top + tracks_top*track_pitch + track_width
    top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_top = tracks_top + 1

    if io_side == "top":
      bottom2 = top - track_width
      top2 = die_height - ioring_width
      right2 = io_offset + io_width - iopin_x
      left2 = right2 - iopin_width
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left, left2), bottom2, max(right, right2), top)
      )
      insert_viarect(
        top_via23_shapes, left2, bottom2, right2, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    elif io_side == "left":
      bottom2 = top - track_width
      top2 = top
      right2 = right
      left2 = x_track1_left - tracks_left*track_pitch - track_width
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_left = tracks_left + 1

      right3 = left2 + track_width
      left3 = ioring_width
      top3 = io_offset + io_width - iopin_x
      bottom3 = top3 - iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom2, right3, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left2, min(bottom2, bottom3), right3, max(top2, top3))
      )
      insert_viarect(
        top_via23_shapes, left2, bottom3, right3, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    else:
      raise Exception("Unsupported io_size {} for top pin side".format(io_side))
  else:
    print("pin_side {} not implemented".format(pin_side))


# Add an antenna diode on the I pin
io_diff_idx = lay_IORing.layer(ti.Diffusion, 0)
io_nimp_idx = lay_IORing.layer(ti.NPlus, 0)
io_cont_idx = lay_IORing.layer(ti.Contact, 0)

io_cell = lay_IORing.cell("PDDWUWSW1216DGZ")

io_diff_shapes = io_cell.shapes(io_diff_idx)
io_nimp_shapes = io_cell.shapes(io_nimp_idx)
io_cont_shapes = io_cell.shapes(io_cont_idx)

left = I_x
right = left + I_width
top = io_height
bottom = top - I_height
io_diff_shapes.insert_box(pya.Box(left, bottom, right, top))
insert_viarect(io_cont_shapes, left, bottom, right, top, 400L, 800L, 250L)
diff_imp_enclosure = 250
left = left - diff_imp_enclosure
right = right + diff_imp_enclosure
bottom = bottom - diff_imp_enclosure
top = top + diff_imp_enclosure
io_nimp_shapes.insert_box(pya.Box(left, bottom, right, top))

idx_IORing = lay_SWII_SRAM5V.add_lib_cell(lib_IORing, cell_IORing.cell_index())
inst_IORing = pya.CellInstArray(idx_IORing, pya.Trans(0,0))
top_cell.insert(inst_IORing)


#
# DC ring; METAL1: VSS, METAL4: VDD
#
dc_dist = 10000L
dc_width = 30000L
dc_space = 1000L

dc_left = ioring_width + dc_dist
dc_bottom = ioring_width + dc_dist
dc_right = die_width - ioring_width - dc_dist
dc_top = die_height - ioring_width - dc_dist
insert_ring(top_m1_shapes, dc_left, dc_bottom, dc_right, dc_top, dc_width)
insert_ring(top_m4_shapes, dc_left, dc_bottom, dc_right, dc_top, dc_width)
# dc_verspace = 700000L
# dc_top2 = (die_height - dc_verspace)/2
# dc_bottom2 = dc_top2 - dc_width
# top_m2_shapes.insert_box(pya.Box(dc_left, dc_bottom2, dc_right, dc_top2))
# top_m4_shapes.insert_box(pya.Box(dc_left, dc_bottom2, dc_right, dc_top2))
# dc_bottom3 = dc_top2 + dc_verspace
# dc_top3 = dc_bottom3 + dc_width
# top_m2_shapes.insert_box(pya.Box(dc_left, dc_bottom3, dc_right, dc_top3))
# top_m4_shapes.insert_box(pya.Box(dc_left, dc_bottom3, dc_right, dc_top3))

# VDD IO connection
io_bottom = 1260000L
io_top = io_bottom + io_width
metal2_length = dc_dist - dc_space
# unrotated cell has pin at the top
iopin_coords = [
  (7000L, 38000L),
  (42000L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
  # left cell; 270 deg rotated
  left = ioring_width
  right = dc_left + dc_width
  bottom = io_top - iopin_right
  top = io_top - iopin_left
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via23_shapes, dc_left, bottom, right, top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  top_m3_shapes.insert_box(pya.Box(dc_left, bottom, right, top))
  insert_viarect(
    top_via34_shapes, dc_left, bottom, right, top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )
  # right cell; 90 deg rotated
  right = die_width - ioring_width
  left = dc_right - dc_width
  bottom = io_bottom + iopin_left
  top = io_bottom + iopin_right
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via23_shapes, left, bottom, dc_right, top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  top_m3_shapes.insert_box(pya.Box(left, bottom, dc_right, top))
  insert_viarect(
    top_via34_shapes, left, bottom, dc_right, top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )

# VSS connection
io_bottom = 1060000L
io_top = io_bottom + io_width
# pins on METAL2
# unrotated cell has pin at the top
iopin_coords = [
  (7000L, 27400L),
  (29800L, 50200L),
  (52600L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
  # left cell; 270deg rotated
  left = ioring_width
  right = dc_left + dc_width
  bottom = io_top - iopin_right
  top = io_top - iopin_left
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via12_shapes, dc_left, bottom, right, top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )
  # right cell; 90def rotated
  left = dc_right - dc_width
  right = die_width - ioring_width
  bottom = io_bottom + iopin_left
  top = io_bottom + iopin_right
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via12_shapes, left, bottom, dc_right, top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )


# Core block connect VDD/GND
coredc_gnd_x = -50L
coredc_gnd_n = 149
coredc_vdd_x = 2350L
coredc_vdd_n = 148
coredc_bottom = -50L
coredc_top = 1381250L
coredc_pitch = 4800L
coredc_width = 1300L
coredc_connlength = 1000L
# Connect GND to bottom
top = SRAM_y + coredc_bottom
bottom = top - coredc_connlength
for i in xrange(coredc_gnd_n):
  left = SRAM_x + coredc_gnd_x + i*coredc_pitch
  right = left + coredc_width
  top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
left = SRAM_x + coredc_gnd_x
right = left + (coredc_gnd_n - 1)*coredc_pitch + coredc_width
top = bottom
bottom = top - dc_width
top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
top2 = bottom
bottom2 = dc_bottom + dc_width + dc_space
left2 = left
right2 = left2 + dc_width
top_m4_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
top3 = bottom2 + dc_width
bottom3 = dc_bottom
insert_viarect(
  top_via34_shapes, left2, bottom2, right2, top3,
  ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)
top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top3))
insert_viarect(
  top_via23_shapes, left2, bottom2, right2, top3,
  ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top3))
insert_viarect(
  top_via12_shapes, left2, bottom2, right2, top3,
  ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
top_m1_shapes.insert_box(pya.Box(left2, bottom3, right2, top3))
right2 = right
left2 = right2 - dc_width
top_m4_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
top3 = bottom2 + dc_width
bottom3 = dc_bottom
insert_viarect(
  top_via34_shapes, left2, bottom2, right2, top3,
  ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)
top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top3))
insert_viarect(
  top_via23_shapes, left2, bottom2, right2, top3,
  ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top3))
insert_viarect(
  top_via12_shapes, left2, bottom2, right2, top3,
  ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
top_m1_shapes.insert_box(pya.Box(left2, bottom3, right2, top3))
# Connect VDD to top
bottom = SRAM_y + coredc_top
top = bottom + coredc_connlength
for i in xrange(coredc_vdd_n):
  left = SRAM_x + coredc_vdd_x + i*coredc_pitch
  right = left + coredc_width
  top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
left = SRAM_x + coredc_vdd_x
right = left + (coredc_vdd_n - 1)*coredc_pitch + coredc_width
bottom = top
top = bottom + dc_width
top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
bottom2 = top
top2 = dc_top
left2 = left
right2 = left2 + dc_width
top_m4_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
right2 = right
left2 = right2 - dc_width
top_m4_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))

# Core block connect to IO ring
shapes = cell_SRAM.shapes(SRAM_m2pin_idx)
for shape in shapes.each():
  t = shape.type()
  if t == TText or t == TTextRef:
    connect_pin(shape)


# Set recognition pins; 22 => PU, 27 => PU
io_side, io_offset = iopads[22-1]
connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
connect_iopin(io_side, io_offset, I_x, I_width, 0)
connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
connect_iopin(io_side, io_offset, PS_x, PS_width, 1)
connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
io_side, io_offset = iopads[27-1]
connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
connect_iopin(io_side, io_offset, I_x, I_width, 0)
connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
connect_iopin(io_side, io_offset, PS_x, PS_width, 1)
connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
connect_iopin(io_side, io_offset, SE_x, SE_width, 0)


# Post process the layer; remove DRC errors.

top_n3v_idx = lay_SWII_SRAM5V.layer(ti.N3V, 0)
top_pimp_idx = lay_SWII_SRAM5V.layer(ti.PPlus, 0)
top_nimp_idx = lay_SWII_SRAM5V.layer(ti.NPlus, 0)
top_poly_idx = lay_SWII_SRAM5V.layer(ti.Poly, 0)
top_cont_idx = lay_SWII_SRAM5V.layer(ti.Contact, 0)
top_work_idx = lay_SWII_SRAM5V.layer(250, 0)

top_n3v_shapes = top_cell.shapes(top_n3v_idx)
top_pimp_shapes = top_cell.shapes(top_pimp_idx)
top_nimp_shapes = top_cell.shapes(top_nimp_idx)
top_poly_shapes = top_cell.shapes(top_poly_idx)
top_cont_shapes = top_cell.shapes(top_cont_idx)
top_work_shapes = top_cell.shapes(top_work_idx)

shapeproc = pya.ShapeProcessor()

# Size N3V 0.3um over/under
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_n3v_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_SRAM5V, top_cell, top_work_idx,
  lay_SWII_SRAM5V, top_cell, top_n3v_idx,
  top_n3v_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size PIMP 0.3um over/under
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_pimp_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_SRAM5V, top_cell, top_work_idx,
  lay_SWII_SRAM5V, top_cell, top_pimp_idx,
  top_pimp_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size NIMP 0.3um over/under
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_nimp_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_SRAM5V, top_cell, top_work_idx,
  lay_SWII_SRAM5V, top_cell, top_nimp_idx,
  top_nimp_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Enforce contact-poly enclosure of 0.2um, avoid minimal space violation
shapeproc.boolean(
  lay_SWII_SRAM5V, top_cell, top_poly_idx,
  lay_SWII_SRAM5V, top_cell, top_cont_idx,
  top_work_shapes, pya.EdgeProcessor.ModeAnd,
  True, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_work_idx, top_poly_shapes, 200L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_poly_idx, top_work_shapes, 220L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_SRAM5V, top_cell, top_work_idx, top_work_shapes, -220L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
top_poly_shapes.clear()
shapeproc.boolean(
  lay_SWII_SRAM5V, top_cell, top_work_idx,
  lay_SWII_SRAM5V, top_cell, top_poly_idx,
  top_poly_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)

lay_SWII_SRAM5V.delete_layer(top_work_idx)


saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
top_cell.write("SnowWhiteII_SRAM5V.gds.gz", saveopts)


print("End")
