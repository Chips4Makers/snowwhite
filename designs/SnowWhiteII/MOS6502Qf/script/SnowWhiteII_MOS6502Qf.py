print("Begin")


import pya
# Get the technogy information
import TSMC_CL035G as ti

TText = pya.Shape.TText
TTextRef = pya.Shape.TTextRef

tech = pya.Technology.technology_by_name("TSMC_CL035")

lib_MOS6502Qf = pya.Library()
lib_MOS6502Qf.register("MOS6502Qf")
lay_MOS6502Qf = lib_MOS6502Qf.layout()
lay_MOS6502Qf.read("../Qflow/MOS6502Qf.gds", tech.load_layout_options)
cell_MOS6502Qf = lay_MOS6502Qf.cell("MOS6502Qf")

lib_IORing = pya.Library()
lib_IORing.register("IORing")
lay_IORing = lib_IORing.layout()
lay_IORing.read("../../common/script/SnowWhiteII_IORing.oas.gz", tech.load_layout_options)
io_pdiff_idx = lay_IORing.find_layer(ti.PDiff, 0)
if io_pdiff_idx is not None:
  lay_IORing.delete_layer(io_pdiff_idx)
cell_IORing = lay_IORing.cell("SnowWhiteII_IORing")


lay_SWII_MOS6502Qf = pya.Layout()
top_cell = lay_SWII_MOS6502Qf.create_cell("SnowWhiteII_MOS6502Qf")

idx_MOS6502Qf = lay_SWII_MOS6502Qf.add_lib_cell(lib_MOS6502Qf, cell_MOS6502Qf.cell_index())
MOS6502Qf_x = 1060000
MOS6502Qf_y = 1080000
inst_MOS6502Qf = pya.CellInstArray(idx_MOS6502Qf, pya.Trans(MOS6502Qf_x,MOS6502Qf_y))
top_cell.insert(inst_MOS6502Qf)

# idx_IORing = lay_SWII_MOS6502Qf.add_lib_cell(lib_IORing, cell_IORing.cell_index())
# inst_IORing = pya.CellInstArray(idx_IORing, pya.Trans(0,0))
# top_cell.insert(inst_IORing)

pins = {
  "clk": ("input", "left", 6),
  "reset": ("input", "left", 5),
  "IRQ": ("input", "left", 4),
  "NMI": ("input", "left", 3),
  "RDY": ("input", "left", 46),
  "WE": ("input", "left", 45),
  "DI[0]": ("input", "bottom", 7),
  "DI[1]": ("input", "bottom", 8),
  "DI[2]": ("input", "bottom", 9),
  "DI[3]": ("input", "bottom", 10),
  "DI[4]": ("input", "bottom", 11),
  "DI[5]": ("input", "bottom", 12),
  "DI[6]": ("input", "bottom", 13),
  "DI[7]": ("input", "bottom", 14),
  "DO[0]": ("output", "top", 44),
  "DO[1]": ("output", "top", 43),
  "DO[2]": ("output", "top", 42),
  "DO[3]": ("output", "top", 41),
  "DO[4]": ("output", "top", 40),
  "DO[5]": ("output", "top", 39),
  "DO[6]": ("output", "top", 38),
  "DO[7]": ("output", "top", 37),
  "A[0]": ("input", "right", 15),
  "A[1]": ("input", "right", 16),
  "A[2]": ("input", "right", 17),
  "A[3]": ("input", "right", 18),
  "A[4]": ("input", "right", 19),
  "A[5]": ("input", "right", 20),
  "A[6]": ("input", "right", 21),
  "A[7]": ("input", "right", 28),
  "A[8]": ("input", "right", 29),
  "A[9]": ("input", "right", 30),
  "A[10]": ("input", "right", 31),
  "A[11]": ("input", "right", 32),
  "A[12]": ("input", "right", 33),
  "A[13]": ("input", "right", 34),
  "A[14]": ("input", "right", 35),
  "A[15]": ("input", "right", 36),
}

iopads = [
  ("left", 1260000),
  ("left", 10600000),
  ("left", 860000),
  ("left", 660000),
  ("left", 460000),
  ("left", 260000),
  ("bottom", 260000),
  ("bottom", 460000),
  ("bottom", 660000),
  ("bottom", 860000),
  ("bottom", 1060000),
  ("bottom", 1260000),
  ("bottom", 1460000),
  ("bottom", 1660000),
  ("bottom", 1860000),
  ("bottom", 2060000),
  ("bottom", 2260000),
  ("bottom", 2460000),
  ("right", 260000),
  ("right", 460000),
  ("right", 660000),
  ("right", 860000),
  ("right", 1060000),
  ("right", 1260000),
  ("right", 1460000),
  ("right", 1660000),
  ("right", 1860000),
  ("right", 2060000),
  ("right", 2260000),
  ("right", 2460000),
  ("top", 2460000),
  ("top", 2260000),
  ("top", 2060000),
  ("top", 1860000),
  ("top", 1660000),
  ("top", 1460000),
  ("top", 1260000),
  ("top", 1060000),
  ("top", 860000),
  ("top", 660000),
  ("top", 460000),
  ("top", 260000),
  ("left", 2460000),
  ("left", 2260000),
  ("left", 2060000),
  ("left", 1860000),
  ("left", 1660000),
  ("left", 1460000),
]
  
MOS6502Qf_m2_idx = lay_MOS6502Qf.layer(ti.Metal2,0)
MOS6502Qf_m3_idx = lay_MOS6502Qf.layer(ti.Metal3,0)

top_m1_idx = lay_SWII_MOS6502Qf.layer(ti.Metal1, 0)
top_via12_idx = lay_SWII_MOS6502Qf.layer(ti.Via12,0)
top_m2_idx = lay_SWII_MOS6502Qf.layer(ti.Metal2,0)
top_via23_idx = lay_SWII_MOS6502Qf.layer(ti.Via23,0)
top_m3_idx = lay_SWII_MOS6502Qf.layer(ti.Metal3,0)
top_via34_idx = lay_SWII_MOS6502Qf.layer(ti.Via34,0)
top_m4_idx = lay_SWII_MOS6502Qf.layer(ti.Metal4,0)

top_m1_shapes = top_cell.shapes(top_m1_idx)
top_via12_shapes = top_cell.shapes(top_via12_idx)
top_m2_shapes = top_cell.shapes(top_m2_idx)
top_via23_shapes = top_cell.shapes(top_via23_idx)
top_m3_shapes = top_cell.shapes(top_m3_idx)
top_via34_shapes = top_cell.shapes(top_via34_idx)
top_m4_shapes = top_cell.shapes(top_m4_idx)

tracks_left = tracks_bottom = tracks_right = tracks_top = 0

io_width = 80000L
io_height = 120000L
ioring_width = 193000L

I_x = 25000L
I_width = 2800L
I_height = 1000L

C_x = 65650L
C_width = 2800L

OEN_x = 2375L
OEN_width = 2800L

IE_x = 76200L
IE_width = 2800L

SE_x = 72600L
SE_width = 2800L

PS_x = 48900L
PS_width = 2800L

PE_x = 52500L
PE_width = 2800L

DS_x = 43200L
DS_width = 2800L

pin_width = 900L # Make width wide enough so a via fits in

track_width = 5000L
track_space = 1000L
track_pitch = track_width + track_space

x_track1_left = 1000000L
x_track1_right = 1800000L
y_track1_bottom = 1000000L
y_track1_top = 1800000L

die_width = 2800000L
die_height = 2800000L

def insert_viarect(shapes, left, bottom, right, top, size, pitch, enclosure):
  """Write an array of vias enclosed in a rectangle"""

  width = right - left
  nx = (width - 2*enclosure - size)/pitch + 1
  dx = (width - size - (nx-1)*pitch)/2 # Center array
  height = top - bottom
  ny = (height - 2*enclosure - size)/pitch + 1
  dy = (height - size - (ny-1)*pitch)/2 # Center array
  for i_x in xrange(nx): # TODO(?): replace with array instance
    for i_y in xrange(ny):
      via_left = left + dx + i_x*pitch
      via_right = via_left + size
      via_bottom = bottom + dy + i_y*pitch
      via_top = via_bottom + size
      shapes.insert_box(pya.Box(via_left, via_bottom, via_right, via_top))

def insert_ring(shapes, left, bottom, right, top,  width):
    """Write a ring, coordinates are outer dimensions"""

    x_left_in = left + width
    x_left_out = left
    x_right_in = right - width
    x_right_out = right
    y_bottom_in = bottom + width
    y_bottom_out = bottom
    y_top_in = top - width
    y_top_out = top

    points = [
      pya.Point(x_left_out, y_bottom_out),
      pya.Point(x_left_out, y_top_out),
      pya.Point(x_right_out, y_top_out),
      pya.Point(x_right_out, y_bottom_out),
      pya.Point(x_left_in, y_bottom_out),
      pya.Point(x_left_in, y_bottom_in),
      pya.Point(x_right_in, y_bottom_in),
      pya.Point(x_right_in, y_top_in),
      pya.Point(x_left_in, y_top_in),
      pya.Point(x_left_in, y_bottom_out),
    ]
    shapes.insert_simple_polygon(pya.SimplePolygon(points))


def connect_iopin(side, offset, pin_x, pin_width, val):
  if side == "left":
    left = ioring_width
    right = dc_left
    top = offset + io_width - pin_x
    bottom = top - pin_width
  elif side == "bottom":
    left = offset + pin_x
    right = left + pin_width
    bottom = ioring_width
    top = dc_bottom
  elif side == "right":
    left = dc_right
    right = die_width - ioring_width
    bottom = offset + pin_x
    top = bottom + pin_width
  elif side == "top":
    right = offset + io_width - pin_x
    left = right - pin_width
    bottom = dc_top
    top = die_height - ioring_width
  else:
    raise Exception("Unsupport side {}".format(side))

  if val == 0:
    top_m1_shapes.insert_box(pya.Box(left, bottom, right, top))
  elif val == 1:
    top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
  else:
    raise Exception("Unsupported val {}".format(val))

def connect_pin(shape):
  """Connect a pin of the block to an IO cell"""

  global tracks_left, tracks_bottom, tracks_right, tracks_top

  pin_dir, pin_side, pin_nr = pins[shape.text_string]
  io_side, io_offset = iopads[pin_nr - 1]

  if pin_dir == "input":
    iopin_x = C_x
    iopin_width = C_width
    connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
    connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
    connect_iopin(io_side, io_offset, I_x, I_width, 0)
    connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
    connect_iopin(io_side, io_offset, PS_x, PS_width, 1)
    connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
    connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
  else:
    assert(pin_dir == "output")
    iopin_x = I_x
    iopin_width = I_width
    connect_iopin(io_side, io_offset, OEN_x, OEN_width, 0)
    connect_iopin(io_side, io_offset, IE_x, IE_width, 0)
    connect_iopin(io_side, io_offset, DS_x, DS_width, 1)
    connect_iopin(io_side, io_offset, PS_x, PS_width, 0)
    connect_iopin(io_side, io_offset, PE_x, PE_width, 0)
    connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
    # Draw antenna diode to avoid antenna violations


  if pin_side == "left":
    bottom = MOS6502Qf_y + shape.text_pos.y - pin_width/2
    top = bottom + pin_width
    left = x_track1_left - tracks_left*track_pitch - track_width
    right = MOS6502Qf_x + shape.text_pos.x
    top_m3_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_left = tracks_left + 1

    if io_side == "left":
      right2 = left + track_width
      left2 = ioring_width
      top2 = io_offset + io_width - iopin_x
      bottom2 = top2 - iopin_width
      insert_viarect(
        top_via23_shapes, left, bottom, right2, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left, min(bottom, bottom2), right2, max(top, top2))
      )
      insert_viarect(
        top_via23_shapes, left, bottom2, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    else:
      raise Exception("Unsupported io_side {} for left pin_side".format(io_side))
  elif pin_side == "bottom":
    top = MOS6502Qf_y + shape.text_pos.y
    bottom = y_track1_bottom - tracks_bottom*track_pitch - track_width
    left = MOS6502Qf_x + shape.text_pos.x - pin_width/2
    right = left + pin_width
    top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_bottom = tracks_bottom + 1

    if io_side == "bottom":
      top2 = bottom + track_width
      bottom2 = ioring_width
      left2 = io_offset + iopin_x
      right2 = left2 + iopin_width
      insert_viarect(
        top_via23_shapes, left, bottom, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left, left2), bottom, max(right, right2), top2)
      )
      insert_viarect(
        top_via23_shapes, left2, bottom, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    else:
      raise Exception("Unsupported io_side {} for bottom pin_side".format(io_side))
  elif pin_side == "right":
    bottom = MOS6502Qf_y + shape.text_pos.y - pin_width/2
    top = bottom + pin_width
    left = MOS6502Qf_x + shape.text_pos.x
    right = x_track1_right + tracks_right*track_pitch + track_width
    top_m3_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_right = tracks_right + 1

    if io_side == "right":
      left2 = right - track_width
      right2 = die_width - ioring_width
      bottom2 = io_offset + iopin_x
      top2 = bottom2 + iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left2, min(bottom, bottom2), right, max(top, top2))
      )
      insert_viarect(
        top_via23_shapes, left2, bottom2, right, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    elif io_side == "bottom":
      left2 = right - track_width
      right2 = right
      top2 = top
      bottom2 = y_track1_bottom - tracks_bottom*track_pitch - track_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_bottom = tracks_bottom + 1
      
      top3 = bottom2 + track_width
      bottom3 = ioring_width
      left3 = io_offset + iopin_x
      right3 = left3 + iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom2, right2, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left2, left3), bottom2, max(right2, right3), top3)
      )
      insert_viarect(
        top_via23_shapes, left3, bottom2, right3, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    elif io_side == "top":
      left2 = right - track_width
      right2 = right
      bottom2 = bottom
      top2 = y_track1_top + tracks_top*track_pitch + track_width
      insert_viarect(
        top_via23_shapes, left2, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_top = tracks_top + 1

      bottom3 = top2 - track_width
      top3 = die_height - ioring_width
      right3 = io_offset + io_width - iopin_x
      left3 = right3 - iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom3, right2, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left2, left3), bottom3, max(right2, right3), top2)
      )
      insert_viarect(
        top_via23_shapes, left3, bottom3, right3, top2,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    else:
      raise Exception("Unsupported io_side {} for right pin_side".format(io_side))
  elif pin_side == "top":
    left = MOS6502Qf_x + shape.text_pos.x - pin_width/2
    right = left + pin_width
    bottom = MOS6502Qf_y + shape.text_pos.y
    top = y_track1_top + tracks_top*track_pitch + track_width
    top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
    tracks_top = tracks_top + 1

    if io_side == "top":
      bottom2 = top - track_width
      top2 = die_height - ioring_width
      right2 = io_offset + io_width - iopin_x
      left2 = right2 - iopin_width
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(
        pya.Box(min(left, left2), bottom2, max(right, right2), top)
      )
      insert_viarect(
        top_via23_shapes, left2, bottom2, right2, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
    elif io_side == "left":
      bottom2 = top - track_width
      top2 = top
      right2 = right
      left2 = x_track1_left - tracks_left*track_pitch - track_width
      insert_viarect(
        top_via23_shapes, left, bottom2, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left2, bottom2, right2, top2))
      tracks_left = tracks_left + 1

      right3 = left2 + track_width
      left3 = ioring_width
      top3 = io_offset + io_width - iopin_x
      bottom3 = top3 - iopin_width
      insert_viarect(
        top_via23_shapes, left2, bottom2, right3, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m2_shapes.insert_box(
        pya.Box(left2, min(bottom2, bottom3), right3, max(top2, top3))
      )
      insert_viarect(
        top_via23_shapes, left2, bottom3, right3, top3,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
      )
      top_m3_shapes.insert_box(pya.Box(left3, bottom3, right3, top3))
    else:
      raise Exception("Unsupported io_size {} for top pin side".format(io_side))
  else:
    print("pin_side {} not implemented".format(pin_side))


# Add an antenna diode on the I pin
io_diff_idx = lay_IORing.layer(ti.Diffusion, 0)
io_nimp_idx = lay_IORing.layer(ti.NPlus, 0)
io_cont_idx = lay_IORing.layer(ti.Contact, 0)

io_cell = lay_IORing.cell("PDDWUWSW1216DGZ")

io_diff_shapes = io_cell.shapes(io_diff_idx)
io_nimp_shapes = io_cell.shapes(io_nimp_idx)
io_cont_shapes = io_cell.shapes(io_cont_idx)

left = I_x
right = left + I_width
top = io_height
bottom = top - I_height
io_diff_shapes.insert_box(pya.Box(left, bottom, right, top))
insert_viarect(io_cont_shapes, left, bottom, right, top, 400L, 800L, 250L)
diff_imp_enclosure = 250
left = left - diff_imp_enclosure
right = right + diff_imp_enclosure
bottom = bottom - diff_imp_enclosure
top = top + diff_imp_enclosure
io_nimp_shapes.insert_box(pya.Box(left, bottom, right, top))

idx_IORing = lay_SWII_MOS6502Qf.add_lib_cell(lib_IORing, cell_IORing.cell_index())
inst_IORing = pya.CellInstArray(idx_IORing, pya.Trans(0,0))
top_cell.insert(inst_IORing)


#
# DC ring; METAL1: VSS, METAL4: VDD
#
dc_dist = 10000L
dc_width = 30000L
dc_space = 1000L

dc_left = ioring_width + dc_dist
dc_bottom = ioring_width + dc_dist
dc_right = die_width - ioring_width - dc_dist
dc_top = die_height - ioring_width - dc_dist
insert_ring(top_m1_shapes, dc_left, dc_bottom, dc_right, dc_top, dc_width)
insert_ring(top_m4_shapes, dc_left, dc_bottom, dc_right, dc_top, dc_width)
# dc_verspace = 700000L
# dc_top2 = (die_height - dc_verspace)/2
# dc_bottom2 = dc_top2 - dc_width
# top_m2_shapes.insert_box(pya.Box(dc_left, dc_bottom2, dc_right, dc_top2))
# top_m4_shapes.insert_box(pya.Box(dc_left, dc_bottom2, dc_right, dc_top2))
# dc_bottom3 = dc_top2 + dc_verspace
# dc_top3 = dc_bottom3 + dc_width
# top_m2_shapes.insert_box(pya.Box(dc_left, dc_bottom3, dc_right, dc_top3))
# top_m4_shapes.insert_box(pya.Box(dc_left, dc_bottom3, dc_right, dc_top3))

# VDD IO connection
io_bottom = 1260000L
io_top = io_bottom + io_width
metal2_length = dc_dist - dc_space
# unrotated cell has pin at the top
iopin_coords = [
  (7000L, 38000L),
  (42000L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
  # left cell; 270 deg rotated
  left = ioring_width
  right = dc_left + dc_width
  bottom = io_top - iopin_right
  top = io_top - iopin_left
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via23_shapes, dc_left, bottom, right, top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  top_m3_shapes.insert_box(pya.Box(dc_left, bottom, right, top))
  insert_viarect(
    top_via34_shapes, dc_left, bottom, right, top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )
  # right cell; 90 deg rotated
  right = die_width - ioring_width
  left = dc_right - dc_width
  bottom = io_bottom + iopin_left
  top = io_bottom + iopin_right
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via23_shapes, left, bottom, dc_right, top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  top_m3_shapes.insert_box(pya.Box(left, bottom, dc_right, top))
  insert_viarect(
    top_via34_shapes, left, bottom, dc_right, top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )

# VSS connection
io_bottom = 1060000L
io_top = io_bottom + io_width
# pins on METAL2
# unrotated cell has pin at the top
iopin_coords = [
  (7000L, 27400L),
  (29800L, 50200L),
  (52600L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
  # left cell; 270deg rotated
  left = ioring_width
  right = dc_left + dc_width
  bottom = io_top - iopin_right
  top = io_top - iopin_left
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via12_shapes, dc_left, bottom, right, top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )
  # right cell; 90def rotated
  left = dc_right - dc_width
  right = die_width - ioring_width
  bottom = io_bottom + iopin_left
  top = io_bottom + iopin_right
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top))
  insert_viarect(
    top_via12_shapes, left, bottom, dc_right, top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )


# Core block connect VDD/GND
corevdd_xs = [87950L, 286350L, 483150L]
coregnd_xs = [187150L, 385850L, 583950L]
coredc_width = 3300L
# VDD
bottom = dc_bottom
top = dc_top
for coredc_x in corevdd_xs:
  left = MOS6502Qf_x + coredc_x
  right = left + coredc_width
  top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
# GND
bottom = bottom + dc_width + dc_space
top = top - dc_width - dc_space
for coredc_x in coregnd_xs:
  left = MOS6502Qf_x + coredc_x
  right = left + coredc_width
  top_m4_shapes.insert_box(pya.Box(left, bottom, right, top))
  top2 = bottom + coredc_width
  bottom2 = top - coredc_width
  insert_viarect(
    top_via34_shapes, left, bottom, right, top2,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )
  insert_viarect(
    top_via34_shapes, left, bottom2, right, top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
  )
  top_m3_shapes.insert_box(pya.Box(left, bottom, right, top2))
  top_m3_shapes.insert_box(pya.Box(left, bottom2, right, top))
  insert_viarect(
    top_via23_shapes, left, bottom, right, top2,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  insert_viarect(
    top_via23_shapes, left, bottom2, right, top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
  )
  top_m2_shapes.insert_box(pya.Box(left, bottom, right, top2))
  top_m2_shapes.insert_box(pya.Box(left, bottom2, right, top))
  insert_viarect(
    top_via12_shapes, left, bottom, right, top2,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )
  insert_viarect(
    top_via12_shapes, left, bottom2, right, top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
  )
  bottom3 = ioring_width + dc_dist
  top3 = die_height - ioring_width - dc_dist
  top_m1_shapes.insert_box(pya.Box(left, bottom3, right, top2))
  top_m1_shapes.insert_box(pya.Box(left, bottom2, right, top3))


# Core block connect to IO ring
shapes = cell_MOS6502Qf.shapes(MOS6502Qf_m2_idx)
for shape in shapes.each():
  t = shape.type()
  if t == TText or t == TTextRef:
    connect_pin(shape)
shapes = cell_MOS6502Qf.shapes(MOS6502Qf_m3_idx)
for shape in shapes.each():
  t = shape.type()
  if t == TText or t == TTextRef:
    connect_pin(shape)


# Set recognition pins; 22 => PD, 27 => PD
io_side, io_offset = iopads[22-1]
connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
connect_iopin(io_side, io_offset, I_x, I_width, 0)
connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
connect_iopin(io_side, io_offset, PS_x, PS_width, 0)
connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
connect_iopin(io_side, io_offset, SE_x, SE_width, 0)
io_side, io_offset = iopads[27-1]
connect_iopin(io_side, io_offset, OEN_x, OEN_width, 1)
connect_iopin(io_side, io_offset, IE_x, IE_width, 1)
connect_iopin(io_side, io_offset, I_x, I_width, 0)
connect_iopin(io_side, io_offset, DS_x, DS_width, 0)
connect_iopin(io_side, io_offset, PS_x, PS_width, 0)
connect_iopin(io_side, io_offset, PE_x, PE_width, 1)
connect_iopin(io_side, io_offset, SE_x, SE_width, 0)


# Post process the layer; remove DRC errors.

top_n3v_idx = lay_SWII_MOS6502Qf.layer(ti.N3V, 0)
top_pimp_idx = lay_SWII_MOS6502Qf.layer(ti.PPlus, 0)
top_nimp_idx = lay_SWII_MOS6502Qf.layer(ti.NPlus, 0)
top_poly_idx = lay_SWII_MOS6502Qf.layer(ti.Poly, 0)
top_cont_idx = lay_SWII_MOS6502Qf.layer(ti.Contact, 0)
top_work_idx = lay_SWII_MOS6502Qf.layer(250, 0)

top_n3v_shapes = top_cell.shapes(top_n3v_idx)
top_pimp_shapes = top_cell.shapes(top_pimp_idx)
top_nimp_shapes = top_cell.shapes(top_nimp_idx)
top_poly_shapes = top_cell.shapes(top_poly_idx)
top_cont_shapes = top_cell.shapes(top_cont_idx)
top_work_shapes = top_cell.shapes(top_work_idx)

shapeproc = pya.ShapeProcessor()

# Size N3V 0.3um over/under
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_n3v_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx,
  lay_SWII_MOS6502Qf, top_cell, top_n3v_idx,
  top_n3v_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size PIMP 0.3um over/under
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_pimp_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx,
  lay_SWII_MOS6502Qf, top_cell, top_pimp_idx,
  top_pimp_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size NIMP 0.3um over/under
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_nimp_idx, top_work_shapes, 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx, top_work_shapes, -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx,
  lay_SWII_MOS6502Qf, top_cell, top_nimp_idx,
  top_nimp_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Enforce contact-poly enclosure of 0.2um, avoid minimal space violation
shapeproc.boolean(
  lay_SWII_MOS6502Qf, top_cell, top_poly_idx,
  lay_SWII_MOS6502Qf, top_cell, top_cont_idx,
  top_work_shapes, pya.EdgeProcessor.ModeAnd,
  True, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx, top_poly_shapes, 200L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_poly_idx, top_work_shapes, 220L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx, top_work_shapes, -220L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
top_poly_shapes.clear()
shapeproc.boolean(
  lay_SWII_MOS6502Qf, top_cell, top_work_idx,
  lay_SWII_MOS6502Qf, top_cell, top_poly_idx,
  top_poly_shapes, pya.EdgeProcessor.ModeANotB,
  True, True, False
)

lay_SWII_MOS6502Qf.delete_layer(top_work_idx)



# Save the file

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
top_cell.write("SnowWhiteII_MOS6502Qf.gds.gz", saveopts)



print("End")
