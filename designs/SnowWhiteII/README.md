# SnowWhiteII

This is the source for the second SnowWhite test tape-out.

Originally the plan was to include an OpenRAM memory block, some 5V IO test structures, a digital block done with Qflow and one with Coriolis.

In the end the 5V test structures and the Coriolis digital block did not make it on the tape-out. Design and layout of the test structures was close to finishing but the integration of it on the test chip with the SRAM block and connecting the structures it to the pads could not be finished in the tape-out timeframe. For the Coriolis block the MOS6502 block itself was place-and-routed using the nsxlib cell library but the time lacked to figure out how to connect this block to the IO cells. This was not helped by some software stability problems after compiling the latest version.

In the following paragraph the three different design are described. The names correspond with subdirectories.

## common: The IO ring

In this directory the design is done for the IO Ring. The script is available in `script` subdirectory. Part of the code for SnowWhiteI was reused and still using old gdsii txt code. In the ioring.txt all IO cells are now using the highest drive strength cells.  
Issue #1 has been opened to replace full IO ring generation with klayout scripting.

## SRAM5V: Test chip with OpenRAM block

This block contains a 1024x8 OpenRAM block generated for scn4m_subm technology. The output is then converted by magic to gdsii file compatible with TSMC.  
The test chip has the following pin-out:

| Pin | Name     | | Pin | Name     |
|-----|----------|-|-----|----------|
| 1   | VDDCORE  | | 25  | VDDIO    |
| 2   | GNDCORE  | | 26  | GNDIO    |
| 3   | WEB0     | | 27  | HI       |
| 4   | CSB0     | | 28  | DIN0[6]  |
| 5   | CLK0     | | 29  | DOUT0[7] |
| 6   | ADDR0[0] | | 30  | DIN0[7]  |
| 7   | ADDR0[1] | | 31  | NC       |
| 8   | ADDR0[2] | | 32  | NC       |
| 9   | DOUT0[0] | | 33  | NC       |
| 10  | DIN0[0]  | | 34  | NC       |
| 11  | DOUT0[1] | | 35  | NC       |
| 12  | DIN0[1]  | | 36  | NC       |
| 13  | DOUT0[2] | | 37  | NC       |
| 14  | DIN0[2]  | | 38  | NC       |
| 15  | DOUT0[3] | | 39  | NC       |
| 16  | DIN0[3]  | | 40  | NC       |
| 17  | DOUT0[4] | | 41  | ADDR0[4] |
| 18  | DIN0[4]  | | 42  | ADDR0[5] |
| 19  | DOUT0[5] | | 43  | ADDR0[6] |
| 20  | DIN0[5]  | | 44  | ADDR0[7] |
| 21  | DOUT0[6] | | 45  | ADDR0[8] |
| 22  | HI       | | 46  | ADDR0[9] |
| 23  | GNDCORE  | | 47  | GNDIO    |
| 24  | VDDCORE  | | 48  | VDDIO    |

Pin 22 and 27 ares used as recognition pins; for this chips both are pulled high. There quite some NC pins that were meant for 5V test structures. These did not make it on the design as the pass-through analog IO cell was an analog IO cell and the other IO cells were digital. In the used IO library these cells may not be put next to each other but have to be separated by a breaker cell and each of the section have to get proper ground and supply connections. Time lacked to perform this change on the IO ring with enough verification.

Subdirectories:

- RAM: The directory where OpenRAM is run to generate the block and convert it to TSMC compatible layout.
- script: contains the klayout script to make the top layout of the design.

## MOS6502Qf: MOS6502 core with osu035 standard cells using Qflow

In this block Arlets MOS6502 core was used and physically implemented using the osu035 standard cells from inside Qflow. An 8-bit core was chosen to have the number of pins fit inside the 48 pins of the DIL package; a 32-bit block would need more pins.  
The packaged chip has to following pin-out:

| Pin | Name     | | Pin | Name     |
|-----|----------|-|-----|----------|
| 1   | VDDCORE  | | 25  | VDDIO    |
| 2   | GNDCORE  | | 26  | GNDIO    |
| 3   | NMI      | | 27  | LO       |
| 4   | IRQ      | | 28  | A[7]     |
| 5   | reset    | | 29  | A[8]     |
| 6   | clk      | | 30  | A[9]     |
| 7   | DI[0]    | | 31  | A[10]    |
| 8   | DI[1]    | | 32  | A[11]    |
| 9   | DI[2]    | | 33  | A[12]    |
| 10  | DI[3]    | | 34  | A[13]    |
| 11  | DI[4]    | | 35  | A[14]    |
| 12  | DI[5]    | | 36  | A[15]    |
| 13  | DI[6]    | | 37  | DO[7]    |
| 14  | DI[7]    | | 38  | DO[6]    |
| 15  | A[0]     | | 39  | DO[5]    |
| 16  | A[1]     | | 40  | DO[4]    |
| 17  | A[2]     | | 41  | DO[3]    |
| 18  | A[3]     | | 42  | DO[2]    |
| 19  | A[4]     | | 43  | DO[1]    |
| 20  | A[5]     | | 44  | DO[0]    |
| 21  | A[6]     | | 45  | WE       |
| 22  | LO       | | 46  | RDY      |
| 23  | GNDCORE  | | 47  | GNDIO    |
| 24  | VDDCORE  | | 48  | VDDIO    |

For this chip both recognition pins 22 & 27 are pulled low.

Subdirectories:

- Qflow: directory where Qflow is run.
- script: contains the klayout script to make the top layout of the design

## MOS6502Qfredm4: MOS6502 core with osu035 standard cells with reduced metal4 pitch using Qflow

This is very similar to the previous design only the metal4 pitch for the osu035 cells has been reduced from 3.2um to 1.6um. This was done with adapted qflow code. The code can be found in the [qflow gitlab repo](https://gitlab.com/Chips4Makers/qflow) under the [SnowWhiteII tag](https://gitlab.com/Chips4Makers/qflow). In the future these changes will be upstreamed. The pin-out of this chip is basically the same as for the other chip:

| Pin | Name     | | Pin | Name     |
|-----|----------|-|-----|----------|
| 1   | VDDCORE  | | 25  | VDDIO    |
| 2   | GNDCORE  | | 26  | GNDIO    |
| 3   | NMI      | | 27  | HI       |
| 4   | IRQ      | | 28  | A[7]     |
| 5   | reset    | | 29  | A[8]     |
| 6   | clk      | | 30  | A[9]     |
| 7   | DI[0]    | | 31  | A[10]    |
| 8   | DI[1]    | | 32  | A[11]    |
| 9   | DI[2]    | | 33  | A[12]    |
| 10  | DI[3]    | | 34  | A[13]    |
| 11  | DI[4]    | | 35  | A[14]    |
| 12  | DI[5]    | | 36  | A[15]    |
| 13  | DI[6]    | | 37  | DO[7]    |
| 14  | DI[7]    | | 38  | DO[6]    |
| 15  | A[0]     | | 39  | DO[5]    |
| 16  | A[1]     | | 40  | DO[4]    |
| 17  | A[2]     | | 41  | DO[3]    |
| 18  | A[3]     | | 42  | DO[2]    |
| 19  | A[4]     | | 43  | DO[1]    |
| 20  | A[5]     | | 44  | DO[0]    |
| 21  | A[6]     | | 45  | WE       |
| 22  | LO       | | 46  | RDY      |
| 23  | GNDCORE  | | 47  | GNDIO    |
| 24  | VDDCORE  | | 48  | VDDIO    |

Only change to the pin-out is that recognition pin 27 is pulled high.


Subdirectories:

- Qflow: directory where Qflow is run.
- script: contains the klayout script to make the top layout of the design
