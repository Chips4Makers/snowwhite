from Hurricane import DbU
import Cfg
import node180.scn6m_deep_09

DbU.setPrecision(1)
DbU.setPhysicalsPerGrid    ( 0.005, DbU.UnitPowerMicro )
DbU.setGridsPerLambda      ( 36 )

Cfg.getParamDouble("gdsDriver.metricDbu").setDouble(5e-9)
