#!/usr/bin/env python

import sys
from Hurricane import (
    DataBase, UpdateSession, Box, Net, Contact, Pin, Horizontal, Vertical, BasicLayer, DbU,
    Instance, Transformation, Rectilinear, Point
)
import CRL
from   helpers   import u

from flexhelpers import draw_diamondvia, draw_rect, draw_viarect, draw_rects, draw_rectsxy, draw_rings, dim_from_area

from TSMC_C018.dr import *

af = CRL.AllianceFramework.get()

def draw_sramsp6t_cell(
    cell,
    l_pu, w_pu, l_pd, w_pd, l_pg, w_pg,
    butted_taps, connect_bitline, node_probe,
):
    assert not(connect_bitline and node_probe)

    pins = {}

    fusednet = Net.create(cell, "*")

    vdd = Net.create(cell, "vdd")
    gnd = Net.create(cell, "gnd")
    wl = Net.create(cell, "wl")
    bl = Net.create(cell, "bl")
    blb = Net.create(cell, "blb")
    n = Net.create(cell, "n")
    nb = Net.create(cell, "nb")

    UpdateSession.open()

    blcut0_bottom = -cut0_width//2
    pgactive_bottom = blcut0_bottom - cut0_active_minenclosure
    blcut0_top = blcut0_bottom + cut0_width
    gndcut0_left = -cut0_width//2
    gndcut0_right = gndcut0_left + cut0_width
    invnactive_left = gndcut0_left - cut0_active_minenclosure
    nimplant_left = invnactive_left - nactive_nimplant_minenclosure

    pgpoly_bottom = blcut0_top + cut0_poly_minspace
    pgpoly_top = pgpoly_bottom + l_pg
    invpoly_bottom = pgpoly_top + poly_minspace
    invnactive_bottom = invpoly_bottom + poly_active_minextension
    invnactive_top = invnactive_bottom + w_pd
    if not butted_taps:
        nimplant_top = invnactive_top + nimplant_ntrans_minextension
    else:
        nimplant_top = invnactive_top + max(nimplant_ntrans_minextension, nactive_pimplant_minspace, transend_implant_space)

    invpoly_left = gndcut0_right + max(
        cut0_poly_minspace,
        cut0_active_minenclosure + active_poly_minspace,
    )
    assert l_pu == l_pd
    invpoly_right = invpoly_left + l_pd
    nbpadpoly_left = invpoly_right + poly_minspace
    npadpoly_left = invpoly_right

    cut0_left = invpoly_right + cut0_poly_minspace
    cut0_right = cut0_left + cut0_width
    cut0_top = invnactive_top - cut0_active_minenclosure
    cut0_bottom = cut0_top - cut0_width
    invnactive_right = cut0_right + cut0_active_minenclosure
    nm1_bottom = cut0_bottom - metal1_cut0_minextension
    wlm1_top = nm1_bottom - metal1_minspace
    nm1_left = cut0_left - cut0_metal1_minenclosure
    nm1_right = cut0_right + cut0_metal1_minenclosure

    pgactive_left = invpoly_right + active_poly_minspace
    pgactive_right = pgactive_left + w_pg
    pgactive_top = invnactive_bottom

    draw_rect(fusednet, active, pgactive_left, pgactive_bottom, pgactive_right, pgactive_top)

    blcut0_left = pgactive_left + (w_pg - cut0_width)//2
    blcut0_right = blcut0_left + cut0_width

    draw_rect(bl, cut0, blcut0_left, blcut0_bottom, blcut0_right, blcut0_top)

    invnactive_right = max(invnactive_right, pgactive_right)

    if not node_probe:
        draw_rect(n, cut0, cut0_left, cut0_bottom, cut0_right, cut0_top)
        draw_rect(fusednet, active, invnactive_left, invnactive_bottom, invnactive_right, invnactive_top)

    wlcut0_left = pgactive_right + active_poly_minspace + cut0_poly_minenclosure
    wlcut0_right = wlcut0_left + cut0_width

    cell_width = 2*(pgactive_right + active_poly_minspace) + 2*cut0_poly_minenclosure + cut0_width

    # pgpoly_left = pgactive_left - poly_active_minextension
    pgpoly_left = u(0.0)
    pgpoly_right = cell_width

    draw_rect(wl, poly, pgpoly_left, pgpoly_bottom, pgpoly_right, pgpoly_top)

    gndptapcut0_left = -cut0_width//2
    gndptapcut0_right = gndptapcut0_left + cut0_width
    pimplant_bottom = nimplant_top
    if not butted_taps:
        gndcut0_top = invnactive_top - cut0_active_minenclosure
        gndcut0_bottom = gndcut0_top - cut0_width

        gndptapactive_bottom = max(
            pimplant_bottom + ptap_pimplant_minenclosure,
            invnactive_top + active_minspace,
            nimplant_top + ptap_nimplant_minspace,
        )
        gndptapcut0_bottom = gndptapactive_bottom + cut0_active_minenclosure
        gndptapcut0_top = gndptapcut0_bottom + cut0_width
        gndptapactive_top = gndptapcut0_top + cut0_active_minenclosure
        gndptapactive_left = gndptapcut0_left - cut0_active_minenclosure
        gndptapactive_right = gndptapcut0_right + cut0_active_minenclosure
        gndptapactive_width = gndptapactive_right - gndptapactive_left
        gndptapactive_height = gndptapactive_top - gndptapactive_bottom
        gndptapactive_minheight = dim_from_area(active_minarea, grid, gndptapactive_width)
        if gndptapactive_minheight > gndptapactive_height:
            gndptapactive_top = gndptapactive_bottom + gndptapactive_height

        draw_rect(gnd, cut0, gndptapcut0_left, gndptapcut0_bottom, gndptapcut0_right, gndptapcut0_top)
        draw_rect(gnd, active, gndptapactive_left, gndptapactive_bottom, gndptapactive_right, gndptapactive_top)

        gndptapcut0_left, gndptapcut0_right = cell_width - gndptapcut0_right, cell_width - gndptapcut0_left
        gndptapactive_left, gndptapactive_right = cell_width - gndptapactive_right, cell_width - gndptapactive_left

        draw_rect(gnd, cut0, gndptapcut0_left, gndptapcut0_bottom, gndptapcut0_right, gndptapcut0_top)
        draw_rect(gnd, active, gndptapactive_left, gndptapactive_bottom, gndptapactive_right, gndptapactive_top)
    else:
        gndptapactive_left = gndptapcut0_left - cut0_active_minenclosure
        gndptapactive_right = gndptapcut0_right + cut0_active_minenclosure
        gndptapcut0_bottom = pimplant_bottom + cut0_pimplant_minenclosure
        gndptapcut0_top = gndptapcut0_bottom + cut0_width
        gndptapactive_bottom = invnactive_top if not node_probe else invnactive_bottom
        gndptapactive_top = gndptapcut0_top + cut0_active_minenclosure

        gndcut0_top = min(
            nimplant_top - cut0_nimplant_minenclosure,
            gndptapcut0_bottom - cut0_minspace,
        )
        gndcut0_bottom = gndcut0_top - cut0_width

        draw_rect(gnd, active, gndptapactive_left, gndptapactive_bottom, gndptapactive_right, gndptapactive_top)
        draw_rect(gnd, cut0, gndptapcut0_left, gndptapcut0_bottom, gndptapcut0_right, gndptapcut0_top) 

        gndptapcut0_left, gndptapcut0_right = cell_width - gndptapcut0_right, cell_width - gndptapcut0_left
        gndptapactive_left, gndptapactive_right = cell_width - gndptapactive_right, cell_width - gndptapactive_left

        draw_rect(gnd, active, gndptapactive_left, gndptapactive_bottom, gndptapactive_right, gndptapactive_top)
        draw_rect(gnd, cut0, gndptapcut0_left, gndptapcut0_bottom, gndptapcut0_right, gndptapcut0_top) 

    gndm1_bottom = gndcut0_bottom - metal1_cut0_minextension
    gndm1_top = gndptapcut0_top + metal1_cut0_minextension
    gndcut1_left = -cut1_width//2
    gndcut1_right = gndcut1_left + cut1_width
    gndcut1_bottom = (gndm1_bottom + gndm1_top - cut1_width)//2
    gndcut1_top = gndcut1_bottom + cut1_width
    gndm1_left = gndcut1_left - cut1_metal1_minenclosure
    gndm1_right = gndcut1_right + cut1_metal1_minenclosure
    gndm2_left = gndcut1_left - cut1_metal2_minenclosure
    gndm2_right = gndcut1_right + cut1_metal2_minenclosure

    draw_rect(gnd, cut0, gndcut0_left, gndcut0_bottom, gndcut0_right, gndcut0_top)
    draw_rect(gnd, metal1, gndm1_left, gndm1_bottom, gndm1_right, gndm1_top)
    draw_rect(gnd, cut1, gndcut1_left, gndcut1_bottom, gndcut1_right, gndcut1_top)

    gndcut0_left, gndcut0_right = cell_width - gndcut0_right, cell_width - gndcut0_left
    gndm1_left, gndm1_right = cell_width - gndm1_right, cell_width - gndm1_left
    gndcut1_left, gndcut1_right = cell_width - gndcut1_right, cell_width - gndcut1_left

    draw_rect(gnd, cut0, gndcut0_left, gndcut0_bottom, gndcut0_right, gndcut0_top)
    draw_rect(gnd, metal1, gndm1_left, gndm1_bottom, gndm1_right, gndm1_top)
    draw_rect(gnd, cut1, gndcut1_left, gndcut1_bottom, gndcut1_right, gndcut1_top)

    # Draw the bitline in metal2 with equal width and spacing in between the GND metal2 lines
    blm2_width = on_grid((cell_width - (gndm2_right - gndm2_left))//5)
    blm2_left = gndm2_right + blm2_width
    blm2_right = blm2_left + blm2_width
    blcut1_bottom = -cut1_width//2
    blcut1_top = blcut1_bottom + cut1_width
    blcut1_left = (blm2_left + blm2_right - cut1_width)//2
    blcut1_right = blcut1_left + cut1_width
    blm1_bottom = min(
        blcut0_bottom - cut0_metal1_minenclosure,
        blcut1_bottom - cut1_metal1_minenclosure,
    )
    blm1_top = max(
        blcut0_top + cut0_metal1_minenclosure,
        blcut1_top + cut1_metal1_minenclosure,
    )
    blm1_height = blm1_top - blm1_bottom
    blm1_left = min(
        blcut0_left - metal1_cut0_minextension,
        blcut1_left - metal1_cut1_minextension,
    )
    blm1_right = max(
        blcut0_right + metal1_cut0_minextension,
        blcut1_right + metal1_cut1_minextension,
    )
    blm1_width = blm1_right - blm1_left
    blm1_minwidth = dim_from_area(metal1_minarea, grid, blm1_height)
    if blm1_minwidth > blm1_width:
        blm1_left = blm1_right - blm1_minwidth
    wlm1_bottom = blm1_top + metal1_minspace

    wlm1_left = u(0.0)
    wlm1_right = cell_width

    pins["wl"] = draw_rect(wl, metal1, wlm1_left, wlm1_bottom, wlm1_right, wlm1_top)

    nwell_bottom = gndptapactive_top + ptap_nwell_minspace

    nimplant_bottom = pgactive_bottom - nactive_nimplant_minenclosure
    pgactive_left, pgactive_right = cell_width - pgactive_right, cell_width - pgactive_left

    draw_rect(fusednet, active, pgactive_left, pgactive_bottom, pgactive_right, pgactive_top)

    cut0_left_m, cut0_right_m = cell_width - cut0_right, cell_width - cut0_left
    invnactive_left, invnactive_right = cell_width - invnactive_right, cell_width - invnactive_left
    nimplant_right = invnactive_right + nactive_nimplant_minenclosure

    if not node_probe:
        draw_rect(nb, cut0, cut0_left_m, cut0_bottom, cut0_right_m, cut0_top)
        draw_rect(fusednet, active, invnactive_left, invnactive_bottom, invnactive_right, invnactive_top)
    draw_rect(fusednet, nimplant, nimplant_left, nimplant_bottom, nimplant_right, nimplant_top)

    blcut0_left, blcut0_right = cell_width - blcut0_right, cell_width - blcut0_left

    draw_rect(blb, cut0, blcut0_left, blcut0_bottom, blcut0_right, blcut0_top)

    invpactive_bottom = nwell_bottom + pactive_nwell_minenclosure
    invpactive_top = invpactive_bottom + w_pu
    if not butted_taps:
        pimplant_top = invpactive_top + pimplant_ptrans_minextension
    else:
        pimplant_top = invpactive_top + max(pimplant_ptrans_minextension, pactive_nimplant_minspace, transend_implant_space)
    vddcut0_left = -cut0_width//2
    vddcut0_right = vddcut0_left + cut0_width
    invpactive_left = vddcut0_left - cut0_active_minenclosure
    pimplant_left = invpactive_left - pactive_pimplant_minenclosure
    nwell_left = invpactive_left - pactive_nwell_minenclosure
    nwell_right = cell_width - nwell_left

    # Center n/nb poly pads vertically in between PU and PD active area
    space = invpactive_bottom - invnactive_top - 2*active_poly_minspace
    height = 2*cut0_poly_minenclosure + cut0_width + poly_minspace + poly_minwidth
    assert space >= height
    delta = on_grid((space - height)//2)
    nbpadpoly_bottom = invnactive_top + active_poly_minspace + delta
    npadpoly_top = invpactive_bottom - active_poly_minspace - delta

    invpoly_top = invpactive_top + poly_active_minextension

    draw_rect(nb, poly, invpoly_left, invpoly_bottom, invpoly_right, invpoly_top)

    invpoly_left, invpoly_right = cell_width - invpoly_right, cell_width - invpoly_left
    npadpoly_right = invpoly_left - poly_minspace
    nbpadpoly_right = invpoly_left

    draw_rect(nb, poly, invpoly_left, invpoly_bottom, invpoly_right, invpoly_top)


    vddntapcut0_left = -cut0_width//2
    vddntapcut0_right = vddntapcut0_left + cut0_width
    nimplant_bottom = pimplant_top
    nimplant_left = pimplant_left
    if not butted_taps:
        vddcut0_bottom = (invpactive_bottom + invpactive_top - cut0_width)//2
        vddcut0_top = vddcut0_bottom + cut0_width

        vddntapactive_bottom = max(
            nimplant_bottom + ntap_nimplant_minenclosure,
            pimplant_top + ntap_pimplant_minspace,
            invpactive_top + active_minspace,
        )
        vddntapcut0_bottom = vddntapactive_bottom + cut0_active_minenclosure
        vddntapcut0_top = vddntapcut0_bottom + cut0_width
        vddntapactive_top = vddntapcut0_top + cut0_active_minenclosure
        nimplant_top = vddntapactive_top + ntap_nimplant_minenclosure
        vddntapactive_left = vddntapcut0_left - cut0_active_minenclosure

        draw_rect(vdd, cut0, vddntapcut0_left, vddntapcut0_bottom, vddntapcut0_right, vddntapcut0_top)

        vddntapcut0_left, vddntapcut0_right = cell_width - vddntapcut0_right, cell_width - vddntapcut0_left
        vddntapactive_right = vddntapcut0_right + cut0_active_minenclosure

        draw_rect(vdd, cut0, vddntapcut0_left, vddntapcut0_bottom, vddntapcut0_right, vddntapcut0_top)
        draw_rect(vdd, active, vddntapactive_left, vddntapactive_bottom, vddntapactive_right, vddntapactive_top)

        # Draw contact in the middle
        vddntapcut0_left = (cell_width - cut0_width)//2
        vddntapcut0_right = vddntapcut0_left + cut0_width

        draw_rect(vdd, cut0, vddntapcut0_left, vddntapcut0_bottom, vddntapcut0_right, vddntapcut0_top)

        nwell_top = vddntapactive_top + ntap_nwell_minenclosure
    else:
        vddntapactive_left = vddntapcut0_left - cut0_active_minenclosure
        vddntapactive_right = vddntapcut0_right + cut0_active_minenclosure
        vddntapcut0_bottom = nimplant_bottom + cut0_nimplant_minenclosure
        vddntapcut0_top = vddntapcut0_bottom + cut0_width
        vddcut0_top = min(
            pimplant_top - cut0_pimplant_minenclosure,
            vddntapcut0_bottom - cut0_minspace,
        )
        vddcut0_bottom = vddcut0_top - cut0_width
        nimplant_top = vddntapcut0_top + cut0_nimplant_minenclosure
        vddntapactive_bottom = invpactive_top if not node_probe else invpactive_bottom
        vddntapactive_top = nimplant_top + nactive_nimplant_minoverlap

        draw_rect(vdd, active, vddntapactive_left, vddntapactive_bottom, vddntapactive_right, vddntapactive_top)
        draw_rect(vdd, cut0, vddntapcut0_left, vddntapcut0_bottom, vddntapcut0_right, vddntapcut0_top)

        vddntapcut0_left, vddntapcut0_right = cell_width - vddntapcut0_right, cell_width - vddntapcut0_left
        vddntapactive_left, vddntapactive_right = cell_width - vddntapactive_right, cell_width - vddntapactive_left

        draw_rect(vdd, active, vddntapactive_left, vddntapactive_bottom, vddntapactive_right, vddntapactive_top)
        draw_rect(vdd, cut0, vddntapcut0_left, vddntapcut0_bottom, vddntapcut0_right, vddntapcut0_top)

        pimplant_left2 = nimplant_left
        pimplant_right2 = nimplant_right
        pimplant_bottom2 = nimplant_top
        pimplant_top2 = pimplant_bottom2 + max(
            pimplant_minwidth,
            nactive_nimplant_minoverlap + nactive_nimplant_minenclosure,
        )

        draw_rect(vdd, pimplant, pimplant_left2, pimplant_bottom2, pimplant_right2, pimplant_top2)

        nwell_top = vddntapactive_top + pactive_nwell_minenclosure
    nimplant_top = max(nimplant_top, nimplant_bottom + nimplant_minwidth)

    vddrailm1_left = vddm1_left = vddcut0_left - cut0_metal1_minenclosure
    vddm1_right = vddcut0_right + cut0_metal1_minenclosure
    vddm1_bottom = vddcut0_bottom - metal1_cut0_minextension
    vddrailm1_top = vddm1_top = vddntapcut0_top + metal1_cut0_minextension

    draw_rect(vdd, cut0, vddcut0_left, vddcut0_bottom, vddcut0_right, vddcut0_top)
    draw_rect(vdd, metal1, vddm1_left, vddm1_bottom, vddm1_right, vddm1_top)

    vddcut0_left, vddcut0_right = cell_width - vddcut0_right, cell_width - vddcut0_left
    vddm1_left, vddm1_right = cell_width - vddm1_right, cell_width - vddm1_left
    vddrailm1_right = vddm1_right

    draw_rect(vdd, cut0, vddcut0_left, vddcut0_bottom, vddcut0_right, vddcut0_top)
    draw_rect(vdd, metal1, vddm1_left, vddm1_bottom, vddm1_right, vddm1_top)

    # Metal1 connection for internal nodes
    invpactive_right = cut0_right + cut0_active_minenclosure
    cut0_bottom = invpactive_bottom + cut0_active_minenclosure
    cut0_top = cut0_bottom + cut0_width
    nm1_top = cut0_top + metal1_cut0_minextension
    vddrailm1_bottom = nm1_top + metal1_minspace

    pins["vdd"] = draw_rect(vdd, metal1, vddrailm1_left, vddrailm1_bottom, vddrailm1_right, vddrailm1_top)
    if not node_probe:
        draw_rect(fusednet, active, invpactive_left, invpactive_bottom, invpactive_right, invpactive_top)

    if not node_probe:
        draw_rect(n, cut0, cut0_left, cut0_bottom, cut0_right, cut0_top)
        draw_rect(n, metal1, nm1_left, nm1_bottom, nm1_right, nm1_top)
    else:
        nm1_left2 = -nm1_left
        nm1_midy2 = (gndm1_top + vddm1_bottom)//2
        nm1_bottom2 = nm1_midy2 - metal1_minwidth//2
        nm1_top2 = nm1_midy2 + metal1_minwidth//2
        Rectilinear.create(n, metal1, [
            Point(nm1_left2, nm1_bottom2),
            Point(nm1_left, nm1_bottom2),
            Point(nm1_left, nm1_bottom),
            Point(nm1_right, nm1_bottom),
            Point(nm1_right, nm1_top),
            Point(nm1_left, nm1_top),
            Point(nm1_left, nm1_top2),
            Point(nm1_left2, nm1_top2),
            Point(nm1_left2, nm1_bottom2),
        ])

    invpactive_left, invpactive_right = cell_width - invpactive_right, cell_width - invpactive_left
    pimplant_right = invpactive_right + nactive_nimplant_minenclosure
    cut0_left, cut0_right = cell_width - cut0_right, cell_width - cut0_left
    nm1_left, nm1_right = cell_width - nm1_right, cell_width - nm1_left

    if not node_probe:
        draw_rect(fusednet, active, invpactive_left, invpactive_bottom, invpactive_right, invpactive_top)
    draw_rect(fusednet, pimplant, pimplant_left, pimplant_bottom, pimplant_right, pimplant_top)
    if not node_probe:
        draw_rect(nb, cut0, cut0_left, cut0_bottom, cut0_right, cut0_top)
        draw_rect(nb, metal1, nm1_left, nm1_bottom, nm1_right, nm1_top)
    else:
        nm1_right2 = 2*cell_width - nm1_right
        Rectilinear.create(n, metal1, [
            Point(nm1_left, nm1_bottom),
            Point(nm1_right, nm1_bottom),
            Point(nm1_right, nm1_bottom2),
            Point(nm1_right2, nm1_bottom2),
            Point(nm1_right2, nm1_top2),
            Point(nm1_right, nm1_top2),
            Point(nm1_right, nm1_top),
            Point(nm1_left, nm1_top),
            Point(nm1_left, nm1_bottom),
        ])

    npadcut0_right = npadpoly_right - cut0_poly_minenclosure
    npadcut0_left = npadcut0_right - cut0_width
    npadcut0_top = npadpoly_top - cut0_poly_minenclosure
    npadcut0_bottom = npadcut0_top - cut0_width
    npadpoly_bottom = npadpoly_top - poly_minwidth
    nbpadcut0_left = nbpadpoly_left + cut0_poly_minenclosure
    nbpadcut0_right = nbpadcut0_left + cut0_width
    nbpadcut0_bottom = nbpadpoly_bottom + cut0_poly_minenclosure
    nbpadcut0_top = nbpadcut0_bottom + cut0_width
    nbpadpoly_top = nbpadpoly_bottom + poly_minwidth

    draw_rect(n, poly, npadpoly_left, npadpoly_bottom, npadpoly_right, npadpoly_top)
    draw_rect(n, poly, npadcut0_left, npadcut0_bottom, npadcut0_right, npadcut0_top, cut0_poly_minenclosure)
    if not node_probe:
        draw_rects(
            n, npadcut0_left, npadcut0_bottom, npadcut0_right, npadcut0_top,
            (
                ("wire", cut0),
                ("wire", metal1, metal1_cut0_minextension),
            )
        )
    else:
        npadcut1_right = blm2_right - cut1_metal2_minenclosure
        npadcut1_left = npadcut1_right - cut1_width
        npadcut1_bottom = nbpadcut0_bottom + (cut0_width - cut1_width)//2
        npadcut1_top = npadcut1_bottom + cut1_width
        draw_rects(
            n, npadcut1_left, npadcut1_bottom, npadcut1_right, npadcut1_top,
            (
                ("wire", cut1),
                ("wire", metal1, metal1_cut1_minextension),
            )
        )
    draw_rect(nb, poly, nbpadpoly_left, nbpadpoly_bottom, nbpadpoly_right, nbpadpoly_top)
    draw_rect(n, poly, nbpadcut0_left, nbpadcut0_bottom, nbpadcut0_right, nbpadcut0_top, cut0_poly_minenclosure)
    if not node_probe:
        draw_rects(
            nb, nbpadcut0_left, nbpadcut0_bottom, nbpadcut0_right, nbpadcut0_top,
            (
                ("wire", cut0),
                ("wire", metal1, metal1_cut0_minextension),
            )
        )
    else:
        nbpadcut1_left = (cell_width - blm2_right) + cut1_metal2_minenclosure
        nbpadcut1_right = nbpadcut1_left + cut1_width
        nbpadcut1_bottom = npadcut0_bottom + (cut0_width - cut1_width)//2
        nbpadcut1_top = nbpadcut1_bottom + cut1_width
        draw_rects(
            n, nbpadcut1_left, nbpadcut1_bottom, nbpadcut1_right, nbpadcut1_top,
            (
                ("wire", cut1),
                ("wire", metal1, metal1_cut1_minextension),
            )
        )


    nimplant_right = pimplant_right

    draw_rect(vdd, nimplant, nimplant_left, nimplant_bottom, nimplant_right, nimplant_top)
    draw_rect(vdd, nwell, nwell_left, nwell_bottom, nwell_right, nwell_top)

    cell_height = (vddntapcut0_bottom + vddntapcut0_top)//2

    gndm2_bottom = u(0.0)
    gndm2_top = cell_height
    blm2_bottom = blcut1_bottom - metal2_cut1_minextension
    blm2_top = cell_height

    _pin_left = draw_rect(gnd, metal2, gndm2_left, gndm2_bottom, gndm2_right, gndm2_top)
    draw_rect(bl, metal1, blm1_left, blm1_bottom, blm1_right, blm1_top)
    if connect_bitline:
        draw_rect(bl, cut1, blcut1_left, blcut1_bottom, blcut1_right, blcut1_top)
    pins["bl"] = draw_rect(bl, metal2, blm2_left, blm2_bottom, blm2_right, blm2_top)

    gndm2_left, gndm2_right = cell_width - gndm2_right, cell_width - gndm2_left
    blm1_left, blm1_right = cell_width - blm1_right, cell_width - blm1_left
    blcut1_left, blcut1_right = cell_width - blcut1_right, cell_width - blcut1_left
    blm2_left, blm2_right = cell_width - blm2_right, cell_width - blm2_left

    _pin_right = draw_rect(gnd, metal2, gndm2_left, gndm2_bottom, gndm2_right, gndm2_top)
    pins["gnd"] = (_pin_left, _pin_right)
    draw_rect(blb, metal1, blm1_left, blm1_bottom, blm1_right, blm1_top)
    if connect_bitline:
        draw_rect(blb, cut1, blcut1_left, blcut1_bottom, blcut1_right, blcut1_top)
    pins["blb"] = draw_rect(blb, metal2, blm2_left, blm2_bottom, blm2_right, blm2_top)

    wlcut0_bottom = wlm1_bottom + cut0_metal1_minenclosure
    wlcut0_top = wlcut0_bottom + cut0_width

    draw_rects(
        wl, wlcut0_left, wlcut0_bottom, wlcut0_right, wlcut0_top,
        (
            ("wire", cut0),
            ("wire", poly, cut0_poly_minenclosure),
        )
    )

    cell.setAbutmentBox(Box(u(0.0), u(0.0), cell_width, cell_height))

    UpdateSession.close()

    return pins

def draw_arraycon(
    cell,
    rows, cols, row_cell, col_cell,
    base_cell, base_pins, array_bottom, array_left,
    gnd_layer, gnd_top, vdd_layer, vdd_top,
    pin_layer, pin_left, pin_right, pin_pitch, pin_top,
    node_probe=False,
):
    assert gnd_layer == metal4
    assert vdd_layer == metal3

    base_bb = base_cell.getAbutmentBox()
    assert (base_bb.getXMin() == 0) and (base_bb.getYMin() == 0)
    base_width = base_bb.getXMax()
    base_height = base_bb.getYMax()
    array_right = array_left + cols*base_width
    array_top = array_bottom + rows*base_height

    # Nets
    fusednet = Net.create(cell, "*")
    gnd = Net.create(cell, "gnd")
    vdd = Net.create(cell, "vdd")
    wl = Net.create(cell, "wl")
    bl = Net.create(cell, "bl")
    blb = Net.create(cell, "blb")
    if node_probe:
        node = Net.create(cell, "node")
        nodeb = Net.create(cell, "nodeb")

    # Convenience and assumtions on the layers and dimensions
    gnd_pins = base_pins["gnd"]
    assert len(gnd_pins) == 2
    for pin in gnd_pins:
        assert pin.getLayer() == metal2
    gnd_basebb = gnd_pins[0].getBoundingBox()
    _bb2 = gnd_pins[1].getBoundingBox()
    assert (gnd_basebb.getXMin() + base_width) == _bb2.getXMin()
    assert (gnd_basebb.getXMax() + base_width) == _bb2.getXMax()
    assert base_pins["vdd"].getLayer() == metal1
    vdd_basebb = base_pins["vdd"].getBoundingBox()
    assert vdd_basebb.getYMin() < base_height < vdd_basebb.getYMax()
    assert base_pins["wl"].getLayer() == metal1
    wl_basebb = base_pins["wl"].getBoundingBox()
    assert base_pins["bl"].getLayer() == metal2
    bl_basebb = base_pins["bl"].getBoundingBox()
    assert base_pins["blb"].getLayer() == metal2
    blb_basebb = base_pins["blb"].getBoundingBox()

    assert blb_basebb.getXMin() > bl_basebb.getXMax()

    # Derived rules
    activecut0_minwidth = cut0_width + 2*cut0_active_minenclosure
    metal1cut0_minwidth = cut0_width + 2*cut0_metal1_minenclosure
    metal1cut0_minwidth4ext = cut0_width + 2*metal1_cut0_minextension
    metal1cut1_minwidth = cut1_width + 2*cut1_metal1_minenclosure
    metal1cut1_minwidth4ext = cut1_width + 2*metal1_cut1_minextension
    metal2cut1_minwidth = cut1_width + 2*cut1_metal2_minenclosure
    metal2cut1_minpitch = metal2cut1_minwidth + metal2_minspace
    metal2cut1_minwidth4ext = cut1_width + 2*metal2_cut1_minextension
    metal2cut1_minpitch4ext = metal2cut1_minwidth4ext + metal2_minspace
    metal2cut2_minwidth = cut2_width + 2*cut2_metal2_minenclosure
    metal2cut2_minpitch = metal2cut2_minwidth + metal2_minspace
    metal2cut2_minwidth4ext = cut2_width + 2*metal2_cut2_minextension
    metal2cut2_minpitch4ext = metal2cut2_minwidth4ext + metal2_minspace
    metal2cut1cut2_minwidth = max(metal2cut1_minwidth, metal2cut2_minwidth)
    metal2cut1cut2_minwidth4ext = max(metal2cut1_minwidth4ext, metal2cut2_minwidth4ext)
    metal3cut2_minwidth = cut3_width + 2*cut2_metal3_minenclosure
    metal3cut2_minpitch = metal3cut2_minwidth + metal3_minspace
    metal3cut2_minwidth4ext = cut2_width + 2*metal3_cut2_minextension
    metal3cut2_minpitch4ext = metal3cut2_minwidth4ext + metal3_minspace
    metal3cut3_minwidth = cut3_width + 2*cut3_metal3_minenclosure
    metal3cut3_minpitch = metal3cut3_minwidth + metal3_minspace
    metal3cut3_minwidth4ext = cut3_width + 2*metal3_cut3_minextension
    metal3cut3_minpitch4ext = metal3cut3_minwidth4ext + metal3_minspace
    metal3cut2cut3_minwidth = max(metal3cut2_minwidth, metal3cut3_minwidth)
    metal3cut2cut3_minwidth4ext = max(metal3cut2_minwidth4ext, metal3cut3_minwidth4ext)
    metal4cut3_minwidth = cut3_width + 2*cut3_metal4_minenclosure
    metal4cut3_minpitch = metal4cut3_minwidth + metal4_minspace
    metal4cut3_minwidth4ext = cut3_width + 2*metal4_cut3_minextension
    metal4cut3_minpitch4ext = metal4cut3_minwidth4ext + metal4_minspace
    metal4cut4_minwidth = cut4_width + 2*cut4_metal4_minenclosure
    metal4cut4_minpitch = metal4cut4_minwidth + metal4_minspace
    metal4cut4_minwidth4ext = cut4_width + 2*metal4_cut4_minextension
    metal4cut4_minpitch4ext = metal4cut4_minwidth4ext + metal4_minspace
    metal4cut3cut4_minwidth = max(metal4cut3_minwidth, metal4cut4_minwidth)
    metal4cut3cut4_minwidth4ext = max(metal4cut3_minwidth4ext, metal4cut4_minwidth4ext)

    # Connect gnd
    gndm2_top = array_bottom + gnd_basebb.getYMin()
    gndm2_bottom = gndm2_top - 3*metal2_minpitch
    _w = max(
        metal2cut2_minwidth4ext,
        metal3cut2cut3_minwidth4ext,
        metal4cut3_minwidth4ext,
    )
    _h = dim_from_area(metal3_minarea, grid, _w)
    gndm2pad_top = gndm2_bottom + _h
    for col in range(cols + 1):
        _x = array_left + col*base_width
        gndm2_left = _x + gnd_basebb.getXMin()
        gndm2_right = _x + gnd_basebb.getXMax()
        gndm2pad_left = _x + gnd_basebb.getXCenter() - _w//2
        gndm2pad_right = _x + gnd_basebb.getXCenter() + _w//2
        Rectilinear.create(gnd, metal2, [
            Point(gndm2pad_left, gndm2_bottom),
            Point(gndm2pad_right, gndm2_bottom),
            Point(gndm2pad_right, gndm2pad_top),
            Point(gndm2_right, gndm2pad_top),
            Point(gndm2_right, gndm2_top),
            Point(gndm2_left, gndm2_top),
            Point(gndm2_left, gndm2pad_top),
            Point(gndm2pad_left, gndm2pad_top),
            Point(gndm2pad_left, gndm2_bottom),
        ])
        draw_rectsxy(
            gnd, _x + gnd_basebb.getXCenter(), (gndm2_bottom + gndm2pad_top)//2,
            (
                (cut2, cut2_width, cut2_width),
                (metal3, _w, _h),
                (cut3, cut3_width, cut3_width),
            )
        )
        gndm4_left = gndm2pad_left
        gndm4_right = gndm2pad_right
        gndm4_top = gndm2pad_top
        gndm4_bottom = gnd_top
        draw_rect(gnd, metal4, gndm4_left, gndm4_bottom, gndm4_right, gndm4_top)

    # Connect vdd
    vddm1_left = array_right - base_width + vdd_basebb.getXMax()
    vddm1_right = vddm1_left + 3*metal1_minpitch
    vddm1_height = vdd_basebb.getHeight()
    _w = max(
        metal1cut1_minwidth4ext,
        metal2cut1cut2_minwidth4ext,
        metal3cut2_minwidth4ext,
        vddm1_height,
    )
    vddm1pad_left = vddm1_right - _w
    vddm1pad_midx = vddm1_right - _w//2
    vddm3_left = vddm1pad_left
    vddm3_right = vddm1_right
    vddm3_bottom = vdd_top
    vddm3_tops = []
    for row in range(0, rows, 2):
        _y = array_bottom + row*base_height
        vddm1_bottom = _y + vdd_basebb.getYMin()
        vddm1_top = _y + vdd_basebb.getYMax()
        vddm1pad_bottom = _y + vdd_basebb.getYCenter() - _w//2
        vddm1pad_top = _y + vdd_basebb.getYCenter() + _w//2
        vddm3_tops.append(vddm1pad_top)
        Rectilinear.create(vdd, metal1, [
            Point(vddm1_left, vddm1_bottom),
            Point(vddm1pad_left, vddm1_bottom),
            Point(vddm1pad_left, vddm1pad_bottom),
            Point(vddm1_right, vddm1pad_bottom),
            Point(vddm1_right, vddm1pad_top),
            Point(vddm1pad_left, vddm1pad_top),
            Point(vddm1pad_left, vddm1_top),
            Point(vddm1_left, vddm1_top),
            Point(vddm1_left, vddm1_bottom),
        ])
        vddm1pad_midy = _y + vdd_basebb.getYCenter()
        draw_rectsxy(vdd, vddm1pad_midx, vddm1pad_midy,
            (
                (cut1, cut1_width, cut1_width),
                (metal2, _w, _w),
                (cut2, cut2_width, cut2_width),
            )
        )
    vddm3_top = max(vddm3_tops)
    draw_rect(vdd, metal3, vddm3_left, vddm3_bottom, vddm3_right, vddm3_top)

    # Connect the word line
    wlm1_right = array_left + wl_basebb.getXMin()
    wlm1_left = wlm1_right - 3*metal2cut1_minpitch
    wlpad_left = wlm1_left
    wlpad_right = wlpad_left + metal2cut1cut2_minwidth4ext + cut1_minpitch
    wlpad_tops = []
    for row in range(rows):
        if (row % 2) == 0:
            wlm1_bottom = array_bottom + row*base_height + wl_basebb.getYMin()
            wlm1_top = array_bottom + row*base_height + wl_basebb.getYMax()
        else: # odd row
            wlm1_bottom = array_bottom + (row + 1)*base_height - wl_basebb.getYMax()
            wlm1_top = array_bottom + (row + 1)*base_height - wl_basebb.getYMin()

        if row == row_cell:
            draw_rect(wl, metal1, wlm1_left, wlm1_bottom, wlm1_right, wlm1_top)
            pin = -1 if not node_probe else -2

            wlm2_bottom = pin_top
            wlm2_bottom2 = wlm1_bottom
            assert wlm2_bottom2 >= wlm2_bottom
            wlm2_top = wlm1_top
            assert (wlm2_top - wlm2_bottom) >= metal2cut1_minwidth
            wlm2_right = wlm1_left + metal2cut1_minwidth4ext
            wlm2_right2 = pin_right + pin*pin_pitch
            assert wlm2_right2 < wlm2_right
            wlm2_left = wlm2_right2 - metal2_minwidth
            draw_viarect(
                wl, cut1,
                wlm1_left, wlm2_bottom2, wlm2_right, wlm2_top,
                cut1_width, cut1_minspace, cut1_metal2_minenclosure,
            )
            Rectilinear.create(wl, metal2, [
                Point(wlm2_left, wlm2_bottom),
                Point(wlm2_right2, wlm2_bottom),
                Point(wlm2_right2, wlm2_bottom2),
                Point(wlm2_right, wlm2_bottom2),
                Point(wlm2_right, wlm2_top),
                Point(wlm2_left, wlm2_top),
                Point(wlm2_left, wlm2_bottom),
            ])
        else:
            draw_rect(gnd, metal1, wlm1_left, wlm1_bottom, wlm1_right, wlm1_top)
            assert (wlm1_top - wlm1_bottom) >= max(
                metal1cut1_minwidth, metal2cut1cut2_minwidth, metal3cut2cut3_minwidth, metal4cut3_minwidth,
            )
            wlpad_bottom = wlm1_bottom
            wlpad_top = wlm1_top
            wlpad_tops.append(wlpad_top)
            draw_rects(
                gnd, wlpad_left, wlpad_bottom, wlpad_right, wlpad_top,
                (
                    ("wire", metal2),
                    ("wire", metal3),
                )
            )
            wlpadcut_left = wlpad_left + metal2_cut1_minextension - cut1_metal2_minenclosure
            wlpadcut_right = wlpad_right - metal2_cut1_minextension + cut1_metal2_minenclosure
            draw_rects(
                gnd, wlpadcut_left, wlpad_bottom, wlpadcut_right, wlpad_top,
                (
                    ("via", cut1, cut1_width, cut1_minspace, max(cut1_metal1_minenclosure, cut1_metal2_minenclosure)),
                    ("via", cut2, cut2_width, cut2_minspace, max(cut2_metal2_minenclosure, cut2_metal3_minenclosure)),
                    ("via", cut3, cut3_width, cut3_minspace, max(cut3_metal3_minenclosure, cut3_metal4_minenclosure)),
                )
            )
    wlgndm4_top = max(wlpad_tops)
    wlgndm4_bottom = gnd_top
    draw_rect(gnd, metal4, wlpad_left, wlgndm4_bottom, wlpad_right, wlgndm4_top)
    
    # Connect the bl
    blm2_left = array_left + col_cell*base_width + bl_basebb.getXMin()
    blm2_right = array_left + col_cell*base_width + bl_basebb.getXMax()
    blm2_bottom = pin_top
    blm2_top = array_bottom + bl_basebb.getYMin()
    assert (blm2_left > pin_left) and (blm2_right < pin_right)
    draw_rect(bl, metal2, blm2_left, blm2_bottom, blm2_right, blm2_top)

    # Connect the blb
    blm2_left = array_left + col_cell*base_width + blb_basebb.getXMin()
    blm2_right = array_left + col_cell*base_width + blb_basebb.getXMax()
    blm2_bottom = pin_top
    blm2_top = array_bottom + blb_basebb.getYMin()
    blm2_left2 = pin_left + pin_pitch
    blm2_right2 = blm2_left2 + metal2_minwidth
    blm2_bottom2 = blm2_bottom + metal2wide_minspace
    blm2_top2 = blm2_bottom2 + metal2_minwidth
    assert blm2_left2 > blm2_right
    Rectilinear.create(blb, metal2, [
        Point(blm2_left2, blm2_bottom),
        Point(blm2_right2, blm2_bottom),
        Point(blm2_right2, blm2_top2),
        Point(blm2_right, blm2_top2),
        Point(blm2_right, blm2_top),
        Point(blm2_left, blm2_top),
        Point(blm2_left, blm2_bottom2),
        Point(blm2_left2, blm2_bottom2),
        Point(blm2_left2, blm2_bottom),
    ])

    if node_probe:
        # Connect node, which corresponds with blb of col - 1
        nodem2_left = array_left + (col_cell - 1)*base_width + blb_basebb.getXMin()
        nodem2_right = array_left + (col_cell - 1)*base_width + blb_basebb.getXMax()
        nodem2_bottom = pin_top
        nodem2_top = array_bottom + blb_basebb.getYMin()
        nodem2_right2 = pin_right - pin_pitch
        nodem2_left2 = nodem2_right2 - metal2_minwidth
        nodem2_bottom2 = nodem2_bottom + metal2wide_minspace
        nodem2_top2 = nodem2_bottom2 + metal2_minwidth
        assert nodem2_right2 < nodem2_left
        Rectilinear.create(node, metal2, [
            Point(nodem2_left2, nodem2_bottom),
            Point(nodem2_right2, nodem2_bottom),
            Point(nodem2_right2, nodem2_bottom2),
            Point(nodem2_right, nodem2_bottom2),
            Point(nodem2_right, nodem2_top),
            Point(nodem2_left, nodem2_top),
            Point(nodem2_left, nodem2_top2),
            Point(nodem2_left2, nodem2_top2),
            Point(nodem2_left2, nodem2_bottom),
        ])

        # Connect nodeb, which corresponds with bl of col + 1           
        nodebm2_left = array_left + (col_cell + 1)*base_width + bl_basebb.getXMin()
        nodebm2_right = array_left + (col_cell + 1)*base_width + bl_basebb.getXMax()
        nodebm2_bottom = pin_top
        nodebm2_top = array_bottom + bl_basebb.getYMin()
        nodebm2_left2 = pin_left + 2*pin_pitch
        nodebm2_right2 = nodebm2_left2 + metal2_minwidth
        nodebm2_bottom2 = nodebm2_bottom + metal2wide_minspace + metal2_minpitch
        nodebm2_top2 = nodebm2_bottom2 + metal2_minwidth
        assert nodebm2_left2 > nodebm2_right
        Rectilinear.create(nodeb, metal2, [
            Point(nodebm2_left2, nodebm2_bottom),
            Point(nodebm2_right2, nodebm2_bottom),
            Point(nodebm2_right2, nodebm2_top2),
            Point(nodebm2_right, nodebm2_top2),
            Point(nodebm2_right, nodebm2_top),
            Point(nodebm2_left, nodebm2_top),
            Point(nodebm2_left, nodebm2_bottom2),
            Point(nodebm2_left2, nodebm2_bottom2),
            Point(nodebm2_left2, nodebm2_bottom),
        ])

def scriptMain ( **kw ):
    #
    # Init
    #
    l_pu = u(0.18)
    w_pu = u(0.42)
    l_pd = u(0.18)
    w_pd = u(0.48)
    l_pg = u(0.22)
    w_pg = u(0.42)

    io_pitch = u(90.0)
    iopin_left = u(10.0)
    iopin_right = u(80.0)

    butted_taps = True

    # SRAMSP6T
    cell = af.createCell('SRAMSP6T')
    if not cell:
        print '[ERROR] Unable to create cell "SRAMSP6T", aborting .'
        return False
    kw['cell'] = cell

    pins = draw_sramsp6t_cell(
        cell,
        l_pu, w_pu, l_pd, w_pd, l_pg, w_pg, butted_taps, connect_bitline=True, node_probe=False,
    )
    _bb = cell.getAbutmentBox()
    cell_width = _bb.getXMax()
    cell_height = _bb.getYMax()

    CRL.Gds.save(cell)

    # 1X2 array of SRAMSP6T
    cell1X2 = af.createCell("SRAMSP6T1X2")

    UpdateSession.open()

    Instance.create(
        cell1X2, "cell_00", cell,
        Transformation(0, 0, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    Instance.create(
        cell1X2, "cell_01", cell,
        Transformation(0, 2*cell_height, Transformation.Orientation.MY),
        Instance.PlacementStatus.FIXED,
    )

    UpdateSession.close()

    CRL.Gds.save(cell1X2)

    # 10X10 array of SRAMSP6T
    cell10X10 = af.createCell("SRAMSP6T10X10")
    UpdateSession.open()

    rows = 5
    cols = 10
    for row in range(rows):
        for col in range(cols):
            Instance.create(
                cell10X10, "r{}c{}".format(row+1, col+1), cell1X2,
                Transformation(col*cell_width, 2*row*cell_height, Transformation.Orientation.ID),
                Instance.PlacementStatus.FIXED,
            )
    
    UpdateSession.close()
    CRL.Gds.save(cell10X10)

    # 10X10 array of SRAMSP6T with IO pin connections
    cell10X10conn = af.createCell("SRAMSP6T10X10CONN")
    UpdateSession.open()

    x = u(20.0)
    y = u(10.0)
    Instance.create(
        cell10X10conn, "array", cell10X10,
        Transformation(x, y, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    
    draw_arraycon(
        cell10X10conn,
        10, 10, 5, 5,
        cell, pins, y, x,
        metal4, u(0.0), metal3, u(0.0),
        metal2, iopin_left, iopin_right, io_pitch, u(0.0),
    )
    UpdateSession.close()
    CRL.Gds.save(cell10X10conn)

    # SRAMSP6T with floating bitline
    cellflbl = af.createCell('SRAMSP6TFLBL')

    draw_sramsp6t_cell(
        cellflbl,
        l_pu, w_pu, l_pd, w_pd, l_pg, w_pg, butted_taps, connect_bitline=False, node_probe=False,
    )
    _bb = cellflbl.getAbutmentBox()
    assert (cell_width == _bb.getXMax()) and (cell_height == _bb.getYMax())

    CRL.Gds.save(cellflbl)

    # 1X2 array of SRAMSP6T with floating bitline
    cellflbl1X2 = af.createCell("SRAMSP6TFLBL1X2")

    UpdateSession.open()

    Instance.create(
        cellflbl1X2, "cell_00", cellflbl,
        Transformation(0, 0, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    Instance.create(
        cellflbl1X2, "cell_01", cellflbl,
        Transformation(0, 2*cell_height, Transformation.Orientation.MY),
        Instance.PlacementStatus.FIXED,
    )

    CRL.Gds.save(cellflbl1X2)

    # SRAMSP6T for probing neighboring node
    cellprobe = af.createCell('SRAMSP6TPROBE')

    draw_sramsp6t_cell(
        cellprobe,
        l_pu, w_pu, l_pd, w_pd, l_pg, w_pg, butted_taps, connect_bitline=False, node_probe=True,
    )
    _bb = cellprobe.getAbutmentBox()
    assert (cell_width == _bb.getXMax()) and (cell_height == _bb.getYMax())

    CRL.Gds.save(cellprobe)

    # 1X2 array of SRAMSP6T with probing of neightbouring cell
    # It contains one probe cell and one floating bit line cell
    cellprobe1X2 = af.createCell("SRAMSP6TPROBE1X2")

    UpdateSession.open()

    Instance.create(
        cellprobe1X2, "cell_00", cellprobe,
        Transformation(0, 0, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    Instance.create(
        cellprobe1X2, "cell_01", cellflbl,
        Transformation(0, 2*cell_height, Transformation.Orientation.MY),
        Instance.PlacementStatus.FIXED,
    )

    CRL.Gds.save(cellprobe1X2)

    # 11X10 array of SRAMSP6T with probing for central cell
    cellprobe11X10 = af.createCell("SRAMSP6TPROBE11X10")

    UpdateSession.open()

    rows = 5
    cols = 11
    for row in range(rows):
        for col in range(cols):
            if col in (4, 6):
                _cell = cellprobe1X2 if row == 2 else cellflbl1X2
            else:
                _cell = cell1X2
                
            Instance.create(
                cellprobe11X10, "r{}c{}".format(row+1, col+1), _cell,
                Transformation(col*cell_width, 2*row*cell_height, Transformation.Orientation.ID),
                Instance.PlacementStatus.FIXED,
            )
    
    UpdateSession.close()

    CRL.Gds.save(cellprobe11X10)

    # 10X10 array of SRAMSP6T with IO pin connections
    cellprobe11X10conn = af.createCell("SRAMSP6TPROBE11X10CONN")
    UpdateSession.open()

    x = u(20.0)
    y = u(10.0)
    Instance.create(
        cellprobe11X10conn, "array", cellprobe11X10,
        Transformation(x, y, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    
    draw_arraycon(
        cellprobe11X10conn,
        10, 11, 5, 5,
        cell, pins, y, x,
        metal4, u(0.0), metal3, u(0.0),
        metal2, iopin_left, iopin_right, io_pitch, u(0.0),
        node_probe=True,
    )
    UpdateSession.close()
    CRL.Gds.save(cellprobe11X10conn)

    # Gallery so everything can be saved in one gds file
    celltop = af.createCell("SRAMTop")

    UpdateSession.open()

    # Share world line between two arrays
    Instance.create(
        celltop, "array10X10", cell10X10conn,
        Transformation(io_pitch, 0, Transformation.Orientation.ID),
        Instance.PlacementStatus.FIXED,
    )
    Instance.create(
        celltop, "arrayprobe11X10", cellprobe11X10conn,
        Transformation(-io_pitch, 0, Transformation.Orientation.MX),
        Instance.PlacementStatus.FIXED,
    )

    UpdateSession.close()
    CRL.Gds.save(celltop)

    print("ScriptMain executed")
    return True


if __name__ == '__main__':
    success      = scriptMain()
    shellSuccess = 0
    if not success:
        shellSuccess = 1

    sys.exit( shellSuccess )
