from Hurricane import DbU
import Cfg
import node180.scn6m_deep_09

DbU.setPrecision(1)
DbU.setPhysicalsPerGrid    ( 0.001, DbU.UnitPowerMicro )
DbU.setGridsPerLambda      ( 180 )

Cfg.getParamDouble("gdsDriver.metricDbu").setDouble(1e-9)
