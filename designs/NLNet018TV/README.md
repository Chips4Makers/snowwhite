# NLNet018TV testchip

This directory and it's descendants contain the source code the the NLNet 0.18um test chip. This test chip is made in order to develop an open source flow for a prototype chip for the [Libre-SOC](https://libre-soc.org/) project [that will be taped out later this year](https://systemeslibres.org/updates/code_to_tapeout/).
This test chip includes some of the building blocks needed to have a digital flow

# Design and package

The layout of the top cell of the chip can be seen in the next picture:

![Top Cell](images/Top.png)

You can see the three types of test structures:

* IO Cells
* SRAM cell
* SR Latches

Also the test inputs for the IO cells and the SR latches are indicated on the picture. The SRAM test structure has their own inputs and outputs at the bottom of the design.

In the next picture then the bonding diagram is given.

![Bonding Diagram](images/BondingDiagram.png)

This picture show how the chip will be placed in the cavity of a CPGA84 package and how the pads on the die will be bondedn to the pads of the package. Be aware the die in this picture is rotated 90 degrees clockwise so that the SRAM structure is connected to the lowest pin number of the package.

In the next two pictures the pin diagram of the package is given together with the interconnection plan for the package wirebond pad to the package pin.

![Pin Diagram](images/PinDiagram.png)
![Pin Interconnection](images/Wirebond2Pin.png)

Combining this information with the pin function of the chip gives the following table:

| Pin | Pad Nr. | IO Nr. | Function | IO Type |
|:-:|:-:|:-:|:-:|:-:|
| B2 | 1 | - | N.C. | - |
| C2 | 2 | - | N.C. | - |
| B1 | 3 | - | N.C. | - |
| C1 | 4 | - | N.C. | - |
| D2 | 5 | 1 | GND | GND |
| D1 | 6 | 2 | 1.8V | VDDCORE |
| E3 | 7 | 3 | SRAM1 INT | ANALOG |
| E2 | 8 | 4 | SRAM1 BL | ANALOG |
| E1 | 9 | 5 | SRAM1 BLn | ANALOG |
| F2 | 10 | 6 | SRAM1 INTn | ANALOG |
| F3 | 11 | 7 | SRAM1/2 WL | ANALOG |
| G3 | 12 | 8 | SRAM2 BL | ANALOG |
| G1 | 13 | 9 | SRAM2 BLn | ANALOG |
| G2 | 14 | 10 | N.C. | ANALOG |
| F1 | 15 | 11 | N.C. | ANALOG |
| H1 | 16 | 12 | 1.8V | VDDCORE |
| H2 | 17 | 13 | GND | GND |
| J1 | 18 | - | N.C. | - |
| K1 | 19 | - | N.C. | - |
| J2 | 20 | - | N.C. | - |
| L1 | 21 | - | N.C. | - |
| K2 | 22 | - | N.C. | - |
| K3 | 23 | - | N.C. | - |
| L2 | 24 | - | N.C. | - |
| L3 | 25 | - | N.C. | - |
| K4 | 26 | 14 | GND | GND |
| L4 | 27 | 15 | 3.3V | VDDIO |
| J5 | 28 | 16 | N.C. | - |
| K5 | 29 | 17 | N.C. | - |
| L5 | 30 | 18 | nsnrlatch_x4_B out | GPIO |
| K6 | 31 | 19 | nsnrlatch_x4_A out | ANALOG |
| J6 | 32 | 20 | nsnrlatch_x1_B out | GPIO |
| J7 | 33 | 21 | nsnrlatch_x1_A out | ANALOG |
| L7 | 34 | 22 | srlatch_x4_B out | GPIO |
| K7 | 35 | 23 | srlatch_x4_A out | ANALOG |
| L6 | 36 | 24 | srlatch_x1_B out | GPIO |
| L8 | 37 | 25 | srlatch_x1_A out | ANALOG |
| K8 | 38 | 26 | 3.3V | VDDIO |
| L9 | 39 | - | N.C. | - |
| L10 | 40 | - | N.C. | - |
| K9 | 41 | - | N.C. | - |
| L11 | 42 | - | N.C. | - |
| K10 | 43 | - | N.C. | - |
| J10 | 44 | - | N.C. | - |
| K11 | 45 | - | N.C. | - |
| J11 | 46 | - | N.C. | - |
| H10 | 47 | 27 | GND | GND |
| H11 | 48 | 28 | R_Rn | ANALOG |
| F10 | 49 | 29 | S_Sn | ANALOG |
| G10 | 50 | 30 | PUSHAREDn | ANALOG |
| G11 | 51 | 31 | PUHIn | ANALOG |
| G9 | 52 | 32 | PU2n | ANALOG |
| F9 | 53 | 33 | PU1n | ANALOG |
| F11 | 54 | 34 | PU0n | ANALOG |
| E11 | 55 | 35 | PDSHARED | ANALOG |
| E10 | 56 | 36 | PDHI | ANALOG |
| E9 | 57 | 37 | PD2 | ANALOG |
| D11 | 58 | 38 | PD1 | ANALOG |
| D10 | 59 | 39 | PD0 | ANALOG |
| C11 | 60 | - | N.C. | - |
| B11 | 61 | - | N.C. | - |
| C10 | 62 | - | N.C. | - |
| A11 | 63 | - | N.C. | - |
| B10 | 64 | - | N.C. | - |
| B9 | 65 | - | N.C. | - |
| A10 | 66 | - | N.C. | - |
| A9 | 67 | - | N.C. | - |
| B8 | 68 | 40 | TestIO1 | GPIO |
| A8 | 69 | 41 | TestIO2 | GPIO |
| B6 | 70 | 42 | TestIO3 | GPIO |
| B7 | 71 | 43 | TestIO4 | GPIO |
| A7 | 72 | 44 | TestIO4.CORE | ANALOG |
| C7 | 73 | 45 | TestIO5 | GPIO |
| C6 | 74 | 46 | TestIO6 | GPIO |
| A6 | 75 | 47 | TestIO7 | GPIO |
| A5 | 76 | 48 | TestIO8 | GPIO |
| B5 | 77 | 49 | TestIO9 | GPIO |
| C5 | 78 | 50 | TestIO10 | GPIO |
| A4 | 79 | 51 | 3.3V | VDDIO |
| B4 | 80 | 52 | GND | GND |
| A3 | 81 | - | N.C. | - |
| A2 | 82 | - | N.C. | - |
| B3 | 83 | - | N.C. | - |
| A1 | 84 | - | N.C. | - |

## IO Cells

Five different IO (Input/Output) cells have been designed for this tape-out:

* GND: The ground connection. There is a shared ground connection for both the core and the IO.
* VDDCORE: This the cell to connect the supply to for the digital logic, also often called the core. For this technology the core transistors are 1.8V.
* VDDIO: This the cell to connect the supply for the IO cells. The IO transistors for this technology are 3.3V.
* GPIO: This is a general purpose digital IO cell that can do both input to as well as output from the core logic. It shift the signal voltages 
* ANALOG: The is an ANALOG input. It allows to connect directly to an internal signal. It does have ESD protection diodes on the input. It can be used for both monitoring an internal signal as for driving an internal signal.

In picture below a close-up is given of the GPIO cell.

![GPIO](images/GPIO.png)

In the close-up of the level-up/level-down circuitry the pins used to connect to/from the core are presented.

![Level-up/down close-up](images/LevelUpDown.png)

Which gives the following input/output pins for each GPIO cell:

| Pin | Direction | Type | Description |
|:-:|:-:|:-:|:-|
| PAD | I/O | 3.3V | Pad opening to connect bond wire |
| PAD2CORE | Output | 1.8V | Down-shifted PAD state, threshold 50% of VDDIO |
| PD0 | Input | 1.8V active-high | Enable pull-down |
| PD1 | Input | 1.8V active-high | Enable 10mA sink driver |
| PD2 | Input | 1.8V active-high | Enable 20mA sink driver |
| PDHI | Input | 1.8V active-high | Enable 230mA sink driver |
| PU0n | Input | 1.8V active-low | Enable pull-up |
| PU1n | Input | 1.8V active-low | Enable 10mA source driver |
| PU2n | Input | 1.8V active-low | Enable 20mA source driver |
| PUHIn | Input | 1.8V active-low | Enable 230 mA source driver |

In a picture and table above already the placement of the IO test cells and the test inputs was given. In the table below it is documented how the test input are connected to the test IO pins. The heading of the table indicates the pin name of the GPIO cell and inside the table the external test signal connected to that pin.

| | PAD2CORE | PD0 | PD1 | PD2 | PDHI | PU0n | PU1n | PU2n | PUHIn |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| **TestIO1** | TestIO2.PD1 | PD0 | PD1 | PD2 | PDHI | PU0n | PU1n | PU2n | PUHIn |
| **TestIO2** | TestIO3.PD2 | PD0 | TestIO1.PAD2CORE | PD2 | PDHI | PU0n | TestIO5.PAD2CORE | PU2n | PUHIn |
| **TestIO3** | TestIO4.PDHI | PD0 | PD1 | TestIO2.PAD2CORE | PDHI | PU0n | PU1n | TestIO6.PAD2CORE | PUHIn |
| **TestIO4** | ANALOG IO | PD0 | PD1 | PD2 | TestIO3.PAD2CORE | PU0n | PU1n | PU2n | TestIO7.PAD2CORE |
| **TestIO5** | TestIO2.PU1n | PDSHARED | PDSHARED | PD2 | PDHI | PUSHAREDn | PUSHAREDn | PU2n | PUHIn |
| **TestIO6** | TestIO3.PU2n | PD0 | PDSHARED | PDSHARED | PDHI | PU0n | PUSHAREDn | PUSHAREDn | PUHIn |
| **TestIO7** | TestIO4.PUHIn | PD0 | PDSHARED | PDSHARED | PDSHARED | PU0n | PUSHAREDn | PUSHAREDn | PUSHAREDn |
| **TestIO8** | N.C. | PD0 | PDSHARED | PD2 | PDSHARED | PU0n | PUSHAREDn | PU2n | PUSHAREDn |
| **TestIO9** | TestIO10.(PD1+PU1n) | PD0 | PD1 | PDSHARED | PDSHARED | PU0n | PU1n | PUSHAREDn | PUSHAREDn |
| **TestIO10** | N.C. | PD0 | TestIO9.PAD2CORE | PD2 | PDHI | PU0n | TestIO9.PAD2CORE | PU2n | PUHIn |

## SRAM cells

In next picture a close-up of one of the two SRAM blocks is given:

![SRAM block](images/SRAMBlock.png)

This provides a block of SRAM cells from which only the center cell is connected out for testing. The difference between the two blocks is that in one also the two internal nodes of the SRAM cell is connected out for measurement and on the other not.

## SR latches

For this tape-out also work was done an a scalable standard library based on the nsxlib standard cell library. In the libre-SOC prototype SR-latches will be used but were not available in the nsxlib. Thus the scalable standard cell framework has been used to generate these cells and put them on this test chip for testing.

Two different SR latches have been designed one based on NAND gates and one on NOR. The former has active-low signal for set and reset and the latter active-high ones. For each type of cells two drive strength versions are available (x1 and x4).
Each of these four cells is put two times on the test chips; once with output connected to an ANALOG IO output and once with output connected to the PD1 and PU1n pins of a GPIO cell. The rest of the PD/PU pins of the GPIO are connected to PDSHARED/PUSHAREDn respectively. The R_Rn external signal is connect to the R or Rn pin of all the eight cells and S_Sn to the S or Sn pin.

# Source code

The source code for making this test chip is located in the subdirectories.

* `flexram`:  
  `doSRAMCell.py` contains the source code for generating the SRAM subcell. It uses Coriolis for drawing the SRAM cells, the SRAM blocks and the SRAM top cell.  
  `Makefile` is there to build the output by calling make
* `flexio`:  
  In this directory I have been experimenting with Jupyter notebook in order to do the layout development. The Jupyter notebook `IOPad.ipynb` contains the source code for the IO cells, the IO ring, the SR latches and the IO top cell which includes all these cells. The reason that both IO cell and SR latches are handled by this code is that some of the test IO inputs are shared between the two.  
  This code is also using mainly Coriolis to generate the layouts.  
  Again a `Makefile` is provided to build the output using make. As can be seen also a Jupyter notebook can be used in batch mode to generate output.
* `nlnet018tv`:  
  This builds the top cell of the tape-out; the main file used for this is `NLNet018TV.py` which is a klayout script. It assembles the subblocks into a top cell and also does metal dummy fill. The `Makefile` also has a separate rule to run a design rule check on the resulting cell.
* `../../../util/tech/priv/TSMC_C018`:  
  In order to generate the layout information from the TSMC PDK needs to be used. This information is shared under an [NDA](https://en.wikipedia.org/wiki/Non-disclosure_agreement) so it can't be disclosed publicly. I kept this NDAed data in a separate directory which is not made public.  
  As a consequence one can't rebuild the files without replicating these files. Of course this makes my open source hart bleed and in a separate project called [PDKMaster](https://gitlab.com/Chips4Makers/PDKMaster) I am working on trying to make the handling of PDK (process design kit) data as open as possible.
* `testing`:  
  In this directory the code for testing the chip and the results will be kept. Currently only the [test plan](testing/TestPlan.md) is available there.

<u>(Some) lessons learned:</u>

TODO
