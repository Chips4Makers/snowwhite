from os.path import dirname

from PySpice.Unit import *
from PySpice.Spice.Netlist import (
    Circuit as PSCircuit, SubCircuitFactory as PSSubCircuitFactory
)
from PySpice.Spice.Library import SpiceLibrary

lib = SpiceLibrary(dirname(__file__))
dio = "d1n4007"
dio_inc = lib[dio]

__all__ = [
    "SubCircuitFactory", "Circuit",
    "BridgeDiodeProbe", "BridgeTwoStage", "TwoStage",
]


class SubCircuitFactory(PSSubCircuitFactory):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def D1N4007(self, name, anode, cathode):
        self.D(name, anode, cathode, model=dio)

    def BridgeRect(self, name, n, l, low, high):
        self.X(name, "bridge_rectifier", n, l, low, high)


class BridgeRect(SubCircuitFactory):
    __name__ = "bridge_rectifier"
    __nodes__ = ("N", "L", "LOW", "HIGH")

    def __init__(self):
        super().__init__()
        self.D1N4007(1, "LOW", "N")
        self.D1N4007(2, "LOW", "L")
        self.D1N4007(3, "N", "HIGH")
        self.D1N4007(4, "L", "HIGH")


class Circuit(PSCircuit):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.include(dio_inc)
        self.subcircuit(BridgeRect())

    def D1N4007(self, name, anode, cathode):
        self.D(name, anode, cathode, model=dio)

    def BridgeRect(self, name, n, l, low, high):
        self.X(name, "bridge_rectifier", n, l, low, high)


class BridgeDiodeProbe(Circuit):
    def __init__(self):
        super().__init__("Loaded Bridge Rectifier")
        c = u_pF(150)

        self.AcLine("grid", "N", "L", rms_voltage=u_V(240), frequency=u_Hz(50))

        self.BridgeRect("rect1", "N", "L", self.gnd, "rect1")
        self.C("store1", "rect1", self.gnd, c)
        self.D1N4007("load1", self.gnd, "rect1")

        self.BridgeRect("rect2", "N", "L", self.gnd, "rect2")
        self.C("store2", "rect2", self.gnd, c)
        self.D1N4007("load2", self.gnd, "rect2")
        self.R("probe2", self.gnd, "rect2", u_MΩ(10))


class BridgeTwoStage(Circuit):
    def __init__(self):
        super().__init__("Bridge followed by two stages")
        c = u_nF(1)

        self.AcLine("grid", "N", "L", rms_voltage=u_V(240), frequency=u_Hz(50))

        self.BridgeRect("rect", "N", "L", self.gnd, "rect")

        self.C(1, "rect", "n1", c)
        self.D1N4007(5, self.gnd, "n1")
        self.D1N4007(17, "n1", "n2")
        self.C(13, "n2", self.gnd, c)

        self.C(7, "n3", "n1", c)
        self.D1N4007(16, "n2", "n3")


class TwoStage(Circuit):
    def __init__(self):
        super().__init__("AC directly connected to tow stages")
        c = u_nF(1)

        self.AcLine("grid", "N", "L", rms_voltage=u_V(240), frequency=u_Hz(50))

        self.V("off", "N", self.gnd, (2.0**0.5)*240)

        self.C(1, "L", "n1", c)
        self.D1N4007(5, self.gnd, "n1")
        self.D1N4007(17, "n1", "n2")
        self.C(13, "n2", self.gnd, c)

        self.C(7, "n3", "n1", c)
        self.D1N4007(16, "n2", "n3")
