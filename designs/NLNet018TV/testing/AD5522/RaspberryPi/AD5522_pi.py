from math import log10, ceil, nan
from enum import Enum
from itertools import product
from spidev import SpiDev


__all__ = ["PMU", "AD5522", "MeasMode", "Channel"]


def _isiterable(v):
    try:
        iter(v)
    except TypeError:
        return False
    return True

def _ispow2(v):
    return (v != 0) and ((v & (v - 1)) == 0)

_channels = (0b0001, 0b0010, 0b0100, 0b1000)
_irangevs = {
    "5µA": 0b000,
    "20µA": 0b001,
    "200µA": 0b010,
    "2mA": 0b011,
    "extA": 0b100,
    "V": 0b101,
}
_ivs = {
    "I": 0b100,
    "V": 0b101,
}
_dacs = {
    "fin": {
        "code": 0b001<<3,
        "ivs": _irangevs,
    },
    "cll": {
        "code": 0b010<<3,
        "ivs": _ivs,
    },
    "clh": {
        "code": 0b011<<3,
        "ivs": _ivs,
    },
    "cpl": {
        "code": 0b100<<3,
        "ivs": _irangevs,
    },
    "cph": {
        "code": 0b101<<3,
        "ivs": _irangevs,
    },
}
_regs = {
    "sysctrl": {
        "code": 0b00<<6,
    },
    "comp": {
        "code": 0b01<<6,
    },
    "alarm": {
        "code": 0b11<<6,
    },
    "offset": {
        "code": 0b11<<6,
        "b0": 0b0001, # Use ch0, register is the same for all channels
    },
    "pmu": {
        "code": 0,
        "channels": _channels,
    },
    "C": {
        "code": 0b01<<6,
        "channels": _channels,
        "dacs": _dacs,
    },
    "M": {
        "code": 0b10<<6,
        "channels": _channels,
        "dacs": _dacs,
    },
    "X1": {
        "code": 0b11<<6,
        "channels": _channels,
        "dacs": _dacs,
    },
}


class SysCtrl:
    def __init__(self, value=0x3fe4a0):
        assert isinstance(value, int) and ((value>>22) == 0) # Mode bits have to zero
        self.value = value

    @property
    def en_clamp0(self):
        return (self.value & (1<<18)) != 0
    @en_clamp0.setter
    def en_clamp0(self, value):
        if value:
            self.value |= (1<<18)
        else:
            self.value &= ~(1<<18)

    @property
    def en_clamp1(self):
        return (self.value & (1<<19)) != 0
    @en_clamp1.setter
    def en_clamp1(self, value):
        if value:
            self.value |= (1<<19)
        else:
            self.value &= ~(1<<19)

    @property
    def en_clamp2(self):
        return (self.value & (1<<20)) != 0
    @en_clamp2.setter
    def en_clamp2(self, value):
        if value:
            self.value |= (1<<20)
        else:
            self.value &= ~(1<<20)

    @property
    def en_clamp3(self):
        return (self.value & (1<<21)) != 0
    @en_clamp3.setter
    def en_clamp3(self, value):
        if value:
            self.value |= (1<<21)
        else:
            self.value &= ~(1<<21)

    @property
    def en_comp(self):
        return (self.value & (1<<13)) != 0
    @en_comp.setter
    def en_comp(self, value):
        if value:
            self.value |= (1<<13)
        else:
            self.value &= ~(1<<13)

    @property
    def en_compout0(self):
        return (self.value & (1<<14)) != 0
    @en_compout0.setter
    def en_compout0(self, value):
        if value:
            self.value |= (1<<14)
        else:
            self.value &= ~(1<<14)

    @property
    def en_compout1(self):
        return (self.value & (1<<15)) != 0
    @en_compout1.setter
    def en_comput1(self, value):
        if value:
            self.value |= (1<<15)
        else:
            self.value &= ~(1<<15)

    @property
    def en_compout2(self):
        return (self.value & (1<<16)) != 0
    @en_compout2.setter
    def en_compout2(self, value):
        if value:
            self.value |= (1<<16)
        else:
            self.value &= ~(1<<16)

    @property
    def en_compout3(self):
        return (self.value & (1<<17)) != 0
    @en_compout3.setter
    def en_compout3(self, value):
        if value:
            self.value |= (1<<17)
        else:
            self.value &= ~(1<<17)

    #TODO: Bit 12

    @property
    def en_guardalarm(self):
        return (self.value & (1<<11)) != 0
    @en_guardalarm.setter
    def en_guardalarm(self, value):
        if value:
            self.value |= (1<<11)
        else:
            self.value &= ~(1<<11)

    @property
    def en_clampalarm(self):
        return (self.value & (1<<10)) != 0
    @en_clampalarm.setter
    def en_clampalarm(self, value):
        if value:
            self.value |= (1<<10)
        else:
            self.value &= ~(1<<10)

    @property
    def en_senseshort10k(self):
        return (self.value & (1<<9)) != 0
    @en_senseshort10k.setter
    def en_senseshort10k(self, value):
        if value:
            self.value |= (1<<9)
        else:
            self.value &= ~(1<<9)

    @property
    def en_guard(self):
        return (self.value & (1<<8)) != 0
    @en_guard.setter
    def en_guard(self, value):
        if value:
            self.value |= (1<<8)
        else:
            self.value &= ~(1<<8)

    @property
    def measgain(self):
        return 1 if (self.value & (1<<7)) == 0 else 0.2
    @measgain.setter
    def measgain(self, value):
        value = round(value, 6)
        if value == 1.0:
            self.value &= ~(1<<7)
        elif value == 0.2:
            self.value |= (1<<7)
        else:
            raise ValueError("measgain value has to be 1.0 or 0.2, not '{}'".format(value))

    @property
    def meascurrentgain(self):
        return 10 if (self.value & (1<<6)) == 0 else 5
    @meascurrentgain.setter
    def meascurrentgain(self, value):
        if value == 10:
            self.value &= ~(1<<6)
        elif value == 5:
            self.value |= (1<<6)
        else:
            raise ValueError("meascurrentgain value has to be 5 or 10, not '{}'".format(value))

    @property
    def en_tempshutdown(self):
        return (self.value & (1<<5)) != 0
    @en_tempshutdown.setter
    def en_tempshutdown(self, value):
        if value:
            self.value |= (1<<5)
        else:
            self.value &= ~(1<<5)

    @property
    def tempthreshold(self):
        t = (self.value & (0b11<<3))>>3
        return ("130℃", "120℃", "110℃", "100℃")[t]
    @tempthreshold.setter
    def tempthreshold(self, value):
        vs = {
            "130℃": 0b00, "130degC": 0b00, "130": 0b00, 130: 0b00,
            "120℃": 0b01, "120degC": 0b01, "120": 0b01, 120: 0b01,
            "110℃": 0b10, "110degC": 0b10, "110": 0b10, 110: 0b00,
            "100℃": 0b11, "100degC": 0b11, "100": 0b11, 100: 0b00,
        }
        try:
            v = vs[value]
        except KeyError:
            raise ValueError(
                "tempthreshold has to be one of {}\n".format(tuple(vs.keys()))
                + "not '{}'".format(value)
            )

    @property
    def en_alarmlatch(self):
        return (self.value & (1<<2)) != 0
    @en_alarmlatch.setter
    def en_alarmlatch(self, value):
        if value:
            self.value |= (1<<2)
        else:
            self.value &= ~(1<<2)


class PMU:
    def __init__(self, *, value=0x1e060):
        assert (value & ((1<<18) | 0b11111)) == 0
        value &= (1<<22) - 1 # on 22 LSB
        self.value = value
    
    def copy(self):
        return PMU(value=self.value)

    @property
    def en_channel(self):
        return (self.value & (1<<21)) != 0
    @en_channel.setter
    def en_channel(self, value):
        if value:
            self.value |= (1<<21)
        else:
            self.value &= ~(1<<21)
    
    @property
    def force(self):
        if (self.value & (1<<12)) != 0:
            vs = ("V", "I", "VZ", "IZ")
            f = (self.value & (0b11<<19))>>19
            return vs[f]
        else:
            return "0I" if (self.value & (1<<19)) != 0 else "0V"
    @force.setter
    def force(self, value):
        vs = {"V": (0b00, 1), "I": (0b01, 1), "0V": (0b00, 0), "0I": (0b01, 0), "VZ": (0b10, 1), "IZ": (0b11, 1)}
        try:
            f, en = vs[value]
        except KeyError:
            raise ValueError(
                "measout has to be one of {}\n".format(tuple(vs.keys()))
                + "not '{}'".format(value)
            )
        else:
            self.value &= ~(0b11<<19) & ~(1<<12)
            self.value |= (f<<19) | (en<<12)

    @property
    def currentrange(self):
        c = (self.value & (0b111<<15))>>15
        return (
            "5µA", "20µA", "200µA", "2mA", "extres", "extres_always_on", "extres_no_always_on"
        )[c]
    @currentrange.setter
    def currentrange(self, value):
        vs = {
            "5µA": 0b000, "5uA": 0b000,
            "20µA": 0b001, "20uA": 0b001,
            "200µA": 0b010, "200uA": 0b010,
            "2mA": 0b011,
            "extres": 0b100,
            "extres_always_on": 0b101,
            "extres_no_always_on": 0b110,
        }
        try:
            v = vs[value]<<15
        except KeyError:
            raise ValueError(
                "currentrange value has to be one of {}\n".format(tuple(vs.keys()))
                + "not '{}'".format(value)
            )
        else:
            self.value = (self.value & ~(0b111<<15)) | v

    @property
    def measout(self):
        m = (self.value & (0b11<<13))>>13
        return ("Isense", "Vsense", "temp", "High-Z")[m]
    @measout.setter
    def measout(self, value):
        vs = {"Isense": 0b00, "Vsense": 0b01, "temp": 0b10, "High-Z": 0b11}
        try:
            v = vs[value]<<13
        except KeyError:
            raise ValueError(
                "measout has to be one of {}\n".format(tuple(vs.keys()))
                + "not '{}'".format(value)
            )
        else:
            self.value &= ~(0b11<<13)
            self.value |= v

    @property
    def en_sysforce(self):
        return (self.value & (1<<11)) != 0
    @en_sysforce.setter
    def en_sysforce(self, value):
        if value:
            self.value |= (1<<11)
        else:
            self.value &= ~(1<<11)
    
    @property
    def en_syssense(self):
        return (self.value & (1<<10)) != 0
    @en_syssense.setter
    def en_syssense(self, value):
        if value:
            self.value |= (1<<10)
        else:
            self.value &= ~(1<<10)
    
    @property
    def en_clamp(self):
        return (self.value & (1<<9)) != 0
    @en_clamp.setter
    def en_clamp(self, value):
        if value:
            self.value |= (1<<9)
        else:
            self.value &= ~(1<<9)
    
    @property
    def en_compout(self):
        return (self.value & (1<<8)) != 0
    @en_compout.setter
    def en_compout(self, value):
        if value:
            self.value |= (1<<8)
        else:
            self.value &= ~(1<<8)
    
    @property
    def iv_comp(self):
        return "V" if (self.value & (1<<7)) != 0 else "I"
    @iv_comp.setter
    def iv_comp(self, value):
        if value == "V":
            self.value |= (1<<7)
        elif value == "I":
            self.value &= ~(1<<7)
        else:
            raise ValueError("iv_comp value has to be 'V' or 'I', not '{}'".format(value))
    
    @property
    def clear(self):
        return (self.value & (1<<6)) != 0
    @clear.setter
    def clear(self, value):
        if value:
            self.value |= (1<<6)
        else:
            self.value &= ~(1<<6)


class AD5522(SpiDev):
    def __init__(self, *, bus, device, vref):
        assert 2.0 <= vref <= 5.0

        super().__init__(bus, device)
        self.mode = 1
        self.max_speed_hz = 1000000

        self.vref = vref
        self._offset = None
        self._sysctrl = None

    def xfer_hex(self, values):
        o = self.xfer(values)
        return "0x" + "".join("{:02x}".format(b) for b in o)

    @staticmethod
    def register_code(*, reg, channel=None, dac=None, iv=None):
        assert reg in _regs.keys()
        regdata = _regs[reg]
        b1 = regdata["code"]
        try:
            channels = regdata["channels"]
        except KeyError:
            assert channel is None
            try:
                b0 = regdata["b0"]
            except KeyError:
                b0 = 0
        else:
            if _isiterable(channel):
                b0 = 0
                for ch in channel:
                    b0 |= channels[ch]
            else:
                b0 = channels[channel]

        try:
            dacs = regdata["dacs"]
        except KeyError:
            assert (dac is None) and (iv is None)
        else:
            dacdata = dacs[dac]
            b1 |= dacdata["code"] | dacdata["ivs"][iv]
            
        return [b0, b1]

    def read_register(self, *, code):
        self.xfer([code[0] | 1<<4, code[1], 0, 0])
        o = self.xfer([0xff, 0xff, 0xff])
        return o[0] << 16 | o[1] << 8 | o[2]

    def read_registers(self):
        ret = {}
        for reg, regdata in _regs.items():
            try:
                channels = regdata["channels"]
            except KeyError:
                code = self.register_code(reg=reg)
                ret[reg] = self.read_register(code=code)
            else:
                try:
                    dacs = regdata["dacs"]
                except KeyError:
                    for channel in range(len(channels)):
                        s = "ch{}.{}".format(channel, reg)
                        code = self.register_code(reg=reg, channel=channel)
                        ret[s] = self.read_register(code=code)
                else:
                    for channel in range(len(channels)):
                        for dac, dacdata in dacs.items():
                            for iv in dacdata["ivs"].keys():
                                s = "ch{}.{}.{}.{}".format(channel, dac, iv, reg)
                                code = self.register_code(reg=reg, channel=channel, dac=dac, iv=iv)
                                ret[s] = self.read_register(code=code)
        
        return ret

    def write_register(self, *, code, value):
        # Only write to one channel at a time, except for sysctrl
        assert (code[0] == 0) or _ispow2(code[0]) or code[1] == 0
        # Value should not clash with code
        assert ((value >> 16) & (code[1])) == 0
        self.xfer([code[0], code[1] | (value >> 16), (value & 0xff00) >> 8, value & 0xff])

    #
        
    def read_pmu(self, *, channel):
        code = self.register_code(reg="pmu", channel=channel)
        return PMU(value=self.read_register(code=code))

    def write_pmu(self, *, channel, pmu):
        assert isinstance(pmu, PMU)
        code = self.register_code(reg="pmu", channel=channel)
        self.write_register(code=code, value=pmu.value)

    #

    def read_dac(self, *, code):
        return self.read_register(code=code) & 0xffff
    
    def write_dac(self, *, code, value):
        assert (value>>16) == 0
        self.write_register(code=code, value=value)

    #

    @property
    def sysctrl(self):
        return self._sysctrl if self._sysctrl is not None else self.read_sysctrl()
    
    def read_sysctrl(self):
        code = self.register_code(reg="sysctrl")
        self._sysctrl = SysCtrl(self.read_register(code=code))
        return self._sysctrl

    def write_sysctrl(self, *, sysctrl):
        assert isinstance(sysctrl, SysCtrl)
        code = self.register_code(reg="sysctrl")
        self.write_register(code=code, value=sysctrl.value)
        self._sysctrl = sysctrl

    #

    @property
    def offset(self):
        return self._offset if self._offset is not None else self.read_offset()
    @property
    def offset_factor(self):
        # output voltage is scaled with 4.5, offset with 3.5
        return -((3.5/4.5)*float(self.offset)/(1<<16))
    
    def read_offset(self):
        code = self.register_code(reg="offset")
        self._offset = self.read_dac(code=code)
        return self._offset
    
    def write_offset(self, value):
        code = self.register_code(reg="offset")
        self.write_dac(code=code, value=value)
        self._offset = value


class MeasMode(Enum):
    # ..MT is for measuring temperature
    FVMI = 0
    FIMV = 1
    FVMV = 2
    FIMI = 3
    FVMT = 4
    FIMT = 5
    ZVMI = 6
    ZIMV = 7
    ZVMV = 8
    ZIMI = 9
    ZVMT = 10
    ZIMT = 11
    HVMI = 12
    HIMV = 13
    HVMV = 14
    HIMI = 15
    HVMT = 16
    HIMT = 17

class Channel:
    def __init__(self, *, smu, channel, extres, gain, set_gain, mode=None, en_meas=None, autoupdate=False):
        assert (
            isinstance(smu, AD5522) and (channel in range(4)) and isinstance(extres, (int, float))
            and isinstance(gain, float) and 0.0 < gain <= 1.0 and isinstance(set_gain, bool)
            and (isinstance(mode, MeasMode) or (mode is None)) and isinstance(autoupdate, bool)
        )
        self.smu = smu
        self.channel = channel
        self.extres = extres
        self.gain = gain
        self.autoupdate = autoupdate
        
        self._pmu = pmu = smu.read_pmu(channel=channel)
        if mode is None:
            assert en_meas is None
            force = pmu.force
            measout = pmu.measout

            self._force_zero = force in ("0V", "0I")
            isv = force in ("V", "0V")
            ishv = force == "VZ"
            isi = force in ("I", "0I")
            ishi = force == "IZ"

            if isv:
                if measout == "Vsense":
                    mode = MeasMode.FVMV
                    en_meas = True
                elif measout == "Isense":
                    mode = MeasMode.FVMI
                    en_meas = True
                elif measout == "temp":
                    mode = MeasMode.FVMT
                    en_meas = True
                elif measout == "High-Z":
                    mode = MeasMode.FVMI # Take this as default
                    en_meas = False
            elif ishv:
                if measout == "Vsense":
                    mode = MeasMode.HVMV
                    en_meas = True
                elif measout == "Isense":
                    mode = MeasMode.HVMI
                    en_meas = True
                elif measout == "temp":
                    mode = MeasMode.HVMT
                    en_meas = True
                elif measout == "High-Z":
                    mode = MeasMode.HVMV # Take this as default
                    en_meas = False
            elif isi:
                if measout == "Vsense":
                    mode = MeasMode.FIMV
                    en_meas = True
                elif measout == "Isense":
                    mode = MeasMode.FIMI
                    en_meas = True
                elif measout == "temp":
                    mode = MeasMode.FIMT
                    en_meas = True
                elif measout == "High-Z":
                    mode = MeasMode.FIMV # Take this as default
                    en_meas = False
            elif ishi:
                if measout == "Vsense":
                    mode = MeasMode.HIMV
                    en_meas = True
                elif measout == "Isense":
                    mode = MeasMode.HIMI
                    en_meas = True
                elif measout == "temp":
                    mode = MeasMode.HIMT
                    en_meas = True
                elif measout == "High-Z":
                    mode = MeasMode.HIMI # Take this as default
                    en_meas = False
            else:
                raise AssertionError("Internal errior")
        else:
            if mode in (MeasMode.FVMI, MeasMode.FVMV, MeasMode.FVMT):
                pmu.force = "V"
            elif mode in (MeasMode.FIMI, MeasMode.FIMV, MeasMode.FIMT):
                pmu.force = "I"
            elif mode in (MeasMode.ZVMI, MeasMode.ZVMV, MeasMode.ZVMT):
                pmu.force = "0V"
            elif mode in (MeasMode.ZIMI, MeasMode.ZIMV, MeasMode.ZIMT):
                pmu.force = "0I"
            else:
                raise AssertionError("Internal error")
            
            if en_meas is None:
                en_meas = False
            if not en_meas:
                pmu.measout = "High-Z"
            elif mode in (MeasMode.FVMV, MeasMode.FIMV, MeasMode.ZVMV, MeasMode.ZIMV):
                pmu.measout = "Vsense"
            elif mode in (MeasMode.FVMI, MeasMode.FIMI, MeasMode.ZVMI, MeasMode.ZIMI):
                pmu.measout = "Isense"
            elif mode in (MeasMode.FVMT, MeasMode.FIMT, MeasMode.ZVMT, MeasMode.ZIMT):
                pmu.measout = "temp"
            else:
                raise AssertionError("Internal error")
            
            if autoupdate:
                self.update()

        if set_gain:
            gain_value = round(0xFFFF*gain)
            for dac in ("fin", "cph", "cpl"):
                for iv in ("V", "5µA", "20µA", "200µA", "2mA", "extA"):
                    code = smu.register_code(reg="C", channel=channel, dac=dac, iv=iv)
                    smu.write_dac(code=code, value=gain_value)
            for dac in ("clh", "cll"):
                for iv in ("V", "I"):
                    code = smu.register_code(reg="C", channel=channel, dac=dac, iv=iv)
                    smu.write_dac(code=code, value=gain_value)

        ivs = ("V", "5µA", "20µA", "200µA", "2mA", "extA")
        self._forceerrors = {iv: 0.0 for iv in ivs}
        self._gainerrors = {iv: 1.0 for iv in ivs}
        self._measerrors = {measiv: 0.0 for measiv in product(("FV", "FI"), ivs)}
        self._mode = mode
        self._en_meas = en_meas
                
    def update(self):
        self.smu.write_pmu(channel=self.channel, pmu=self._pmu)

    @property
    def _iv(self):
        if self.mode in (
            MeasMode.FVMV, MeasMode.FVMI, MeasMode.FVMT,
            MeasMode.ZVMV, MeasMode.ZVMI, MeasMode.ZVMT,
            MeasMode.HVMV, MeasMode.HVMI, MeasMode.HVMT,
        ):
            return "V"
        elif self.mode in (
            MeasMode.FIMV, MeasMode.FIMI, MeasMode.FIMT,
            MeasMode.ZIMV, MeasMode.ZIMI, MeasMode.ZIMT,
            MeasMode.HIMV, MeasMode.HIMI, MeasMode.HIMT,
        ):
            iv = self.pmu.currentrange
            if iv.startswith("extres"):
                iv = "extA"
            return iv
        else:
            raise AssertionError("Internal error")
    @property
    def _ivmeas(self):
        if self.mode in (MeasMode.FVMT, MeasMode.ZVMT, MeasMode.HVMT, MeasMode.FIMT, MeasMode.ZIMT, MeasMode.HIMT):
            raise ValueError("temperature measurement does not have iv index for measerror")

        
        if self.mode in (
            MeasMode.FVMV, MeasMode.FVMI, MeasMode.FVMT,
            MeasMode.ZVMV, MeasMode.ZVMI, MeasMode.ZVMT,
            MeasMode.HVMV, MeasMode.HVMI, MeasMode.HVMT,
        ):
            f = "FV"
        elif self.mode in (
            MeasMode.FIMV, MeasMode.FIMI, MeasMode.FIMT,
            MeasMode.ZIMV, MeasMode.ZIMI, MeasMode.ZIMT,
            MeasMode.HIMV, MeasMode.HIMI, MeasMode.HIMT,
        ):
            f = "FI"
        else:
            raise AssertionError("Internal error")
            
        if self.mode in (MeasMode.FVMV, MeasMode.FIMV, MeasMode.ZVMV, MeasMode.ZIMV, MeasMode.HVMV, MeasMode.HIMV):
            m = "V"
        elif self.mode in (MeasMode.FVMI, MeasMode.FIMI, MeasMode.ZVMI, MeasMode.ZIMI, MeasMode.HVMI, MeasMode.HIMI):
            m = self.pmu.currentrange
            if m.startswith("extres"):
                m = "extA"
        else:
            raise AssertionError("Internal error")

        return (f, m)

    @property
    def pmu(self):
        return self._pmu.copy()
    @pmu.setter
    def pmu(self, pmu):
        assert isinstance(pmu, PMU)
        self._pmu = pmu
        if self.autoupdate:
            self.update()

    @property
    def en(self):
        return self.pmu.en_channel
    @en.setter
    def en(self, en):
        en = bool(en)
        pmu = self.pmu
        if en != pmu.en_channel:
            pmu.en_channel = en
            self.pmu = pmu

    @property
    def mode(self):
        return self._mode
    @mode.setter
    def mode(self, mode):
        assert isinstance(mode, MeasMode)
        update = False

        pmu = self.pmu
        
        oldforce = pmu.force
        if mode in (MeasMode.FVMI, MeasMode.FVMV, MeasMode.FVMT):
            newforce = "V"
        elif mode in (MeasMode.FIMI, MeasMode.FIMV, MeasMode.FIMT):
            newforce = "I"
        elif mode in (MeasMode.ZVMI, MeasMode.ZVMV, MeasMode.ZVMT):
            newforce = "0V"
        elif mode in (MeasMode.ZIMI, MeasMode.ZIMV, MeasMode.ZIMT):
            newforce = "0I"
        elif mode in (MeasMode.HVMI, MeasMode.HVMV, MeasMode.HVMT):
            newforce = "VZ"
        elif mode in (MeasMode.HIMI, MeasMode.HIMV, MeasMode.HIMT):
            newforce = "IZ"
        else:
            raise AssertionError("Internal error")
        if newforce != oldforce:
            pmu.force = newforce
            update = True

        if self.en_meas:
            oldmeasout = pmu.measout
            if mode in (MeasMode.FVMV, MeasMode.FIMV, MeasMode.ZVMV, MeasMode.ZIMV, MeasMode.HVMV, MeasMode.HIMV):
                newmeasout = "Vsense"
            elif mode in (MeasMode.FVMI, MeasMode.FIMI, MeasMode.ZVMI, MeasMode.ZIMI, MeasMode.HVMI, MeasMode.HIMI):
                newmeasout = "Isense"
            elif mode in (MeasMode.FVMT, MeasMode.FIMT, MeasMode.ZVMT, MeasMode.ZIMT, MeasMode.HVMT, MeasMode.HIMT):
                newmeasout = "temp"
            else:
                raise AssertionError("Internal error")
            if newmeasout != oldmeasout:
                pmu.measout = newmeasout
                update = True

        if update:
            self.pmu = pmu

        self._mode = mode

    @property
    def en_meas(self):
        return self._en_meas
    @en_meas.setter
    def en_meas(self, en_meas):
        if en_meas != self._en_meas:
            self._en_meas = en_meas
            mode = self.mode
            pmu = self.pmu
            if not en_meas:
                pmu.measout = "High-Z"
            elif mode in (MeasMode.FVMV, MeasMode.FIMV, MeasMode.ZVMV, MeasMode.ZIMV, MeasMode.HVMV, MeasMode.HIMV):
                pmu.measout = "Vsense"
            elif mode in (MeasMode.FVMI, MeasMode.FIMI, MeasMode.ZVMI, MeasMode.ZIMI, MeasMode.HVMI, MeasMode.HIMI):
                pmu.measout = "Isense"
            elif mode in (MeasMode.FVMT, MeasMode.FIMT, MeasMode.ZVMT, MeasMode.ZIMT, MeasMode.HVMT, MeasMode.HIMT):
                pmu.measout = "temp"
            else:
                raise AssertionError("Internal error")
            self.pmu = pmu

    @property
    def currentrange(self):
        return self.pmu.currentrange
    @currentrange.setter
    def currentrange(self, irange):
        pmu = self.pmu
        pmu.currentrange = irange
        self.pmu = pmu

    @property
    def currentres(self):
        vs = {
            "5µA": 200000.0,
            "20µA": 50000.0,
            "200µA": 5000.0,
            "2mA": 500.0,
            "extres": self.extres,
            "extres_always_on": self.extres,
            "extres_no_always_on":self.extres,
        }
        return vs[self.pmu.currentrange]

    @property
    def force(self):
        smu = self.smu
        if self.mode in (MeasMode.FVMV, MeasMode.FVMI, MeasMode.FVMT):
            code = smu.register_code(reg="X1", dac="fin", channel=self.channel, iv="V")
            dac_code = smu.read_dac(code=code)
            return round(self.gain*4.5*smu.vref*(dac_code/(1<<16) + smu.offset_factor), 4)
        elif self.mode in (MeasMode.FIMV, MeasMode.FIMI, MeasMode.FIMT):
            code = smu.register_code(reg="X1", dac="fin", channel=self.channel, iv=self._iv)
            dac_code = smu.read_dac(code=code)
            r = self.currentres
            digits = ceil(log10(r)) + 5
            return round(self.gain*4.5*smu.vref*(dac_code/(1<<16) - 0.5)/(smu.sysctrl.meascurrentgain*r), digits)
        elif self.mode in (
            MeasMode.ZVMV, MeasMode.ZVMI, MeasMode.ZVMT,
            MeasMode.ZIMV, MeasMode.ZIMI, MeasMode.ZIMT,
        ):
            return 0.0
        elif self.mode in (
            MeasMode.HVMV, MeasMode.HVMI, MeasMode.HVMT,
            MeasMode.HIMV, MeasMode.HIMI, MeasMode.HIMT,
        ):
            return nan
        else:
            raise AssertionError("Internal error")
    @force.setter
    def force(self, force):
        smu = self.smu
        if self.mode in (
            MeasMode.FVMV, MeasMode.FVMI, MeasMode.FVMT,
            MeasMode.ZVMV, MeasMode.ZVMI, MeasMode.ZVMT,
            MeasMode.HVMV, MeasMode.HVMI, MeasMode.HVMT,
        ):
            code = smu.register_code(reg="X1", dac="fin", channel=self.channel, iv="V")
            if isinstance(force, str):
                assert force in ("min", "max")
                dac_code = 0 if force == "min" else (1<<16) - 1
            else:
                dac_code = int(round((1<<16)*(force/(self.gain*4.5*smu.vref) - self.smu.offset_factor)))
                if not (0 <= dac_code < 1<<16):
                    raise ValueError("Force value {} out of range".format(force))
        elif self.mode in (
            MeasMode.FIMV, MeasMode.FIMI, MeasMode.FIMT,
            MeasMode.ZIMV, MeasMode.ZIMI, MeasMode.ZIMT,
            MeasMode.HIMV, MeasMode.HIMI, MeasMode.HIMT,
        ):
            code = smu.register_code(reg="X1", dac="fin", channel=self.channel, iv=self._iv)
            if isinstance(force, str):
                assert force in ("min", "max")
                dac_code = 0 if force == "min" else (1<<16) - 1
            else:
                r = self.currentres
                dac_code = int(round((1<<16)*(force*smu.sysctrl.meascurrentgain*r/(self.gain*4.5*smu.vref) + 0.5)))
                if not (0 <= dac_code < 1<<16):
                    raise ValueError("Force value {} out of range".format(force))
        else:
            raise AssertionError("Internal error")
        smu.write_dac(code=code, value=dac_code)
    @property
    def forcelsb(self):
        smu = self.smu
        if self.mode in (
            MeasMode.FVMV, MeasMode.FVMI, MeasMode.FVMT,
            MeasMode.ZVMV, MeasMode.ZVMI, MeasMode.ZVMT,
            MeasMode.HVMV, MeasMode.HVMI, MeasMode.HVMT,
        ):
            return self.gain*4.5*smu.vref/(1<<16)
        elif self.mode in (
            MeasMode.FIMV, MeasMode.FIMI, MeasMode.FIMT,
            MeasMode.ZIMV, MeasMode.ZIMI, MeasMode.ZIMT,
            MeasMode.HIMV, MeasMode.HIMI, MeasMode.HIMT,
        ):
            return self.gain*4.5*smu.vref/((1<<16)*smu.sysctrl.meascurrentgain*self.currentres)
        else:
            raise AssertionError("Internal error")
    @property
    def forceerror(self):
        return self._forceerrors[self._iv]
    @forceerror.setter
    def forceerror(self, forceerror):
        smu = self.smu
        iv = self._iv
        value = 0x8000 + round(forceerror/self.forcelsb)
        for dac in ("fin", "cpl", "cph"):
            code = smu.register_code(reg="M", channel=self.channel, dac=dac, iv=iv)
            smu.write_dac(code=code, value=value)
        self._forceerrors[iv] = forceerror

    @property
    def gainerror(self):
        return self._gainerrors[self._iv]
    @gainerror.setter
    def gainerror(self, gainerror):
        smu = self.smu
        iv = self._iv
        value = round(0xFFFF*self.gain/gainerror)
        for dac in ("fin", "cpl", "cph"):
            code = smu.register_code(reg="C", channel=self.channel, dac=dac, iv=iv)
            smu.write_dac(code=code, value=value)
        self._gainerrors[iv] = gainerror

    @property
    def measerror(self):
        return self._measerrors[self._ivmeas]
    @measerror.setter
    def measerror(self, measerror):
        self._measerrors[self._ivmeas] = measerror
    def convert_measuredratio(self, ratio):
        smu = self.smu
        assert isinstance(ratio, float)

        if not self.en_meas:
            return None
        assert smu.sysctrl.measgain == 0.2, "Unsupported configuration"
        mode = self.mode
        if mode in (MeasMode.FVMV, MeasMode.FIMV, MeasMode.ZVMV, MeasMode.ZIMV, MeasMode.HVMV, MeasMode.HIMV):
            unit = "V"
            value = round(smu.vref*(5*ratio + 4.5*self.smu.offset_factor) - self.measerror, 4) 
        elif mode in (MeasMode.FVMI, MeasMode.FIMI, MeasMode.ZVMI, MeasMode.ZIMI, MeasMode.HVMI, MeasMode.HIMI):
            unit = "I"
            r = self.currentres
            digits = ceil(log10(r)) + 5
            value = round(5*smu.vref*(ratio - 0.45)/(smu.sysctrl.meascurrentgain*r) - self.measerror, digits)
        else:
            raise NotImplementedError("mode {} not supported".format(mode))

        return (value, unit)
