from spidev import SpiDev


__all__ = ["AD7685"]


class AD7685(SpiDev):
    def __init__(self, *, bus, device):
        super().__init__(bus, device)
        
        self.mode = 1
        self.max_speed_hz = 1000000
    
    # The end of a read_value will trigger a new data capture, so value will be old
    # if previous capture has been some time ago.
    def read_value(self):
        o = self.readbytes(2)
        return (o[0]<<8) | o[1]
    
    # Same as read_value but without conversion to integer to be as fast as possible
    def read_raw(self):
        return self.readbytes(2)