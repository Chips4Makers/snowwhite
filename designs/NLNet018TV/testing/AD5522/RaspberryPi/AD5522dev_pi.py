from math import ceil, log10
from time import sleep, time

from AD5522_pi import SysCtrl, PMU, AD5522, MeasMode, Channel
from AD7685_pi import AD7685

class AD5522dev:
    """Support class for AD5522 development board
    
    It assumes that all meas outputs are connected to the ADC input. It will make sure only one
    of the measurement outputs will be active.
    """
    GLOBAL_GAIN = 0.99

    def __init__(self, *, smu_bus=0, smu_device=0, adc_bus=0, adc_device=1, vref, calibrated=True):
        self.smu = smu = AD5522(bus=smu_bus, device=smu_device, vref=vref)
        self.adc = AD7685(bus=adc_bus, device=adc_device)

        self.channels = tuple(
            Channel(smu=smu, channel=channel, extres=100, gain=self.GLOBAL_GAIN, set_gain=True)
            for channel in range(4)
        )
        self.default(calibrated=calibrated)

        for channel in self.channels:
            channel.en_meas = False
            channel.en = True

    def default(self, *, calibrated):
        """Reset dev board to default state.
        
        Default state is all channels on in FVMI mode with measurement output in High-Z
        """
        # Initialize SMU to default state
        sysctrl = SysCtrl()
        sysctrl.en_comp = True
        sysctrl.en_guardalarm = True
        sysctrl.en_clampalarm = True
        sysctrl.en_senseshort10k = False
        sysctrl.en_guard = False
        sysctrl.measgain = 0.2
        sysctrl.meascurrentgain = 5
        sysctrl.en_tempshutdown = True
        sysctrl.tempthreshold = 100
        sysctrl.en_alarmlatch = False
        self.smu.write_sysctrl(sysctrl=sysctrl)

        # Use hard coded calibration data
        currentranges = ("5µA", "20µA", "200µA", "2mA", "extres")
        forcecaldata = (
            # Channel 0
            {
                "voltagegainerror": 0.99995,
                "voltageforceerror": 347,
                "currentgainerror": {
                    "5µA": 0.99996, "20µA": 0.99997, "200µA": 0.99999, "2mA": 0.99997, "extres": 0.99998,
                },
                "currentforceerror": 312,
            },
            # Channel 1
            {
                "voltagegainerror": 1.00040,
                "voltageforceerror": 315,
                "currentgainerror": {
                    "5µA": 0.99979, "20µA": 0.99982, "200µA": 0.99978, "2mA": 0.99979, "extres": 0.99979,
                },
                "currentforceerror": 277,
            },
            # Channel 2
            {
                "voltagegainerror": 1.00082,
                "voltageforceerror": 338,
                "currentgainerror": {
                    "5µA": 0.99969, "20µA": 0.99968, "200µA": 0.99970, "2mA": 0.99969, "extres": 0.99970,
                },
                "currentforceerror": 277,
            },
            # Channel 3
            {
                "voltagegainerror": 1.00048,
                "voltageforceerror": 315,
                "currentgainerror": {
                    "5µA": 0.99992, "20µA": 0.99991, "200µA": 0.99993, "2mA": 0.99990, "extres": 0.99989,
                },
                "currentforceerror": 260,
            },
        )
        meascaldata = (
            # Channel 0
            {
                "FV": {
                    "voltagemeaserror": 0.0220,
                    "currentmeaserror": {
                        "5µA": 9.53e-9, "20µA": 3.97e-8, "200µA": 3.97e-7, "2mA": 3.72e-6, "extres": 1.83e-5,
                    }
                },
                "FI": {
                    "voltagemeaserror": 0.0218,
                    "currentmeaserror": {
                        "5µA": 9.68e-9, "20µA": 3.87e-8, "200µA": 3.47e-7, "2mA": 3.86e-6, "extres": 1.94e-5,
                    }
                },
            },
            # Channel 1
            {
                "FV": {
                    "voltagemeaserror": 0.0225,
                    "currentmeaserror": {
                        "5µA": 16.24e-9, "20µA": 6.51e-8, "200µA": 6.36e-7, "2mA": 6.11e-6, "extres": 3.05e-5,
                    }
                },
                "FI": {
                    "voltagemeaserror": 0.0227,
                    "currentmeaserror": {
                        "5µA": 16.21e-9, "20µA": 6.34e-8, "200µA": 6.48e-7, "2mA": 6.49e-6, "extres": 3.24e-5,
                    }
                },
            },
            # Channel 2
            {
                "FV": {
                    "voltagemeaserror": 0.0182,
                    "currentmeaserror": {
                        "5µA": 9.98e-9, "20µA": 4.24e-8, "200µA": 4.26e-7, "2mA": 3.87e-6, "extres": 1.88e-5,
                    }
                },
                "FI": {
                    "voltagemeaserror": 0.0182,
                    "currentmeaserror": {
                        "5µA": 9.68e-9, "20µA": 3.87e-8, "200µA": 3.90e-7, "2mA": 3.88e-6, "extres": 1.95e-5,
                    }
                },
            },
            # Channel 3
            {
                "FV": {
                    "voltagemeaserror": 0.0204,
                    "currentmeaserror": {
                        "5µA": 7.66e-9, "20µA": 3.13e-8, "200µA": 3.06e-7, "2mA": 2.80e-6, "extres": 1.39e-5,
                    }
                },
                "FI": {
                    "voltagemeaserror": 0.0205,
                    "currentmeaserror": {
                        "5µA": 7.08e-9, "20µA": 2.83e-8, "200µA": 2.68e-7, "2mA": 2.84e-6, "extres": 1.49e-5,
                    }
                },
            },
        )

        for channel, ch in enumerate(self.channels):
            ch.autoupdate = False
            ch.en = True
            ch.en_meas = False
            pmu = ch.pmu
            pmu.en_sysforce = False
            pmu.en_syssense = False
            pmu.en_clamp = True
            pmu.en_compout = False
            pmu.iv_comp = "I"
            pmu.clear = True
            ch.autoupdate = True
            ch.pmu = pmu

            chforcecal = forcecaldata[channel]
            chmeascal = meascaldata[channel]
            
            ch.mode = MeasMode.HVMV
            if calibrated:
                ch.forceerror = chforcecal["voltageforceerror"]*ch.forcelsb
                ch.gainerror = chforcecal["voltagegainerror"]
            else:
                ch.forcerror = 0.0
                ch.gainerror = 1.0
            for currentrange in currentranges:
                ch.currentrange = currentrange
                if calibrated:
                    ch.measerror = chmeascal["FV"]["voltagemeaserror"]
                else:
                    ch.measerror = 0.0
                
            ch.mode = MeasMode.HVMI
            for currentrange in currentranges:
                ch.currentrange = currentrange
                if calibrated:
                    ch.measerror = chmeascal["FV"]["currentmeaserror"][currentrange]
                else:
                    ch.measerror = 0.0

            ch.mode = MeasMode.HIMV
            for currentrange in currentranges:
                ch.currentrange = currentrange
                ch.mode = MeasMode.HIMV
                if calibrated:
                    ch.forceerror = chforcecal["currentforceerror"]*ch.forcelsb
                    ch.gainerror = chforcecal["currentgainerror"][currentrange]
                else:
                    ch.forceerror = 0.0
                    ch.gainerror = 1.0
            for currentrange in currentranges:
                ch.currentrange = currentrange
                if calibrated:
                    ch.measerror = chmeascal["FI"]["voltagemeaserror"]
                else:
                    ch.measerror = 0.0
                
            ch.mode = MeasMode.HIMI
            for currentrange in currentranges:
                ch.currentrange = currentrange
                if calibrated:
                    ch.measerror = chmeascal["FI"]["currentmeaserror"][currentrange]
                else:
                    ch.measerror = 0.0
            
            # Set default mode
            ch.currentrange = "2mA"
            ch.mode = MeasMode.HVMV
        self._measchannel = None
        
    #
    
    @property
    def sysctrl(self):
        return self.smu.sysctrl

    #
    
    @property
    def measchannel(self):
        return self._measchannel
    
    @measchannel.setter
    def measchannel(self, channel):
        assert (channel is None) or (channel in range(4))
        oldchannel = self._measchannel
        if oldchannel != channel:
            if oldchannel is not None:
                self.channels[oldchannel].en_meas = False
            if channel is not None:
                self.channels[channel].en_meas = True
            self._measchannel = channel

    #
    
    def measure(self, *, count=1, dummyread=True, average=True, addtime=False):
        assert not (average and addtime), "Adding time for average not implemented"
        adc = self.adc
        smu = self.smu

        if self._measchannel is None:
            return None
        else:
            if dummyread:
                adc.read_value() # Trigger capture
            
            # First try to read all values, do processing afterwards
            # TODO: Move core measurement code to C for faster sampling
            times = []
            values = []
            f = adc.read_raw
            for _ in range(count):
                # We assume that adding the time will take long enough for the ADC conversion.
                times.append(time())
                values.append(f())
            times.append(time())
            values = tuple((value[0]<<8) | value[1] for value in values)

            ch = self.channels[self._measchannel]
            if average:
                ratio = sum(values)/((1<<16)*count)
                return ch.convert_measuredratio(ratio)
            else:
                if not addtime:
                    return tuple(
                        ch.convert_measuredratio(value/(1<<16))
                        for value in values
                    )
                else: # addtime
                    return tuple(
                        (*ch.convert_measuredratio(values[i]/(1<<16)), 0.5*(times[i] + times[i + 1]))
                        for i in range(count)
                    )

    def measure_fast_time(self, *, count=1):
        # Fast measurement function with minimized checking
        times = []
        values = []
        f = self.adc.read_raw
        f() # Dummy read to sample first value
        for _ in range(count):
            times.append(time())
            values.append(f())
        times.append(time())

        assert self._measchannel is not None
        ch = self.channels[self._measchannel]
        return tuple(
            (*ch.convert_measuredratio(((values[i][0]<<8) | values[i][1])/(1<<16)), times[i])
            for i in range(count)
        )

    #
    
    def calibrate_voltageoffset(self, *, channel, debug=False):
        """Calibre offset for voltage
        
        The channel has to be shorted during calibration.
        The offset value is influenced by the gain setting so best to do gain calibration
        before offset calibration.
        
        Calibration of the voltage offset will reset the measurement offset.
        """
        smu = self.smu
        
        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        ch.mode = MeasMode.FVMI
        oldrange = ch.currentrange
        oldforce = ch.force
        ch.currentrange = "5µA"

        # Reset error
        ch.forceerror = 0.0

        lsb = ch.forcelsb
        clow = -10e-6
        chigh = 10e-6
        def _setandmeasure(n):
            ch.force = n*lsb
            sleep(0.3)
            c = self.measure(count=2)[0]
            i = 0 # Allow maximum 2.4 seconds (12*0.2) for settling
            while (clow < c < chigh) and (i < 12):
                sleep(0.2)
                c = self.measure(count=2)[0]
                i += 1
            return c
        
        b = 9
        c = _setandmeasure(0)
        if c > 0:
            left = -(1<<b)
            print(c, _setandmeasure(left))
            assert _setandmeasure(left) < 0
        else:
            left = 0
            assert _setandmeasure(1<<b) > 0
        if debug:
            print(b, left, c)

        while b > 0:
            b -= 1
            mid = left + (1<<b)
            c = _setandmeasure(mid)
            if debug:
                print(b, left, mid, c)
            if c < clow:
                left = mid
            elif c < chigh:
                # Not an extreme value
                if b > 1:
                    left = mid - (1<<(b-1)) + 1
                    b -= 1

        ch.forceerror = left*lsb

        # Verify setting
        def verify():
            ch.force = -5*lsb
            sleep(0.2)
            c1 = self.measure(count=10)[0]
            if c1 > clow:
                if debug:
                    print("c1 first", c1)
                sleep(0.8)
                c1 = self.measure(count=10)[0]
            ch.force = 5*lsb
            sleep(0.1)
            c2 = self.measure(count=10)[0]
            if c2 < chigh:
                if debug:
                    print("c2 first", c2)
                sleep(0.9)
                c2 = self.measure(count=10)[0]

            if debug:
                print("verify", c1, c2)

            return (c1 < clow) and (c2 > chigh)

        ok = verify()

        ch.force = oldforce
        ch.currentrange = oldrange
        ch.mode = oldmode
        self.measchannel = oldchannel
        
        return (left, ok)
        
    def calibrate_currentoffset(self, *, channel, debug=False):
        """Calibre offset for current
        
        The channel has to be open during the calibration.
        
        It is assumed that the offset is the same for all the current ranges.
        The offset is calibrated for 5µA current range and then applied to all
        channels including the current clamps.
        It is verified that for all current ranges the offset in between -5*lsb to 5*lsb
        The offset value is influenced by the gain setting so best to do gain calibration
        before offset calibration.
        
        Calibration of the current offset will reset the measurement offset.
        """
        smu = self.smu

        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        ch.mode = MeasMode.FIMV
        oldrange = ch.currentrange
        ch.currentrange = "20µA"

        # Reset error
        ch.forceerror = 0.0

        lsb = ch.forcelsb
        def _setandmeasure(n):
            ch.force = n*lsb
            sleep(0.5)
            v = self.measure(count=2)[0]
            i = 0 # Allow maxumum 2.4 seconds (12*0.2) for settling
            while (-11 < v < 11) and (i < 12):
                sleep(0.2)
                v = self.measure(count=2)[0]
                i += 1
            return v
        
        b = 9
        v = _setandmeasure(0)
        if v > 0:
            left = -(1<<b)
            v2 = _setandmeasure(left)
            if debug:
                print(b, left, v, v2)
            assert v2 < 0
        else:
            left = 0
            v2 = _setandmeasure(1<<b)
            if debug:
                print(b, left, v, v2)
            assert v2 > 0

        while b > 0:
            b -= 1
            mid = left + (1<<b)
            v = _setandmeasure(mid)
            if debug:
                print(b, left, mid, v)
            if v < -11:
                left = mid
            elif v < 11:
                # Not an extreme value
                if b > 1:
                    left = mid - (1<<(b-1)) + 1
                    b -= 1

        # Verify support function
        def verify(irange):
            ch.currentrange = irange
            lsb = ch.forcelsb
            
            ch.force = -5*lsb
            sleep(0.2)
            v1 = self.measure(count=10)[0]
            if v1 > -11:
                sleep(0.8)
                v1 = self.measure(count=10)[0]
            ch.force = 5*lsb
            sleep(0.1)
            v2 = self.measure(count=10)[0]
            if v2 < 11:
                sleep(0.9)
                v2 = self.measure(count=10)[0]
            ch.force = 0.0

            if debug:
                print(irange, v1, v2)

            return (v1 < -10) and (v2 > 10)

        ok = []
        # Apply and verify
        for irange in ("5µA", "20µA", "200µA", "2mA", "extres"):
            ch.currentrange = irange
            ch.measerror = 0.0
            ch.forceerror = left*ch.forcelsb
            ok.append((irange, verify(irange)))

        ch.mode = oldmode
        ch.currentrange = oldrange
        self.measchannel = oldchannel
        
        return (left, ok)

    def calibrate_voltagegain(self, *, channel, debug=False):
        """Calibre the voltage gain for a channel.
        
        The channel has to have a resistive load high enough so extreme voltages can be reached
        without clamping. It may be open.
        """
        smu = self.smu

        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        ch.mode = MeasMode.FVMV

        # Reset gain
        ch.gainerror = 1.0

        oldforce = ch.force
        
        ch.force = "min"
        v1 = ch.force
        sleep(0.1)
        meas1 = self.measure(count=10)[0]
        ch.force = "max"
        v2 = ch.force
        sleep(0.1)
        meas2 = self.measure(count=10)[0]
        
        ch.force = oldforce
        
        gainerror = (meas2 - meas1)/(v2 - v1)
        offset = 0.5*(meas1 + meas2)
        if debug:
            print(v1, meas1, v2, meas2, gainerror, 0.5*(meas1 + meas2)/ch.forcelsb)
        ch.gainerror = gainerror
        
        ch.mode = oldmode
        self.measchannel = oldchannel
        
        return (gainerror, offset)

    def calibrate_currentgain(self, *, channel, currentrange, debug=False):
        """Calibrate the current gain for a channel and current range.
        
        The channel has to have a resistive load small enough so extreme currents can be reached
        without clamping. It may be shorted.
        """
        smu = self.smu

        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        ch.mode = MeasMode.FIMI
        oldrange = ch.currentrange
        ch.currentrange = currentrange
        oldforce = ch.force

        # Reset gain
        ch.gainerror = 1.0

        
        ch.force = "min"
        c1 = ch.force
        sleep(0.1)
        meas1 = self.measure(count=10)[0]
        ch.force = "max"
        c2 = ch.force
        sleep(0.1)
        meas2 = self.measure(count=10)[0]
        
        
        gainerror = (meas2 - meas1)/(c2 - c1)
        offset = 0.5*(meas1 + meas2)
        if debug:
            print(c1, meas1, c2, meas2, gainerror, 0.5*offset/ch.forcelsb)

        ch.gainerror = gainerror

        ch.force = oldforce
        ch.currentrange = oldrange
        ch.mode = oldmode
        self.measchannel = oldchannel
        
        return (gainerror, offset)

    def calibrate_measoffset_fv(self, *, channel, currentrange, debug=False):
        """Calibrate the measurement offset with forced voltage; it calibrates both the
        measure current and voltage offset.
        
        Best is to open the channel so no current can flow for the measuremnt current offset
        calibration.
        This should normally be done after the force voltage offset has been calibrated.
        """
        #
        # Setup
        #
        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        oldrange = ch.currentrange
        ch.currentrange = currentrange
        
        #
        # Voltage measurement
        #
        ch.mode = MeasMode.FVMV
        oldforce = ch.force
        ch.force = 0
        
        # Reset offset
        ch.measerror = 0.0
        
        voffset = self.measure(count=2500)[0]
        ch.measerror = voffset
        ch.force = oldforce

        #
        # Current measurement
        #
        ch.mode = MeasMode.FVMI
        
        # Reset offset
        ch.measerror = 0.0
        
        ioffset = self.measure(count=2500)[0]
        ch.measerror = ioffset

        if debug:
            print("V offset:", voffset, "I offset:", ioffset)

        #
        # Clean up
        #
        ch.force = oldforce
        ch.currentrange = oldrange
        ch.mode = oldmode
        self.measchannel = oldchannel
        
        return (voffset, ioffset)

    def calibrate_measoffset_fi(self, *, channel, currentrange, debug=False):
        """Calibrate the voltage measurement offset.
        
        The channel be shorted during this calibration.
        This should normally be done after the force current offset has been calibrated.
        """
        #
        # Setup
        #
        oldchannel = self.measchannel
        self.measchannel = channel
        ch = self.channels[channel]
        oldmode = ch.mode
        oldrange = ch.currentrange
        ch.currentrange = currentrange

        #
        # Measure voltage
        #
        ch.mode = MeasMode.FIMV
        oldforce = ch.force
        ch.force = 0
        
        # Reset offset
        ch.measerror = 0.0
        
        voffset = self.measure(count=2500)[0]
        ch.measerror = voffset

        #
        # Measure current
        #
        ch.mode = MeasMode.FIMI
        
        # Reset offset
        ch.measerror = 0.0
        
        ioffset = self.measure(count=2500)[0]
        ch.measerror = ioffset

        #
        # Cleanup
        #
        ch.force = oldforce
        ch.mode = oldmode
        self.measchannel = oldchannel

        return (voffset, ioffset)
