# Test Plan

This document give a list of metric to be measured on the test chip. Using the [test chip description](../README.md) a test procedure needs to be made. This test procedure then needs to be executed and a report made on the test results.

## IO cells

* verify functionality of GND, VDDCORE and VDDIO cells
* verify level shifting on the PAD input of the IO cell, determine switching threshold. Compare results with simulation.
* measure drive strength of each of IO drivers, together with V_oh and V_ol
  * using analog test inputs
  * using output of one IO cell as driver enabler

  compare results with simulation
* measure [human-body model](https://en.wikipedia.org/wiki/Human-body_model) (HBM) ESD performance of the cells

## SRAM cell

* measure static noise margin (SNM) on the SRAM cells both in hold and read mode; e.g. can the cell be read without destroying it's content.
* measure write trip point (WTP) of the cell; e.g. can the cell be written to.
* measure the read current of the cell; e.g. what speed can we expect.
* compare results with simulation.

## SR latches

* verify logical functionality of the SR latches on the design