import os
from datetime import date

import pya
from TSMC_C018 import tech_info
from C4MLib import CellAdder, get_shapes

def u(dim):
    return long(1000L*dim)

class Europractice_CPGA84:
    pins = 84
    pinsperside = pins//4
    cavity_center = pya.Point(u(11045), u(18975))
    cavity_width = u(9130)
    cavity_corner_offset = u(450)
    pinpitch = (cavity_width - 2*cavity_corner_offset)//(pinsperside - 1)

    def __init__(self,
        layout, package_gdsfile,
        design_gdsfile, design_topcell,
        design_pins, design_pad_corner_offset, design_pad_edge_offset,
        design_rotate,
        tech=None,
    ):
        self.layout = layout
        pkg_cells = CellAdder("_pkg_cells", package_gdsfile, layout, tech=tech)
        self.pkg_cell = layout.cell(pkg_cells["CPGA84_A4"])
        design_cells = CellAdder("_design_cells", design_gdsfile, layout, tech=tech)
        self.design_cell = cell = layout.cell(design_cells[design_topcell])
        bnd_idx = layout.layer(tech_info.Boundary, 0)
        self.design_bb = cell.bbox_per_layer(bnd_idx)
        self.design_pins = design_pins
        assert (design_pins > 0) and ((design_pins % 4) == 0)
        self.design_pinsperside = design_pins//4
        assert self.design_pinsperside <= self.pinsperside
        self.design_pad_corner_offset = design_pad_corner_offset
        self.design_pad_edge_offset = design_pad_edge_offset
        self.design_rotate = design_rotate

    def get_pkgpin_center(self, pin):
        pin0 = pin - 1
        assert pin0 < self.pins
        side = pin0 // self.pinsperside
        pin0s = pin0 % self.pinsperside
        # Pin 1 is top left pin
        if side == 0:
            t = pya.Trans(
                -self.cavity_width//2,
                self.cavity_width//2 - self.cavity_corner_offset - pin0s*self.pinpitch,
            )
        elif side == 1:
            t = pya.Trans(
                -self.cavity_width//2 + self.cavity_corner_offset + pin0s*self.pinpitch,
                -self.cavity_width//2
            )
        elif side == 2:
            t = pya.Trans(
                self.cavity_width//2,
                -self.cavity_width//2 + self.cavity_corner_offset + pin0s*self.pinpitch,
            )
        elif side == 3:
            t = pya.Trans(
                self.cavity_width//2 - self.cavity_corner_offset - pin0s*self.pinpitch,
                self.cavity_width//2
            )
        return t*self.cavity_center
    
    def get_designpin_center(self, pin):
        pin0 = pin - 1
        assert pin0 < self.design_pins
        side = pin0 // self.design_pinsperside
        pin0s = pin0 % self.design_pinsperside
        width = self.design_bb.width()
        assert self.design_bb.height() == width
        pinpitch = (width - 2*self.design_pad_corner_offset)//(self.design_pinsperside - 1)
        # Pin 1 is top left pin
        if side == 0:
            t = pya.Trans(
                -width//2 + self.design_pad_edge_offset,
                width//2 - self.design_pad_corner_offset - pin0s*pinpitch,
            )
        elif side == 1:
            t = pya.Trans(
                -width//2 + self.design_pad_corner_offset + pin0s*pinpitch,
                -width//2 + self.design_pad_edge_offset,
            )
        elif side == 2:
            t = pya.Trans(
                width//2 - self.design_pad_edge_offset,
                -width//2 + self.design_pad_corner_offset + pin0s*pinpitch,
            )
        elif side == 3:
            t = pya.Trans(
                width//2 - self.design_pad_corner_offset - pin0s*pinpitch,
                width//2 - self.design_pad_edge_offset,
            )
        return t*self.cavity_center

    def generate_cell(self, cellname):
        cell = self.layout.create_cell(cellname)
        cell.insert(pya.CellInstArray(self.pkg_cell.cell_index(), pya.Trans(0,0)))
        t_cellcenter = pya.Trans(pya.Vector(self.design_bb.center()))
        t_cell = t_cellcenter.inverted()
        if self.design_rotate is not None:
            t_cell = self.design_rotate * t_cell
        t_cavitycenter = pya.Trans(pya.Vector(self.cavity_center))
        cell.insert(pya.CellInstArray(
            self.design_cell.cell_index(),
            pya.Trans(t_cavitycenter*t_cell),
        ))

        bnd2_idx = layout.layer(tech_info.Bonding2, 0)
        bnd2_shapes = cell.shapes(bnd2_idx)

        pkg_pin_offset = (self.pinsperside - self.design_pinsperside)//2
        pin_connects = sum(
            ((
                (i + pkg_pin_offset + 1, i + 1),
                (self.pinsperside + i + pkg_pin_offset + 1, self.design_pinsperside + i + 1),
                (2*self.pinsperside + i + pkg_pin_offset + 1, 2*self.design_pinsperside + i + 1),
                (3*self.pinsperside + i + pkg_pin_offset + 1, 3*self.design_pinsperside + i + 1),
             ) for i in range(self.design_pinsperside)
            ),
            tuple(),
        )
        lengths = []
        for pkg_pin, dsg_pin in pin_connects:
            p_pkg = self.get_pkgpin_center(pkg_pin)
            p_dsg = self.get_designpin_center(dsg_pin)
            path = pya.Path([p_pkg, p_dsg], u(30))
            bnd2_shapes.insert(path)
            lengths.append(path.length())
        
        return max(lengths)


layout = pya.Layout()
tech = pya.Technology.technology_by_name("TSMC_C018")

cpga84 = Europractice_CPGA84(
    layout, os.environ["GDSPACKAGES"], os.environ["GDSOUTFILE"], "NLNet018TV",
    4*13, u(270), u(64.4), pya.Trans.R270,
    tech=tech,
)
max_length = cpga84.generate_cell("NLNet018TV_PACK")

bnd4_idx = layout.layer(tech_info.Bonding4, 0)
pkg_bnd4_shapes = layout.cell("A4").shapes(bnd4_idx)
for shape in pkg_bnd4_shapes.each():
    if shape.type() in (pya.Shape.TText, pya.Shape.TTextRef):
        s = shape.text_string
        extra = {
            "Request:": " NLNet018TV",
            "MPW:": " 79244/E17850/01",
            "Die:": " NLNet018TV",
            "Qty packaged:": " 15",
            "Qty naked:": " 25",
            "Glass": " XX",
            "Date:": " " + date.today().isoformat(),
            "Size incl scribe:": " 1660um by 1660um",
            "Die Attach:": " default",
            "Wire:": " default",
            "Max. wire length :": " {:0.2f}mm".format(float(max_length)/u(1000)),
            "Pad-pitch :": " 90um",
            "Pad-opening:": " 80um by 62um",
            "Die-thickness:": " 11mil (279um)",
        }
        shape.text_string = s + extra.get(s, "")

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write(os.environ["GDSPACKOUTFILE"], saveopts)
