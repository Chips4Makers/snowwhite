print("Begin")

import os

import pya
from TSMC_C018 import tech_info
from C4MLib import CellAdder, get_indices, get_shapes

def delete_layer(tech_info, layout, layer_name):
    num = getattr(tech_info, layer_name)
    try:
        num, datatype = num
    except:
        datatype = 0
    layout.delete_layer(layout.layer(num, datatype))

shapeproc = pya.ShapeProcessor()

um = 1000L

#
# Create top cell
#

tech = pya.Technology.technology_by_name("TSMC_C018")
layout = pya.Layout()
top_cell = layout.create_cell("NLNet018TV")

sealring_cells = CellAdder("sealring_cells", os.environ["GDSSEALRINGFILE"], layout)
io_cells = CellAdder("io_cells", os.environ["GDSIOFILE"], layout)
sram_cells = CellAdder("sram_cells", os.environ["GDSSRAMFILE"], layout)
logo_cells = CellAdder("logo_cells", os.environ["GDSLOGOFILE"], layout)
fill_cells = CellAdder("fill_cells", os.environ["GDSFILLFILE"], layout)

# TODO: Get these parameters from the IO_Ring
io_offset = 225*um
io_width = 90*um
io_height = 214*um

top_cell.insert(pya.CellInstArray(sealring_cells["sealring_1_block_2017"], pya.Trans(0*um, 0*um)))
top_cell.insert(pya.CellInstArray(io_cells["IO_Ring"], pya.Trans(0*um, 0*um)))
top_cell.insert(pya.CellInstArray(sram_cells["SRAMTop"], pya.Trans(io_offset + 6*io_width, io_height)))
top_cell.insert(pya.CellInstArray(logo_cells["C4MLogo"], pya.Trans(0*um, 0*um)))

#
# Save file without dummies
#

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write("ND_" + os.environ["GDSOUTFILE"], saveopts)

#
# Generate dummies
#

# First delete existing dummy layers
delete_layer(tech_info, layout, "Metal1_Dummy")
delete_layer(tech_info, layout, "Metal2_Dummy")
delete_layer(tech_info, layout, "Metal3_Dummy")
delete_layer(tech_info, layout, "Metal4_Dummy")
delete_layer(tech_info, layout, "Metal5_Dummy")
delete_layer(tech_info, layout, "Metal6_Dummy")

layer_names = (
    "NWell",
    "Diffusion", "Diffusion_Dummy", "Diffusion_Block",
    "Poly", "Poly_Dummy", "Poly_Block",
    "Metal1", "Metal1_Dummy", "Metal1_Block",
    "Metal2", "Metal2_Dummy", "Metal2_Block",
    "Metal3", "Metal3_Dummy", "Metal3_Block",
    "Metal4", "Metal4_Dummy", "Metal4_Block",
    "Metal5", "Metal5_Dummy", "Metal5_Block",
    "Metal6", "Metal6_Dummy", "Metal6_Block",
    "Sealring", "Logo",
    "Temp",
)
top_indices = get_indices(tech_info, layout, top_cell, layer_names)
top_shapes = get_shapes(tech_info, layout, top_cell, layer_names)

shapeproc.size(
    layout, top_cell, top_indices["NWell"], top_shapes["Temp"], 0L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
nwell_region = pya.Region(top_shapes["Temp"])
nwell_region_big = nwell_region.sized(450L)
nwell_region_small = nwell_region.sized(-450L)

shapeproc.size(
    layout, top_cell, top_indices["Sealring"], top_shapes["Temp"], 5000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
fill_bbox = pya.Region(top_cell.bbox()) - pya.Region(top_shapes["Temp"])

# Diffusion/Poly
print("Generating dummies for Diffusion/Poly")
diffpoly_exclude = pya.Region()
for layer_name, size in (
    ("Diffusion", 1050L),
    ("Diffusion_Block", 4000L),
    ("Poly", 1050L),
    ("Poly_Block", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    diffpoly_exclude += pya.Region(top_shapes["Temp"])

fill_region = ((fill_bbox - nwell_region_big) + nwell_region_small) - diffpoly_exclude.sized(1000L)
fill_idx = fill_cells["DiffPolyFill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

shapeproc.size(
    layout, top_cell, top_indices["Diffusion_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
diffpoly_exclude += pya.Region(top_shapes["Temp"])
shapeproc.size(
    layout, top_cell, top_indices["Poly_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
diffpoly_exclude += pya.Region(top_shapes["Temp"])
fill_region = ((fill_bbox - nwell_region_big) + nwell_region_small) - diffpoly_exclude
fill_idx = fill_cells["DiffPolyFillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

# Metal1
print("Generating dummies for Metal1")
metal1_exclude = pya.Region()
for layer_name, size in (
    ("Metal1", 700L),
    ("Metal1_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal1_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal1_exclude.sized(1000L)
fill_idx = fill_cells["Metal1Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

shapeproc.size(
    layout, top_cell, top_indices["Metal1_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal1_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal1_exclude
fill_idx = fill_cells["Metal1FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

# Metal2
print("Generating dummies for Metal2")
metal2_exclude = pya.Region()
for layer_name, size in (
    ("Metal2", 700L),
    ("Metal2_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal2_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal2_exclude.sized(1000L)
fill_idx = fill_cells["Metal2Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

shapeproc.size(
    layout, top_cell, top_indices["Metal2_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal2_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal2_exclude
fill_idx = fill_cells["Metal2FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

# Metal3
print("Generating dummies for Metal3")
metal3_exclude = pya.Region()
for layer_name, size in (
    ("Metal3", 700L),
    ("Metal3_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal3_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal3_exclude.sized(1000L)
fill_idx = fill_cells["Metal3Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

shapeproc.size(
    layout, top_cell, top_indices["Metal3_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal3_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal3_exclude
fill_idx = fill_cells["Metal3FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

# Metal4
print("Generating dummies for Metal4")
metal4_exclude = pya.Region()
for layer_name, size in (
    ("Metal4", 700L),
    ("Metal4_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal4_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal4_exclude.sized(1000L)
fill_idx = fill_cells["Metal4Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

shapeproc.size(
    layout, top_cell, top_indices["Metal4_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal4_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal4_exclude
fill_idx = fill_cells["Metal4FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

# Metal5
print("Generating dummies for Metal5")
metal5_exclude = pya.Region()
for layer_name, size in (
    ("Metal5", 700L),
    ("Metal5_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal5_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal5_exclude.sized(1000L)
fill_idx = fill_cells["Metal5Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

shapeproc.size(
    layout, top_cell, top_indices["Metal5_Dummy"], top_shapes["Temp"], 1000L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal5_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal5_exclude
fill_idx = fill_cells["Metal5FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0,0))

# Metal6
print("Generating dummies for Metal6")
metal6_exclude = pya.Region()
for layer_name, size in (
    ("Metal6", 2000L),
    ("Metal6_Block", 0L),
    ("Logo", 0L),
):
    shapeproc.size(
        layout, top_cell, top_indices[layer_name], top_shapes["Temp"], size,
        pya.EdgeProcessor.ModeOr, True, True, False,
    )
    metal6_exclude += pya.Region(top_shapes["Temp"])

fill_region = fill_bbox - metal6_exclude.sized(2000L)
fill_idx = fill_cells["Metal6Fill"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

shapeproc.size(
    layout, top_cell, top_indices["Metal6_Dummy"], top_shapes["Temp"], 1500L,
    pya.EdgeProcessor.ModeOr, True, True, False,
)
metal6_exclude += pya.Region(top_shapes["Temp"])
fill_region = fill_bbox - metal6_exclude
fill_idx = fill_cells["Metal6FillFine"]
fill_cell = layout.cell(fill_idx)
top_cell.fill_region(fill_region, fill_idx, fill_cell.bbox(), pya.Point(0, 0))

# Delete temporary layers
delete_layer(tech_info, layout, "Temp")

#
# Save the file
#

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write(os.environ["GDSOUTFILE"], saveopts)

print("End")
