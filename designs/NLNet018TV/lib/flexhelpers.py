from Hurricane import Contact, Horizontal, Vertical, Polygon, Rectilinear, Point, DbU, Box
from helpers import u

__all__ = [
    "draw_rect", "draw_viarect", "draw_rects", "draw_rectsxy", "draw_ring", "draw_viaring", "draw_rings",
    "draw_diamondvia", "dim_from_area", "draw_path",
]

def draw_rect(net, layer, left, bottom, right, top, sizeup=0):
    """Draw a rectangle wire.
    
    An optional sizeup parameter allows to size up the rectangle; this is a convenience
    to ease for example drawing implant regions around other regions.
    """
    left -= sizeup
    bottom -= sizeup
    right += sizeup
    top += sizeup
    return Polygon.create(net, layer, [
        Point(left, bottom),
        Point(right, bottom),
        Point(right, top),
        Point(left, top),
        Point(left, bottom),
    ])

def draw_viarect(net, layer, rect_left, rect_bottom, rect_right, rect_top, via_width, via_space, enclosure):
    """Write an array of vias enclosed in a rectangle"""
    try:
        xenclosure, yenclosure = enclosure
    except:
        xenclosure = yenclosure = enclosure

    vias = []

    via_pitch = via_width + via_space
    rect_width = rect_right - rect_left
    rect_height = rect_top - rect_bottom

    nx = (rect_width - 2*xenclosure - via_width)//via_pitch + 1
    width2 = nx*via_pitch - via_space
    x0 = rect_left + via_width//2 + (rect_width - width2)//2 # Center array
    ny = (rect_height - 2*yenclosure - via_width)//via_pitch + 1
    height2 = ny*via_pitch - via_space
    y0 = rect_bottom + via_width//2 + (rect_height - height2)//2 # Center array
    for i_x in xrange(nx):
        for i_y in xrange(ny):
            vias.append(Contact.create(net, layer, x0 + i_x*via_pitch, y0 + i_y*via_pitch, via_width, via_width))
    
    return vias

def draw_rects(net, left, bottom, right, top, rects):
    rects_ = []

    for rect in rects:
        type_ = rect[0]
        if type_ == "wire":
            layer = rect[1]
            try:
                sizeup = rect[2]
            except IndexError:
                sizeup = 0
            rects_.append(draw_rect(net, layer, left, bottom, right, top, sizeup))
        elif type_ == "via":
            _, layer, via_width, via_space, enclosure = rect
            rects_.append(draw_viarect(net, layer, left, bottom, right, top, via_width, via_space, enclosure))
        else:
            raise ValueError("Unsupport ring specification type '{}'".format(type_))

    return rects_

def draw_rectsxy(net, x, y, rects):
    return [draw_rect(net, layer, x - width//2, y - height//2, x + width//2, y + height//2) for layer, width, height in rects]

def draw_ring(net, layer, left, bottom, right, top,  width):
    """Write a ring, coordinates are middle of the ring"""

    x_left_in = left + width//2
    x_left_out = x_left_in - width
    x_right_in = right - width//2
    x_right_out = x_right_in + width
    y_bottom_in = bottom + width//2
    y_bottom_out = y_bottom_in - width
    y_top_in = top - width//2
    y_top_out = y_top_in + width

    return Rectilinear.create(net, layer, [
        Point(x_left_out, y_bottom_out),
        Point(x_right_out, y_bottom_out),
        Point(x_right_out, y_top_out),
        Point(x_left_out, y_top_out),
        Point(x_left_out, y_bottom_in),
        Point(x_left_in, y_bottom_in),
        Point(x_left_in, y_top_in),
        Point(x_right_in, y_top_in),
        Point(x_right_in, y_bottom_in),
        Point(x_left_out, y_bottom_in),
        Point(x_left_out, y_bottom_out),
    ])

def draw_viaring(net, layer, left, bottom, right, top, ring_width, via_width, via_space, enclosure):
    """Write a ring of vias, coordinates are the middle of the ring"""
    vias = []

    # Make sure horizontal via is minimum space from vertical one, not enclosure
    d = via_space - 2*enclosure
    vias += draw_viarect(
        net, layer,
        left - ring_width//2, bottom - ring_width//2, left + ring_width//2, top + ring_width//2,
        via_width, via_space, enclosure
    )
    vias += draw_viarect(
        net, layer,
        left + ring_width//2 + d, bottom - ring_width//2, right - ring_width//2 - d, bottom + ring_width//2,
        via_width, via_space, enclosure,
    )
    vias += draw_viarect(
        net, layer,
        left + ring_width//2 + d, top - ring_width//2, right - ring_width//2 - d, top + ring_width//2,
        via_width, via_space, enclosure,
    )
    vias += draw_viarect(
        net, layer,
        right - ring_width//2, bottom - ring_width//2, right + ring_width//2, top + ring_width//2,
        via_width, via_space, enclosure
    )

    return vias

def draw_rings(net, left, bottom, right, top, rings):
    for ring in rings:
        type_ = ring[0]
        if type_ == "wire":
            _, layer, width = ring
            draw_ring(net, layer, left, bottom, right, top, width)
        elif type_ == "via":
            _, layer, ring_width, via_width, via_space, enclosure = ring
            draw_viaring(net, layer, left, bottom, right, top, ring_width, via_width, via_space, enclosure)
        else:
            raise ValueError("Unsupport ring specification type '{}'".format(type_))

def draw_diamondvia(net, layer, left, bottom, right, top, via_width, via_space, center_via, corner_distance):
    """draw a diamond chape of vias in a specified rectangle

    Arguments:
    - layer (Layer): Layer to draw the vias in
    - left, bottom, width, height (int, DbU): specification of the rectangle location.
    - via_width, via_space (int, DbU): specification of via dimensions.
    - center_via (bool): indicating if middle should be on the center of the rectangle.
        If false four middle vias will be spaced equally around the center of the rectangle.
    - corner_distance (int, DbU): the distance to the rectangle corners to define left out vias for the diamond shape
    """
    def check_corner(x, y):
        via_left = x - via_width//2
        via_bottom = y - via_width//2
        via_right = via_left + via_width
        via_top = via_bottom + via_width

        if corner_distance - (via_left - left) > (via_bottom - bottom): # In bottom-left corner
            return False
        elif corner_distance - (right - via_right) > (via_bottom - bottom): # In bottom-right corner
            return False
        elif corner_distance - (via_left - left) > (top - via_top): # In top-left corner
            return False
        elif corner_distance - (right - via_right) > (top - via_top): # In bottom-left corner
            return False
        else:
            return True

    via_pitch = via_width + via_space
    width = right - left
    height = top - bottom
    if center_via:
        # Odd number of vias in horizontal and vertical direction
        nx = (((width - via_width)/via_pitch)//2)*2 + 1
        ny = (((height - via_width)/via_pitch)//2)*2 + 1
    else:
        # Even number of vias in horizontal and vertical direction
        nx = (((width + via_space)/via_pitch)//2)*2
        ny = (((height + via_space)/via_pitch)//2)*2

    real_width = nx*via_pitch - via_space
    real_height = ny*via_pitch - via_space
    real_left = left + (width - real_width)//2 + via_width//2
    real_bottom = bottom + (height - real_height)//2 + via_width//2

    vias = 0
    for i in range(nx):
        x = real_left + i*via_pitch
        for j in range(ny):
            y = real_bottom + j*via_pitch
            if check_corner(x, y):
                Contact.create(net, layer, x, y, via_width, via_width)
                vias += 1

    return vias

def dim_from_area(physarea, grid, dim1):
    physgrid = DbU.toPhysical(grid, DbU.UnitPowerMicro)
    physdim2 = physarea/DbU.toPhysical(dim1, DbU.UnitPowerMicro)
    dim2 = u(long((physdim2/physgrid) + 0.99)*physgrid)
    return dim2

def draw_path(net, width, layer, start, cmds):
    point = start

    prev_x = None
    for cmd, value in cmds:
        if prev_x is not None:
            if prev_x:
                d_end = width//2 if new_point.getX() > point.getX() else -width//2
                xstart = point.getX()
                if point != start:
                    xstart -= d_end
                xend = new_point.getX() + d_end
                Horizontal.create(net, layer, point.getY(), width, xstart, xend)
                assert cmd in ("Y", "dY"), "X command not following Y command"
            else:
                d_end = width//2 if new_point.getY() > point.getY() else - width//2
                ystart = point.getY()
                if point != start:
                    ystart -= d_end
                yend = new_point.getY() + d_end
                Vertical.create(net, layer, point.getX(), width, ystart, yend)
                assert cmd in ("X", "dX"), "Y command not following X command"
            point = new_point

        if cmd in ("X", "dX"):
            new_point = Point(value if cmd == "X" else point.getX() + value , point.getY())

            prev_x = True

        elif cmd in ("Y", "dY"):
            new_point = Point(point.getX(), value if cmd == "Y" else point.getY() + value)

            prev_x = False
    # Draw last section without extension at the end
    if prev_x is not None:
        if prev_x:
            d_end = width//2 if new_point.getX() > point.getX() else -width//2
            xstart = point.getX()
            if point != start:
                xstart -= d_end
            xend = new_point.getX() + d_end
            Horizontal.create(net, layer, point.getY(), width, xstart, xend)
        else:
            d_end = width//2 if new_point.getY() > point.getY() else - width//2
            ystart = point.getY()
            if point != start:
                ystart -= d_end
            yend = new_point.getY() + d_end
            Vertical.create(net, layer, point.getX(), width, ystart, yend)
