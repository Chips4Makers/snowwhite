# SnowWhiteIII

This is the source for the third SnowWhite test tape-out.

Originally the plan was to include a minimal version of the Retro-uC on this tape-out.
So the code for that was developed but during P&R it was discovered that current state
of the toolchain and it's configuration was not able to handle this size of design. Finally
a design with Z80 and one with a MOS6502 was taped out. On the fourth die some ESD test
structures were made available.

## Common code

The IO Ring generation code was made (more) generic and has now been put in a python module
in util/C4MLib/python to be used in klayout scripts. Not released to the public but also
some support layouts are available internally in util/C4MLib/layout/C4MLib.oas.gz

## Retro-uC code

nmigen development was done on the Retro-uC. This was to improve on the existing
[Retro-uC code](https://gitlab.com/Chips4Makers/Retro-uC/tree/fc2d23be) for the
test-chip and later on back-port this code to the [Retro-uC repo](https://gitlab.com/Chips4Makers/Retro-uC).

Previous code was VHDL for top and migen was used for a few sub-blocks. For the new code now nmigen is used
for the top and sub-blocks are wrapped in nmigen classes. There was code ready for a top cell with the MOS6502,
Z80 and Motorola 68000 processor. But when doing P&R with Qflow and the OSU 0.35um cells with reduced M4 pitch it
was discovered that the flow had too much overhead; around 50% extra space had to be added for P&R to complete.
Also it was seen that graywolf and qrouter used in Qflow don't scale well with size of the block. First it was
tried to only remove the Motorola 68000 processor but this still was too big and then finally settled on having
one design with a Z80 on die A and one with a MOS6502 on die B. For die C also the design with the MOS6502 was
used.

### Die A/B/C pin-out

| Pin | Name     | | Pin | Name     | | Pin | Name     | | Pin | Name     |
|-----|----------|-|-----|----------|-|-----|----------|-|-----|----------|
| 1   | NC       | | 33  | NC       | | 65  | NC       | | 97  | NC       |
| 2   | GNDCORE  | | 34  | GNDCORE  | | 66  | GNDCORE  | | 98  | GNDCORE  |
| 3   | GNDCORE  | | 35  | GNDCORE  | | 67  | GNDCORE  | | 99  | GNDCORE  |
| 4   | VDDCORE  | | 36  | VDDCORE  | | 68  | VDDCORE  | | 100 | VDDCORE  |
| 5   | CLK      | | 37  | DO[0]    | | 69  | GPIO[14] | | 101 | GPIO[38] |
| 6   | RESET_N  | | 38  | DO[1]    | | 70  | GPIO[15] | | 102 | GPIO[39] |
| 7   | ADDR[0]  | | 39  | DO[2]    | | 71  | GPIO[16] | | 103 | GPIO[40] |
| 8   | ADDR[1]  | | 40  | DO[3]    | | 72  | GPIO[17] | | 104 | GPIO[41] |
| 9   | ADDR[2]  | | 41  | DO[4]    | | 73  | GPIO[18] | | 105 | GPIO[42] |
| 10  | ADDR[3]  | | 42  | DO[5]    | | 74  | GPIO[19] | | 106 | GPIO[43] |
| 11  | ADDR[4]  | | 43  | DO[6]    | | 75  | GPIO[20] | | 107 | GPIO[44] |
| 12  | ADDR[5]  | | 44  | DO[7]    | | 76  | GPIO[21] | | 108 | GPIO[45] |
| 13  | ADDR[6]  | | 45  | WE       | | 77  | GPIO[22] | | 109 | GPIO[46] |
| 14  | ADRR[7]  | | 46  | EN       | | 78  | GPIO[23] | | 110 | GPIO[47] |
| 15  | ADDR[8]  | | 47  | GPIO[0]  | | 79  | GPIO[24] | | 111 | GPIO[48] |
| 16  | ADDR[9]  | | 48  | GPIO[1]  | | 80  | GPIO[25] | | 112 | GPIO[49] |
| 17  | ADDR[10] | | 49  | GPIO[2]  | | 81  | GPIO[26] | | 113 | GPIO[50] |
| 18  | ADDR[11] | | 50  | GPIO[3]  | | 82  | GPIO[27] | | 114 | GPIO[51] |
| 19  | ADDR[12] | | 51  | GPIO[4]  | | 83  | GPIO[28] | | 115 | GPIO[52] |
| 20  | ADDR[13] | | 52  | GPIO[5]  | | 84  | GPIO[29] | | 116 | GPIO[53] |
| 21  | DI[0]    | | 53  | GPIO[6]  | | 85  | GPIO[30] | | 117 | GPIO[54] |
| 22  | DI[1]    | | 54  | GPIO[7]  | | 86  | GPIO[31] | | 118 | GPIO[55] |
| 23  | DI[2]    | | 55  | GPIO[8]  | | 87  | GPIO[32] | | 119 | GPIO[56] |
| 24  | DI[3]    | | 56  | GPIO[9]  | | 88  | GPIO[33] | | 120 | HALT     |
| 25  | DI[4]    | | 57  | GPIO[10] | | 89  | GPIO[34] | | 121 | TCK      |
| 26  | DI[5]    | | 58  | GPIO[11] | | 90  | GPIO[35] | | 122 | TMS      |
| 27  | DI[6]    | | 59  | GPIO[12] | | 91  | GPIO[36] | | 123 | TDO      |
| 28  | DI[7]    | | 60  | GPIO[13] | | 92  | GPIO[37] | | 124 | TDI      |
| 29  | VDDIO    | | 61  | VDDIO    | | 93  | VDDIO    | | 125 | VDDIO    |
| 30  | GNDIO    | | 62  | GNDIO    | | 94  | GNDIO    | | 126 | GNDIO    |
| 31  | GNDIO    | | 63  | GNDIO    | | 95  | GNDIO    | | 127 | GNDIO    |
| 32  | NC       | | 64  | NC       | | 96  | NC       | | 128 | NC       |


## Other digital test investigation

For die C a minimal VexRiscv design was tried. The P&R was OK but the size was just a little too big to allow the
connection of the pins to the IO cells with the klayout scripts. Some of the Qflow directories are working with a
Makefile; others don't have a Makefile. In these directories qflow was run from with `qflow gui`.
In MOS6502Cor a snapshot of the development for P&R with Coriolis is present but this is unfinished. On die D
a P&R result of Arlet 6502 is present with Coriolis2 and the NSXLIB standard cell library; this was done in a local
checkout of alliance-check-toolkit. The pins of the digital block were not connected to the IO cells so this exercise
just shows that the block passes the TSMC DRC checks.

## 5V ESD test structures

On die D also an 5V ESD test structure is present. It's to test drive strength of the 5V transistor with minimal
dimensions for ESD compliance.

## Used software versions

- [nmigen @ 72cf4ca](https://github.com/m-labs/nmigen/tree/72cf4ca)
- [yosys @ 463f710](https://github.com/YosysHQ/yosys/tree/463f710) for Verilog synthesis and nmigen RTL code generation
- yosys from Symbiotic EDA v20190521A with SERP license for VHDL code synthesis
- [graywolf @ 6c5e24f](https://github.com/rubund/graywolf/tree/6c5e24f)
- [qrouter 1.4.60](https://github.com/RTimothyEdwards/qrouter/tree/44bbb9b)
- [magic 8.2.110](https://github.com/RTimothyEdwards/magic/tree/1c3a5b8)
- [qflow 1.3.14](https://github.com/RTimothyEdwards/qflow/tree/25b2844)
