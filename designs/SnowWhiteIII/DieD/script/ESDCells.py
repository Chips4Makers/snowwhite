print("Begin")

import os

import pya
import TSMC_CL035G as tech_info
from C4MLib import *

def um(d):
    return long(d*1000L)

def insert_esd(tech_info, shapes, w, fingers, nmos, thickgate, x=0, y=0):
    assert(fingers%2 == 0) # even

    if thickgate:
        l = um(0.8)
    else:
        l = um(0.4)
    # We draw ESD implant => 1um space to
    esd_contact_poly_space = um(1)
    esd_contact_poly_space_source = um(0.5)
    esd_diffusion_contact_enclosure = um(1)

    finger_pitch = tech_info.Contact_Width + esd_contact_poly_space_source + l + esd_contact_poly_space
    od_width = fingers*finger_pitch + tech_info.Contact_Width + 2*esd_contact_poly_space_source

    od_left = x
    od_bottom = y
    od_right = od_left + od_width
    od_top = od_bottom + w
    shapes["Diffusion"].insert(pya.Box(od_left, od_bottom, od_right, od_top))

    left_ch_left = od_left + esd_contact_poly_space_source
    source_m1_width = 2*tech_info.Metal2_Via23_Enclosure + tech_info.Via23_Width
    source_m1_left = left_ch_left + tech_info.Contact_Width/2 - source_m1_width/2
    drain_m1_left = (
        left_ch_left + finger_pitch + tech_info.Contact_Width/2 - tech_info.Via12_Width/2 -
        tech_info.Metal1_Via12_Enclosure
    )
    drain_m1_width = 2*tech_info.Metal1_Via12_Enclosure + tech_info.Via12_Width

    # Compute poly extension to also avoid metal1 spacing violations
    poly_od_ext = max(
        tech_info.Poly_Diffusion_Space,
        2*tech_info.Metal1_Contact_Enclosure + tech_info.Metal3_MinSpace_Wide -
        tech_info.Diffusion_Contact_Enclosure - tech_info.Poly_Contact_Enclosure
    )
    # Compute drain metal1 top/bottom so source metal regions can be connected with metal3
    source_m1_offset = tech_info.Diffusion_Contact_Enclosure - tech_info.Metal1_Contact_Enclosure
    drain_m1_offset = max(
        esd_contact_poly_space - tech_info.Metal1_Contact_Enclosure,
        source_m1_offset + source_m1_width + tech_info.Metal3_MinSpace_Wide
    )
    if nmos:
        impl_encl = tech_info.NPlus_Diffusion_Enclosure
        impl_space = tech_info.NPlus_Diffusion_Space
        impl_shapes = shapes["NPlus"]
        if thickgate:
            impl_shapes2 = shapes["N5V"]
        else:
            impl_shapes2 = shapes["N3V"]
    else:
        impl_encl = tech_info.PPlus_Diffusion_Enclosure
        impl_space = tech_info.PPlus_Diffusion_Space
        impl_shapes = shapes["PPlus"]
        impl_shapes2 = None
    left = od_left - impl_encl
    right = od_right + impl_encl
    bottom = od_bottom - impl_encl
    top = od_top + impl_encl
    impl_shapes.insert(pya.Box(left, bottom, right, top))
    if impl_shapes2 is not None:
        impl_shapes2.insert(pya.Box(left, bottom, right, top))

    # Mark transistor as ESD
    #hapes["SDI"].insert(pya.Box(left, bottom, right, top))

    # ESD implant
    if nmos:
        if thickgate:
            left = od_left - tech_info.ESD5V_Diffusion_Enclosure
            right = od_right + tech_info.ESD5V_Diffusion_Enclosure
            bottom = od_bottom - tech_info.ESD5V_Diffusion_Enclosure
            top = od_top + tech_info.ESD5V_Diffusion_Enclosure
            shapes["ESD5V"].insert(pya.Box(left, bottom, right, top))

            encl = tech_info.ESD5V_Diffusion_Space
            guard_inner_left = left - encl
            guard_inner_bottom = bottom - encl
            guard_inner_right = right + encl
            guard_inner_top = top + encl
        else:
            # Done for each finger
            left_esd_left = (
                left_ch_left + tech_info.Contact_Width + tech_info.Contact_Poly_Space +
                l + tech_info.ESD_Poly_Space
            )
            left_esd_right = (
                left_ch_left + 2*finger_pitch - tech_info.Contact_Poly_Space -
                l - tech_info.ESD_Poly_Space
            )

            encl = impl_encl + impl_space
            guard_inner_left = od_left - encl
            guard_inner_bottom = od_bottom - encl
            guard_inner_right = od_right + encl
            guard_inner_top = od_top + encl
    else:
        encl = impl_encl + impl_space
        guard_inner_left = od_left - encl
        guard_inner_bottom = od_bottom - encl
        guard_inner_right = od_right + encl
        guard_inner_top = od_top + encl

    if thickgate:
        encl = tech_info.ThickGate_Diffusion_Enclosure
        left = od_left - encl
        right = od_right + encl
        bottom = od_bottom - encl
        top = od_top + encl
        shapes["ThickGate"].insert(pya.Box(left, bottom, right, top))
        space = tech_info.ThickGate_Diffusion_Space
        guard_inner_left = min(guard_inner_left, left - space)
        guard_inner_bottom = min(guard_inner_bottom, bottom - space)
        guard_inner_right = max(guard_inner_right, right + space)
        guard_inner_top = max(guard_inner_top, top + space)

    # Shapes for each finger
    for i in range(fingers/2):
        encl = tech_info.Metal1_Contact_Enclosure
        encl2 = tech_info.Metal1_Via12_Enclosure

        # Contact
        left =  source_m1_left + 2*i*finger_pitch
        right = left + source_m1_width
        bottom = od_bottom + source_m1_offset
        top = od_top - source_m1_offset
        insert_viarect(
            shapes["Contact"], left, bottom, right, top,
            tech_info.Contact_Width, tech_info.Contact_MinPitch, encl,
        )
        shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
        left = drain_m1_left + 2*i*finger_pitch
        if i == 0:
            drain_m2_left = left
        right = left + drain_m1_width
        if i == (fingers/2 - 1):
            drain_m2_right = right
        bottom = od_bottom + drain_m1_offset
        top = od_top - drain_m1_offset
        insert_viarect(
            shapes["Contact"], left, bottom, right, top,
            tech_info.Contact_Width, tech_info.Contact_MinPitch, encl,
        )
        shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
        insert_viarect(
            shapes["Via12"], left, bottom, right, top,
            tech_info.Via12_Width, tech_info.Via12_MinPitch, encl2,
        )
        
        # Poly
        left = (
            left_ch_left + 2*i*finger_pitch + tech_info.Contact_Width + esd_contact_poly_space_source
        )
        if i == 0:
            poly_left = left
        right = left + l
        bottom = od_bottom - poly_od_ext
        top = od_bottom + w + poly_od_ext
        shapes["Poly"].insert(pya.Box(left, bottom, right, top))
        right = (
            left_ch_left + 2*(i + 1)*finger_pitch - esd_contact_poly_space_source
        )
        if i == (fingers/2 - 1):
            poly_right = right
        left = right - l
        shapes["Poly"].insert(pya.Box(left, bottom, right, top))

        # ESD implant
        if nmos and not thickgate:
            left = left_esd_left + 2*i*finger_pitch
            right = left_esd_right + 2*i*finger_pitch
            bottom = od_bottom + tech_info.Diffusion_ESD_Enclosure
            top = od_top - tech_info.Diffusion_ESD_Enclosure
            shapes["ESD"].insert(pya.Box(left, bottom, right, top))

    # Last contact row
    encl = tech_info.Metal1_Contact_Enclosure
    left = source_m1_left + fingers*finger_pitch
    right = left + source_m1_width
    bottom = od_bottom + source_m1_offset
    top = od_top - source_m1_offset
    insert_viarect(
        shapes["Contact"], left, bottom, right, top,
        tech_info.Contact_Width, tech_info.Contact_MinPitch, encl,
    )
    shapes["Metal1"].insert(pya.Box(left, bottom, right, top))

    # Source M1 connection + up to metal3
    bottom = od_bottom + source_m1_offset
    top = bottom + source_m1_width
    left = source_m1_left
    right = left + fingers*finger_pitch
    shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via12"], left, bottom, right, top,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via23"], left, bottom, right, top,
        tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
    )
    m3_bottom = bottom
    top = od_top - source_m1_offset
    bottom = top - source_m1_width
    shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via12"], left, bottom, right, top,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via23"], left, bottom, right, top,
        tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
    )
    m3_top = top
    shapes["Metal3"].insert(pya.Box(left, m3_bottom, right, m3_top))

    # Drain M2
    bottom = od_bottom + drain_m1_offset
    top = od_top - drain_m1_offset
    shapes["Metal2"].insert(pya.Box(drain_m2_left, bottom, drain_m2_right, top))

    # Horizontal poly
    encl = tech_info.Poly_Contact_Enclosure
    encl2 = tech_info.Metal1_Contact_Enclosure

    top = od_bottom - poly_od_ext
    bottom = top - 2*encl - tech_info.Contact_Width
    shapes["Poly"].insert(pya.Box(poly_left, bottom, poly_right, top))
    guard_inner_bottom = min(
        guard_inner_bottom, bottom - tech_info.Poly_Diffusion_Space,
    )
    insert_viarect(
        shapes["Contact"], poly_left, bottom, poly_right, top,
        tech_info.Contact_Width, tech_info.Contact_MinPitch, encl,
    )
    left = poly_left + encl - encl2
    right = poly_right - encl + encl2
    top = top - encl + encl2
    bottom = top - source_m1_width
    guard_inner_bottom = min(
        guard_inner_bottom, bottom - tech_info.Metal1_MinSpace,
    )
    shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via12"], poly_left, bottom, poly_right, top,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    shapes["Metal2"].insert(pya.Box(left, bottom, right, top))

    bottom = od_bottom + w + poly_od_ext
    top = bottom + 2*encl + tech_info.Contact_Width
    shapes["Poly"].insert(pya.Box(poly_left, bottom, poly_right, top))
    guard_inner_top = max(
        guard_inner_top, top + tech_info.Poly_Diffusion_Space,
    )
    insert_viarect(
        shapes["Contact"], poly_left, bottom, poly_right, top,
        tech_info.Contact_Width, tech_info.Contact_MinPitch, encl,
    )
    left = poly_left + encl - encl2
    right = poly_right - encl + encl2
    bottom = bottom + encl - encl2
    top = bottom + source_m1_width
    guard_inner_top = max(
        guard_inner_top, top + tech_info.Metal1_MinSpace,
    )
    shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
    insert_viarect(
        shapes["Via12"], poly_left, bottom, poly_right, top,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    shapes["Metal2"].insert(pya.Box(poly_left, bottom, poly_right, top))

    # guard ring
    guard_width = tech_info.Contact_Width + 2*max(
        tech_info.Diffusion_Contact_Enclosure,
        tech_info.Metal1_Contact_Enclosure,
    )
    left = guard_inner_left - guard_width
    right = guard_inner_right + guard_width
    bottom = guard_inner_bottom - guard_width
    top = guard_inner_top + guard_width
    insert_ring(shapes["Diffusion"], left, bottom, right, top, guard_width)
    insert_viaring(
        shapes["Contact"], left, bottom, right, top, guard_width,
        tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure,
    )
    insert_ring(shapes["Metal1"], left, bottom, right, top, guard_width)
    if nmos:
        impl_encl = tech_info.PPlus_Diffusion_Enclosure
        impl_shapes = shapes["PPlus"]
    else:
        impl_encl = tech_info.NPlus_Diffusion_Enclosure
        impl_shapes = shapes["NPlus"]
    left2 = left - impl_encl
    right2 = right + impl_encl
    bottom2 = bottom - impl_encl
    top2 = top + impl_encl
    insert_ring(impl_shapes, left2, bottom2, right2, top2, guard_width + 2*impl_encl)

    # NWell
    if not nmos:
        encl = tech_info.NWell_NDiff_Enclosure
        left2 = left - encl
        bottom2 = bottom - encl
        right2 = right + encl
        top2 = top + encl
        shapes["NWell"].insert(pya.Box(left2, bottom2, right2, top2))

tech = pya.Technology.technology_by_name("TSMC_CL035")
layout = pya.Layout()

layers = (
    "NWell", "Diffusion", "ThickGate", "NPlus", "N3V", "N5V", "PPlus", "NWell",
    "Poly", "Contact", "Metal1", "Via12", "Metal2", "Via23", "Metal3",
    "ESD", "ESD5V", "SDI",
)
cell = layout.create_cell("ESD_NMOS_5V_15U8F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(15), 8, True, True)

cell = layout.create_cell("ESD_NMOS_5V_30U4F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(30), 4, True, True)

cell = layout.create_cell("ESD_NMOS_5V_30U2F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(30), 2, True, True)

cell = layout.create_cell("ESD_NMOS_5V_60U2F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(60), 2, True, True)

cell = layout.create_cell("ESD_PMOS_5V_15U16F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(15), 16, False, True)

cell = layout.create_cell("ESD_PMOS_5V_30U8F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(30), 8, False, True)

cell = layout.create_cell("ESD_PMOS_5V_60U4F")
shapes = get_shapes(tech_info, layout, cell, layers)
insert_esd(tech_info, shapes, um(60), 4, False, True)



saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write(os.environ["GDSFILE"], saveopts)


print("End")
