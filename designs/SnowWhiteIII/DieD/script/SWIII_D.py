print("Begin")


import pya
import TSMC_CL035G as tech_info
from C4MLib import *

um = 1000L

tech = pya.Technology.technology_by_name("TSMC_CL035")
layout = pya.Layout()
top_cell = layout.create_cell("SWIII_D")

ioring = IORing(tech, tech_info, layout, "IORing", break_top=True)
ioring_inst = pya.CellInstArray(ioring.cell_index(), pya.Trans(0,0))
top_cell.insert(ioring_inst)


arlet6502_cells = CellAdder(
    "arlet6502",
    "/home/verhaegs/coriolis-2.x/src/alliance-check-toolkit/benchs/arlet6502/arlet6502_cts_r_scale.gds",
    layout,
)
top_cell.insert(pya.CellInstArray(arlet6502_cells["arlet6502_cts_r"], pya.Trans(1000*um, 300*um)))


esd_cells = CellAdder("ESDCells", "ESDCells.oas.gz", layout)
esd_cell = layout.create_cell("ESD_test")

esd_cell.insert(pya.CellInstArray(esd_cells["ESD_NMOS_5V_60U2F"], pya.Trans(3, False, 0*um, 0*um)))
esd_cell.insert(pya.CellInstArray(esd_cells["ESD_PMOS_5V_60U4F"], pya.Trans(3, False, 0*um, 60*um)))

layers = (
    "NWell", "Diffusion", "ThickGate", "N3V", "NPlus", "PPlus", "NWell",
    "Poly", "Contact", "Metal1", "Via12", "Metal2", "Via23", "Metal3",
    "ESD", "ESD5V", "SDI",
)
esd_shapes = get_shapes(tech_info, layout, esd_cell, layers)

cell_top = 70*um

left = -2600L
right = 62600L
bottom = 2100L
top = 45700L

pplus_left = left - 5*um
nplus_right = right + 5*um

mid_y = (bottom + top)/2

nwell_top = mid_y
nwell_encl = max(tech_info.NWell_NDiff_Enclosure, tech_info.Diffusion_MinSpace/2)
od_top = nwell_top - nwell_encl
od_bottom = od_top - 2*um
esd_shapes["Diffusion"].insert(pya.Box(left, od_bottom, nplus_right, od_top))
insert_viarect(
    esd_shapes["Contact"], left, od_bottom, nplus_right, od_top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
encl_offset = tech_info.Metal1_Contact_Enclosure - tech_info.Diffusion_Contact_Enclosure
esd_shapes["Metal1"].insert(pya.Box(left, od_bottom - encl_offset, nplus_right, od_top + encl_offset))
encl = tech_info.NPlus_Diffusion_Enclosure
left2 = left - encl
right2 = nplus_right + encl
bottom2 = od_bottom - encl
top2 = od_top + encl
esd_shapes["NPlus"].insert(pya.Box(left2, bottom2, right2, top2))
nwell_bottom = od_bottom - nwell_encl
nwell_left = left - nwell_encl
nwell_right = nplus_right + nwell_encl
esd_shapes["NWell"].insert(pya.Box(nwell_left, nwell_bottom, nwell_right, nwell_top))

od_top = od_bottom - tech_info.Diffusion_MinSpace
esd_shapes["Diffusion"].insert(pya.Box(pplus_left, bottom, right, od_top))
insert_viarect(
    esd_shapes["Contact"], pplus_left, bottom, right, od_top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
encl_offset = tech_info.Metal1_Contact_Enclosure - tech_info.Diffusion_Contact_Enclosure
esd_shapes["Metal1"].insert(pya.Box(pplus_left, bottom, right, od_top + encl_offset))
encl = tech_info.PPlus_Diffusion_Enclosure
left2 = pplus_left - encl
right2 = right + encl
bottom2 = bottom - encl
top2 = od_top + encl
esd_shapes["PPlus"].insert(pya.Box(left2, bottom2, right2, top2))

od_bottom = mid_y + nwell_encl
od_top = od_bottom + 2*um
esd_shapes["Diffusion"].insert(pya.Box(pplus_left, od_bottom, right, od_top))
insert_viarect(
    esd_shapes["Contact"], pplus_left, od_bottom, right, od_top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
encl_offset = tech_info.Metal1_Contact_Enclosure - tech_info.Diffusion_Contact_Enclosure
esd_shapes["Metal1"].insert(pya.Box(pplus_left, od_bottom - encl_offset, right, od_top + encl_offset))
encl = tech_info.PPlus_Diffusion_Enclosure
left2 = pplus_left - encl
right2 = right + encl
bottom2 = od_bottom - encl
top2 = od_top + encl
esd_shapes["PPlus"].insert(pya.Box(left2, bottom2, right2, top2))
left2 = pplus_left
right2 = left2 + 2*um
top2 = od_bottom
bottom2 = top2 - 2*um - 4*nwell_encl
esd_shapes["Diffusion"].insert(pya.Box(left2, bottom2, right2, top2))
insert_viarect(
    esd_shapes["Contact"], left2, bottom2, right2, top2,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
esd_shapes["Metal1"].insert(pya.Box(left2, bottom2 + encl_offset, right2, top2 - encl_offset))
left2 -= encl
right2 += encl
esd_shapes["PPlus"].insert(pya.Box(left2, bottom2, right2, top2))

od_bottom = od_top + tech_info.Diffusion_MinSpace
esd_shapes["Diffusion"].insert(pya.Box(left, od_bottom, nplus_right, top))
insert_viarect(
    esd_shapes["Contact"], left, od_bottom, nplus_right, top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
encl_offset = tech_info.Metal1_Contact_Enclosure - tech_info.Diffusion_Contact_Enclosure
esd_shapes["Metal1"].insert(pya.Box(left, od_bottom - encl_offset, nplus_right, top))
encl = tech_info.NPlus_Diffusion_Enclosure
left2 = left - encl
right2 = nplus_right + encl
bottom2 = od_bottom - encl
top2 = top + encl
esd_shapes["NPlus"].insert(pya.Box(left2, bottom2, right2, top2))
nwell_top = top + nwell_encl
nwell_bottom = od_bottom - nwell_encl
nwell_left = left - nwell_encl
nwell_right = nplus_right + nwell_encl
esd_shapes["NWell"].insert(pya.Box(nwell_left, nwell_bottom, nwell_right, nwell_top))
right2 = nplus_right
left2 = right2 - 2*um
top2 = od_bottom
bottom2 = mid_y - nwell_encl
esd_shapes["Diffusion"].insert(pya.Box(left2, bottom2, right2, top2))
insert_viarect(
    esd_shapes["Contact"], left2, bottom2, right2, top2,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
esd_shapes["Metal1"].insert(pya.Box(left2, bottom2 + encl_offset, right2, top2 - encl_offset))
left2 -= encl
right2 += encl
esd_shapes["NPlus"].insert(pya.Box(left2, bottom2, right2, top2))
left2 -= nwell_encl - encl
right2 += nwell_encl - encl
esd_shapes["NWell"].insert(pya.Box(left2, bottom2, right2, top2))


m2_bottom = long(-3.4*um)
m2_left = 2*um
m2_right = m2_left + 25*um
esd_shapes["Metal2"].insert(pya.Box(m2_left, m2_bottom, m2_right, cell_top))
m2_right = 58*um
m2_left = m2_right - 25*um
esd_shapes["Metal2"].insert(pya.Box(m2_left, m2_bottom, m2_right, cell_top))


left = 67*um
bottom = 23*um
top = 45*um
left2 = 82*um
right2 = 112*um
esd_shapes["Metal1"].insert(pya.Polygon([
    pya.Point(left, bottom),
    pya.Point(left, top),
    pya.Point(left2, top),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, bottom),
]))
bottom2 = cell_top - 5*um
insert_viarect(
    esd_shapes["Via12"], left2, bottom2, right2, cell_top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
esd_shapes["Metal2"].insert(pya.Box(left2, bottom2, right2, cell_top))
insert_viarect(
    esd_shapes["Via23"], left2, bottom2, right2, cell_top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
esd_shapes["Metal3"].insert(pya.Box(left2, bottom2, right2, cell_top))

left = long(61.5*um)
bottom = long(49.5*um)
top = long(58.5*um)
left2 = 162*um
right2 = 192*um
esd_shapes["Metal2"].insert(pya.Polygon([
    pya.Point(left, bottom),
    pya.Point(left, top),
    pya.Point(left2, top),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, bottom),
]))
bottom2 = cell_top - 20*um
insert_viarect(
    esd_shapes["Via12"], left2, bottom2, right2, cell_top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
esd_shapes["Metal1"].insert(pya.Box(left2, bottom2, right2, cell_top))
insert_viarect(
    esd_shapes["Contact"], left2, bottom2, right2, cell_top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
esd_shapes["Diffusion"].insert(pya.Box(left2, bottom2, right2, cell_top))
encl = tech_info.NPlus_Diffusion_Enclosure
left2 -= encl
right2 += encl
bottom2 -= encl
top2 = cell_top + encl
esd_shapes["NPlus"].insert(pya.Box(left2, bottom, right2, top2))

left = long(59.5*um)
bottom = long(48.5*um)
top = long(59.5*um)
left2 = 242*um
right2 = 272*um
esd_shapes["Metal3"].insert(pya.Polygon([
    pya.Point(left, bottom),
    pya.Point(left, top),
    pya.Point(left2, top),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, bottom),
]))

right = long(-6.5*um)
bottom = long(2.5*um)
top = long(25.5*um)
right2 = -22*um
left2 = right2 - 30*um
esd_shapes["Metal1"].insert(pya.Polygon([
    pya.Point(left2, bottom),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, top),
    pya.Point(right, top),
    pya.Point(right, bottom),
]))
bottom2 = cell_top - 5*um
insert_viarect(
    esd_shapes["Via12"], left2, bottom2, right2, cell_top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
esd_shapes["Metal2"].insert(pya.Box(left2, bottom2, right2, cell_top))
insert_viarect(
    esd_shapes["Via23"], left2, bottom2, right2, cell_top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
esd_shapes["Metal3"].insert(pya.Box(left2, bottom2, right2, cell_top))

right = long(-1*um)
bottom = long(-7*um)
top = long(0*um)
right2 = -102*um
left2 = right2 - 30*um
esd_shapes["Metal2"].insert(pya.Polygon([
    pya.Point(left2, bottom),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, top),
    pya.Point(right, top),
    pya.Point(right, bottom),
]))
bottom2 = cell_top - 20*um
insert_viarect(
    esd_shapes["Via12"], left2, bottom2, right2, cell_top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
esd_shapes["Metal1"].insert(pya.Box(left2, bottom2, right2, cell_top))
insert_viarect(
    esd_shapes["Contact"], left2, bottom2, right2, cell_top,
    tech_info.Contact_Width, tech_info.Contact_MinPitch, tech_info.Diffusion_Contact_Enclosure
)
esd_shapes["Diffusion"].insert(pya.Box(left2, bottom2, right2, cell_top))
encl = tech_info.NPlus_Diffusion_Enclosure
left2 -= encl
right2 += encl
bottom2 -= encl
top2 = cell_top + encl
esd_shapes["NPlus"].insert(pya.Box(left2, bottom2, right2, top2))

right = long(0.5*um)
bottom = long(-7*um)
top = long(0*um)
right2 = -182*um
left2 = right2 - 30*um
esd_shapes["Metal3"].insert(pya.Polygon([
    pya.Point(left2, bottom),
    pya.Point(left2, cell_top),
    pya.Point(right2, cell_top),
    pya.Point(right2, top),
    pya.Point(right, top),
    pya.Point(right, bottom),
]))

esd_idx = esd_cell.cell_index()
x = 624*um
y = 2525*um
top_cell.insert(pya.CellInstArray(esd_idx, pya.Trans(x, y)))
x += 7*80*um
top_cell.insert(pya.CellInstArray(esd_idx, pya.Trans(x, y)))
x += 7*80*um
top_cell.insert(pya.CellInstArray(esd_idx, pya.Trans(x, y)))

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write("SWIII_D.oas.gz", saveopts)


print("End")
