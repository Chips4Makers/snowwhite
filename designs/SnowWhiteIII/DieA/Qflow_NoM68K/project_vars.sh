#!/bin/tcsh -f
#------------------------------------------------------------
# project variables for project /home/verhaegs/eda/Chips4Makers/snowwhite/designs/SnowWhiteII/6502Qf/Qflow
#------------------------------------------------------------

# Synthesis command options:
# -------------------------------------------
# set hard_macros =
set yosys_options = "-s ${TOPCELL}.ys"
# set yosys_script =
# set yosys_debug =
# set abc_script =
# set nobuffers =
# set inbuffers =
# set postproc_options =
# set xspice_options =
# set fill_ratios =
# set nofanout =
# set fanout_options = "-l 200 -c 30"
# set source_file_list =
# set is_system_verilog =

# Placement command options:
# -------------------------------------------
set initial_density = 0.55
set graywolf_options = -n
set addspacers_options = "-stripe 3.2 80.0 PG"

# Router command options:
# -------------------------------------------
# set route_show =
# set route_layers =
# set via_use =
# set via_stacks =
# set qrouter_options =

# STA command options:
# -------------------------------------------

# Minimum period of the clock use "--period value" (value in ps)
# set vesta_options = "--period 1E5"
# set run_vesta = 1

# Other options:
# -------------------------------------------
# set migrate_options =
# set lef_options =
# set drc_gdsview =
# set drc_options =
# set gds_options =

#------------------------------------------------------------

