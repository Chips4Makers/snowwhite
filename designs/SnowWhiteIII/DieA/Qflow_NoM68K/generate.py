#!/bin/env python3
import os

from nmigen import *

from plat import *
from top import Top_NoM68K as Top

p = RTLPlatform()
io_count = 92

a_pins = ["a_{}".format(i) for i in range(10)]
a_pins.reverse()
di_pins = ["di_{}".format(i) for i in range(32)]
di_pins.reverse()
do_pins = ["do_{}".format(i) for i in range(32)]
do_pins.reverse()
we_pins = ["we_{}".format(i) for i in range(4)]
we_pins.reverse()

p.add_resources([
    *[Resource("io", i, Pins("io_{}".format(i), dir="io")) for i in range(io_count)],
    Resource("mem", 0,
        Subsignal("addr", Pins(" ".join(a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(do_pins), dir="o")),
        Subsignal("we", Pins(" ".join(we_pins), dir="o")),
        Subsignal("en", Pins("en", dir="o")),
    ),
    Resource("jtag", 0,
        Subsignal("tck", Pins("tck", dir="i")),
        Subsignal("tms", Pins("tms", dir="i")),
        Subsignal("tdo", Pins("tdo", dir="o")),
        Subsignal("tdi", Pins("tdi", dir="i")),
    )
])
ios = [p.request("io", i) for i in range(io_count)]
mem = p.request("mem", 0)
jtag = p.request("jtag", 0)

f = Top(ios, mem, jtag)

plan = p.prepare(f, os.environ["TOPCELL"])
for filename in plan.files:
    f = open("source/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
