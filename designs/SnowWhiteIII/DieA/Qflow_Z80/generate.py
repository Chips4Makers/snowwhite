#!/bin/env python3
import os

from nmigen import *

from plat import *
from top import Top_Z80 as Top

p = RTLPlatform()
io_count = 57

int_a_pins = ["int_a_{}".format(i) for i in range(10)]
int_a_pins.reverse()
int_di_pins = ["int_di_{}".format(i) for i in range(8)]
int_di_pins.reverse()
int_do_pins = ["int_do_{}".format(i) for i in range(8)]
int_do_pins.reverse()

ext_a_pins = ["ext_a_{}".format(i) for i in range(14)]
ext_a_pins.reverse()
ext_di_pins = ["ext_di_{}".format(i) for i in range(8)]
ext_di_pins.reverse()
ext_do_pins = ["ext_do_{}".format(i) for i in range(8)]
ext_do_pins.reverse()

p.add_resources([
    *[
        Resource("io", i,
            Subsignal("i", Pins("io_{}_i".format(i), dir="i")),
            Subsignal("o", Pins("io_{}_o".format(i), dir="o")),
            Subsignal("oe", Pins("io_{}_oe".format(i), dir="o")),
        ) for i in range(io_count)
    ],
    Resource("mem", 0,
        Subsignal("addr", Pins(" ".join(int_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(int_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(int_do_pins), dir="o")),
        Subsignal("we", PinsN("int_we", dir="o")),
        Subsignal("en", Pins("int_en", dir="o")),
    ),
    Resource("mem", 1,
        Subsignal("addr", Pins(" ".join(ext_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(ext_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(ext_do_pins), dir="o")),
        Subsignal("we", Pins("ext_we", dir="o")),
        Subsignal("en", Pins("ext_en", dir="o")),
    ),
    Resource("jtag", 0,
        Subsignal("tck", Pins("tck", dir="i")),
        Subsignal("tms", Pins("tms", dir="i")),
        Subsignal("tdo", Pins("tdo", dir="o")),
        Subsignal("tdi", Pins("tdi", dir="i")),
    ),
    Resource("halt", 0, Pins("halt", dir="i")),
])
ios = [p.request("io", i) for i in range(io_count)]
int_mem = p.request("mem", 0)
ext_mem = p.request("mem", 1)
jtag = p.request("jtag", 0)
halt = p.request("halt", 0)

f = Top(ios, int_mem, ext_mem, jtag, halt)

plan = p.prepare(f, os.environ["TOPCELL"])
for filename in plan.files:
    f = open("source/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
