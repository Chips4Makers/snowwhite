from nmigen import *
from ao68000 import *

class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        m.domains.sync = ClockDomain()
        
        m.submodules.m68k = m68k = ao68000()
        wb = m68k.wishbone

        m.submodules.dump = Instance("dump", i_clk=ClockSignal())

        m.d.comb += [
            ClockSignal().eq(platform.request("clk").i),
            ResetSignal().eq(platform.request("rst").i),
            wb.ack.eq(wb.stb),
            wb.dat_r.eq(Const(69)),
        ]

        return m
            
