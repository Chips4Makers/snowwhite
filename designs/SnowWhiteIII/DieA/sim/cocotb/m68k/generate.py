#!/bin/env python3
from tb import *
from plat import RTLPlatform

p = RTLPlatform()
f = Top()

p.add_file("dump.v",
"""
module dump(input clk);

initial begin
    $dumpfile("test.vcd");
    $dumpvars;
end

endmodule
"""
)

plan = p.prepare(f, name="ao68000_top")
for filename in plan.files:
    f = open("code/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
