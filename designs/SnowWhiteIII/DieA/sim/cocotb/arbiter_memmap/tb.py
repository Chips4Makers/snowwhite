from nmigen import *

from retro_uc import Arbiter, MemoryMap
from wishbone import *

__all__ = ["Top", "tb_outputs"]

tb_outputs = [
    WishboneRTLResource(32, 14,    4, 0, pin_prefix="M68KWB_"),
    WishboneRTLResource( 8, 16, None, 1, pin_prefix="Z80WB_"),
    WishboneRTLResource( 8, 16, None, 2, pin_prefix="MOS6502WB_"),
]
    

class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        m.domains.sync = ClockDomain()

        # Generate a vcd file
        m.submodules.dump = Instance("dump", i_clk=ClockSignal())

        m.submodules.memmap = memmap = MemoryMap()

        m.d.comb += [
            ClockSignal().eq(platform.request("clk").i),
            ResetSignal().eq(platform.request("rst").i),
        ]

        ext_wb = Wishbone(32, 14, 4)
        m68k_wb   , m68k_statements    = Wishbone.request(platform, 0)
        z80_wb    , z80_statements     = Wishbone.request(platform, 1)
        mos6502_wb, mos6502_statements = Wishbone.request(platform, 2)

        m.submodules.arbiter = arbiter = Arbiter(
            ext_wb, m68k_wb, z80_wb, mos6502_wb, memmap.wishbone
        )
        m.d.comb += m68k_statements + z80_statements + mos6502_statements

        return m
            
