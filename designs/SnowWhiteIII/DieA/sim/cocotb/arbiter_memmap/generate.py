#!/bin/env python3
import os

tb_outputs = None

from tb import *
from plat import *

p = RTLPlatform()
f = Top()

if tb_outputs is not None:
    p.add_resources(tb_outputs)

p.add_file("dump.v",
"""
module dump(input clk);

initial begin
    $dumpfile("test.vcd");
    $dumpvars;
end

endmodule
"""
)

plan = p.prepare(f, name=os.environ["TOP_CELL"])
for filename in plan.files:
    f = open("code/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
