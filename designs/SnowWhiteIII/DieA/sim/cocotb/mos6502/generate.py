#!/bin/env python3
from tb import *
from plat import *

p = RTLPlatform()
f = Top()

p.add_file("dump.v",
"""
module dump(input clk);

initial begin
    $dumpfile("test.vcd");
    $dumpvars;
end

endmodule
"""
)

plan = p.prepare(f, name="mos6502_wb")
for filename in plan.files:
    f = open("code/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
