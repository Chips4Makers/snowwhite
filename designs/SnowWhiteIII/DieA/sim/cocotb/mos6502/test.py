import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

# We assume that the T65 core is battle tested so just quick test here to see
# if wishbone wrapper seems to de the right thing
@cocotb.test()
def test01_resetrun(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    dut.rst_0 <= 0

    yield Timer(3*clk_period)

    dut.rst_0 <= 1

    yield Timer(20*clk_period)
