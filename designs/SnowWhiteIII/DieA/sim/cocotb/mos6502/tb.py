from nmigen import *
from t65 import *


class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        m.domains.sync = ClockDomain()
        
        m.submodules.mos6502 = mos6502 = MOS6502_WB()
        wb = mos6502.wishbone

        m.submodules.dump = Instance("dump", i_clk=ClockSignal())

        m.d.comb += [
            ClockSignal().eq(platform.request("clk").i),
            ResetSignal().eq(platform.request("rst").i),
            wb.ack.eq(wb.stb),
            wb.dat_r.eq(Const(69)),
        ]

        return m
            
