import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue
from cocotb.drivers.wishbone import WishboneMaster, WBOp
from cocotb.monitors.wishbone import WishboneSlave

@cocotb.test()
def test01_resetrun(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    dut.rst_0 <= 0

    yield Timer(3*clk_period)

    dut.rst_0 <= 1

    yield Timer(20*clk_period)


@cocotb.test()
def test02_each(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    wb_ext = WishboneMaster(
        dut, "wb_0_", dut.clk_0, timeout=5,
        aliases={"adr": "addr", "datwr": "dat_w", "datrd": "dat_r"}
    )

    wb_m68k = WishboneMaster(
        dut, "wb_1_", dut.clk_0, timeout=5,
        aliases={"adr": "addr", "datwr": "dat_w", "datrd": "dat_r"}
    )
    wb_z80 = WishboneMaster(
        dut, "wb_2_", dut.clk_0, timeout=5,
        aliases={"adr": "addr", "datwr": "dat_w", "datrd": "dat_r"}
    )
    wb_mos6502 = WishboneMaster(
        dut, "wb_3_", dut.clk_0, timeout=5,
        aliases={"adr": "addr", "datwr": "dat_w", "datrd": "dat_r"}
    )
    wb_memmap = WishboneSlave(
        dut, "wb_4_", dut.clk_0, reset_n=dut.rst_0,
        aliases={"adr": "addr", "datwr": "dat_w", "datrd": "dat_r"},
    )

    # Reset
    dut.rst_0 <= 0

    yield Timer(3*clk_period)

    dut.rst_0 <= 1

    yield Timer(3*clk_period)

    # WB cycles
    yield wb_m68k.send_cycle([WBOp(0x0000, 0x03020100), WBOp(0x0001, 0x07060504)])
    yield wb_mos6502.send_cycle([WBOp(0x0), WBOp(0x1, 0x01)])

    # Check priority of M68K over Z80
    cocotb.fork(wb_mos6502.send_cycle([WBOp(0x2)]))
    cocotb.fork(wb_m68k.send_cycle([WBOp(0x0002, 0x0B0A0908)]))

    yield Timer(10*clk_period)

    # Check priority of M68K over Z80 with M68K one clock cycle delay
    cocotb.fork(wb_mos6502.send_cycle([WBOp(0x3), WBOp(0x4)]))
    yield Timer(clk_period)
    cocotb.fork(wb_m68k.send_cycle([WBOp(0x0003, 0x0F0E0D0C)]))

    yield Timer(20*clk_period)
