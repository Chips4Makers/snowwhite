import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue
from cocotb.drivers.wishbone import WishboneMaster, WBOp
from cocotb.monitors.wishbone import WishboneSlave

@cocotb.test()
def test01_resetrun(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    dut.m68k_enable_0 <= 1
    dut.z80_enable_0 <= 0
    dut.mos6502_enable_0 <= 0

    dut.rst_0 <= 0

    yield Timer(int(5.5*clk_period))

    dut.rst_0 <= 1

    yield Timer(30*clk_period)
