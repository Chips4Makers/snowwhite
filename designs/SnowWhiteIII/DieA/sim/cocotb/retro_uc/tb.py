from nmigen import *
from nmigen.build import *

from retro_uc import Retro_uC
from wishbone import *

__all__ = ["Top", "tb_outputs"]

tb_outputs = [
    Resource("m68k_enable", 0, Pins("m68k_enable", dir="i")),
    Resource("z80_enable", 0, Pins("z80_enable", dir="i")),
    Resource("mos6502_enable", 0, Pins("mos6502_enable", dir="i")),
    Resource("io", 0, Pins("io0", dir="io")),
]
    

class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        m.domains.sync = ClockDomain()

        # Generate a vcd file
        m.submodules.dump = Instance("dump", i_clk=ClockSignal())

        io = platform.request("io", 0)
        m.submodules.retro_uc = retro_uc = Retro_uC((io,))

        m.d.comb += [
            ClockSignal().eq(platform.request("clk").i),
            ResetSignal().eq(platform.request("rst").i),
            retro_uc.m68k_enable.eq(platform.request("m68k_enable").i),
            retro_uc.z80_enable.eq(platform.request("z80_enable").i),
            retro_uc.mos6502_enable.eq(platform.request("mos6502_enable").i),
        ]

        return m
            
