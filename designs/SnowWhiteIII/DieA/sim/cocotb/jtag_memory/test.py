import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue
from cocotb.drivers.wishbone import WishboneMaster, WBOp
from cocotb.monitors.wishbone import WishboneSlave

from c4m_jtag import JTAG_Master

@cocotb.test()
def test01_resetrun(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    dut.rst_0 <= 0

    yield Timer(3*clk_period)

    dut.rst_0 <= 1

    yield Timer(20*clk_period)

@cocotb.test()
def test02_idcode(dut):
    """
    Test the IDCODE command
    """

    # Run @ 10MHz
    clk_period = get_sim_steps(100, "ns")
    master = JTAG_Master(dut.jtag_0__TCK, dut.jtag_0__TMS, dut.jtag_0__TDI, dut.jtag_0__TDO, clk_period=clk_period)
    master.IDCODE = [0, 0, 1]

    # Init
    dut.rst_0 <= 0
    dut.clk_0 <= 0

    dut._log.info("Trying to get IDCODE...")

    yield master.idcode()
    result1 = master.result
    dut._log.info("IDCODE1: {}".format(result1))
    assert(result1 == BinaryValue("00000000000000000001100011111111"))

    yield master.idcode()
    result2 = master.result
    dut._log.info("IDCODE2: {}".format(result2))

    assert(result1 == result2)

@cocotb.test()
def test03_memory(dut):
    """
    Test writing/reading to/from memory
    """

    # Run core @ 100 MHz
    clk_period = get_sim_steps(10, "ns")
    cocotb.fork(Clock(dut.clk_0, clk_period).start())
    # Run JTAG @ 50MHz
    tclk_period = get_sim_steps(20, "ns")
    
    master = JTAG_Master(dut.jtag_0__TCK, dut.jtag_0__TMS, dut.jtag_0__TDI, dut.jtag_0__TDO, clk_period=tclk_period)

    MEMADDR = BinaryValue("011", 3)
    MEMRD = BinaryValue("100", 3)
    MEMWR = BinaryValue("101", 3)

    data0 = BinaryValue(0, 8)
    dataFF = BinaryValue("11111111", 8)
    data10 = BinaryValue("10101010", 8)
    data01 = BinaryValue("01010101", 8)

    addr = BinaryValue(256, 10, bigEndian=False)

    yield Timer(2*clk_period)
    dut.rst_0 = 1

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # WRITE => 0 & FFFF
    dut._log.info("Writing data")
    yield master.load_ir(MEMWR)
    yield master.shift_data(data0)
    yield master.shift_data(dataFF)

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 0 & 255; WRITE => 10 & 01
    dut._log.info("Read & write data")
    yield master.load_ir(MEMWR)
    yield master.shift_data(data01)
    assert(master.result == data0)
    yield master.shift_data(data10)
    assert(master.result == dataFF)

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 66 & CC
    dut._log.info("Read data")
    yield master.load_ir(MEMRD)
    yield master.shift_data(data0)
    assert(master.result == data01)
    yield master.shift_data(data0)
    assert(master.result == data10)

    # MEMADDR => 0
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 66 & CC
    dut._log.info("Read data")
    yield master.load_ir(MEMRD)
    yield master.shift_data(data0)
    assert(master.result == data01)
    yield master.shift_data(data0)
    assert(master.result == data10)
