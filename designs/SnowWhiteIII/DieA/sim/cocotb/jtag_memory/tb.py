from nmigen import *
from nmigen.build import *

from jtag import *
from wishbone import *

__all__ = ["Top", "tb_outputs"]

tb_outputs = [
    Resource("jtag", 0,
        Subsignal("TCK", Pins("TCK", dir="i")),
        Subsignal("TMS", Pins("TMS", dir="i")),
        Subsignal("TDI", Pins("TDI", dir="i")),
        Subsignal("TDO", Pins("TDO", dir="o")),
    ),
    Resource("io", 0, Pins("IO0", dir="io")),
    Resource("io", 1, Pins("IO1", dir="io")),
]
    

class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        m.domains.sync = ClockDomain()
        m.d.comb += [
            ClockSignal().eq(platform.request("clk").i),
            ResetSignal().eq(platform.request("rst").i),
        ]

        # Generate a vcd file
        m.submodules.dump = Instance("dump", i_clk=ClockSignal())

        jtag_out = platform.request("jtag")

        ios = 2
        io = [platform.request("io", n) for n in range(ios)]

        m.submodules.jtag = jtag = JTAG(ios)
        m.d.comb += [
            jtag.tck.eq(jtag_out.TCK.i),
            jtag.tms.eq(jtag_out.TMS.i),
            jtag.tdi.eq(jtag_out.TDI.i),
            jtag_out.TDO.o.eq(jtag.tdo),
            *[jtag.pad[n].i.eq(io[n].i) for n in range(ios)],
            *[io[n].o.eq(jtag.pad[n].o) for n in range(ios)],
            *[io[n].oe.eq(jtag.pad[n].oe) for n in range(ios)],
        ]
        
        jtag_wb = jtag.add_wishbone([3, 4, 5], 10, 8)
        m.submodules.mem_wb = mem_wb = WishboneMemory(8, 1024)
        m.d.comb += mem_wb.wishbone.connect(jtag_wb)

        return m
            
