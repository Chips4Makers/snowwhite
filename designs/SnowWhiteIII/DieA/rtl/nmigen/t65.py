import os

from nmigen import *
from nmigen.lib.io import *
from wishbone import Wishbone

__all__ = ["T65", "T65_WB", "MOS6502", "MOS6502_WB", "WDC65C816", "WDC65C816_WB"]

def _add_files(platform):
    if not ".t65_added" in platform.extra_files:
        platform.add_file(".t65_added", "")

        t65vhdldir = os.environ["T65VHDLDIR"]
        
        for fname in [
            "T65_Pack.vhd",
            "T65_MCode.vhd",
            "T65_ALU.vhd",
            "T65.vhd",
        ]:
            f = open("{}/{}".format(t65vhdldir, fname), "r")
            platform.add_file(fname, f)
            f.close()


class T65(Elaboratable):
    def __init__(self):
        self.mode = Pin(2, "i") # 00: 6502, 01: 65C02, 10: 65C816
        # Signal for different cpus, all signals are valid though
        # independent of mode
        # 6502 and 65C816
        self.ready = Pin(1, "i", reset=1)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.addr = Pin(24, "o")
        self.data = Pin(8, "io")
        # 6502/65C02
        self.set_overflow = Pin(1, "i") # SO
        self.sync = Pin(1, "o")
        # 65C02/65C816
        self.vector_pull = Pin(1, "o") # VP
        self.mem_lock = Pin(1, "o") # ML
        # To be implemented on higher level: BE
        # 65C816
        self.valid_addr = Pin(2, "o") # {VDA, VPA}
        # Unsupported: Abort_n, M/X, E
        # Extra pins
        self.ce = Pin(1, "i", reset=1)

    def elaborate(self, platform):
        _add_files(platform)

        m = Module()

        reset_n = Signal()
        irq_n = Signal()
        nmi_n = Signal()
        so_n = Signal()
        r_w_n = Signal()
        ml_n = Signal()
        vp_n = Signal()

        m.d.comb += [
            reset_n.eq(~ResetSignal()),
            irq_n.eq(~self.irq.i),
            nmi_n.eq(~self.nmi.i),
            so_n.eq(~self.set_overflow.i),
            self.data.oe.eq(~r_w_n),
            self.mem_lock.o.eq(~ml_n),
            self.vector_pull.o.eq(~vp_n),
        ]
        m.submodules._t65 = Instance("T65",
            i_Mode = self.mode.i,
            i_Res_n = reset_n,
            i_Enable = self.ce.i,
            i_Clk = ClockSignal(),
            i_Rdy = self.ready.i,
            i_IRQ_n = irq_n,
            i_NMI_n = nmi_n,
            i_SO_n = so_n,
            o_R_W_n = r_w_n,
            o_Sync = self.sync.o,
            o_ML_n = ml_n,
            o_VP_n = vp_n,
            o_VDA = self.valid_addr.o[0],
            o_VPA = self.valid_addr.o[1],
            o_A = self.addr.o,
            i_DI = self.data.i,
            o_DO = self.data.o,
        )

        return m

class T65_WB(Elaboratable):
    def __init__(self):
        self.mode = Pin(2, "i") # 00: 6502, 01: 65C02, 10: 65C816
        tags = (
            ("sync", 1, "DOWN"),
            ("set_overflow", 1, "UP"), # SO
            ("vector_pull", 1, "DOWN"), # VP
            ("valid_addr", 2, "DOWN"), # {VDA, VPA}
        )
        self.wishbone = Wishbone(8, 24, master=True, tags=tags)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.ce = Pin(1, "i", reset=1)

    def elaborate(self, platform):
        wb = self.wishbone

        m = Module()

        m.submodules._t65 = t65 = T65()

        we = t65.data.oe
        m.d.comb += [
            t65.mode.i.eq(self.mode.i),
            t65.irq.i.eq(self.irq.i),
            t65.nmi.i.eq(self.nmi.i),
            wb.addr.eq(t65.addr.o),
            wb.dat_w.eq(t65.data.o),
            t65.data.i.eq(wb.dat_r),
            wb.we.eq(we),
            t65.set_overflow.i.eq(wb.set_overflow),
            wb.sync.eq(t65.sync.o),
            wb.vector_pull.eq(t65.vector_pull.o),
            wb.lock.eq(t65.mem_lock.o),
            wb.valid_addr.eq(t65.valid_addr.o),
            t65.ce.i.eq(self.ce.i),
        ]

        # Do read or write cycle each clock cycle
        # Let CPU wait when stall is high or no ack is present after first cycle.
        first = Signal(reset=1)
        m.d.sync += first.eq(Const(0))
        m.d.comb += [
            t65.ready.i.eq(~wb.stall & (first | wb.ack)),
            wb.cyc.eq(Const(1)),
            wb.stb.eq(Const(1)),
        ]

        return m


class MOS6502(Elaboratable):
    def __init__(self, *, cmos=False):
        self.ready = Pin(1, "i", reset=1)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.addr = Pin(16, "o")
        self.data = Pin(8, "io")
        self.set_overflow = Pin(1, "i")
        self.sync = Pin(1, "o")
        if cmos: # WDC65C02 version
            self.vector_pull = Pin(1, "o")
            self.mem_lock = Pin(1, "o")
            # To be implemented on higher level: BE
        self.ce = Pin(1, "i", reset=1)

        ##

        self._cmos = cmos

    def elaborate(self, platform):
        m = Module()

        m.submodules._t65 = t65 = T65()

        m.d.comb += [
            t65.mode.i.eq(Const(0b00, 2) if not self._cmos else Const(0b01, 2)),
            t65.ready.i.eq(self.ready.i),
            t65.irq.i.eq(self.irq.i),
            t65.nmi.i.eq(self.nmi.i),
            self.addr.o.eq(t65.addr.o[:16]),
            t65.data.i.eq(self.data.i),
            self.data.o.eq(t65.data.o),
            self.data.oe.eq(t65.data.oe),
            t65.set_overflow.i.eq(self.set_overflow.i),
            self.sync.o.eq(t65.sync.o),
            t65.ce.i.eq(self.ce.i),
        ]
        if self._cmos:
            m.d.comb += [
                self.vector_pull.o.eq(t65.vector_pull.o),
                self.mem_lock.o.eq(t65.mem_lock.o),
            ]

        return m

class MOS6502_WB(Elaboratable):
    def __init__(self, *, cmos=False):
        tags = (
            ("sync", 1, "DOWN"),
            ("set_overflow", 1, "UP"), # SO
        )
        if cmos:
            tags.extend((
                ("vector_pull", 1, "DOWN"), # VP
            ))
        self.wishbone = Wishbone(8, 16, master=True, tags=tags)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.ce = Pin(1, "i", reset=1)

        ##

        self._cmos = cmos

    def elaborate(self, platform):
        wb = self.wishbone

        m = Module()

        m.submodules._t65 = t65 = T65_WB()

        m.d.comb += [
            wb.sync.eq(t65.wishbone.sync),
            t65.wishbone.set_overflow.eq(wb.set_overflow),
            wb.addr.eq(t65.wishbone.addr[:16]),
            t65.wishbone.dat_r.eq(wb.dat_r),
            wb.dat_w.eq(t65.wishbone.dat_w),
            wb.we.eq(t65.wishbone.we),
            t65.wishbone.ack.eq(wb.ack),
            wb.cyc.eq(t65.wishbone.cyc),
            wb.stb.eq(t65.wishbone.stb),
            t65.mode.i.eq(Const(0b00, 2) if not self._cmos else Const(0b01, 2)),
            t65.irq.i.eq(self.irq.i),
            t65.nmi.i.eq(self.nmi.i),
            t65.ce.i.eq(self.ce.i),
        ]
        if self._cmos:
            m.d.comb += wb.vector_pull.eq(t65.wishbone.vector_pull)

        return m


class WDC65C816(Elaboratable):
    def __init__(self):
        self.ready = Pin(1, "i", reset=1)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        # 24 bit address, no multiplexing on d most significant byte
        self.addr = Pin(24, "o")
        self.data = Pin(8, "io")
        self.vector_pull = Pin(1, "o") # VP
        self.mem_lock = Pin(1, "o") # ML
        self.valid_addr = Pin(2, "o") # {VDA, VPA}
        # To be implemented on higher level: BE
        # Unsupported: M/X, E
        self.ce = Pin(1, "i", reset=1)

    def elaborate(self, platform):
        m = Module()

        m.submodules._t65 = t65 = T65()

        m.d.comb += [
            t65.mode.i.eq(Const(0b10, 2)),
            t65.ready.i.eq(self.ready.i),
            t65.irq.i.eq(self.irq.i),
            t65.nmi.i.eq(self.nmi.i),
            self.addr.o.eq(t65.addr.o),
            t65.data.i.eq(self.data.i),
            self.data.o.eq(t65.data.o),
            self.data.oe.eq(t65.data.oe),
            self.vector_pull.o.eq(t65.vector_pull.o),
            self.mem_lock.o.eq(t65.mem_lock.o),
            self.valid_addr.o.eq(t65.valid_addr.o),
            t65.ce.i.eq(self.ce.i),
        ]

        return m

class WDC65C816_WB(Elaboratable):
    def __init__(self):
        tags = (
            ("sync", 1, "DOWN"),
            ("set_overflow", 1, "UP"), # SO
            ("vector_pull", 1, "DOWN"), # VP
            ("valid_addr", 2, "DOWN"), #  {VDA, VPA}
        )
        self.wishbone = Wishbone(8, 24, master=True, tags=tags)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.ce = Pin(1, "i", reset=1)

    def elaborate(self, platform):
        wb = self.wishbone

        m = Module()

        m.submodules._t65 = t65 = T65_WB()

        m.d.comb += [
            wb.sync.eq(t65.wishbone.sync),
            t65.wishbone.set_overflow.eq(wb.set_overflow),
            wb.vector_pull.eq(t65.wishbone.vector_pull),
            wb.vector_pull.eq(t65.wishbone.vector_pull),
            wb.valid_addr.eq(t65.wishbone.valid_addr),
            wb.addr.eq(t65.wishbone.addr),
            t65.wishbone.dat_r.eq(wb.dat_r),
            wb.dat_w.eq(t65.wishbone.dat_w),
            wb.we.eq(t65.wishbone.we),
            t65.wishbone.ack.eq(wb.ack),
            wb.cyc.eq(t65.wishbone.cyc),
            wb.stb.eq(t65.wishbone.stb),
            t65.mode.i.eq(Const(0b10, 2)),
            t65.irq.i.eq(self.irq.i),
            t65.nmi.i.eq(self.nmi.i),
            t65.ce.i.eq(self.ce.i),
        ]

        return m
