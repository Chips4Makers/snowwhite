import os

from nmigen import *
from nmigen.lib.io import *

from wishbone import *

__all__ = ["ao68000"]

def _add_files(platform):
    if not ".ao68000_added" in platform.extra_files:
        platform.add_file(".ao68000_added", "")

        ao68000verilogdir = os.environ["AO68000VERILOGDIR"]
        
        for fname in [
            "alu_mult_generic.v",
            "memory_registers_generic.v",
            "ao68000_microcode_b",
            "ao68000.v",
        ]:
            f = open("{}/{}".format(ao68000verilogdir, fname), "r")
            platform.add_file(fname, f)
            f.close()


class ao68000(Elaboratable):
    def __init__(self, *, with_stall=False):
        tags = (
            ("sgl", 1, "DOWN"),
            ("blk", 1, "DOWN"),
            ("rmw", 1, "DOWN"),
            ("fc",  3, "DOWN"),
            ("ipl", 3, "UP"),
        )
        self.wishbone = Wishbone(32, 30, 4,
            master=True, with_stall=with_stall, registered_feedback=True, tags=tags
        )

    
    def elaborate(self, platform):
        _add_files(platform)

        m = Module()

        wb = self.wishbone
        stb = Signal()

        m.submodules.m68k = m68k = Instance("ao68000",
            i_CLK_I = ClockSignal(),
            i_reset_n = ~ResetSignal(),
            o_CYC_O = wb.cyc,
            o_ADR_O = wb.addr,
            o_DAT_O = wb.dat_w,
            i_DAT_I = wb.dat_r,
            o_SEL_O = wb.sel,
            o_STB_O = stb,
            o_WE_O = wb.we,
            i_ACK_I = wb.ack,
            i_ERR_I = wb.err,
            i_RTY_I = wb.rty,
            o_SGL_O = wb.sgl,
            o_BLK_O = wb.blk,
            o_RMW_O = wb.rmw,
            o_CTI_O = wb.cti,
            o_BTE_O = wb.bte,
            o_fc_o = wb.fc,
            i_ipl_i = wb.ipl,
        )
        # Lock bus in either block burst or rmw cycle
        m.d.comb += wb.lock.eq(wb.blk | wb.rmw)

        # Implement stall generation
        if not hasattr(wb, "stall"):
            m.d.comb += wb.stb.eq(stb)
        else:
            with m.FSM():
                with m.State("IDLE"):
                    m.d.comb += wb.stb.eq(stb)
                    with m.If(wb.stb & ~wb.stall):
                        m.next = "WAIT4ACK"
                with m.State("WAIT4ACK"):
                    m.d.comb += wb.stb.eq(0)
                    with m.If(wb.ack | wb.err | wb.rty):
                        m.next = "IDLE"

        return m
