from nmigen import *

from wishbone import WishboneSRAM
from jtag import JTAG
from retro_uc import Retro_uC_NoM68K as Retro_uC, Arbiter8, MemoryMap8
from wishbone import Wishbone
from t80 import Z80_WB
from t65 import MOS6502_WB


class Top(Elaboratable):
    """This is top cell specific for SnowWhite III die A"""

    def __init__(self, ios, io_mem, io_jtag):
        self.ios = ios
        self.io_mem = io_mem
        self.io_jtag = io_jtag


    def elaborate(self, platform):
        m = Module()

        m.submodules.onchipmem = onchipmem = WishboneSRAM(32, 10, 4)
        m.d.comb += [
            self.io_mem.addr.o.eq(onchipmem.addr),
            onchipmem.d.i.eq(self.io_mem.di.i),
            self.io_mem.do.o.eq(onchipmem.d.o),
            self.io_mem.we.eq(onchipmem.we),
            self.io_mem.ce.o.eq(onchipmem.ce),
        ]

        io_count = len(self.ios)
        m.submodules.jtag = jtag = JTAG(io_count, part_number=Const(0b1000, 16))
        m.d.comb += [
            jtag.tck.eq(self.io_jtag.tck.i),
            jtag.tms.eq(self.io_jtag.tms.i),
            self.io_jtag.tdo.o.eq(jtag.tdo),
            jtag.tdi.eq(self.io_jtag.tdi.i),
        ]
        for i in range(io_count):
            io = self.ios[i]
            pad = jtag.pad[i]
            m.d.comb += [
                io.o.eq(pad.o),
                pad.i.eq(io.i),
                io.oe.eq(pad.oe),
            ]
        jtag_wb = jtag.add_wishbone([3, 4, 5], 14, 32, 4)
        
        m.submodules.retro_uc = retro_uc = Retro_uC(
            jtag.core, ext_wishbone=jtag_wb, onchipmem_wb=onchipmem.wishbone
        )

        return m


class Top_NoM68K(Elaboratable):
    """This is top cell specific for SnowWhite III die A"""

    def __init__(self, ios, io_mem, io_jtag):
        self.ios = ios
        self.io_mem = io_mem
        self.io_jtag = io_jtag


    def elaborate(self, platform):
        m = Module()

        m.submodules.onchipmem = onchipmem = WishboneSRAM(32, 10, 4)
        m.d.comb += [
            self.io_mem.addr.o.eq(onchipmem.addr),
            onchipmem.d.i.eq(self.io_mem.di.i),
            self.io_mem.do.o.eq(onchipmem.d.o),
            self.io_mem.we.o.eq(onchipmem.we),
            self.io_mem.en.o.eq(onchipmem.ce),
        ]

        io_count = len(self.ios)
        m.submodules.jtag = jtag = JTAG(io_count, part_number=Const(0b1000, 16))
        m.d.comb += [
            jtag.tck.eq(self.io_jtag.tck.i),
            jtag.tms.eq(self.io_jtag.tms.i),
            self.io_jtag.tdo.o.eq(jtag.tdo),
            jtag.tdi.eq(self.io_jtag.tdi.i),
        ]
        for i in range(io_count):
            io = self.ios[i]
            pad = jtag.pad[i]
            m.d.comb += [
                io.o.eq(pad.o),
                pad.i.eq(io.i),
                io.oe.eq(pad.oe),
            ]
        jtag_wb = jtag.add_wishbone([3, 4, 5], 14, 32, 4)
        
        m.submodules.retro_uc = retro_uc = Retro_uC(
            jtag.core, ext_wishbone=jtag_wb, onchipmem_wb=onchipmem.wishbone
        )

        return m


class Top_Z80(Elaboratable):
    """Top with only the Z80"""

    def __init__(self, ios, io_int_mem, io_ext_mem, io_jtag, io_halt):
        self.ios = ios
        self.io_int_mem = io_int_mem
        self.io_ext_mem = io_ext_mem
        self.io_jtag = io_jtag
        self.io_halt = io_halt

    def elaborate(self, platform):
        m = Module()

        io_count = len(self.ios)

        m.submodules.z80 = z80 = Z80_WB(with_ce=True)
        m.submodules.int_mem_port = int_mem_port = WishboneSRAM(8, 10)
        int_mem_wb = int_mem_port.wishbone
        m.submodules.ext_mem_port = ext_mem_port = WishboneSRAM(8, 14)
        ext_mem_wb = ext_mem_port.wishbone
        mem_wb = Wishbone(8,15)
        m.submodules.jtag = jtag = JTAG(io_count, part_number=Const(0b1000, 16))
        jtag_wb = jtag.add_wishbone([3, 4, 5], 16, 8)
        m.submodules.memmap = memmap = MemoryMap8(jtag.core, mem_wb=mem_wb)
        m.submodules.arbiter = arbiter = Arbiter8(jtag_wb, z80.wishbone, memmap.wishbone)

        # Connect halt
        m.d.comb += z80.ce.i.eq(self.io_halt.i)

        # Connect ios
        for i, io in enumerate(self.ios):
            pad_pin = jtag.pad[i]
            m.d.comb += [
                pad_pin.i.eq(io.i.i),
                io.o.o.eq(pad_pin.o),
                io.oe.o.eq(pad_pin.oe),
            ]
                
        # Connect int/ext_mem_wb tp mem_wb; we ack always a cycle; no error/retry support
        int_mem_cycle = Signal()
        ext_mem_cycle = Signal()
        m.d.comb += [
            int_mem_cycle.eq(mem_wb.addr[10:] == Const(0b10000,5)),
            ext_mem_cycle.eq(~mem_wb.addr[14:]),

            int_mem_wb.addr.eq(mem_wb.addr[:10]),
            ext_mem_wb.addr.eq(mem_wb.addr[:14]),
            int_mem_wb.dat_w.eq(mem_wb.dat_w),
            ext_mem_wb.dat_w.eq(mem_wb.dat_w),
            int_mem_wb.we.eq(mem_wb.we & int_mem_cycle),
            ext_mem_wb.we.eq(mem_wb.we & ext_mem_cycle),
            int_mem_wb.cyc.eq(mem_wb.cyc & int_mem_cycle),
            ext_mem_wb.cyc.eq(mem_wb.cyc & ext_mem_cycle),
            int_mem_wb.stb.eq(mem_wb.stb & int_mem_cycle),
            ext_mem_wb.stb.eq(mem_wb.stb & ext_mem_cycle),
            int_mem_wb.lock.eq(mem_wb.lock & int_mem_cycle),
            ext_mem_wb.lock.eq(mem_wb.lock & ext_mem_cycle),
            mem_wb.err.eq(0),
            mem_wb.rty.eq(0),
            mem_wb.stall.eq(0),
        ]
        int_mem_cycle_hold = Signal()
        m.d.sync += [
            mem_wb.ack.eq(mem_wb.cyc & mem_wb.ack),
            int_mem_cycle_hold.eq(int_mem_cycle),
        ]
        with m.If(int_mem_cycle_hold):
            m.d.comb += mem_wb.dat_r.eq(int_mem_wb.dat_r)
        with m.Else():
            m.d.comb += mem_wb.dat_r.eq(ext_mem_wb.dat_r)

        # Connect int_mem_wb
        m.d.comb += [
            self.io_int_mem.addr.o.eq(int_mem_port.addr),
            int_mem_port.d.i.eq(self.io_int_mem.di.i),
            self.io_int_mem.do.o.eq(int_mem_port.d.o),
            self.io_int_mem.we.o.eq(int_mem_port.we),
            self.io_int_mem.en.o.eq(int_mem_port.ce),
        ]

        # Connect ext_mem_wb
        m.d.comb += [
            self.io_ext_mem.addr.o.eq(ext_mem_port.addr),
            ext_mem_port.d.i.eq(self.io_ext_mem.di.i),
            self.io_ext_mem.do.o.eq(ext_mem_port.d.o),
            self.io_ext_mem.we.o.eq(ext_mem_port.we),
            self.io_ext_mem.en.o.eq(ext_mem_port.ce),
        ]

        # Connect jtag
        m.d.comb += [
            jtag.tck.eq(self.io_jtag.tck.i),
            jtag.tms.eq(self.io_jtag.tms.i),
            self.io_jtag.tdo.o.eq(jtag.tdo),
            jtag.tdi.eq(self.io_jtag.tdi.i),
        ]

        return m


class Top_MOS6502(Elaboratable):
    """Top with only the MOS6502"""

    def __init__(self, ios, io_int_mem, io_ext_mem, io_jtag, io_halt):
        self.ios = ios
        self.io_int_mem = io_int_mem
        self.io_ext_mem = io_ext_mem
        self.io_jtag = io_jtag
        self.io_halt = io_halt

    def elaborate(self, platform):
        m = Module()

        io_count = len(self.ios)

        m.submodules.mos6502 = mos6502 = MOS6502_WB()
        m.submodules.int_mem_port = int_mem_port = WishboneSRAM(8, 10)
        int_mem_wb = int_mem_port.wishbone
        m.submodules.ext_mem_port = ext_mem_port = WishboneSRAM(8, 14)
        ext_mem_wb = ext_mem_port.wishbone
        mem_wb = Wishbone(8,15)
        m.submodules.jtag = jtag = JTAG(io_count, part_number=Const(0b1000, 16))
        jtag_wb = jtag.add_wishbone([3, 4, 5], 16, 8)
        m.submodules.memmap = memmap = MemoryMap8(jtag.core, mem_wb=mem_wb)
        m.submodules.arbiter = arbiter = Arbiter8(jtag_wb, mos6502.wishbone, memmap.wishbone)

        # Connect halt
        m.d.comb += mos6502.ce.i.eq(~self.io_halt.i)

        # Connect ios
        for i, io in enumerate(self.ios):
            pad_pin = jtag.pad[i]
            m.d.comb += [
                pad_pin.i.eq(io.i.i),
                io.o.o.eq(pad_pin.o),
                io.oe.o.eq(pad_pin.oe),
            ]
                
        # Connect int/ext_mem_wb tp mem_wb; we ack always a cycle; no error/retry support
        int_mem_cycle = Signal()
        ext_mem_cycle = Signal()
        m.d.comb += [
            int_mem_cycle.eq(mem_wb.addr[10:] == Const(0b00000,5)),
            ext_mem_cycle.eq(mem_wb.addr[14:]),

            int_mem_wb.addr.eq(mem_wb.addr[:10]),
            ext_mem_wb.addr.eq(mem_wb.addr[:14]),
            int_mem_wb.dat_w.eq(mem_wb.dat_w),
            ext_mem_wb.dat_w.eq(mem_wb.dat_w),
            int_mem_wb.we.eq(mem_wb.we & int_mem_cycle),
            ext_mem_wb.we.eq(mem_wb.we & ext_mem_cycle),
            int_mem_wb.cyc.eq(mem_wb.cyc & int_mem_cycle),
            ext_mem_wb.cyc.eq(mem_wb.cyc & ext_mem_cycle),
            int_mem_wb.stb.eq(mem_wb.stb & int_mem_cycle),
            ext_mem_wb.stb.eq(mem_wb.stb & ext_mem_cycle),
            int_mem_wb.lock.eq(mem_wb.lock & int_mem_cycle),
            ext_mem_wb.lock.eq(mem_wb.lock & ext_mem_cycle),
            mem_wb.err.eq(0),
            mem_wb.rty.eq(0),
            mem_wb.stall.eq(0),
        ]
        int_mem_cycle_hold = Signal()
        m.d.sync += [
            mem_wb.ack.eq(mem_wb.cyc & mem_wb.ack),
            int_mem_cycle_hold.eq(int_mem_cycle),
        ]
        with m.If(int_mem_cycle_hold):
            m.d.comb += mem_wb.dat_r.eq(int_mem_wb.dat_r)
        with m.Else():
            m.d.comb += mem_wb.dat_r.eq(ext_mem_wb.dat_r)

        # Connect int_mem_wb
        m.d.comb += [
            self.io_int_mem.addr.o.eq(int_mem_port.addr),
            int_mem_port.d.i.eq(self.io_int_mem.di.i),
            self.io_int_mem.do.o.eq(int_mem_port.d.o),
            self.io_int_mem.we.o.eq(int_mem_port.we),
            self.io_int_mem.en.o.eq(int_mem_port.ce),
        ]

        # Connect ext_mem_wb
        m.d.comb += [
            self.io_ext_mem.addr.o.eq(ext_mem_port.addr),
            ext_mem_port.d.i.eq(self.io_ext_mem.di.i),
            self.io_ext_mem.do.o.eq(ext_mem_port.d.o),
            self.io_ext_mem.we.o.eq(ext_mem_port.we),
            self.io_ext_mem.en.o.eq(ext_mem_port.ce),
        ]

        # Connect jtag
        m.d.comb += [
            jtag.tck.eq(self.io_jtag.tck.i),
            jtag.tms.eq(self.io_jtag.tms.i),
            self.io_jtag.tdo.o.eq(jtag.tdo),
            jtag.tdi.eq(self.io_jtag.tdi.i),
        ]

        return m
