import os

from nmigen import *
from nmigen.lib.io import *

from wishbone import Wishbone

__all__ = ["T80", "T80_WB", "Z80", "Z80_WB"]

def _add_files(platform):
    if not ".t80_added" in platform.extra_files:
        platform.add_file(".t80_added", "")

        t80vhdldir = os.environ["T80VHDLDIR"]
        
        for fname in [
            "T80_Pack.vhd",
            "T80_Reg.vhd",
            "T80_MCode.vhd",
            "T80_ALU.vhd",
            "T80.vhd",
            "T80al.vhd",
            "T80s.vhd",
            "T80se.vhd",
        ]:
            f = open("{}/{}".format(t80vhdldir, fname), "r")
            platform.add_file(fname, f)
            f.close()


class T80(Elaboratable):
    MODE_Z80 = 0
    MODE_FASTZ80 = 1
    MODE_8080 = 2
    MODE_GB = 3

    def __init__(self, mode=MODE_Z80, *, sync=True, with_ce=False, iowait=True):
        self.wait = Pin(1, "i")
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        self.bus_req = Pin(1, "i")
        self.bus_ack = Pin(1, "o")
        self.m1 = Pin(1, "o")
        self.mem_req = Pin(1, "o")
        self.io_req = Pin(1, "o")
        self.read = Pin(1, "o")
        self.write = Pin(1, "o")
        self.refresh = Pin(1, "o")
        self.halt = Pin(1, "o")
        self.a = Pin(16, "o")
        self.d = Pin(8, "io")
        if with_ce:
            self.ce = Pin(1, "i", reset=1)

        ##

        self._mode = mode
        self._sync = sync
        self._with_ce = with_ce
        self._iowait = iowait

    def elaborate(self, platform):
        _add_files(platform)

        m = Module()

        reset_n = Signal(reset_less=True)
        wait_n = Signal(reset_less=True)
        int_n = Signal(reset_less=True)
        nmi_n = Signal(reset_less=True)
        busrq_n = Signal(reset_less=True)
        m1_n = Signal(reset_less=True)
        mreq_n = Signal(reset_less=True)
        iorq_n = Signal(reset_less=True)
        rd_n = Signal(reset_less=True)
        wr_n = Signal(reset_less=True)
        rfsh_n = Signal(reset_less = True)
        halt_n = Signal(reset_less = True)
        busak_n = Signal(reset_less = True)
        m.d.comb += [
            reset_n.eq(~ResetSignal()),
            wait_n.eq(~self.wait.i),
            int_n.eq(~self.irq.i),
            nmi_n.eq(~self.nmi.i),
            busrq_n.eq(~self.bus_req.i),
            self.m1.o.eq(~m1_n),
            self.mem_req.o.eq(~mreq_n),
            self.io_req.o.eq(~iorq_n),
            self.read.o.eq(~rd_n),
            self.write.o.eq(~wr_n),
            self.refresh.o.eq(~rfsh_n),
            self.halt.o.eq(~halt_n),
            self.bus_ack.o.eq(~busak_n),
            self.d.oe.eq(self.write.o),
        ]

        if self._sync:
            if self._with_ce:
                cell = "T80se"
            else:
                cell = "T80s"
        else:
            assert(not self._with_ce)
            cell = "T80al"

        args = [
            ("p", "Mode", self._mode),
            ("p", "IOWait", 1 if self._iowait else 0),
            ("i", "RESET_n", reset_n),
            ("i", "CLK_n", ClockSignal()),
            ("i", "WAIT_n", wait_n),
            ("i", "INT_n", int_n),
            ("i", "NMI_n", nmi_n),
            ("i", "BUSRQ_n", busrq_n),
            ("o", "M1_n", m1_n),
            ("o", "MREQ_n", mreq_n),
            ("o", "IORQ_n", iorq_n),
            ("o", "RD_n", rd_n),
            ("o", "WR_n", wr_n),
            ("o", "RFSH_n", rfsh_n),
            ("o", "HALT_n", halt_n),
            ("o", "BUSAK_n", busak_n),
            ("o", "A", self.a.o),
            ("i", "DI", self.d.i),
            ("o", "DO", self.d.o),
        ]
        if self._with_ce:
            args.append(("i", "CLKEN", self.ce.i))

        m.submodules.t80 = Instance(cell, *args)

        return m

class T80_WB(Elaboratable):
    def __init__(self, mode=T80.MODE_Z80, *, with_ce=False, iowait=True):
        tags = (
            ("m1", 1, "DOWN"),
            ("io_req", 1, "DOWN"),
            ("mem_req", 1, "DOWN"),
            ("halt", 1, "DOWN"),
        )
        self.wishbone = Wishbone(8, 16, master=True, tags=tags)
        self.irq = Pin(1, "i")
        self.nmi = Pin(1, "i")
        if with_ce:
            self.ce = Pin(1, "i")

        ##

        self._mode = mode
        self._with_ce = with_ce
        self._iowait = iowait

    def elaborate(self, platform):
        wb = self.wishbone

        m = Module()

        m.submodules._t80 = t80 = T80(mode=self._mode, with_ce=self._with_ce, iowait=self._iowait)

        m.d.comb += [
            t80.irq.i.eq(self.irq.i),
            t80.nmi.i.eq(self.nmi.i),
            t80.bus_req.i.eq(0),
            wb.m1.eq(t80.m1.o),
            wb.mem_req.eq(t80.mem_req.o),
            wb.io_req.eq(t80.io_req.o),
            wb.we.eq(t80.write.o),
            wb.halt.eq(t80.halt.o),
            wb.addr.eq(t80.a.o),
            t80.d.i.eq(wb.dat_r),
            wb.dat_w.eq(t80.d.o),
        ]
        if self._with_ce:
            m.d.comb += t80.ce.i.eq(self.ce.i)

        # Do a Wishbone cycle when read or write and not refresh
        cyc = (t80.read.o | t80.write.o) & ~t80.refresh & ~t80.halt
        # wait when Wishbone is stalled or a Wishbone cycle was done in previous cycle
        # and no ack is received yet.
        cyc_prev = Signal(reset = 0)
        m.d.sync += cyc_prev.eq(cyc & ~wb.stall)
        wait = wb.stall | (cyc_prev & ~wb.ack)
        m.d.comb += [
            t80.wait.i.eq(wait),
            wb.cyc.eq(cyc | wait | wb.ack),
            wb.stb.eq(cyc),
        ]

        return m


def Z80(fast=False, sync=True, with_ce=False, iowait=True):
    mode = T80.MODE_Z80 if not fast else T80.MODE_FASTZ80
    return T80(mode, sync=sync, with_ce=with_ce, iowait=iowait)

def Z80_WB(fast=False, with_ce=False, iowait=True):
    mode = T80.MODE_Z80 if not fast else T80.MODE_FASTZ80
    return T80_WB(mode, with_ce=with_ce, iowait=iowait)
