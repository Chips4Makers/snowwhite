export TOPDIR := $(realpath ../../../..)
include $(TOPDIR)/Makefile.conf

export TOPCELL := Retro_uC

.PHONY: all
all: $(TOPCELL).gds signoff


# Generate source files with nmigen
NMIGENDIR = $(realpath ../rtl/nmigen)

ifeq ($(PYTHONPATH),)
  PYTHONPATH := $(NMIGENDIR):$(realpath ../sim/cocotb)
else
  PYTHONPATH := $(PYTHONPATH):$(NMIGENDIR):$(realpath ../sim/cocotb)
endif
export PYTHONPATH

SOURCE_FILES := \
  source/alu_mult_generic.v \
  source/memory_registers_generic.v \
  source/ao68000_microcode_b \
  source/ao68000.v \
  source/.ao68000_added \
  source/T80_Pack.vhd \
  source/T80_ALU.vhd \
  source/T80_Reg.vhd \
  source/T80_MCode.vhd \
  source/T80.vhd \
  source/T80s.vhd \
  source/T80se.vhd \
  source/T80al.vhd \
  source/.t80_added \
  source/T65_Pack.vhd \
  source/T65_ALU.vhd \
  source/T65_MCode.vhd \
  source/T65.vhd \
  source/.t65_added \
  source/c4m_jtag_pkg.vhdl \
  source/c4m_jtag_idblock.vhdl \
  source/c4m_jtag_iocell.vhdl \
  source/c4m_jtag_ioblock.vhdl \
  source/c4m_jtag_irblock.vhdl \
  source/c4m_jtag_tap_fsm.vhdl \
  source/c4m_jtag_tap_controller.vhdl \
  source/build_Retro_uC.sh \
  source/Retro_uC.il \
  source/Retro_uC.v


.PHONY: rtl
rtl: $(SOURCE_FILES)

$(SOURCE_FILES): generate_all

.INTERMEDIATE: generate_all
generate_all: generate.py ../sim/cocotb/plat.py
	@echo "Assembling the RTL files"
	@mkdir -p source
	@./generate.py
	@cp $(TOPCELL).ys source

clean::
	rm -f $(SOURCE_FILES)


# Qflow
QFLOWTOP := $(TOPCELL)
QFLOWTECH := osu035_redm4

include $(TOPDIR)/util/Qflow/Makefile.Qflow

synthesis/$(TOPCELL).blif: $(SOURCE_FILES)


# Convert gds
GDSIN := layout/$(TOPCELL).gds
GDSOUT := $(TOPCELL).gds

include $(TOPDIR)/util/tech/priv/TSMC_CL035G/convert_SCMOS/Makefile.convert
