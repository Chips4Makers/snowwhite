#!/bin/env python3
from nmigen import *
from nmigen_boards.atlys import AtlysPlatform

from t80 import Z80_WB

class Z80_test(Elaboratable):
    """A test for t80.
    This is a non-functional test so won't be programmed on the board.
    Just see if no errors are seen during synthesis and P&R"""

    def __init__(self, clk_name="clk"):
        self._clk_name = clk_name

    def elaborate(self, platform):
        m = Module()

        clk = platform.request(self._clk_name)
        m.domains.sync = ClockDomain()
        m.d.comb += ClockSignal().eq(clk.i)

        m.submodules.z80 = z80 = Z80_WB(with_ce=True)
        # Divide memory in 1KB pages
        page = Signal(6)
        m.d.comb += page.eq(z80.wishbone.addr[10:])

        leds = []
        for n in range(8):
            try:
                leds.append(platform.request("led", n))
            except ResourceError:
                break
        leds = Cat(leds[n].o if n < len(leds) else Signal() for n in range(8))
        leds_n = len(leds)
        with m.If((page == Const(1, 6)) & z80.wishbone.we):
            m.d.sync += leds.eq(z80.wishbone.dat_w)

        mem = Memory(8, 1024, simulate=False)
        ismempage = Signal()
        m.d.comb += ismempage.eq(page == Const(0, 6))
        m.submodules.readport = readport = mem.read_port("comb")
        m.submodules.writeport = writeport = mem.write_port()
        m.d.comb += writeport.en.eq(ismempage & z80.wishbone.we)

        m.d.comb += [
            readport.addr.eq(z80.wishbone.addr[:10]),
            writeport.addr.eq(z80.wishbone.addr[:10]),
            writeport.data.eq(z80.wishbone.dat_w),
        ]
        with m.If(~z80.wishbone.we):
            m.d.sync += z80.wishbone.dat_r.eq(readport.data)

        return m


p = AtlysPlatform()
test = Z80_test(clk_name="clk100")

products = p.build(test, build_dir="build_z80")
