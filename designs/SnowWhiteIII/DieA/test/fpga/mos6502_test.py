#!/bin/env python3
import itertools

# nMigen
from nmigen import *
from nmigen.build import *
from nmigen_boards.blackice import BlackIcePlatform
from nmigen_boards.arty_a7 import ArtyA7Platform
from nmigen_boards.atlys import AtlysPlatform

from t65 import MOS6502, WDC65C816_WB


class MOS6502_test(Elaboratable):
    """A test for t65.
    This is a non-functional test so won't be programmed on the board.
    Just see if no errors are seen during synthesis and P&R"""

    def __init__(self, clk_name="clk"):
        self._clk_name = clk_name

    def elaborate(self, platform):
        m = Module()

        clk = platform.request(self._clk_name)
        m.domains.sync = ClockDomain()
        m.d.comb += ClockSignal().eq(clk.i)

        use6502 = platform.request("switch", 0).i # Selects between 6502 and 65816
        usemem = platform.request("switch", 1).i # Selects between mem and ddr2

        m.submodules.mos6502 = mos6502 = MOS6502()
        m.submodules.wdc65c816 = wdc65c816 = WDC65C816_WB()

        leds = []
        for n in range(8):
            try:
                leds.append(platform.request("led", n))
            except ResourceError:
                break
        leds = Cat(leds[n].o if n < len(leds) else Signal() for n in range(8))
        leds_n = len(leds)
        with m.If(use6502):
            m.d.comb += leds.eq(mos6502.data.o)
        with m.Else():
            m.d.comb += leds.eq(wdc65c816.wishbone.dat_w)

        ddr2 = platform.request("ddr2")
        m.d.comb += ddr2.clk.o.eq(ClockSignal())

        mem = Memory(8, 1024, simulate=False)
        m.submodules.readport = readport = mem.read_port()
        m.submodules.writeport = writeport = mem.write_port()

        a = Signal(10)
        with m.If(use6502):
            m.d.comb += a.eq(mos6502.addr.o[:10])
        with m.Else():
            m.d.comb += a.eq(wdc65c816.wishbone.addr[:10])
        m.d.comb += [
            readport.addr.eq(a),
            writeport.addr.eq(a),
            ddr2.a.o.eq(Cat(a, Const(0,3))),
        ]

        dat_r = Signal(8)
        with m.If(usemem):
            m.d.comb += dat_r.eq(readport.data)
        with m.Else():
            m.d.comb += dat_r.eq(ddr2.dq.i[:8])
        m.d.comb += [
            mos6502.data.i.eq(dat_r),
            wdc65c816.wishbone.dat_r.eq(dat_r),
        ]

        dat_w = Signal(8)
        with m.If(use6502):
            m.d.comb += dat_w.eq(mos6502.data.o)
        with m.Else():
            m.d.comb += dat_w.eq(wdc65c816.wishbone.dat_w)
        m.d.comb += [
            writeport.data.eq(dat_w),
            ddr2.dq.o.eq(Cat(dat_w, Const(0,8))),
        ]

        we = Signal()
        with m.If(use6502):
            m.d.comb += we.eq(mos6502.data.oe)
        with m.Else():
            m.d.comb += we.eq(wdc65c816.wishbone.we)
        with m.If(usemem):
            m.d.comb += [
                writeport.en.eq(we),
                ddr2.we.o.eq(Const(0)),
            ]
        with m.Else():
            m.d.comb += [
                writeport.en.eq(Const(0)),
                ddr2.we.o.eq(we),
            ]

        return m


p = AtlysPlatform()
test = MOS6502_test(clk_name="clk100")

#products = p.build(test, do_program=True)
products = p.build(test, build_dir="build_mos6502")
