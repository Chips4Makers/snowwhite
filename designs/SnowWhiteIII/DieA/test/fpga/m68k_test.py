#!/bin/env python3
from traceback import print_tb

from nmigen import *
from nmigen_boards.blackice import BlackIcePlatform
from nmigen_boards.atlys import AtlysPlatform

from ao68000 import ao68000

class M68K_test(Elaboratable):
    """A test for ao68000.
    This is a non-functional test so won't be programmed on the board.
    Just see if no errors are seen during synthesis and P&R"""

    def __init__(self, clk_name="clk"):
        self._clk_name = clk_name

    def elaborate(self, platform):
        m = Module()

        reset = platform.request("button")
        clk = platform.request(self._clk_name)
        m.domains.sync = sync = ClockDomain()
        m.d.comb += [
            sync.clk.eq(clk),
            sync.rst.eq(reset),
        ]

        m.submodules.m68k = m68k = ao68000()
        wb = m68k.wishbone

        stb_s = Signal()
        m.d.sync += stb_s.eq(wb.stb)
        wecyc = wb.cyc & wb.stb & wb.we

        if isinstance(platform, BlackIcePlatform):
            sram = platform.request("sram")
            m.d.comb += [
                sram.a.eq(wb.addr[:len(sram.a)]),
                sram.d.o.eq(wb.dat_w[:16]),
                wb.dat_r[:16].eq(sram.d.i),
                wb.dat_r[16:].eq(Const(0, 16)),
                sram.d.oe.eq(wecyc),
                sram.we.eq(wecyc),
                wb.ack.eq(stb_s & wb.cyc & wb.stb),
            ]
        elif isinstance(platform, AtlysPlatform):
            ddr2 = platform.request("ddr2")
            m.d.comb += [
                ddr2.clk.o.eq(ClockSignal()),
                ddr2.clk_en.o.eq(Const(1)),
                ddr2.we.o.eq(wecyc),
                ddr2.a.o.eq(wb.addr[:13]),
                wb.dat_r[:16].eq(ddr2.dq.i),
                wb.dat_r[16:].eq(Const(0)),
                ddr2.dq.o.eq(wb.dat_w[:16]),
                ddr2.dq.oe.eq(wecyc),
                wb.ack.eq(stb_s & wb.cyc & wb.stb),
            ]
        else:
            raise "Unsupported platform"

        return m

test = M68K_test("clk100")

p = AtlysPlatform()
try:
    products = p.build(test, build_dir="build_m68k_atlys")
except Exception as e:
    print("NOK: build_m68k failed to build for Atlys")
    print_tb(e)

p = BlackIcePlatform()
try:
    products = p.build(test, build_dir="build_m68k_blackice")
except Exception as e:
    print("OK: build_m68k failed to build for BlackIcePlatform, is known to be too big")
    print_tb(e)
