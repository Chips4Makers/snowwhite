#!/bin/env python3
import itertools

# nMigen
from nmigen import *
from nmigen.build import *
from nmigen_boards.blackice import BlackIcePlatform
from nmigen_boards.arty_a7 import ArtyA7Platform
from nmigen_boards.atlys import AtlysPlatform

from jtag import JTAG, PmodJTAGSlaveResource


class JTAG_test(Elaboratable):
    def __init__(self, jtag_num=None):
        ##

        self._jtag_num = jtag_num

    def elaborate(self, platform):
        m = Module()

        leds = []
        for n in itertools.count():
            try:
                leds.append(platform.request("led", n))
            except ResourceError:
                break
        leds = Cat(led.o for led in leds)
        leds_n = len(leds)

        _args = ("jtag_s",)
        if self._jtag_num is not None:
            _args.append(self._jtag_num)
        pads_jtag = platform.request(*_args)
        
        m.submodules.jtag = jtag = JTAG(leds_n)
        m.d.comb += [
            jtag.tck.eq(pads_jtag.TCK.i),
            jtag.tms.eq(pads_jtag.TMS.i),
            pads_jtag.TDO.o.eq(jtag.tdo),
            jtag.tdi.eq(pads_jtag.TDI.i),
            *[leds[i].eq(jtag.pad[i].o) for i in range(leds_n)],
        ]

        return m


#p = BlackIcePlatform()
#p = ArtyA7Platform()
#p.add_resources([PmodJTAGSlaveResource("jtag_s", 0, pmod=0)])

p = AtlysPlatform(JP12="3V3")
res = PmodJTAGSlaveResource("jtag_s", 0,
    pmod=0, attrs=Attrs(IOSTANDARD=AtlysPlatform.bank2_iostandard, CLOCK_DEDICATED_ROUTE="FALSE")
)
p.add_resources([res])

test = JTAG_test()
products = p.build(test, do_program=True, build_dir="build_jtag")
