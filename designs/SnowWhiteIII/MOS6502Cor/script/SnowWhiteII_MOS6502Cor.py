print("Begin")


import pya

TText = pya.Shape.TText
TTextRef = pya.Shape.TTextRef

tech = pya.Technology.technology_by_name("TSMC_CL035")

lib_MOS6502Cor = pya.Library()
lib_MOS6502Cor.register("MOS6502Cor")
lay_MOS6502Cor = lib_MOS6502Cor.layout()
lay_MOS6502Cor.read("/home/verhaegs/coriolis-2.x/src/alliance-check-toolkit/benchs/VexRiscv/TSMC_CL035G/vexriscv_clocked_r.gds", tech.load_layout_options)
cell_MOS6502Cor = lay_MOS6502Cor.cell("vexriscv_clocked_r")

lib_IORing = pya.Library()
lib_IORing.register("IORing")
lay_IORing = lib_IORing.layout()
lay_IORing.read("../../common/script/SnowWhiteII_IORing.gds", tech.load_layout_options)
io_pdiff_idx = lay_IORing.find_layer(11, 0)
if io_pdiff_idx is not None:
  lay_IORing.delete_layer(io_pdiff_idx)
cell_IORing = lay_IORing.cell("SnowWhiteII_IORing")


lay_SWII_MOS6502Cor = pya.Layout()
top_cell = lay_SWII_MOS6502Cor.create_cell("SnowWhiteII_MOS6502Cor")

idx_MOS6502Cor = lay_SWII_MOS6502Cor.add_lib_cell(lib_MOS6502Cor, cell_MOS6502Cor.cell_index())
x_MOS6502Cor = 700000
y_MOS6502Cor = 700000
inst_MOS6502Cor = pya.CellInstArray(idx_MOS6502Cor, pya.Trans(x_MOS6502Cor,y_MOS6502Cor))
top_cell.insert(inst_MOS6502Cor)

idx_IORing = lay_SWII_MOS6502Cor.add_lib_cell(lib_IORing, cell_IORing.cell_index())
inst_IORing = pya.CellInstArray(idx_IORing, pya.Trans(0,0))
top_cell.insert(inst_IORing)

m2_idx = lay_MOS6502Cor.find_layer(18,0)
shapes = cell_MOS6502Cor.shapes(m2_idx)
# for shape in shapes.each():
#   t = shape.type()
#   if t == TText or t == TTextRef:
#     pos = shape.text_pos
#     print(pins[shape.text_string])


saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
top_cell.write("SnowWhiteII_MOS6502Cor.gds.gz", saveopts)


print("End")
