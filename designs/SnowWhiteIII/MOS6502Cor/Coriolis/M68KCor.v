/*
 * Bare wrapper around code mainly to give module the right name
 */

`include "memory_registers_generic.v"
`include "alu_mult_generic.v"
`include "ao68000_l.v"

module M68KCor(
    //****************** WISHBONE
    input           clk,
    input           reset,

    output          CYC_O,
    output  [31:2]  ADR_O,
    output  [31:0]  DAT_O,
    input   [31:0]  DAT_I,
    output  [3:0]   SEL_O,
    output          STB_O,
    output          WE_O,

    input           ACK_I,
    input           ERR_I,
    input           RTY_I,

    // TAG_TYPE: TGC_O
    output          SGL_O,
    output          BLK_O,
    output          RMW_O,

    // TAG_TYPE: TGA_O
    output [2:0]    CTI_O,
    output [1:0]    BTE_O,

    // TAG_TYPE: TGC_O
    output [2:0]    fc_o,
    
    //****************** OTHER
    /* interrupt acknowlege:
     * ACK_I: interrupt vector on DAT_I[7:0]
     * ERR_I: spurious interrupt
     * RTY_I: autovector
     */
    input [2:0]     ipl_i,
    output          reset_o,
    output          blocked_o           //% \copydoc blocked_o
    );

ao68000 M68K(
    .CLK_I(clk), .reset_n(reset), .CYC_O(CYC_O), .ADR_O(ADR_O), .DAT_O(DAT_O), .DAT_I(DAT_I),
    .SEL_O(SEL_O), .STB_O(STB_O), .WE_O(WE_O), .ACK_I(ACK_I), .ERR_I(ERR_I), .RTY_I(RTY_I),
    .SGL_O(SGL_O), .BLK_O(BLK_O), .RMW_O(RMW_O), .CTI_O(CTI_O), .BTE_O(BTE_O), .fc_o(fc_o),
    .ipl_i(ipl_i), .reset_o(reset_o), .blocked_o(blocked_o)
);

endmodule // MOS6502Qf
