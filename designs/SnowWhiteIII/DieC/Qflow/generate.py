#!/bin/env python3
import os

from nmigen import *
from nmigen.build import *

from vexriscv import VexRiscv
from plat import *

p = RTLPlatform()

def get_pins(prefix, num):
    pins = ["{}{}".format(prefix, i) for i in range(num)]
    pins.reverse()
    return " ".join(pins)

# We get all 30 address lines but on connect as much as we can to IO pins
addr_pins = get_pins("addr_", 30)
di_pins = get_pins("di_", 32)
do_pins = get_pins("do_", 32)
we_pins = get_pins("we_", 4)

p.add_resources([
    Resource("mem", 0,
        Subsignal("addr", Pins(addr_pins, dir="o")),
        Subsignal("di", Pins(di_pins, dir="i")),
        Subsignal("do", Pins(do_pins, dir="o")),
        Subsignal("we", Pins(we_pins, dir="o")),
        Subsignal("mem_en", Pins("mem_en", dir="o")),
    ),
    Resource("halt", 0, Pins("halt", dir="i")),
    Resource("irq", 0, Pins("irq", dir="i")),
])


class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        # Generate a vcd file
        m.submodules.vexriscv = vexriscv = VexRiscv()

        mem = platform.request("mem", 0)
        halt = platform.request("halt", 0)
        irq = platform.request("irq", 0)
        m.d.comb += [
            mem.addr.o.eq(vexriscv.addr),
            vexriscv.di.eq(mem.di.i),
            mem.do.o.eq(vexriscv.do),
            mem.we.o.eq(vexriscv.we),
            mem.mem_en.o.eq(vexriscv.ce),
            vexriscv.halt.eq(halt.i),
            vexriscv.interrupt.eq(irq.i),
        ]

        return m
            

f = Top()

plan = p.prepare(f, os.environ["TOPCELL"])
for filename in plan.files:
    f = open("source/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
