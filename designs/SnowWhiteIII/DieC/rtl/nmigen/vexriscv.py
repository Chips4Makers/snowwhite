import os

from nmigen import *
from wishbone import Wishbone

__all__ = ["VexRiscv"]

def _add_files(platform):
    if not ".vexriscv_added" in platform.extra_files:
        platform.add_file(".vexriscv_added", "")

        d = os.environ["VEXRISCVVERILOGDIR"]
        
        f = open("{}/VexRiscv_Min.v".format(d), "r")
        platform.add_file("VexRiscv.v", f)
        f.close()


class VexRiscv(Elaboratable):
    """VexRiscv with an SRAM interface"""

    def __init__(self):
        self.addr = Signal(30)
        self.di = Signal(32)
        self.do = Signal(32)
        self.we = Signal(4)
        self.ce = Signal()
        self.halt = Signal()
        self.interrupt = Signal()

    def elaborate(self, platform):
        _add_files(platform)

        m = Module()

        # TODO: support registered feedback
        ibus_wb = Wishbone(32, 30, 4, master=True, with_stall=False)
        dbus_wb = Wishbone(32, 30, 4, master=True, with_stall=False)

        interrupts = Signal(32)
        m.d.comb += interrupts.eq(Cat(self.interrupt, Const(0, 31)))

        m.submodules.vexriscv = Instance("VexRiscv",
            i_externalResetVector = Const(0, 32),
            i_timerInterrupt = Const(0),
            i_softwareInterrupt = Const(0),
            i_externalInterruptArray = interrupts,
            o_iBusWishbone_CYC = ibus_wb.cyc,
            o_iBusWishbone_STB = ibus_wb.stb,
            i_iBusWishbone_ACK = ibus_wb.ack,
            o_iBusWishbone_WE = ibus_wb.we,
            o_iBusWishbone_ADR = ibus_wb.addr,
            i_iBusWishbone_DAT_MISO = ibus_wb.dat_r,
            o_iBusWishbone_DAT_MOSI = ibus_wb.dat_w,
            o_iBusWishbone_SEL = ibus_wb.sel,
            i_iBusWishbone_ERR = ibus_wb.err,
            o_dBusWishbone_CYC = dbus_wb.cyc,
            o_dBusWishbone_STB = dbus_wb.stb,
            i_dBusWishbone_ACK = dbus_wb.ack,
            o_dBusWishbone_WE = dbus_wb.we,
            o_dBusWishbone_ADR = dbus_wb.addr,
            i_dBusWishbone_DAT_MISO = dbus_wb.dat_r,
            o_dBusWishbone_DAT_MOSI = dbus_wb.dat_w,
            o_dBusWishbone_SEL = dbus_wb.sel,
            i_dBusWishbone_ERR = dbus_wb.err,
            i_clk = ClockSignal(),
            i_reset = ResetSignal(),
        );

        ibus_we_sel = Signal(4)
        dbus_we_sel = Signal(4)

        m.d.comb += [
            ibus_wb.dat_r.eq(self.di),
            dbus_wb.dat_r.eq(self.di),
            ibus_we_sel.eq(Cat(ibus_wb.we & sel for sel in ibus_wb.sel)),
            dbus_we_sel.eq(Cat(dbus_wb.we & sel for sel in dbus_wb.sel)),
        ]
        with m.FSM():
            with m.State("IDLE"):
                m.d.comb += [
                    ibus_wb.ack.eq(0),
                    dbus_wb.ack.eq(0),
                ]
                with m.If(ibus_wb.cyc & ibus_wb.stb):
                    m.d.comb += [
                        self.addr.eq(ibus_wb.addr),
                        self.do.eq(ibus_wb.dat_w),
                        self.we.eq(ibus_we_sel),
                        self.ce.eq(1),
                    ]
                    m.next = "IBUS"
                with m.Elif(dbus_wb.cyc & dbus_wb.stb):
                    m.d.comb += [
                        self.addr.eq(ibus_wb.addr),
                        self.do.eq(dbus_wb.dat_w),
                        self.we.eq(dbus_we_sel),
                        self.ce.eq(1),
                    ]
                    m.next = "DBUS"
                with m.Else():
                    m.d.comb += [
                        self.addr.eq(0),
                        self.do.eq(0),
                        self.we.eq(0),
                        self.ce.eq(0),
                    ]
            with m.State("IBUS"):
                m.d.comb += [
                    ibus_wb.ack.eq(~self.halt),
                    dbus_wb.ack.eq(0),
                    self.addr.eq(ibus_wb.addr),
                    self.do.eq(ibus_wb.dat_w),
                    self.we.eq(ibus_we_sel),
                    self.ce.eq(1),
                ]
                with m.If(~(ibus_wb.cyc & ibus_wb.stb)):
                    m.next = "IDLE"
            with m.State("DBUS"):
                m.d.comb += [
                    ibus_wb.ack.eq(0),
                    dbus_wb.ack.eq(~self.halt),
                    self.addr.eq(dbus_wb.addr),
                    self.do.eq(dbus_wb.dat_w),
                    self.we.eq(dbus_we_sel),
                    self.ce.eq(1),
                ]
                with m.If(~(dbus_wb.cyc & dbus_wb.stb)):
                    m.next = "IDLE"
            
        return m
