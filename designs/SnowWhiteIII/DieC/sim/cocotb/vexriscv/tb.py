from nmigen import *
from nmigen.build import *

from vexriscv import VexRiscv
from wishbone import *

__all__ = ["Top", "tb_outputs"]

tb_outputs = [
    Resource("halt", 0, Pins("halt", dir="i")),
]
    

class Top(Elaboratable):
    def elaborate(self, platform):
        m = Module()

        # Generate a vcd file
        m.submodules.dump = Instance("dump", i_clk=ClockSignal())
        m.submodules.vexriscv = vexriscv = VexRiscv()

        halt = platform.request("halt", 0)
        m.d.comb += vexriscv.halt.eq(halt.i)

        return m
            
