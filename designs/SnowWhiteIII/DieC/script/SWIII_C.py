print("Begin")


import pya
import TSMC_CL035G as tech_info
from C4MLib import IORing, CellAdder, get_shapes, get_indices

um = 1000L

tech = pya.Technology.technology_by_name("TSMC_CL035")
layout = pya.Layout()
layout.read("../../DieB/script/SWIII_B.gds.gz")

top_cell = layout.cell("SWIII_B")
top_cell.name = "SWIII_C"

saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write("SWIII_C.gds.gz", saveopts)


print("End")
