print("Begin")

import os

import pya
import TSMC_CL035G as tech_info
from C4MLib import *

um = 1000L

tech = pya.Technology.technology_by_name("TSMC_CL035")
layout = pya.Layout()
top_cell = layout.create_cell("SWIII_B")


C4M_cells = CellAdder(
    "c4m_cells",
    "{}/layout/C4MLib.oas.gz".format(os.environ["C4MLIBDIR"]),
    layout,
)


#
print("Core")
#

# We first add the logic cell, then fix DRC with size over/under
# and then do the rest
retrouc_cells = CellAdder(
    "Retro_uC",
    "../Qflow_MOS6502/Retro_uC.gds",
    layout,
)
retrouc_x = 400
retrouc_y = 500
top_cell.insert(pya.CellInstArray(retrouc_cells["Retro_uC"], pya.Trans(retrouc_x*um, retrouc_y*um)))

retrouc_cell = retrouc_cells.lib_layout.cell("Retro_uC")
layer_names = ("Metal2", "Metal3", "Metal4")
retrouc_shapes = get_shapes(tech_info, layout, retrouc_cell, layer_names)
retrouc_pins = {}
for layer in layer_names:
    for shape in retrouc_shapes[layer].each():
        t = shape.text
        if t:
            retrouc_pins[t.string] = (retrouc_x*um + t.x, retrouc_y*um + t.y)

# Fix CLKBUF1 as it will be inserted later
layer_names = ("Poly",)
clkbuf1_cell = layout.cell("CLKBUF1")
clkbuf1_shapes = get_shapes(tech_info, layout, clkbuf1_cell, layer_names)
bottom = long(7.2*um)
top = 8*um
left = long(1.4*um)
right = long(3.375*um)
clkbuf1_shapes["Poly"].insert(pya.Box(left, bottom, right, top))
left = long(4.625*um)
right = long(6.575*um)
clkbuf1_shapes["Poly"].insert(pya.Box(left, bottom, right, top))
left = long(7.825*um)
right = long(9.775*um)
clkbuf1_shapes["Poly"].insert(pya.Box(left, bottom, right, top))
left = long(10.825*um)
right = long(12.975*um)
clkbuf1_shapes["Poly"].insert(pya.Box(left, bottom, right, top))


layer_names = (
    "NPlus", "N3V", "PPlus", "Poly",
    "Contact", "Metal1", "Via12", "Metal2", "Via23", "Metal3", "Via34", "Metal4",
    "Temp",
)
top_indices = get_indices(tech_info, layout, top_cell, layer_names)
top_shapes = get_shapes(tech_info, layout, top_cell, layer_names)

shapeproc = pya.ShapeProcessor()

# Size N3V 0.3um over/under
shapeproc.size(
  layout, top_cell, top_indices["N3V"], top_shapes["Temp"], 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Temp"], -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  layout, top_cell, top_indices["Temp"],
  layout, top_cell, top_indices["N3V"],
  top_shapes["N3V"], pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size PIMP 0.3um over/under
shapeproc.size(
  layout, top_cell, top_indices["PPlus"], top_shapes["Temp"], 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Temp"], -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  layout, top_cell, top_indices["Temp"],
  layout, top_cell, top_indices["PPlus"],
  top_shapes["PPlus"], pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size NIMP 0.3um over/under
shapeproc.size(
  layout, top_cell, top_indices["NPlus"], top_shapes["Temp"], 300L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Temp"], -300L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  layout, top_cell, top_indices["Temp"],
  layout, top_cell, top_indices["NPlus"],
  top_shapes["NPlus"], pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Enforce contact-poly enclosure of 0.2um, avoid minimal space violation
shapeproc.boolean(
  layout, top_cell, top_indices["Poly"],
  layout, top_cell, top_indices["Contact"],
  top_shapes["Temp"], pya.EdgeProcessor.ModeAnd,
  True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Poly"], 200L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Poly"], top_shapes["Temp"], 220L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Temp"], -220L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
top_shapes["Poly"].clear()
shapeproc.boolean(
  layout, top_cell, top_indices["Temp"],
  layout, top_cell, top_indices["Poly"],
  top_shapes["Poly"], pya.EdgeProcessor.ModeANotB,
  True, True, False
)
# Size Metal1 0.220um over/under
shapeproc.size(
  layout, top_cell, top_indices["Metal1"], top_shapes["Temp"], 220L,
  pya.EdgeProcessor.ModeOr, True, True, False
)
shapeproc.size(
  layout, top_cell, top_indices["Temp"], top_shapes["Temp"], -220L,
  pya.EdgeProcessor.ModeOr, False, True, False
)
shapeproc.boolean(
  layout, top_cell, top_indices["Temp"],
  layout, top_cell, top_indices["Metal1"],
  top_shapes["Metal1"], pya.EdgeProcessor.ModeANotB,
  True, True, False
)

layout.delete_layer(layout.layer(tech_info.Temp, 0))

#
print("IORing")
#

ioring_cell = IORing(tech, tech_info, layout, "IORing")
ioring_inst = pya.CellInstArray(ioring_cell.cell_index(), pya.Trans(0,0))
top_cell.insert(ioring_inst)

# We will draw Metal1/Metal4 connections to pin inside IORing
ioring_shapes = get_shapes(tech_info, layout, ioring_cell, ("Metal1", "Metal4"))

#
print("SRAM")
#

sram_cells = CellAdder(
    "SRAM",
    "{}/sram/gds/tm035ssram1024x8_210a/tm035ssram1024x8_210a.gds".format(os.environ["TSMC_TM035SSRAM1024X8"]),
    layout,
)
sram_x = 1800
sram_y = 600
top_cell.insert(pya.CellInstArray(sram_cells["TM035SSRAM1024X8"], pya.Trans(sram_x*um, sram_y*um)))
top_cell.insert(pya.CellInstArray(C4M_cells["SRAMfix"], pya.Trans(sram_x*um, sram_y*um)))
top_cell.insert(pya.CellInstArray(C4M_cells["SRAMN3V"], pya.Trans(sram_x*um, sram_y*um)))

sram_cell = sram_cells.lib_layout.cell("TM035SSRAM1024X8")
layer_names = ("Metal2_Pin",)
sram_shapes = get_shapes(tech_info, sram_cells.lib_layout, sram_cell, layer_names)
sram_pins = {}
for shape in sram_shapes["Metal2_Pin"].each():
    t = shape.text
    if t and t.string != "VDD":
        sram_pins[t.string] = (sram_x*um + t.x, sram_y*um + t.y)

#
print("Connecting pins")
#

conn_width = 3*um/2
conn_space = 8*um/10
conn_pitch = conn_width + conn_space

# Connect the bottom pins
# TODO: make IORing class where these values can stored.
io_width = 80*um
io_x = 374*um
io_y = 193*um
conn_top = (retrouc_y - 10)*um
conn_bottom = conn_top - conn_width
pins = (
    ("input", "clk_0"),
    ("input", "rst_0"),
    ("output", "mem_1__addr[0]"),
    ("output", "mem_1__addr[1]"),
    ("output", "mem_1__addr[2]"),
    ("output", "mem_1__addr[3]"),
    ("output", "mem_1__addr[4]"),
    ("output", "mem_1__addr[5]"),
    ("output", "mem_1__addr[6]"),
    ("output", "mem_1__addr[7]"),
    ("output", "mem_1__addr[8]"),
    ("output", "mem_1__addr[9]"),
    ("output", "mem_1__addr[10]"),
    ("output", "mem_1__addr[11]"),
    ("output", "mem_1__addr[12]"),
    ("output", "mem_1__addr[13]"),
    ("input", "mem_1__di[0]"),
    ("input", "mem_1__di[1]"),
    ("input", "mem_1__di[2]"),
    ("input", "mem_1__di[3]"),
    ("input", "mem_1__di[4]"),
    ("input", "mem_1__di[5]"),
    ("input", "mem_1__di[6]"),
    ("input", "mem_1__di[7]"),
)
for i, (direction, name) in enumerate(pins):
    core_pin_x, core_pin_y = retrouc_pins[name]
    if direction == "input":
        io_pin_left = io_x + IO_TRI5VT_dims["C_x"]
        io_pin_right = io_pin_left + IO_TRI5VT_dims["C_width"]
        pin_spec = (
            ("PE", 0),
            ("PS", 0),
            ("IE", 1),
            ("SE", 1 if name in ("clk_0", "rst_0") else 0),
            ("DS", 0),
            ("I", 0),
            ("OEN", 1),
        )
    elif direction == "output":
        io_pin_left = io_x + IO_TRI5VT_dims["I_x"]
        io_pin_right = io_pin_left + IO_TRI5VT_dims["I_width"]
        pin_spec = (
            ("PE", 0),
            ("PS", 0),
            ("IE", 0),
            ("SE", 0),
            ("DS", 1),
            ("OEN", 0),
        )
    config_io(i, "bottom", pin_spec, ioring_shapes)

    left = core_pin_x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = core_pin_y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
    if io_pin_right < right:
        bottom = conn_bottom - i*conn_pitch
        left2 = io_pin_left
        right2 = io_pin_right
        bottom2 = io_y
        top2 = bottom + conn_width
        top_shapes["Metal3"].insert(pya.Polygon([
            pya.Point(left, top),
            pya.Point(right, top),
            pya.Point(right, bottom),
            pya.Point(right2, bottom),
            pya.Point(right2, bottom2), 
            pya.Point(left2, bottom2),
            pya.Point(left2, top2),
            pya.Point(left, top2),
        ]))
    else:
        bottom = conn_bottom - (len(pins) - i - 1)*conn_pitch
        left2 = io_pin_left
        right2 = io_pin_right
        bottom2 = io_y
        top2 = bottom + conn_width
        top_shapes["Metal3"].insert(pya.Polygon([
            pya.Point(left, top),
            pya.Point(right, top),
            pya.Point(right, top2),
            pya.Point(right2, top2),
            pya.Point(right2, bottom2),
            pya.Point(left2, bottom2),
            pya.Point(left2, bottom),
            pya.Point(left, bottom),
        ]))
    if name == "clk_0":
        clk_left = left
        clk_bottom = bottom
        clk_right = right
        clk_top = top2

    io_x += io_width


# Connect the right pins
core_conn_left = (retrouc_x + 1170)*um
core_conn_right = core_conn_left + conn_width
sram_addr_conn_bottom = (sram_y + 640)*um
sram_addr_conn_top = sram_addr_conn_bottom + conn_width
sram_d_conn_top = (sram_y - 10)*um
sram_d_conn_bottom = sram_d_conn_top - conn_width
sram_pin_width = 2*um
io_conn_left = (sram_x + 470)*um
io_conn_right = io_conn_left + conn_width

# SRAM d pins
sram_d_pins = (
    ("mem_0__di[7]", "DIN[7]"),
    ("mem_0__do[7]", "DOUT[7]"),
    ("mem_0__di[6]", "DIN[6]"),
    ("mem_0__do[6]", "DOUT[6]"),
    ("mem_0__di[5]", "DIN[5]"),
    ("mem_0__do[5]", "DOUT[5]"),
    ("mem_0__di[4]", "DIN[4]"),
    ("mem_0__do[4]", "DOUT[4]"),
    ("mem_0__we", "WEB"),
    ("mem_0__do[3]", "DOUT[3]"),
    ("mem_0__di[3]", "DIN[3]"),
    ("mem_0__do[2]", "DOUT[2]"),
    ("mem_0__di[2]", "DIN[2]"),
    ("mem_0__do[1]", "DOUT[1]"),
    ("mem_0__di[1]", "DIN[1]"),
    ("mem_0__do[0]", "DOUT[0]"),
    ("mem_0__di[0]", "DIN[0]"),
    # Don't connect mem_0__en
)
sram_d_lanes = len(sram_d_pins)
core_down_lane = 0
core_up_lane = core_down_lane + len(sram_d_pins) - 1
sram_lane = sram_d_lanes - 1
for core_pin, sram_pin in sram_d_pins:
    core_pin_x, core_pin_y = retrouc_pins[core_pin]
    sram_pin_x, sram_pin_y = sram_pins[sram_pin]

    left = core_pin_x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = core_pin_y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
    top2 = sram_d_conn_top - sram_lane*conn_pitch
    bottom2 = top2 - conn_width
    right3 = sram_pin_x + sram_pin_width/2
    left3 = right3 - sram_pin_width
    top3 = sram_pin_y
    if bottom < bottom2:
        left2 = core_conn_left + core_up_lane*conn_pitch
        core_up_lane -= 1
        right2 = left2 + conn_width
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, top),
            pya.Point(left2, top),
            pya.Point(left2, top2),
            pya.Point(left3, top2),
            pya.Point(left3, top3),
            pya.Point(right3, top3),
            pya.Point(right3, bottom2),
            pya.Point(right2, bottom2),
            pya.Point(right2, bottom),
        ]))
    else:
        left2 = core_conn_left + core_down_lane*conn_pitch
        core_down_lane += 1
        right2 = left2 + conn_width
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, top),
            pya.Point(right2, top),
            pya.Point(right2, top2),
            pya.Point(left3, top2),
            pya.Point(left3, top3),
            pya.Point(right3, top3),
            pya.Point(right3, bottom2),
            pya.Point(left2, bottom2),
            pya.Point(left2, bottom),
        ]))

    sram_lane -= 1

# SRAM address pins
sram_addr_pins = (
    ("mem_0__addr[0]", "A[0]"),
    ("mem_0__addr[1]", "A[1]"),
    ("mem_0__addr[6]", "A[6]"),
    ("mem_0__addr[5]", "A[5]"),
    ("mem_0__addr[4]", "A[4]"),
    ("mem_0__addr[7]", "A[7]"),
    ("mem_0__addr[8]", "A[8]"),
    ("mem_0__addr[9]", "A[9]"),
    ("mem_0__addr[2]", "A[2]"),
    ("mem_0__addr[3]", "A[3]"),
)
sram_addr_lanes = len(sram_addr_pins)
core_down_lane = sram_d_lanes
core_up_lane = core_down_lane + sram_addr_lanes - 1
sram_lane = 0
for core_pin, sram_pin in sram_addr_pins:
    core_pin_x, core_pin_y = retrouc_pins[core_pin]
    sram_pin_x, sram_pin_y = sram_pins[sram_pin]

    left = core_pin_x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = core_pin_y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
    top2 = sram_addr_conn_top + sram_lane*conn_pitch
    bottom2 = top2 - conn_width
    right3 = sram_pin_x + sram_pin_width/2
    left3 = right3 - sram_pin_width
    bottom3 = sram_pin_y
    if bottom < bottom2:
        left2 = core_conn_left + core_up_lane*conn_pitch
        core_up_lane -= 1
        right2 = left2 + conn_width
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, top),
            pya.Point(left2, top),
            pya.Point(left2, top2),
            pya.Point(right3, top2),
            pya.Point(right3, bottom3),
            pya.Point(left3, bottom3),
            pya.Point(left3, bottom2),
            pya.Point(right2, bottom2),
            pya.Point(right2, bottom),
        ]))
    else:
        left2 = core_conn_left + core_down_lane*conn_pitch
        core_down_lane += 1
        right2 = left2 + conn_width
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, top),
            pya.Point(right2, top),
            pya.Point(right2, top2),
            pya.Point(right3, top2),
            pya.Point(right3, bottom3),
            pya.Point(left3, bottom3),
            pya.Point(left3, bottom2),
            pya.Point(left2, bottom2),
            pya.Point(left2, bottom),
        ]))

    sram_lane += 1

# IO pins
io_x = (2668-193)*um
io_y = 374*um
io_pins = (
    ("output", "mem_1__do[0]"),
    ("output", "mem_1__do[1]"),
    ("output", "mem_1__do[2]"),
    ("output", "mem_1__do[3]"),
    ("output", "mem_1__do[4]"),
    ("output", "mem_1__do[5]"),
    ("output", "mem_1__do[6]"),
    ("output", "mem_1__do[7]"),
    ("output", "mem_1__we"),
    ("output", "mem_1__en"),
    ("io", ("io_0__oe", "io_0__o", "io_0__i")),
    ("io", ("io_1__oe", "io_1__o", "io_1__i")),
    ("io", ("io_2__oe", "io_2__o", "io_2__i")),
    ("io", ("io_3__oe", "io_3__o", "io_3__i")),
    ("io", ("io_4__oe", "io_4__o", "io_4__i")),
    ("io", ("io_5__oe", "io_5__o", "io_5__i")),
    ("io", ("io_6__oe", "io_6__o", "io_6__i")),
    ("io", ("io_7__oe", "io_7__o", "io_7__i")),
    ("io", ("io_8__oe", "io_8__o", "io_8__i")),
    ("io", ("io_9__oe", "io_9__o", "io_9__i")),
    ("io", ("io_10__oe", "io_10__o", "io_10__i")),
    ("io", ("io_11__oe", "io_11__o", "io_11__i")),
    ("io", ("io_12__oe", "io_12__o", "io_12__i")),
    ("io", ("io_13__oe", "io_13__o", "io_13__i")),
)
io_lanes = sum([3 if direction == "io" else 1 for direction, _ in io_pins])
core_down_lane = sram_d_lanes + sram_addr_lanes
core_up_lane = core_down_lane + io_lanes - 1
sram_lane = sram_addr_lanes
io_down_lane = 0
io_up_lane = io_lanes - 1
def io_conn_right(
    x, y,
    left2, right2, 
    bottom2, top2,
    left3, right3,
    bottom3, top3,
    right4,
):
    left = x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
    
    if bottom2 <= bottom:
        if bottom3 <= bottom2:
            top_shapes["Metal2"].insert(pya.Polygon([
                pya.Point(left, bottom),
                pya.Point(left, top),
                pya.Point(right2, top),
                pya.Point(right2, top2),
                pya.Point(right3, top2),
                pya.Point(right3, top3),
                pya.Point(right4, top3),
                pya.Point(right4, bottom3),
                pya.Point(left3, bottom3),
                pya.Point(left3, bottom2),
                pya.Point(left2, bottom2),
                pya.Point(left2, bottom),
            ]))
        else:
            if bottom3 <= bottom:
                top_shapes["Metal2"].insert(pya.Polygon([
                    pya.Point(left, bottom),
                    pya.Point(left, top),
                    pya.Point(right2, top),
                    pya.Point(right2, top3),
                    pya.Point(right4, top3),
                    pya.Point(right4, bottom3),
                    pya.Point(left2, bottom3),
                    pya.Point(left2, bottom),
                ]))
            else:
                top_shapes["Metal2"].insert(pya.Polygon([
                    pya.Point(left, bottom),
                    pya.Point(left, top),
                    pya.Point(left3, top),
                    pya.Point(left3, top3),
                    pya.Point(right4, top3),
                    pya.Point(right4, bottom3),
                    pya.Point(right3, bottom3),
                    pya.Point(right3, bottom),
                ]))

    else:
        print("bottom2 > bottom not support yet")
        return
for i , (direction, core_pin) in enumerate(io_pins):
    if direction in ("input", "output"):
        core_pin_x, core_pin_y = retrouc_pins[core_pin]
        if direction == "input":
            io_pin_bottom = io_y + IO_TRI5VT_dims["C_x"]
            io_pin_top = io_pin_bottom + IO_TRI5VT_dims["C_width"]
            pin_spec = (
                ("PE", 0),
                ("PS", 0),
                ("IE", 1),
                ("SE", 0),
                ("DS", 0),
                ("I", 0),
                ("OEN", 1),
            )
        elif direction == "output":
            io_pin_bottom = io_y + IO_TRI5VT_dims["I_x"]
            io_pin_top = io_pin_bottom + IO_TRI5VT_dims["I_width"]
            pin_spec = (
                ("PE", 0),
                ("PS", 0),
                ("IE", 0),
                ("SE", 0),
                ("DS", 1),
                ("OEN", 0),
            )

        x = core_pin_x
        y = core_pin_y
        bottom2 = sram_addr_conn_bottom + sram_lane*conn_pitch
        sram_lane += 1
        top2 = bottom2 + conn_width
        if bottom2 <= x - tech_info.Via23_Width/2 - tech_info.Metal2_Via23_Enclosure:
            left2 = core_conn_left + core_down_lane*conn_pitch
            core_down_lane += 1
        else:
            left2 = core_conn_left + core_up_lane*conn_pitch
            core_up_lane -= 1
        right2 = left2 + conn_width
        bottom3 = io_pin_bottom
        top3 = io_pin_top
        if (bottom3 <= bottom2):
            left3 = io_conn_left + io_down_lane*conn_pitch
            io_down_lane += 1
        else:
            left3 = io_conn_left + io_up_lane*conn_pitch
            io_up_lane -= 1
        right3 = left3 + conn_width
        right4 = io_x
        io_conn_right(
            x, y,
            left2, right2, 
            bottom2, top2,
            left3, right3,
            bottom3, top3,
            right4,
        )
    elif direction == "io":
        pin_spec = (
            ("PE", 0),
            ("PS", 0),
            ("IE", 1),
            ("SE", 0),
            ("DS", 1),
        )
        names = core_pin
        for core_pin in names:
            core_pin_x, core_pin_y = retrouc_pins[core_pin]
            if core_pin[-2:] == "oe":
                io_pin_bottom = io_y + IO_TRI5VT_dims["OEN_x"]
                io_pin_top = io_pin_bottom + IO_TRI5VT_dims["OEN_width"]
            elif core_pin[-1] == "i":
                io_pin_bottom = io_y + IO_TRI5VT_dims["C_x"]
                io_pin_top = io_pin_bottom + IO_TRI5VT_dims["C_width"]
            elif core_pin[-1] == "o":
                io_pin_bottom = io_y + IO_TRI5VT_dims["I_x"]
                io_pin_top = io_pin_bottom + IO_TRI5VT_dims["I_width"]
            else:
                raise("core_pin not understood")

            x = core_pin_x
            y = core_pin_y
            bottom2 = sram_addr_conn_bottom + sram_lane*conn_pitch
            sram_lane += 1
            top2 = bottom2 + conn_width
            if bottom2 <= x - tech_info.Via23_Width/2 - tech_info.Metal2_Via23_Enclosure:
                left2 = core_conn_left + core_down_lane*conn_pitch
                core_down_lane += 1
            else:
                left2 = core_conn_left + core_up_lane*conn_pitch
                core_up_lane -= 1
            right2 = left2 + conn_width
            bottom3 = io_pin_bottom
            top3 = io_pin_top
            if (bottom3 <= bottom2):
                left3 = io_conn_left + io_down_lane*conn_pitch
                io_down_lane += 1
            else:
                left3 = io_conn_left + io_up_lane*conn_pitch
                io_up_lane -= 1
            right3 = left3 + conn_width
            right4 = io_x
            io_conn_right(
                x, y,
                left2, right2, 
                bottom2, top2,
                left3, right3,
                bottom3, top3,
                right4,
            )
    else:
        pin_spec = ()
        print("Error: {} direction not supported".format(direction))

    config_io(i, "right", pin_spec, ioring_shapes)

    io_y += io_width

# Connect the top pins
core_conn_bottom = (retrouc_y + 1640)*um
core_conn_top = core_conn_bottom + conn_width

io_x = 374*um
io_y = (2668 - 193)*um
pins = (
    ("io", ("io_37__i", "io_37__o", "io_37__oe")),
    ("io", ("io_36__i", "io_36__o", "io_36__oe")),
    ("io", ("io_35__i", "io_35__o", "io_35__oe")),
    ("io", ("io_34__i", "io_34__o", "io_34__oe")),
    ("io", ("io_33__i", "io_33__o", "io_33__oe")),
    ("io", ("io_32__i", "io_32__o", "io_32__oe")),
    ("io", ("io_31__i", "io_31__o", "io_31__oe")),
    ("io", ("io_30__i", "io_30__o", "io_30__oe")),
    ("io", ("io_29__i", "io_29__o", "io_29__oe")),
    ("io", ("io_28__i", "io_28__o", "io_28__oe")),
    ("io", ("io_27__i", "io_27__o", "io_27__oe")),
    ("io", ("io_26__i", "io_26__o", "io_26__oe")),
    ("io", ("io_25__i", "io_25__o", "io_25__oe")),
    ("io", ("io_24__i", "io_24__o", "io_24__oe")),
    ("io", ("io_23__i", "io_23__o", "io_23__oe")),
    ("io", ("io_22__i", "io_22__o", "io_22__oe")),
    ("io", ("io_21__i", "io_21__o", "io_21__oe")),
    ("io", ("io_20__i", "io_20__o", "io_20__oe")),
    ("io", ("io_19__i", "io_19__o", "io_19__oe")),
    ("io", ("io_18__i", "io_18__o", "io_18__oe")),
    ("io", ("io_17__i", "io_17__o", "io_17__oe")),
    ("io", ("io_16__i", "io_16__o", "io_16__oe")),
    ("io", ("io_15__i", "io_15__o", "io_15__oe")),
    ("io", ("io_14__i", "io_14__o", "io_14__oe")),
)
lane_count = sum([3 if direction == "io" else 1 for direction, _ in pins])
core_left_lane = 0
core_right_lane = lane_count - 1
def io_conn_top(
    x, y,
    left2, right2,
    bottom2, top2,
    top3,
):
    left = x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
    
    if left2 <= left:
        top_shapes["Metal3"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, bottom2),
            pya.Point(left2, bottom2),
            pya.Point(left2, top3),
            pya.Point(right2, top3),
            pya.Point(right2, top2),
            pya.Point(right, top2),
            pya.Point(right, bottom),
        ]))
    else:
        top_shapes["Metal3"].insert(pya.Polygon([
            pya.Point(left, bottom),
            pya.Point(left, top2),
            pya.Point(left2, top2),
            pya.Point(left2, top3),
            pya.Point(right2, top3),
            pya.Point(right2, bottom2),
            pya.Point(right, bottom2),
            pya.Point(right, bottom),
        ]))
for i, (direction, core_pin) in enumerate(pins):
    if direction in ("input", "output"):
        core_pin_x, core_pin_y = retrouc_pins[core_pin]
        if direction == "input":
            io_pin_right = io_x + io_width - IO_TRI5VT_dims["C_x"]
            io_pin_left = io_pin_right - IO_TRI5VT_dims["C_width"]
            pin_spec = (
                ("PE", 0),
                ("PS", 0),
                ("IE", 1),
                ("SE", 0),
                ("DS", 0),
                ("I", 0),
                ("OEN", 1),
            )
        elif direction == "output":
            io_pin_right = io_x + io_width - IO_TRI5VT_dims["I_x"]
            io_pin_left = io_pin_right - IO_TRI5VT_dims["I_width"]
            pin_spec = (
                ("PE", 0),
                ("PS", 0),
                ("IE", 0),
                ("SE", 0),
                ("DS", 1),
                ("OEN", 0),
            )

        x = core_pin_x
        y = core_pin_y
        if io_pin_right < core_pin_x:
            bottom2 = core_conn_bottom + core_left_lane*conn_pitch
        else:
            bottom2 = core_conn_bottom + core_right_lane*conn_pitch
        core_left_lane += 1
        core_right_lane -= 1
        top2 = bottom2 + conn_width
        left2 = io_pin_left
        right2 = io_pin_right
        top3 = io_y
        io_conn_top(
            x, y,
            left2, right2, 
            bottom2, top2,
            top3,
        )
    elif direction == "io":
        pin_spec = (
            ("PE", 0),
            ("PS", 0),
            ("IE", 1),
            ("SE", 0),
            ("DS", 1),
        )
        names = core_pin
        for core_pin in names:
            core_pin_x, core_pin_y = retrouc_pins[core_pin]
            if core_pin[-2:] == "oe":
                io_pin_right = io_x + io_width - IO_TRI5VT_dims["OEN_x"]
                io_pin_left = io_pin_right - IO_TRI5VT_dims["OEN_width"]
            elif core_pin[-1] == "i":
                io_pin_right = io_x + io_width - IO_TRI5VT_dims["C_x"]
                io_pin_left = io_pin_right - IO_TRI5VT_dims["C_width"]
            elif core_pin[-1] == "o":
                io_pin_right = io_x + io_width - IO_TRI5VT_dims["I_x"]
                io_pin_left = io_pin_right - IO_TRI5VT_dims["I_width"]
            else:
                raise("core_pin not understood")

            x = core_pin_x
            y = core_pin_y
            if io_pin_right < core_pin_x:
                bottom2 = core_conn_bottom + core_left_lane*conn_pitch
            else:
                bottom2 = core_conn_bottom + core_right_lane*conn_pitch
            core_left_lane += 1
            core_right_lane -= 1
            top2 = bottom2 + conn_width
            left2 = io_pin_left
            right2 = io_pin_right
            top3 = io_y
            io_conn_top(
                x, y,
                left2, right2, 
                bottom2, top2,
                top3,
            )
    else:
        pin_spec = ()
        print("Error: {} direction not supported".format(direction))

    config_io(i, "top", pin_spec, ioring_shapes)

    io_x += io_width

# Connect the left pins
core_conn_right = (retrouc_x - 10)*um
core_conn_left = core_conn_right - conn_width

io_x = 193*um
io_y = 374*um
pins = (
    ("input", "jtag_0__tdi"),
    ("output", "jtag_0__tdo"),
    ("input", "jtag_0__tms"),
    ("input", "jtag_0__tck"),
    ("input", "halt_0"),
    ("io", ("io_56__i", "io_56__o", "io_56__oe")),
    ("io", ("io_55__i", "io_55__o", "io_55__oe")),
    ("io", ("io_54__i", "io_54__o", "io_54__oe")),
    ("io", ("io_53__i", "io_53__o", "io_53__oe")),
    ("io", ("io_52__i", "io_52__o", "io_52__oe")),
    ("io", ("io_51__i", "io_51__o", "io_51__oe")),
    ("io", ("io_50__i", "io_50__o", "io_50__oe")),
    ("io", ("io_49__i", "io_49__o", "io_49__oe")),
    ("io", ("io_48__i", "io_48__o", "io_48__oe")),
    ("io", ("io_47__i", "io_47__o", "io_47__oe")),
    ("io", ("io_46__i", "io_46__o", "io_46__oe")),
    ("io", ("io_45__i", "io_45__o", "io_45__oe")),
    ("io", ("io_44__i", "io_44__o", "io_44__oe")),
    ("io", ("io_43__i", "io_43__o", "io_43__oe")),
    ("io", ("io_42__i", "io_42__o", "io_42__oe")),
    ("io", ("io_41__i", "io_41__o", "io_41__oe")),
    ("io", ("io_40__i", "io_40__o", "io_40__oe")),
    ("io", ("io_39__i", "io_39__o", "io_39__oe")),
    ("io", ("io_38__i", "io_38__o", "io_38__oe")),
)
lane_count = sum([3 if direction == "io" else 1 for direction, _ in pins])
core_down_lane = 0
core_up_lane = lane_count - 1
def io_conn_left(
    x, y,
    left2, right2,
    bottom2, top2,
    left3,
):
    left = x - tech_info.Via23_Width / 2
    right = left + tech_info.Via23_Width
    bottom = y - tech_info.Via23_Width / 2
    top = bottom + tech_info.Via23_Width
    top_shapes["Via23"].insert(pya.Box(left, bottom, right, top))
    left = left - tech_info.Metal2_Via23_Enclosure
    right = right + tech_info.Metal2_Via23_Enclosure
    top = top + tech_info.Metal2_Via23_Enclosure
    bottom = bottom - tech_info.Metal2_Via23_Enclosure
    top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
    
    if bottom2 <= bottom:
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(right, bottom),
            pya.Point(right, top),
            pya.Point(left2, top),
            pya.Point(left2, top2),
            pya.Point(left3, top2),
            pya.Point(left3, bottom2),
            pya.Point(right2, bottom2),
            pya.Point(right2, bottom),
        ]))
    else:
        top_shapes["Metal2"].insert(pya.Polygon([
            pya.Point(right, bottom),
            pya.Point(right, top),
            pya.Point(right2, top),
            pya.Point(right2, top2),
            pya.Point(left3, top2),
            pya.Point(left3, bottom2),
            pya.Point(left2, bottom2),
            pya.Point(left2, bottom),
        ]))
for i, (direction, core_pin) in enumerate(pins):
    if direction in ("input", "output"):
        core_pin_x, core_pin_y = retrouc_pins[core_pin]
        if direction == "input":
            io_pin_top = io_y + io_width - IO_TRI5VT_dims["C_x"]
            io_pin_bottom = io_pin_top - IO_TRI5VT_dims["C_width"]
            pin_spec = (
                ("PE", 1 if core_pin == ("halt_0", "jtag_0__tck", "jtag_0__tms") else 0), # PD halt input
                ("PS", 0),
                ("IE", 1),
                ("SE", 0),
                ("DS", 0),
                ("I", 0),
                ("OEN", 1),
            )
        elif direction == "output":
            io_pin_top = io_y + io_width - IO_TRI5VT_dims["I_x"]
            io_pin_bottom = io_pin_top - IO_TRI5VT_dims["I_width"]
            pin_spec = (
                ("PE", 0),
                ("PS", 0),
                ("IE", 0),
                ("SE", 0),
                ("DS", 1),
                ("OEN", 0),
            )
        x = core_pin_x
        y = core_pin_y
        if io_pin_top < core_pin_y:
            left2 = core_conn_left - core_down_lane*conn_pitch
        else:
            left2 = core_conn_left - core_up_lane*conn_pitch
        core_down_lane += 1
        core_up_lane -= 1
        right2 = left2 + conn_width
        bottom2 = io_pin_bottom
        top2 = io_pin_top
        left3 = io_x
        io_conn_left(
            x, y,
            left2, right2, 
            bottom2, top2,
            left3,
        )
    elif direction == "io":
        pin_spec = (
            ("PE", 1 if i in (5, 6) else 0), # PD/PU on two selected pins
            ("PS", 1 if i == 6 else 0),
            ("IE", 1),
            ("SE", 0),
            ("DS", 1),
        )
        names = core_pin
        for core_pin in names:
            core_pin_x, core_pin_y = retrouc_pins[core_pin]
            if core_pin[-2:] == "oe":
                io_pin_top = io_y + io_width - IO_TRI5VT_dims["OEN_x"]
                io_pin_bottom = io_pin_top - IO_TRI5VT_dims["OEN_width"]
            elif core_pin[-1] == "i":
                io_pin_top = io_y + io_width - IO_TRI5VT_dims["C_x"]
                io_pin_bottom = io_pin_top - IO_TRI5VT_dims["C_width"]
            elif core_pin[-1] == "o":
                io_pin_top = io_y + io_width - IO_TRI5VT_dims["I_x"]
                io_pin_bottom = io_pin_top - IO_TRI5VT_dims["I_width"]
            else:
                raise("core_pin not understood")

            x = core_pin_x
            y = core_pin_y
            if io_pin_top < core_pin_y:
                left2 = core_conn_left - core_down_lane*conn_pitch
            else:
                left2 = core_conn_left - core_up_lane*conn_pitch
            core_down_lane += 1
            core_up_lane -= 1
            right2 = left2 + conn_width
            bottom2 = io_pin_bottom
            top2 = io_pin_top
            left3 = io_x
            io_conn_left(
                x, y,
                left2, right2, 
                bottom2, top2,
                left3,
            )
    else:
        pin_spec = ()
        print("Error: {} direction not supported".format(direction))

    config_io(i, "left", pin_spec, ioring_shapes)

    io_y += io_width

# Connect clock to SRAM

bufx4_idx = layout.cell("BUFX4").cell_index()
x = (sram_x + 236)*um
y = (sram_y - 25)*um

# DC connection
dc_width = long(1.3*um)
left = x - dc_width/2
bottom = y - long(0.45*um)
top = bottom + long(21.25)*um
# Stupid wide Metal1 rules
right = left + 3*dc_width
insert_ring(top_shapes["Metal1"], left, bottom, right, top, dc_width)
left = right
right = (2668 - 198)*um
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
right = x - long(19.35*um)
left = right - 3*dc_width
insert_ring(top_shapes["Metal1"], left, bottom, right, top, dc_width)
right = left
left = right - 7*um
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
right = left + conn_width
insert_viarect(
    top_shapes["Via12"], left, bottom, right, top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
insert_viarect(
    top_shapes["Via23"], left, bottom, right, top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
insert_viarect(
    top_shapes["Via34"], left, bottom, right, top,
    tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure,
)
right = (2668 - 198)*um
top_shapes["Metal4"].insert(pya.Box(left, bottom, right, top))
# OEB to gnd
pin_x, pin_y = sram_pins["OEB"]
left = pin_x - sram_pin_width/2
right = left + sram_pin_width
top2 = top
bottom = top2 - conn_width
top = pin_y
right2 = x + 3*dc_width
top_shapes["Metal3"].insert(pya.Polygon([
    pya.Point(left, top),
    pya.Point(right, top),
    pya.Point(right, top2),
    pya.Point(right2, top2),
    pya.Point(right2, bottom),
    pya.Point(left, bottom),
]))
right = right2
left = right - conn_width
top = top2
insert_viarect(
    top_shapes["Via23"], left, bottom, right, top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
insert_viarect(
    top_shapes["Via12"], left, bottom, right, top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)


# Clock input
top_cell.insert(pya.CellInstArray(bufx4_idx, pya.Trans(1, False, x, y)))
left = clk_left
bottom = clk_bottom
right = clk_right
top = clk_top
insert_viarect(
    top_shapes["Via23"], left, bottom, right, top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
right = x - long(7.75*um)
left2 = x - long(9.45*um)
top2 = retrouc_y*um
top_shapes["Metal2"].insert(pya.Polygon([
    pya.Point(left, bottom),
    pya.Point(left, top),
    pya.Point(left2, top),
    pya.Point(left2, top2),
    pya.Point(right, top2),
    pya.Point(right, bottom),
]))
left = left2
top = top2
bottom = top - conn_width
insert_viarect(
    top_shapes["Via23"], left, bottom, right, top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
top = y + long(1.45)*um
top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
bottom = top - conn_width
insert_viarect(
    top_shapes["Via23"], left, bottom, right, top,
    tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
)
top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
insert_viarect(
    top_shapes["Via12"], left, bottom, right, top,
    tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
)
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))

# BUFX4 out to CLKBUF1 in
left = x - long(8.05*um)
right = x - long(6.55*um)
bottom = y + long(4.65*um)
top = bottom + long(2.1*um)
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))

# Fix implants
left = x - long(5.4*um)
right = x - 1*um
bottom = y + long(6.2*um)
top = bottom + long(0.4*um)
top_shapes["NPlus"].insert(pya.Box(left, bottom, right, top))
top_shapes["N3V"].insert(pya.Box(left, bottom, right, top))
left = x - 19*um
right = x - long(10.6*um)
top_shapes["PPlus"].insert(pya.Box(left, bottom, right, top))

clkbuf1_idx = layout.cell("CLKBUF1").cell_index()
y += long(6.4*um)
top_cell.insert(pya.CellInstArray(clkbuf1_idx, pya.Trans(1, False, x, y)))

# CLKBUF1 out to CE pin
pin_x, pin_y = sram_pins["CE"]
left = pin_x - sram_pin_width/2
right = left + sram_pin_width
top = pin_y
bottom = y + long(14.05*um)
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))

# Retro-uC DC
# VDD
leftright = (
    (long((retrouc_x + 71.95)*um), long((retrouc_x + 75.25)*um)),
    (long((retrouc_x + 236.75)*um), long((retrouc_x + 240.05)*um)),
    (long((retrouc_x + 403.15)*um), long((retrouc_x + 406.45)*um)),
    (long((retrouc_x + 569.55)*um), long((retrouc_x + 572.85)*um)),
    (long((retrouc_x + 737.35)*um), long((retrouc_x + 740.85)*um)),
    (long((retrouc_x + 903.95)*um), long((retrouc_x + 907.25)*um)),
    (long((retrouc_x + 1070.35)*um), long((retrouc_x + 1073.65)*um)),
)
bottom = 198*um
top = (2668 - 198)*um
for left, right in leftright:
    top_shapes["Metal4"].insert(pya.Box(left, bottom, right, top))
# GND
leftright = (
    (long((retrouc_x + 155.15)*um), long((retrouc_x + 158.45)*um)),
    (long((retrouc_x + 321.55)*um), long((retrouc_x + 324.85)*um)),
    (long((retrouc_x + 487.95)*um), long((retrouc_x + 491.25)*um)),
    (long((retrouc_x + 654.35)*um), long((retrouc_x + 657.65)*um)),
    (long((retrouc_x + 820.75)*um), long((retrouc_x + 824.05)*um)),
    (long((retrouc_x + 987.15)*um), long((retrouc_x + 990.45)*um)),
)
top = (retrouc_y - 4)*um
bottom = top - 4*um
bottom2 = (retrouc_y + 1624)*um
top2 = bottom2 + 4*um
bottom3 = 198*um
top3 = (2668 - 198)*um
for left, right in leftright:
    top_shapes["Metal4"].insert(pya.Box(left, bottom, right, top))
    top_shapes["Metal4"].insert(pya.Box(left, bottom2, right, top2))
    insert_viarect(
        top_shapes["Via34"], left, bottom, right, top,
        tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure,
    )
    insert_viarect(
        top_shapes["Via34"], left, bottom2, right, top2,
        tech_info.Via34_Width, tech_info.Via34_MinPitch, tech_info.Metal3_Via34_Enclosure,
    )
    top_shapes["Metal3"].insert(pya.Box(left, bottom, right, top))
    top_shapes["Metal3"].insert(pya.Box(left, bottom2, right, top2))
    insert_viarect(
        top_shapes["Via23"], left, bottom, right, top,
        tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
    )
    insert_viarect(
        top_shapes["Via23"], left, bottom2, right, top2,
        tech_info.Via23_Width, tech_info.Via23_MinPitch, tech_info.Metal2_Via23_Enclosure,
    )
    top_shapes["Metal2"].insert(pya.Box(left, bottom, right, top))
    top_shapes["Metal2"].insert(pya.Box(left, bottom2, right, top2))
    insert_viarect(
        top_shapes["Via12"], left, bottom, right, top,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    insert_viarect(
        top_shapes["Via12"], left, bottom2, right, top2,
        tech_info.Via12_Width, tech_info.Via12_MinPitch, tech_info.Metal1_Via12_Enclosure,
    )
    top_shapes["Metal1"].insert(pya.Box(left, bottom3, right, top))
    top_shapes["Metal1"].insert(pya.Box(left, bottom2, right, top3))


# SRAM DC
left = (sram_x + 440)*um
right = (2668 - 198)*um
bottom = sram_y*um
top = bottom + 15*um
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
bottom = long((sram_y + 562.6)*um)
top = bottom + 12*um
top_shapes["Metal1"].insert(pya.Box(left, bottom, right, top))
bottom = (sram_y + 20)*um
top = bottom + 12*um
top_shapes["Metal4"].insert(pya.Box(left, bottom, right, top))
bottom = long((sram_y + 608.8)*um)
top = bottom + 12*um
top_shapes["Metal4"].insert(pya.Box(left, bottom, right, top))


saveopts = tech.save_layout_options.dup()
saveopts.write_context_info = False
layout.write("SWIII_B.gds.gz", saveopts)


print("End")
