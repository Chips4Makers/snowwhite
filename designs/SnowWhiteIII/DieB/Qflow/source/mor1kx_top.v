module mor1kx_top(
    input 			      clk,
    input 			      rst,

    // Wishbone interface
    output [31:0] 		      iwbm_adr_o,
    output 			      iwbm_stb_o,
    output 			      iwbm_cyc_o,
    input 			      iwbm_ack_i,
    input 			      iwbm_err_i,
    input 			      iwbm_rty_i,
    output [3:0] 		      iwbm_sel_o,
    output 			      iwbm_we_o,
    output [2:0] 		      iwbm_cti_o,
    output [1:0] 		      iwbm_bte_o,
    output [31:0] 		      iwbm_dat_o,
    input [31:0] 		      iwbm_dat_i,

    output [31:0] 		      dwbm_adr_o,
    output 			      dwbm_stb_o,
    output 			      dwbm_cyc_o,
    input 			      dwbm_ack_i,
    input 			      dwbm_err_i,
    input 			      dwbm_rty_i,
    output [3:0] 		      dwbm_sel_o,
    output 			      dwbm_we_o,
    output [2:0] 		      dwbm_cti_o,
    output [1:0] 		      dwbm_bte_o,
    output [31:0] 		      dwbm_dat_o,
    input [31:0] 		      dwbm_dat_i,

    input [31:0] 		      irq_i
    );

mor1kx #(
	.FEATURE_DEBUGUNIT("ENABLED"),
	.FEATURE_CMOV("ENABLED"),
	.FEATURE_INSTRUCTIONCACHE("NONE"),
	.FEATURE_IMMU("NONE"),
	.FEATURE_DATACACHE("NONE"),
	.FEATURE_DMMU("NONE"),
	.OPTION_PIC_TRIGGER("LATCHED_LEVEL"),

	.IBUS_WB_TYPE("B3_REGISTERED_FEEDBACK"),
	.DBUS_WB_TYPE("B3_REGISTERED_FEEDBACK"),
	.OPTION_CPU0("CAPPUCCINO"),
	.OPTION_RESET_PC(32'hf0000000)
) mor1kx0 (
	.clk(clk),
	.rst(rst),

	.iwbm_adr_o(iwbm_adr_o),
	.iwbm_stb_o(iwbm_stb_o),
	.iwbm_cyc_o(iwbm_cyc_o),
	.iwbm_sel_o(iwbm_sel_o),
	.iwbm_we_o (iwbm_we_o),
	.iwbm_cti_o(iwbm_cti_o),
	.iwbm_bte_o(iwbm_bte_o),
	.iwbm_dat_o(iwbm_dat_o),

	.dwbm_adr_o(dwbm_adr_o),
	.dwbm_stb_o(dwbm_stb_o),
	.dwbm_cyc_o(dwbm_cyc_o),
	.dwbm_sel_o(dwbm_sel_o),
	.dwbm_we_o (dwbm_we_o ),
	.dwbm_cti_o(dwbm_cti_o),
	.dwbm_bte_o(dwbm_bte_o),
	.dwbm_dat_o(dwbm_dat_o),

	.iwbm_err_i(iwbm_err_i),
	.iwbm_ack_i(iwbm_ack_i),
	.iwbm_dat_i(iwbm_dat_i),
	.iwbm_rty_i(iwbm_rty_i),

	.dwbm_err_i(dwbm_err_i),
	.dwbm_ack_i(dwbm_ack_i),
	.dwbm_dat_i(dwbm_dat_i),
	.dwbm_rty_i(dwbm_rty_i),

	.irq_i(irq_i),

	.traceport_exec_valid_o  (),
	.traceport_exec_pc_o     (),
	.traceport_exec_insn_o   (),
	.traceport_exec_wbdata_o (),
	.traceport_exec_wbreg_o  (),
	.traceport_exec_wben_o   (),

	.multicore_coreid_i   (32'd0),
	.multicore_numcores_i (32'd0),

	.snoop_adr_i (32'd0),
	.snoop_en_i  (1'b0),

	.du_addr_i(16'd0),
	.du_stb_i(0),
	.du_dat_i(32'd0),
	.du_we_i(0),
	.du_dat_o(),
	.du_ack_o(),
	.du_stall_i(0),
	.du_stall_o()
);

endmodule // mor1kx_top
