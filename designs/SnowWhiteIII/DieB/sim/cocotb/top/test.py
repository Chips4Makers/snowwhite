import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue
from cocotb.drivers.wishbone import WishboneMaster, WBOp
from cocotb.monitors.wishbone import WishboneSlave

from c4m_jtag import JTAG_Master

@cocotb.test()
def test01_resetrun(dut):
    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    dut.halt_0 <= 0
    dut.jtag_0__tck <= 0

    dut.rst_0 <= 0

    yield Timer(int(5.5*clk_period))

    dut.rst_0 <= 1

    yield Timer(30*clk_period)

@cocotb.test()
def test02_idcode(dut):
    """
    Test the IDCODE command
    """

    # Run @ 10MHz
    clk_period = get_sim_steps(100, "ns")
    master = JTAG_Master(dut.jtag_0__tck, dut.jtag_0__tms, dut.jtag_0__tdi, dut.jtag_0__tdo, clk_period=clk_period)
    master.IDCODE = [0, 0, 1]

    # Init
    dut.rst_0 <= 0
    dut.clk_0 <= 0

    dut._log.info("Trying to get IDCODE...")

    yield master.idcode()
    result1 = master.result
    dut._log.info("IDCODE1: {}".format(result1))
    assert(result1 == BinaryValue("00000000000000001000100011111111"))

    yield master.idcode()
    result2 = master.result
    dut._log.info("IDCODE2: {}".format(result2))

    assert(result1 == result2)

@cocotb.test()
def test03_memory(dut):
    """
    Test writing to different regions in the chain
    """

    dut.rst_0 <= 1

    # Simulate @ 100MHz
    clk_period = get_sim_steps(10, "ns")
    cocotb.fork(Clock(dut.clk_0, clk_period).start())

    # Run @ 20MHz
    jtag_period = get_sim_steps(50, "ns")
    master = JTAG_Master(dut.jtag_0__tck, dut.jtag_0__tms, dut.jtag_0__tdi, dut.jtag_0__tdo, clk_period=jtag_period)

    MEMADDR = BinaryValue("011", 3)
    MEMRD = BinaryValue("100", 3)
    MEMWR = BinaryValue("101", 3)

    data0 = BinaryValue(0, 8)
    dataFF = BinaryValue("11111111", 8)
    data10 = BinaryValue("10101010", 8)
    data01 = BinaryValue("01010101", 8)

    yield master.load_ir(MEMADDR)
    yield master.shift_data(BinaryValue("0000000000000000", 16))
    yield master.load_ir(MEMWR)
    yield master.shift_data(data10)

    yield master.load_ir(MEMADDR)
    yield master.shift_data(BinaryValue("0100000000000000", 16))
    yield master.load_ir(MEMWR)
    yield master.shift_data(data01)

    yield master.load_ir(MEMADDR)
    yield master.shift_data(BinaryValue("1000000000000000", 16))
    yield master.load_ir(MEMWR)
    yield master.shift_data(dataFF)

    yield Timer(10*clk_period)
