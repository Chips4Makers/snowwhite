#!/bin/env python3

# This file has to be kept in sync with ../../../Qflow_MOS6502/generate.py
# TODO: find a better way to keep them in sync
import os

from nmigen import *
from nmigen.build import *

from top import Top_MOS6502

io_count = 57

int_a_pins = ["int_a_{}".format(i) for i in range(10)]
int_a_pins.reverse()
int_di_pins = ["int_di_{}".format(i) for i in range(8)]
int_di_pins.reverse()
int_do_pins = ["int_do_{}".format(i) for i in range(8)]
int_do_pins.reverse()

ext_a_pins = ["ext_a_{}".format(i) for i in range(14)]
ext_a_pins.reverse()
ext_di_pins = ["ext_di_{}".format(i) for i in range(8)]
ext_di_pins.reverse()
ext_do_pins = ["ext_do_{}".format(i) for i in range(8)]
ext_do_pins.reverse()

tb_outputs = [
    *[
        Resource("io", i,
            Subsignal("i", Pins("io_{}_i".format(i), dir="i")),
            Subsignal("o", Pins("io_{}_o".format(i), dir="o")),
            Subsignal("oe", Pins("io_{}_oe".format(i), dir="o")),
        ) for i in range(io_count)
    ],
    Resource("mem", 0,
        Subsignal("addr", Pins(" ".join(int_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(int_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(int_do_pins), dir="o")),
        Subsignal("we", Pins("int_we", dir="o")),
        Subsignal("en", Pins("int_en", dir="o")),
    ),
    Resource("mem", 1,
        Subsignal("addr", Pins(" ".join(ext_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(ext_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(ext_do_pins), dir="o")),
        Subsignal("we", Pins("ext_we", dir="o")),
        Subsignal("en", Pins("ext_en", dir="o")),
    ),
    Resource("jtag", 0,
        Subsignal("tck", Pins("tck", dir="i")),
        Subsignal("tms", Pins("tms", dir="i")),
        Subsignal("tdo", Pins("tdo", dir="o")),
        Subsignal("tdi", Pins("tdi", dir="i")),
    ),
    Resource("halt", 0, Pins("halt", dir="i")),
]

class Top(Elaboratable):
    def elaborate(self, p):
        ios = [p.request("io", i) for i in range(io_count)]
        int_mem = p.request("mem", 0)
        ext_mem = p.request("mem", 1)
        jtag = p.request("jtag", 0)
        halt = p.request("halt", 0)

        m = Module()

        m.submodules.dump = Instance("dump", i_clk=ClockSignal())
        m.submodules.top_mos6502 = Top_MOS6502(ios, int_mem, ext_mem, jtag, halt)

        return m
