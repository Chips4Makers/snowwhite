#!/bin/env python
from gdstxt_funcs import *
# Get private tech information
import priv.TSMC_CL035G as ti


f = open("SnowWhiteI_S.txt", "w")
write_cellhead(f, "SnowWhiteI_S")

write_inst(f, "IORing", "ioring", 0L, 0L)
write_inst(f, "Core", "SnowWhiteI_Core", 0L, 0L)
write_inst(f, "Logo", "C4MLogo", 5000L, 5000L)

#Boundary
left = 0L
bottom = 0L
right = 2800000L
top = 2800000L
write_rect(f, ti.Boundary, left, bottom, right, top)

write_celltail(f, "SnowWhiteI_S")
f.close()
