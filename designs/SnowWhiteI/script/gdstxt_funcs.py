#!/bin/env python

def write_cellhead(f, cellname):
    """Write the header of a cell"""

    f.write("""
BGNSTR __date__ __date__
STRNAME {}
""".format(cellname)
    )

def write_celltail(f, cellname):
    """Write the tail of a cell"""

    f.write("\nENDSTR\n")


def write_rect(f, layerspec, left, bottom, right, top):
    """Write a rectangular shape"""

    try:
        layer, datatype = layerspec
    except:
        layer = layerspec
        datatype = 0

    f.write("""
BOUNDARY
LAYER {}
DATATYPE {}
XY """.format(layer, datatype)
    )
    f.write("{}: {}\n".format(left, bottom))
    f.write("{}: {}\n".format(left, top))
    f.write("{}: {}\n".format(right, top))
    f.write("{}: {}\n".format(right, bottom))
    f.write("ENDEL\n")


def write_viarect(f, layerspec, left, bottom, right, top, size, pitch, enclosure):
    """Write an array of vias enclosed in a rectangle"""

    try:
        layer, datatype = layerspec
    except:
        layer = layerspec
        datatype = 0
    
    width = right - left
    nx = (width - 2*enclosure - size)/pitch + 1
    dx = (width - size - (nx-1)*pitch)/2 # Center array
    height = top - bottom
    ny = (height - 2*enclosure - size)/pitch + 1
    dy = (height - size - (ny-1)*pitch)/2 # Center array
    for i_x in xrange(nx): # TODO(?): replace with array instance
        for i_y in xrange(ny):
            via_left = left + dx + i_x*pitch
            via_right = via_left + size
            via_bottom = bottom + dy + i_y*pitch
            via_top = via_bottom + size
            write_rect(f, layer, via_left, via_bottom, via_right, via_top)


def write_ring(f, layerspec, left, bottom, right, top,  width):
    """Write a ring, coordinates are outer dimensions"""

    try:
        layer, datatype = layerspec
    except:
        layer = layerspec
        datatype = 0

    x_left_in = left + width
    x_left_out = left
    x_right_in = right - width
    x_right_out = right
    y_bottom_in = bottom + width
    y_bottom_out = bottom
    y_top_in = top - width
    y_top_out = top

    f.write("""
BOUNDARY
LAYER {}
DATATYPE {}
XY """.format(layer, datatype)
    )
    f.write("{}: {}\n".format(x_left_out, y_bottom_out))
    f.write("{}: {}\n".format(x_left_out, y_top_out))
    f.write("{}: {}\n".format(x_right_out, y_top_out))
    f.write("{}: {}\n".format(x_right_out, y_bottom_out))
    f.write("{}: {}\n".format(x_left_in, y_bottom_out))
    f.write("{}: {}\n".format(x_left_in, y_bottom_in))
    f.write("{}: {}\n".format(x_right_in, y_bottom_in))
    f.write("{}: {}\n".format(x_right_in, y_top_in))
    f.write("{}: {}\n".format(x_left_in, y_top_in))
    f.write("{}: {}\n".format(x_left_in, y_bottom_out))
    f.write("ENDEL\n")


def write_viaring(f, layerspec, left, bottom, right, top, size, pitch):
    """Write a via ring, coordinates are outer dimensions"""

    try:
        layer, datatype = layerspec
    except:
        layer = layerspec
        datatype = 0

    x_left_in = left + size
    x_left_out = left
    x_right_in = right - size
    x_right_out = right
    y_bottom_in = bottom + size
    y_bottom_out = bottom
    y_top_in = top - size
    y_top_out = top

    #Bottom and top row
    count = (x_right_in - x_left_out) / pitch
    y2_bottom = y_bottom_out
    y2_top = y_bottom_in
    y3_bottom = y_top_in
    y3_top = y_top_out
    for i in xrange(count): # TODO: replace with array inst
        x2_left = x_left_out + i*pitch
        x2_right = x2_left + size
        write_rect(f, layerspec, x2_left, y2_bottom, x2_right, y2_top)
        write_rect(f, layerspec, x2_left, y3_bottom, x2_right, y3_top)

    #Left and right row
    count = (y_top_in - y_bottom_out) / pitch
    x2_left = x_left_out
    x2_right = x2_left + size
    x3_right = x_right_out
    x3_left = x3_right - size
    # First on left is already drawn
    for i in xrange(1, count): # TODO: replace with array inst
        y2_bottom = y_bottom_out + i*pitch
        y2_top = y2_bottom + size
        write_rect(f, layerspec, x2_left, y2_bottom, x2_right, y2_top)
        write_rect(f, layerspec, x3_left, y2_bottom, x3_right, y2_top)

    # Two right corners
    x2_left = x_right_in
    x2_right = x_right_out
    y2_bottom = y_bottom_out
    y2_top = y_bottom_in
    y3_bottom = y_top_in
    y3_top = y_top_out
    write_rect(f, layerspec, x2_left, y2_bottom, x2_right, y2_top)
    write_rect(f, layerspec, x2_left, y3_bottom, x2_right, y3_top)


def write_inst(f, instname, cellname, x, y, angle=0):
    """Write a cell instance"""

    # TODO: support mirroring of cells
    f.write("SREF\nSNAME {}\n".format(cellname))
    if angle != 0:
        f.write("STRANS 0\nANGLE {}: {}\n".format(angle))
    f.write(
"""XY {}: {}
PROPATTR 1
PROPVALUE {}
ENDEL
""".format(x, y, instname)
    )
