#!/bin/env python
from gdstxt_funcs import *
# Get private tech information
import priv.TSMC_CL035G as ti


die_width = 2800000L
die_height = 2800000L
ioring_width = 193000L # 73um of bondpad and 120um of IO ringr

io1_left = 260000L
io_pitch = 200000L
io_width = 80000L


#
# Head
#
f = open("SnowWhiteI_Core.txt", "w")
write_cellhead(f, "SnowWhiteI_Core")

#
# Boundary
#
write_rect(f, ti.Boundary, ioring_width, ioring_width, die_width - ioring_width, die_height - ioring_width)


#
# DC ring; METAL2: VSS, METAL4: VDD
#
dc_dist = 10000L
dc_width = 30000L
dc_space = 1000L

dc_left = ioring_width + dc_dist
dc_bottom = ioring_width + dc_dist
dc_right = die_width - ioring_width - dc_dist
dc_top = die_height - ioring_width - dc_dist
write_ring(f, ti.Metal2, dc_left, dc_bottom, dc_right, dc_top, dc_width)
write_ring(f, ti.Metal4, dc_left, dc_bottom, dc_right, dc_top, dc_width)
dc_verspace = 700000L
dc_top2 = (die_height - dc_verspace)/2
dc_bottom2 = dc_top2 - dc_width
write_rect(f, ti.Metal2, dc_left, dc_bottom2, dc_right, dc_top2)
write_rect(f, ti.Metal4, dc_left, dc_bottom2, dc_right, dc_top2)
dc_bottom3 = dc_top2 + dc_verspace
dc_top3 = dc_bottom3 + dc_width
write_rect(f, ti.Metal2, dc_left, dc_bottom3, dc_right, dc_top3)
write_rect(f, ti.Metal4, dc_left, dc_bottom3, dc_right, dc_top3)

# VDD IO connection
io_bottom = 1260000L
io_top = io_bottom + io_width
metal2_length = dc_dist - dc_space
# unrotated cell has pin at the top
iopin_coords = [
    (7000L, 38000L),
    (42000L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
    # left cell; 270 deg rotated
    left = ioring_width
    right = left + metal2_length
    bottom = io_top - iopin_right
    top = io_top - iopin_left
    write_rect(f, ti.Metal2, left, bottom, right, top)
    left = left + dc_space
    write_viarect(
        f, ti.Via23, left, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, left, bottom, dc_left + dc_width, top)
    write_viarect(
        f, ti.Via34, left, bottom, dc_left + dc_width, top,
        ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
    )
    write_rect(f, ti.Metal4, left, bottom, dc_left + dc_width, top)
    # right cell; 90 deg rotated
    right = die_width - ioring_width
    left = right - metal2_length
    bottom = io_bottom + iopin_left
    top = io_bottom + iopin_right
    write_rect(f, ti.Metal2, left, bottom, right, top)
    right = right - dc_space
    write_viarect(
        f, ti.Via23, left, bottom, right, top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, dc_right - dc_width, bottom, right, top)
    write_viarect(
        f, ti.Via34, dc_right - dc_width, bottom, right, top,
        ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
    )
    write_rect(f, ti.Metal4, dc_right - dc_width, bottom, right, top)

# VSS connection
io_bottom = 1060000L
io_top = io_bottom + io_width
# pins on METAL2
# unrotated cell has pin at the top
iopin_coords = [
    (7000L, 27400L),
    (29800L, 50200L),
    (52600L, 73000L),
]
for iopin_left, iopin_right in iopin_coords:
    # left cell; 270deg rotated
    bottom = io_top - iopin_right
    top = io_top - iopin_left
    write_rect(f, ti.Metal2, ioring_width, bottom, dc_left, top)
    # right cell; 90def rotated
    bottom = io_bottom + iopin_left
    top = io_bottom + iopin_right
    write_rect(f, ti.Metal2, dc_right, bottom, die_width - ioring_width, top)

#
# SRAMs
#
#TODO: Read info from LEF
sram_width = 448375L
sram_height = 620000L
sram_macro = "TM035SSRAM1024X8"
sram_space = 100000L
sram_iodist = 200000L

sram_pitch = sram_width + sram_space

sram1_left = (die_width - sram_space)/2 - 2*sram_width - sram_space
sram1_right = sram1_left + sram_width
sram_bottom = (die_height - sram_height)/2
sram_top = sram_bottom + sram_height

for i in xrange(4): # TODO: replace with array instance
    write_inst(f, "RAM{}".format(i + 1), sram_macro, sram1_left + i*sram_pitch, sram_bottom)
    write_inst(f, "FIX{}".format(i + 1), "SRAMfix", sram1_left + i*sram_pitch, sram_bottom)
    write_inst(f, "N3V{}".format(i + 1), "SRAMN3V", sram1_left + i*sram_pitch, sram_bottom)

# DC in between SRAM
dc_left2 = sram1_left + sram_width + (sram_space - dc_width)/2
dc_right2 = dc_left2 + dc_width
for i in xrange(3):
    left = dc_left2 + i*sram_pitch
    right = left + dc_width
    write_rect(f, ti.Metal2, left, dc_top2, right, dc_bottom3)
    write_rect(f, ti.Metal4, left, dc_top2, right, dc_bottom3)

# SRAM pins
# VSS; METAL1
# 1: left  
pin_bottom = sram_bottom + 22000L # TODO: derive info from LEF
pin_top = sram_bottom + 37000L
write_rect(f, ti.Metal1, dc_left, pin_bottom, sram1_left, pin_top)
write_viarect(
    f, ti.Via12, dc_left, pin_bottom, dc_left + dc_width, pin_top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
for i in xrange(3):
    left = dc_left2 + i*sram_pitch
    right = sram1_left + (i+1)*sram_pitch
    write_rect(f, ti.Metal1, left, pin_bottom, right, pin_top)
    write_viarect(
        f, ti.Via12, left, pin_bottom, left + dc_width, pin_top,
        ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
    )
# 2&4: left & right
pin_bottom = sram_bottom + 562600L
pin_top = sram_bottom + 574600L
write_rect(f, ti.Metal1, dc_left, pin_bottom, sram1_left, pin_top)
write_viarect(
    f, ti.Via12, dc_left, pin_bottom, dc_left + dc_width, pin_top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
for i in xrange(3):
    left = sram1_right + i*sram_pitch
    right = sram1_left + (i+1)*sram_pitch
    write_rect(f, ti.Metal1, left, pin_bottom, right, pin_top)
    left = dc_left2 + i*sram_pitch
    write_viarect(
        f, ti.Via12, left, pin_bottom, left + dc_width, pin_top,
        ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
    )
write_rect(f, ti.Metal1, sram1_right + 3*sram_pitch, pin_bottom, dc_right, pin_top)
write_viarect(
    f, ti.Via12, dc_right - dc_width, pin_bottom, dc_right, pin_top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
# 3: right
pin_bottom = sram_bottom + 0L
pin_top = sram_bottom + 15000L
pin_dx = -1900L
write_rect(f, ti.Metal1, sram1_right + 3*sram_pitch + pin_dx, pin_bottom, dc_right, pin_top)
write_viarect(
    f, ti.Via12, dc_right - dc_width, pin_bottom, dc_right, pin_top,
    ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
)
for i in xrange(3):
    left = sram1_right + i*sram_pitch + pin_dx
    right = dc_right2 + i*sram_pitch
    write_rect(f, ti.Metal1, left, pin_bottom, right, pin_top)
    write_viarect(
        f, ti.Via12, right - dc_width, pin_bottom, right, pin_top,
        ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
    )
# VDD; METAL2
metal2_length = 20000L
metal23_overlap = 15000L
# 1; left
pin_bottom = sram_bottom + 0L
pin_top = sram_bottom + 12000L
pin_dx = 1900L
right = sram1_left + pin_dx
left = right - metal2_length
write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
right = left + metal23_overlap
write_viarect(
    f, ti.Via23, left, pin_bottom, right, pin_top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
write_rect(f, ti.Metal3, dc_left, pin_bottom, right, pin_top)
write_viarect(
    f, ti.Via34, dc_left, pin_bottom, dc_left + dc_width, pin_top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)
for i in xrange(3):
    right = sram1_left + (i+1)*sram_pitch
    left = right - metal2_length
    write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
    right = left + metal23_overlap
    write_viarect(
        f, ti.Via23, left, pin_bottom, right, pin_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    left = dc_left2 + i*sram_pitch
    write_rect(f, ti.Metal3, left, pin_bottom, right, pin_top)
    write_viarect(
        f, ti.Via34, left, pin_bottom, left + dc_width, pin_top,
        ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
    )
# 2&4: left & right
pin_bottom = sram_bottom + 608800L
pin_top = sram_bottom + 620800L
pin_dx = 1900L
right = sram1_left + pin_dx
left = right - metal2_length
write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
right = left + metal23_overlap
write_viarect(
    f, ti.Via23, left, pin_bottom, right, pin_top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
write_rect(f, ti.Metal3, dc_left, pin_bottom, right, pin_top)
write_viarect(
    f, ti.Via34, dc_left, pin_bottom, dc_left + dc_width, pin_top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)
for i in xrange(3):
    left = sram1_right - pin_dx + i*sram_pitch
    right = left + metal2_length
    right2 = sram1_left + (i+1)*sram_pitch + pin_dx
    left2 = right2 - metal2_length
    write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
    write_rect(f, ti.Metal2, left2, pin_bottom, right2, pin_top)
    left = right - metal23_overlap
    right2 = left2 + metal23_overlap
    write_viarect(
        f, ti.Via23, left, pin_bottom, right, pin_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_viarect(
        f, ti.Via23, left2, pin_bottom, right2, pin_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, left, pin_bottom, right2, pin_top)
    left = dc_left2 + i*sram_pitch
    write_viarect(
        f, ti.Via34, left, pin_bottom, left + dc_width, pin_top,
        ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
    )
left = sram1_right + 3*sram_pitch
right = left + metal2_length
write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
left = right - metal23_overlap
write_viarect(
    f, ti.Via23, left, pin_bottom, right, pin_top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
write_rect(f, ti.Metal3, left, pin_bottom, dc_right, pin_top)
write_viarect(
    f, ti.Via34, dc_right - dc_width, pin_bottom, dc_right, pin_top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)
# 3: right
pin_bottom = sram_bottom + 20000L
pin_top = sram_bottom + 32000L
for i in xrange(3):
    left = sram1_right + i*sram_pitch
    right = left + metal2_length
    write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
    left = right - metal23_overlap
    write_viarect(
        f, ti.Via23, left, pin_bottom, right, pin_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    right = dc_left2 + dc_width + i*sram_pitch
    write_rect(f, ti.Metal3, left, pin_bottom, right, pin_top)
    left = right - dc_width
    write_viarect(
        f, ti.Via34, left, pin_bottom, right, pin_top,
        ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
    )
left = sram1_right + 3*sram_pitch
right = left + metal2_length
write_rect(f, ti.Metal2, left, pin_bottom, right, pin_top)
left = right - metal23_overlap
write_viarect(
    f, ti.Via23, left, pin_bottom, right, pin_top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
write_rect(f, ti.Metal3, left, pin_bottom, dc_right, pin_top)
write_viarect(
    f, ti.Via34, dc_right - dc_width, pin_bottom, dc_right, pin_top,
    ti.Via34_Width, ti.Via34_MinPitch, ti.Metal3_Via34_Enclosure
)

signal_width = 5000L
signal_space = 1000L
signal_pitch = signal_width + signal_space

# A[*] pins
srampin_coords = [
    # (left, right)
    (113825L, 115825L),
    (117700L, 119700L),
    (335100L, 337100L),
    (339100L, 341100L),
    (185150L, 187150L),
    (179750L, 181750L),
    (122025L, 124025L),
    (229050L, 231050L),
    (275025L, 277025L),
    (285375L, 287375L),
]
c_left = 65650L
c_right = 68450L

for i_pin, (srampin_left, srampin_right) in enumerate(srampin_coords):
    metal3_bottom = dc_top3 + signal_space + i_pin*signal_pitch
    metal3_top = metal3_bottom + signal_width

    # IO 180 deg rotated
    iopin_right = io1_left + io_width - c_left + i_pin*io_pitch
    iopin_left = io1_left + io_width - c_right + i_pin*io_pitch

    for i in xrange(4):
        left = sram1_left + srampin_left + i*sram_pitch
        right = sram1_left + srampin_right + i*sram_pitch
        write_rect(f, ti.Metal1, left, sram_top, right, metal3_top)
        write_viarect(
            f, ti.Via12, left, metal3_bottom, right, metal3_top,
            ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
        )

    left = min(sram1_left + srampin_left, iopin_left)
    right = max(sram1_left + srampin_right + 3*sram_pitch, iopin_right)
    write_rect(f, ti.Metal2, left, metal3_bottom, right, metal3_top)
    write_viarect(
        f, ti.Via23, iopin_left, metal3_bottom, iopin_right, metal3_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, iopin_left, metal3_bottom, iopin_right, die_height - ioring_width)

# OEB/WEB pins
iopin_left = 65650L
iopin_right = 68450L
oeb_left = 209325L
oeb_right = 211325L
web_left = 212950L
web_right = 214950L
# SRAM1; to left IO pin
# OEB
io_bottom = 860000L
io_top = io_bottom + io_width
# IO 270deg rotated
iopin_bottom = io_top - iopin_right
iopin_top = io_top - iopin_left
srampin_left = sram1_left + oeb_left
srampin_right = sram1_left + oeb_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, ioring_width, iopin_bottom, srampin_right, iopin_top)
# WEB
io_bottom = io_bottom - io_pitch
io_top = io_top - io_pitch
# IO 270deg rotated
iopin_bottom = io_top - iopin_right
iopin_top = io_top - iopin_left
srampin_left = sram1_left + web_left
srampin_right = sram1_left + web_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, ioring_width, iopin_bottom, srampin_right, iopin_top)
# SRAM2; to left IO pin
# OEB
io_bottom = io_bottom - io_pitch
io_top = io_bottom + io_width
# IO 270deg rotated
iopin_bottom = io_top - iopin_right
iopin_top = io_top - iopin_left
srampin_left = sram1_left + sram_pitch + oeb_left
srampin_right = sram1_left + sram_pitch + oeb_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, ioring_width, iopin_bottom, srampin_right, iopin_top)
# WEB
io_bottom = io_bottom - io_pitch
io_top = io_top - io_pitch
# IO 270deg rotated
iopin_bottom = io_top - iopin_right
iopin_top = io_top - iopin_left
srampin_left = sram1_left + sram_pitch + web_left
srampin_right = sram1_left + sram_pitch + web_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, ioring_width, iopin_bottom, srampin_right, iopin_top)
# SRAM3; to right IO pin
# OEB
# IO location stays the same
# IO 90deg rotated
iopin_bottom = io_bottom + iopin_left
iopin_top = io_bottom + iopin_right
srampin_left = sram1_left + 2*sram_pitch + oeb_left
srampin_right = sram1_left + 2*sram_pitch + oeb_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, srampin_left, iopin_bottom, die_width - ioring_width, iopin_top)
# WEB
io_bottom = io_bottom + io_pitch
io_top = io_top + io_pitch
# IO 90deg rotated
iopin_bottom = io_bottom + iopin_left
iopin_top = io_bottom + iopin_right
srampin_left = sram1_left + 2*sram_pitch + web_left
srampin_right = sram1_left + 2*sram_pitch + web_right
write_rect(f, ti.Metal1, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal1, srampin_left, iopin_bottom, die_width - ioring_width, iopin_top)
# SRAM4; to right IO pin; drawn in metal3
# OEB
io_bottom = io_bottom + io_pitch
io_top = io_top + io_pitch
# IO 90deg rotated
iopin_bottom = io_bottom + iopin_left
iopin_top = io_bottom + iopin_right
srampin_left = sram1_left + 3*sram_pitch + oeb_left
srampin_right = sram1_left + 3*sram_pitch + oeb_right
write_rect(f, ti.Metal3, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal3, srampin_left, iopin_bottom, die_width - ioring_width, iopin_top)
# WEB
io_bottom = io_bottom + io_pitch
io_top = io_top + io_pitch
# IO 90deg rotated
iopin_bottom = io_bottom + iopin_left
iopin_top = io_bottom + iopin_right
srampin_left = sram1_left + 3*sram_pitch + web_left
srampin_right = sram1_left + 3*sram_pitch + web_right
write_rect(f, ti.Metal3, srampin_left, iopin_top, srampin_right, sram_bottom)
write_rect(f, ti.Metal3, srampin_left, iopin_bottom, die_width - ioring_width, iopin_top)

# DIR pin; bottom left IO connected to IE & OEN of 8 pins on the right
c_left = 65650L
c_right = 68450L
oen_left = 2375L
oen_right = 5175L
ie_left = 76200L
ie_right = 79000L
io_left = 260000L
io_right = 340000L
left = io_left + c_left
right = io_left + c_right
bottom = ioring_width
top = bottom + signal_space
write_rect(f, ti.Metal1, left, bottom, right, top)
for i in xrange(8):
    left = io_left + (i + 1)*io_pitch + oen_left
    right = io_left + (i + 1)*io_pitch + oen_right
    write_rect(f, ti.Metal1, left, bottom, right, top)
    left = io_left + (i + 1)*io_pitch + ie_left
    right = io_left + (i + 1)*io_pitch + ie_right
    write_rect(f, ti.Metal1, left, bottom, right, top)
left = io_left + c_left
right = io_left + 8*io_pitch + ie_right
bottom = top
top = bottom + signal_width
write_rect(f, ti.Metal1, left, bottom, right, top)

# DIN[*]/DOUT[*] pins
# DIN -> C pin on IO; DOU -> I pin on IO
c_left = 65650L
c_right = 68450L
i_left = 25000L
i_right = 27800L
srampin_coords = [
    # (din_left, din_right, dout_left, dout_right)
    (47750L, 49750L, 57975L, 59975L),
    (89950L, 91950L, 100175L, 102175L),
    (132150L, 134150L, 142375L, 144375L),
    (174350L, 176350L, 184575L, 186575L),
    (278750L, 280750, 268525L, 270525L),
    (320950L, 322950L, 310725L, 312725L),
    (363150L, 365150L, 352925L, 354925L),
    (405350L, 407350L, 395125L, 397125L),
]

# We start from io_left/io_right from DIR pin
metal3_top = dc_bottom2 - signal_space
metal3_bottom = metal3_top - signal_width
for i_pin, (din_left, din_right, dout_left, dout_right) in enumerate(srampin_coords):
    # DIN -> C
    metal3_top -= signal_pitch
    metal3_bottom -= signal_pitch
    io_left += io_pitch
    io_right += io_pitch

    # IO not rotated
    iopin_left = io_left + c_left
    iopin_right = io_left + c_right
    for i in xrange(4):
        left = sram1_left + din_left + i*sram_pitch
        right = sram1_left + din_right + i*sram_pitch
        write_rect(f, ti.Metal1, left, metal3_bottom, right, sram_bottom)
        write_viarect(
            f, ti.Via12, left, metal3_bottom, right, metal3_top,
            ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
        )

    left = min(sram1_left + din_left, iopin_left)
    right = max(sram1_left + din_right + 3*sram_pitch, iopin_right)
    write_rect(f, ti.Metal2, left, metal3_bottom, right, metal3_top)
    write_viarect(
        f, ti.Via23, iopin_left, metal3_bottom, iopin_right, metal3_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, iopin_left, ioring_width, iopin_right, metal3_top)

    # DOUT -> I
    metal3_top -= signal_pitch
    metal3_bottom -= signal_pitch

    # IO not rotated
    iopin_left = io_left + i_left
    iopin_right = io_left + i_right
    for i in xrange(4):
        left = sram1_left + dout_left + i*sram_pitch
        right = sram1_left + dout_right + i*sram_pitch
        write_rect(f, ti.Metal1, left, metal3_bottom, right, sram_bottom)
        write_viarect(
            f, ti.Via12, left, metal3_bottom, right, metal3_top,
            ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
        )

    left = min(sram1_left + dout_left, iopin_left)
    right = max(sram1_left + dout_right + 3*sram_pitch, iopin_right)
    write_rect(f, ti.Metal2, left, metal3_bottom, right, metal3_top)
    write_viarect(
        f, ti.Via23, iopin_left, metal3_bottom, iopin_right, metal3_top,
        ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
    )
    write_rect(f, ti.Metal3, iopin_left, ioring_width, iopin_right, metal3_top)

# CE pin -> C
metal3_top -= signal_pitch
metal3_bottom -= signal_pitch
io_left += io_pitch
io_right += io_pitch
ce_left = 226000L
ce_right = 228000L
# IO not rotated
iopin_left = io_left + c_left
iopin_right = io_left + c_right
for i in xrange(4):
    left = sram1_left + ce_left + i*sram_pitch
    right = sram1_left + ce_right + i*sram_pitch
    write_rect(f, ti.Metal1, left, metal3_bottom, right, sram_bottom)
    write_viarect(
        f, ti.Via12, left, metal3_bottom, right, metal3_top,
        ti.Via12_Width, ti.Via12_MinPitch, ti.Metal1_Via12_Enclosure
    )

left = min(sram1_left + ce_left, iopin_left)
right = max(sram1_left + ce_right + 3*sram_pitch, iopin_right)
write_rect(f, ti.Metal2, left, metal3_bottom, right, metal3_top)
write_viarect(
    f, ti.Via23, iopin_left, metal3_bottom, iopin_right, metal3_top,
    ti.Via23_Width, ti.Via23_MinPitch, ti.Metal2_Via23_Enclosure
)
write_rect(f, ti.Metal3, iopin_left, ioring_width, iopin_right, metal3_top)


#
# Connect In to Out; C -> I; in METAL1
#
c_left = 65650L
c_right = 68450L
i_left = 25000L
i_right = 27800L
# 17->18; @ bottom
io1_left = 260000L + 10*io_pitch
io2_left = io1_left + io_pitch
# IO not rotated
io1pin_left = io1_left + c_left
io1pin_right = io1_left + c_right
io2pin_left = io2_left + i_left
io2pin_right = io2_left + i_right
bottom = ioring_width
top = bottom + signal_space
write_rect(f, ti.Metal1, io1pin_left, bottom, io1pin_right, top)
write_rect(f, ti.Metal1, io2pin_left, bottom, io2pin_right, top)
bottom = top
top = bottom + signal_width
write_rect(f, ti.Metal1, io1pin_left, bottom, io2pin_right, top)
# 27->28; @ right
io1_bottom = 260000L + 8*io_pitch
io2_bottom = io1_bottom + io_pitch
# IO 90 deg rotated
io1pin_bottom = io1_bottom + c_left
io1pin_top = io1_bottom + c_right
io2pin_bottom = io2_bottom + i_left
io2pin_top = io2_bottom + i_right
right = die_width - ioring_width
left = right - signal_space
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io1pin_top)
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io2pin_top)
right = left
left = right - signal_width
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io2pin_top)
# 29->30; @ right
io1_bottom += 2*io_pitch
io2_bottom += 2*io_pitch
# IO 90 deg rotated
io1pin_bottom = io1_bottom + c_left
io1pin_top = io1_bottom + c_right
io2pin_bottom = io2_bottom + i_left
io2pin_top = io2_bottom + i_right
right = die_width - ioring_width
left = right - signal_space
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io1pin_top)
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io2pin_top)
right = left
left = right - signal_width
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io2pin_top)
# 31->32; @ top
io1_right = 340000L + 11*io_pitch
io2_right = io1_right - io_pitch
# IO 180 deg rotated
io1pin_left = io1_right - c_right
io1pin_right = io1_right - c_left
io2pin_left = io2_right - i_right
io2pin_right = io2_right - i_left
top = die_height - ioring_width
bottom = top - signal_space
write_rect(f, ti.Metal1, io1pin_left, bottom, io1pin_right, top)
write_rect(f, ti.Metal1, io2pin_left, bottom, io2pin_right, top)
top = bottom
bottom = top - signal_width
write_rect(f, ti.Metal1, io2pin_left, bottom, io1pin_right, top)
# 43->44; @ left
io1_top = 340000L + 11*io_pitch
io2_top = io1_top - io_pitch
# IO 270 deg rotated
io1pin_bottom = io1_top - c_right
io1pin_top = io1_top - c_left
io2pin_bottom = io2_top - i_right
io2pin_top = io2_top - i_left
left = ioring_width
right = left + signal_space
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io1pin_top)
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io2pin_top)
left = right
right = left + signal_width
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io1pin_top)
# 45->46; @ left
io1_top -= 2*io_pitch
io2_top -= 2*io_pitch
# IO 270 deg rotated
io1pin_bottom = io1_top - c_right
io1pin_top = io1_top - c_left
io2pin_bottom = io2_top - i_right
io2pin_top = io2_top - i_left
left = ioring_width
right = left + signal_space
write_rect(f, ti.Metal1, left, io1pin_bottom, right, io1pin_top)
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io2pin_top)
left = right
right = left + signal_width
write_rect(f, ti.Metal1, left, io2pin_bottom, right, io1pin_top)


pincoords = {
    "C": (65650L, 68450L),
    "OEN": (2375L, 5175L),
    "IE": (76200L, 79000L),
    "I": (25000L, 27800L),
    "SE": (72600L, 75400L),
    "PS": (48900L, 51700L),
    "PE": (52500L, 55300L),
    "DS": (43200L, 46000L),
}
pinconfigs = []
# Inputs only
pins = [
    3, 4, 5, 6, 19, 20, 21, 22, # WEB?/OEB?
    7, # DIR
    33, 34, 35, 36, 37, 38, 39, 40, 41, 42, # A[*]
]
for pin in pins:
    pinconfigs += [
        (pin, "OEN", 1),
        (pin, "IE", 1),
        (pin, "I", 0),
        (pin, "DS", 0),
        (pin, "PS", 1),
        (pin, "PE", 1),
        (pin, "SE", 0),
    ]
# DIO[*]
pins = [8, 9, 10, 11, 12, 13, 14, 15]
for pin in pins:
    pinconfigs += [
        (pin, "DS", 0),
        (pin, "PS", 0),
        (pin, "PE", 1),
        (pin, "SE", 0),
    ]
# CE; Schmitt trigger
pinconfigs += [
    (16, "OEN", 1),
    (16, "IE", 1),
    (16, "I", 0),
    (16, "DS", 0),
    (16, "PS", 0),
    (16, "PE", 1),
    (16, "SE", 1),
]
# Direct in
# DS 0
pins = [17, 27, 29, 31, 43, 45]
for pin in pins:
    pinconfigs += [
        (pin, "OEN", 1),
        (pin, "IE", 1),
        (pin, "I", 0),
        (pin, "DS", 0),
        (pin, "PS", 1),
        (pin, "PE", 1),
        (pin, "SE", 0),
    ]
# Direct out
pins = [pin+1 for pin in pins]
ds = 0
for pin in pins:
    pinconfigs += [
        (pin, "OEN", 0),
        (pin, "IE", 0),
        (pin, "DS", ds),
        (pin, "PS", 0),
        (pin, "PE", 0),
        (pin, "SE", 0),
    ]
    ds = 1 if ds == 0 else 0
    
for pin, pinname, value in pinconfigs:
    pin_left, pin_right = pincoords[pinname]
    layer = 18 if value == 0 else 31
    if pin <= 6:
        # On left; 270 deg rotated
        io_top = 340000L + (6 - pin)*io_pitch
        bottom = io_top - pin_right
        top = io_top - pin_left
        left = ioring_width
        right = dc_left
    elif pin <= 18:
        # On left; not rotated
        io_left = 260000L + (pin - 7)*io_pitch
        left = io_left + pin_left
        right = io_left + pin_right
        bottom = ioring_width
        top = dc_bottom
    elif pin <= 30:
        # On right; 90 deg rotated
        io_bottom = 260000L + (pin - 19)*io_pitch
        bottom = io_bottom + pin_left
        top = io_bottom + pin_right
        right = die_width - ioring_width
        left = dc_right
    elif pin <= 42:
        # On top; 180 def rotated
        io_right = 340000L + (42 - pin)*io_pitch
        left = io_right - pin_right
        right = io_right - pin_left
        top = die_height - ioring_width
        bottom = dc_top
    elif pin <= 48:
        # On left; 270 deg rotated
        io_top = 340000L + (48 + 6 - pin)*io_pitch
        bottom = io_top - pin_right
        top = io_top - pin_left
        left = ioring_width
        right = dc_left
    else:
        raise(Exception("Wrong pin number"))

    write_rect(f, layer, left, bottom, right, top)


#
# Tail
#
write_celltail(f, "SnowWhiteI_Core")
f.close()
