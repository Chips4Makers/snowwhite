#!/bin/env python
from gdstxt_funcs import *
# Get private tech information
import priv.TSMC_CL035G as ti


# core dimensions
# Dimensions are in nm and long values used
core_width = 2800000L
core_height = 2800000L
core2seal_space = 19750L

x_left = -core2seal_space
x_right = core_width + core2seal_space
y_bottom = -core2seal_space
y_top = core_height + core2seal_space


#
f = open("SnowWhiteI_SealRing.txt", "w")


#
# Header
#
write_cellhead(f, "SnowWhiteI_SealRing")


#
# PDIFF
#
width = 10000L
ring_left = x_left - width
ring_right = x_right + width
ring_bottom = y_bottom - width
ring_top = y_top + width
write_ring(f, ti.Diffusion, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.PPlus, ring_left - 250L, ring_bottom - 250L, ring_right + 250L, ring_top + 250L, width + 500L)


#
# CONT
#

#ring1
dist = 5000L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Contact, ring_left, ring_bottom, ring_right, ring_top, ti.Contact_Width, ti.Contact_MinPitch)

#ring2
dist = 3200L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Contact, ring_left, ring_bottom, ring_right, ring_top, ti.Contact_Width, ti.Contact_MinPitch)

#ring3
dist = 1400L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Contact, ring_left, ring_bottom, ring_right, ring_top, ti.Contact_Width, ti.Contact_MinPitch)


#
#metal1/2/3/4
#
width = 6000L
ring_left = x_left - width
ring_right = x_right + width
ring_bottom = y_bottom - width
ring_top = y_top + width
write_ring(f, ti.Metal1, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal2, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal3, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal4, ring_left, ring_bottom, ring_right, ring_top, width)


#
#  VIA12/34
#

#ring1
dist = 4100L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Via12, ring_left, ring_bottom, ring_right, ring_top, ti.Via12_Width, ti.Via12_MinPitch)
write_viaring(f, ti.Via34, ring_left, ring_bottom, ring_right, ring_top, ti.Via34_Width, ti.Via34_MinPitch)

#ring2
dist = 2400L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Via12, ring_left, ring_bottom, ring_right, ring_top, ti.Via12_Width, ti.Via12_MinPitch)
write_viaring(f, ti.Via34, ring_left, ring_bottom, ring_right, ring_top, ti.Via34_Width, ti.Via34_MinPitch)


#
#  VIA23
#

#ring1
dist = 5100L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Via23, ring_left, ring_bottom, ring_right, ring_top, ti.Via23_Width, ti.Via23_MinPitch)

#ring2
dist = 3300L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Via23, ring_left, ring_bottom, ring_right, ring_top, ti.Via23_Width, ti.Via23_MinPitch)

#ring3
dist = 1500L
ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_viaring(f, ti.Via23, ring_left, ring_bottom, ring_right, ring_top, ti.Via23_Width, ti.Via23_MinPitch)


#
# PAD
#
dist = 10000L
width = 2000L

ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_ring(f, ti.Pad, ring_left, ring_bottom, ring_right, ring_top, width)


#
# DMEXCL
#
dist = 20000L
width = 30000L

ring_left = x_left - dist
ring_right = x_right + dist
ring_bottom = y_bottom - dist
ring_top = y_top + dist
write_ring(f, ti.Metal1_Block, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal2_Block, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal3_Block, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Metal4_Block, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Diffusion_Block, ring_left, ring_bottom, ring_right, ring_top, width)
write_ring(f, ti.Poly_Block, ring_left, ring_bottom, ring_right, ring_top, width)


#
# Footer
#
write_celltail(f, "SnowWhiteI_SealRing")
f.close()
