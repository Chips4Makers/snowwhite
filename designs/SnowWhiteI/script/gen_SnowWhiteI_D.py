#!/bin/env python
from gdstxt_funcs import *
# Get private tech information
import priv.TSMC_CL035G as ti


die_width = 2800000L
die_height = 2800000L
sealring_width = 30000L
die_space = 340000L


f = open("SnowWhiteI_D.txt", "w")
write_cellhead(f, "SnowWhiteI_D")

# Dice
x = 0L
y = 0L
write_inst(f, "DIE1", "SnowWhiteI_S", x, y)
write_inst(f, "DIE1SEAL", "SnowWhiteI_SealRing", x, y)
x += die_width + die_space
write_inst(f, "DIE2", "SnowWhiteI_S", x, y)
write_inst(f, "DIE2SEAL", "SnowWhiteI_SealRing", x, y)

# boundary
left = -sealring_width
bottom = -sealring_width
right = 2*die_width + die_space + sealring_width
top = die_height + sealring_width
write_rect(f, ti.Boundary, left, bottom, right, top)

# No dummies in between dice
left = die_width
right = left + die_space
bottom = -sealring_width
top = die_height + sealring_width
write_rect(f, ti.Diffusion_Block, left, bottom, right, top)
write_rect(f, ti.Poly_Block, left, bottom, right, top)
write_rect(f, ti.Metal1_Block, left, bottom, right, top)
write_rect(f, ti.Metal2_Block, left, bottom, right, top)
write_rect(f, ti.Metal3_Block, left, bottom, right, top)
write_rect(f, ti.Metal4_Block, left, bottom, right, top)


write_celltail(f, "SnowWhiteI_D")
f.close()
