# Description

This is the design for the first SnowWhite test chip on 0.35um.

As time was too short to get place-and-route going with the TSMC standard cell library the first design contains test SRAMs. It will be used to test the SRAMs, the IOs and look at packaging options.

The content in designs/SnowWhiteI is not kept up to date with the changes in the repo. So in order to go back to state when SnowWhiteI was taped one has to sheckout the SnowWhiteI tag.

# Pinout

| Pin | Name    | | Pin | Name  |
|-----|---------|-|-----|-------|
| 1   | VDDCORE | | 25  | VDDIO |
| 2   | GNDCORE | | 26  | GNDIO |
| 3   | OEB1    | | 27  | IN2   |
| 4   | WEB1    | | 28  | OUT2  |
| 5   | OEB2    | | 29  | IN3   |
| 6   | WEB2    | | 30  | OUT3  |
| 7   | DIR     | | 31  | IN4   |
| 8   | DIO[0]  | | 32  | OUT4  |
| 9   | DIO[1]  | | 33  | A[9]  |
| 10  | DIO[2]  | | 34  | A[8]  |
| 11  | DIO[3]  | | 35  | A[7]  |
| 12  | DIO[4]  | | 36  | A[6]  |
| 13  | DIO[5]  | | 37  | A[5]  |
| 14  | DIO[6]  | | 38  | A[4]  |
| 15  | DIO[7]  | | 39  | A[3]  |
| 16  | CLK     | | 40  | A[2]  |
| 17  | IN1     | | 41  | A[1]  |
| 18  | OUT1    | | 42  | A[0]  |
| 19  | OEB3    | | 43  | IN5   |
| 20  | WEB3    | | 44  | OUT5  |
| 21  | OEB4    | | 45  | IN6   |
| 22  | WEB4    | | 46  | OUT6  |
| 23  | GNDCORE | | 47  | GNDIO |
| 25  | VDDCORE | | 48  | VDDIO |

# Signal description

There are 4 SRAM blocks on the design: SRAM1-SRAM4; they are 1024x8 in size. They are synchronous so the read or write cycle is started on rising edge of the clock signal. It has the following inputs:

* A[9:0]: Address
* DIN[7:0]: The input data to write in the SRAM
* DOUT[7:0]: The output data of the last read/write cycle
* CE: The clock signal
* WEB: write-enable, active low
* OEB: output-enable, active low

Description of the DIL package pins:

* VDDCORE/GNDCORE: supply and ground for the SRAMs
* VDDIO/GNDIO: supply and ground for the IOs
* A[9]-A[0]: address signal; connected to address of the four SRAMs
* DIO[7]-DIO[0]: data signals; value depens on DIR pin
* DIR: direction of the data signals  
  1: DIO input to DIN[7:0] of the four SRAM blocks  
  0: DIO output from DOUT[7:0] of the four SRAM blocks; only one OEBn signal should be low 
* WEBn/OEBn: write- and data-enable signal connected each to WEB/OEB for SRAMn
* INn/OUTn: connections

For all cells the 0204 drive strength cells are used with DS set to 0. Excpetion is the
OUTn signal where drive strengths is varied:

| Signal | IO   | DS |
|--------|------|----|
| OUT1   | 0204 | 0  |
| OUT2   | 0204 | 1  |
| OUT3   | 0812 | 0  |
| OUT4   | 0812 | 1  |
| OUT5   | 1216 | 0  |
| OUT6   | 1216 | 1  |

# Remarks

* ioring.txt has been fixed manually for the drive strength change; it does not match ioring.def
  anymore
