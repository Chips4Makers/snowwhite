// Top level of SnowWhiteI for simulation/verification

module SnowWhiteI_S(
    inout OEB1,
    inout WEB1,
    inout OEB2,
    inout WEB2,
    inout OEB3,
    inout WEB3,
    inout OEB4,
    inout WEB4,
    inout DIR,
    inout DIO[7:0],
    inout A[9:0],
    inout CLK,
    inout IN1,
    inout OUT1,
    inout IN2,
    inout OUT2,
    inout IN3,
    inout OUT3,
    inout IN4,
    inout OUT4,
    inout IN5,
    inout OUT5,
    inout IN6,
    inout OUT6
);

initial begin
    $dumpfile("test.vcd");
    $dumpvars;
end

wire OEB1int, OEB2int, OEB3int, OEB4int;
wire WEB1int, WEB2int, WEB3int, WEB4int;
wire DIRint;
wire DINint[7:0], DOUTint[7:0];
wire Aint[9:0];
wire CLKint;
wire INOUT1, INOUT2, INOUT3, INOUT4, INOUT5, INOUT6;


PDDWUWSW0204DGZ IO3(
    .PAD(OEB1),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(OEB1int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO4(
    .PAD(WEB1),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(WEB1int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO5(
    .PAD(OEB2),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(OEB2int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO6(
    .PAD(WEB2),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(WEB2int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO7(
    .PAD(DIR),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(DIRint),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO8(
    .PAD(DIO[1'd0]),
    .OEN(DIRint),
    .I(DOUTint[1'd0]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[1'd0]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO9(
    .PAD(DIO[1]),
    .OEN(DIRint),
    .I(DOUTint[1]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[1]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO10(
    .PAD(DIO[2]),
    .OEN(DIRint),
    .I(DOUTint[2]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[2]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO11(
    .PAD(DIO[3]),
    .OEN(DIRint),
    .I(DOUTint[3]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[3]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO12(
    .PAD(DIO[4]),
    .OEN(DIRint),
    .I(DOUTint[4]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[4]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO13(
    .PAD(DIO[5]),
    .OEN(DIRint),
    .I(DOUTint[5]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[5]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO14(
    .PAD(DIO[6]),
    .OEN(DIRint),
    .I(DOUTint[6]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[6]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO15(
    .PAD(DIO[7]),
    .OEN(DIRint),
    .I(DOUTint[7]),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(DINint[7]),
    .SE(1'd0),
    .IE(DIRint)
);

PDDWUWSW0204DGZ IO16(
    .PAD(CLK),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd1),
    .C(CLKint),
    .SE(1'd1),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO17(
    .PAD(IN1),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT1),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO18(
    .PAD(OUT1),
    .OEN(1'd0),
    .I(INOUT1),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);

PDDWUWSW0204DGZ IO19(
    .PAD(OEB3),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(OEB3int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO20(
    .PAD(WEB3),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(WEB3int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO21(
    .PAD(OEB4),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(OEB4int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO22(
    .PAD(WEB4),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(WEB4int),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO27(
    .PAD(IN2),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd1),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT2),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO28(
    .PAD(OUT2),
    .OEN(1'd0),
    .I(INOUT2),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);

PDDWUWSW0204DGZ IO29(
    .PAD(IN3),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT3),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0812DGZ IO30(
    .PAD(OUT3),
    .OEN(1'd0),
    .I(INOUT3),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);

PDDWUWSW0204DGZ IO31(
    .PAD(IN4),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd1),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT4),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0812DGZ IO32(
    .PAD(OUT4),
    .OEN(1'd0),
    .I(INOUT4),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);
    
PDDWUWSW0204DGZ IO33(
    .PAD(A[9]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[9]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO34(
    .PAD(A[8]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[8]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO35(
    .PAD(A[7]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[7]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO36(
    .PAD(A[6]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[6]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO37(
    .PAD(A[5]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[5]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO38(
    .PAD(A[4]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[4]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO39(
    .PAD(A[3]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[3]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO40(
    .PAD(A[2]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[2]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO41(
    .PAD(A[1]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[1]),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW0204DGZ IO42(
    .PAD(A[1'd0]),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(Aint[1'd0]),
    .SE(1'd0),
    .IE(1'd1)
);

PDDWUWSW0204DGZ IO43(
    .PAD(IN5),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd0),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT5),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW1216DGZ IO44(
    .PAD(OUT5),
    .OEN(1'd0),
    .I(INOUT5),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);

PDDWUWSW0204DGZ IO45(
    .PAD(IN6),
    .OEN(1'd1),
    .I(1'd0),
    .DS(1'd1),
    .PS(1'd1),
    .PE(1'd1),
    .C(INOUT6),
    .SE(1'd0),
    .IE(1'd1)
);
    
PDDWUWSW1216DGZ IO46(
    .PAD(OUT6),
    .OEN(1'd0),
    .I(INOUT6),
    .DS(1'd0),
    .PS(1'd0),
    .PE(1'd0),
    .C(),
    .SE(1'd0),
    .IE(1'd0)
);

TM035SSRAM1024X8 SRAM1(
    .CE(CLKint),
    .OEB(OEB1int),
    .WEB(WEB1int),
    .A(Aint[9:0]),
    .DIN(DINint[7:0]),
    .DOUT(DOUTint[7:0])
);

TM035SSRAM1024X8 SRAM2(
    .CE(CLKint),
    .OEB(OEB2int),
    .WEB(WEB2int),
    .A(Aint[9:0]),
    .DIN(DINint[7:0]),
    .DOUT(DOUTint[7:0])
);

TM035SSRAM1024X8 SRAM3(
    .CE(CLKint),
    .OEB(OEB3int),
    .WEB(WEB3int),
    .A(Aint[9:0]),
    .DIN(DINint[7:0]),
    .DOUT(DOUTint[7:0])
);

TM035SSRAM1024X8 SRAM4(
    .CE(CLKint),
    .OEB(OEB4int),
    .WEB(WEB4int),
    .A(Aint[9:0]),
    .DIN(DINint[7:0]),
    .DOUT(DOUTint[7:0])
);


endmodule
