-- The Retro_uC generic RAM block

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TM035SSRAM1024X8 is
  port (
    CE:         in std_logic;
    OEB:        in std_logic;
    WEB:        in std_logic;
    -- Width of address will determine number of words in the RAM
    A:          in std_logic_vector;
    -- DIN and DOUT have to have the same width
    DIN:        in std_logic_vector;
    DOUT:       out std_logic_vector
  );
end entity TM035SSRAM1024X8;

architecture rtl of TM035SSRAM1024X8 is
  type ram_type is array (0 to (2**A'length)-1) of std_logic_vector(DIN'range);
  signal RAM:           ram_type;
  signal A_hold:  std_logic_vector(A'range);
begin
  process(CE) is
  begin
    if (rising_edge(CE)) then
      A_hold <= A;
      if WEB = '0' then
        -- Write cycle
        RAM(to_integer(unsigned(A))) <= DIN;
      end if;
    end if;
  end process;
  DOUT <= RAM(to_integer(unsigned(A_hold))) when OEB = '0' else
          "ZZZZZZZZ";
end architecture rtl;

