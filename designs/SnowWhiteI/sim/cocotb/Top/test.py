import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

@cocotb.test()
def test01_resetrun(dut):
    "Basic test"

    us = get_sim_steps(1, "us")

    dut.DIR <= 1
    dut.CLK <= 0

    yield Timer(us)

    dut.DIO[0] <= 0
    dut.DIO[1] <= 0
    dut.DIO[2] <= 0
    dut.DIO[3] <= 0
    dut.DIO[4] <= 0
    dut.DIO[5] <= 0
    dut.DIO[6] <= 0
    dut.DIO[7] <= 0
    dut.OEB1 <= 0
    dut.OEB2 <= 1
    dut.OEB3 <= 1
    dut.OEB4 <= 1
    dut.WEB1 <= 0
    dut.WEB2 <= 0
    dut.WEB3 <= 0
    dut.WEB4 <= 0
    dut.A[0] <= 0
    dut.A[1] <= 1
    dut.A[2] <= 0
    dut.A[3] <= 1
    dut.A[4] <= 0
    dut.A[5] <= 1
    dut.A[6] <= 0
    dut.A[7] <= 1
    dut.A[8] <= 0
    dut.A[9] <= 1
    dut.DIO[0] <= 0
    dut.DIO[1] <= 0
    dut.DIO[2] <= 0
    dut.DIO[3] <= 0
    dut.DIO[4] <= 0
    dut.DIO[5] <= 0
    dut.DIO[6] <= 0
    dut.DIO[7] <= 0
    dut.IN1 <= 0
    dut.IN2 <= 0
    dut.IN3 <= 0
    dut.IN4 <= 0
    dut.IN5 <= 0
    dut.IN6 <= 0

    yield Timer(us)

    dut.CLK <= 1
    dut.IN1 <= 1
    dut.IN2 <= 1
    dut.IN3 <= 1
    dut.IN4 <= 1
    dut.IN5 <= 1
    dut.IN6 <= 1

    yield Timer(us)

    dut.IN1 <= 0

    yield Timer(us)
