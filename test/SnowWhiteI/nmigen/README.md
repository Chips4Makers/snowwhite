This directory contains scripts that were used to test SnowWhiteI.

The sram_test.py is used to test the SRAMs on the SnowWhiteI chip.
It was done using a BlackIce board and using nmigen + nmigen-boards.
Look for definition of swI_res in the file to see how signals need to be
connected to the chip.

TODO: test drive strength of the different IO cells.
