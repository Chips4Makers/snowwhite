#!/bin/env python3
import types

from nmigen import *
from nmigen.build import *
from nmigen_boards.blackice import BlackIcePlatform

class Debounce(Elaboratable):
    def __init__(self, sig=None, samplerate=1000, samples=4):
        self.samplerate = samplerate
        self.samples = samples
        self.i = Signal() if sig is None else sig
        self.o = Signal()

    def elaborate(self, platform):
        MAX_SAMPLED = 2**self.samples - 1

        m = Module()

        sampled = Signal(self.samples)
        cnt = Signal(max=self.samplerate-1)
        with m.If(cnt == 0):
            m.d.sync += [
                sampled[0].eq(self.i),
                sampled[1:].eq(sampled[:-1]),
                cnt.eq(self.samplerate - 1),
            ]
        with m.Else():
            m.d.sync += cnt.eq(cnt - 1)

        with m.If(sampled == 0):
            m.d.sync += self.o.eq(0)
        with m.Elif(sampled == MAX_SAMPLED):
            m.d.sync += self.o.eq(1)

        return m

def debounce(m, sig):
    deb = Debounce(sig)
    m.submodules += deb

    return deb.o


class Rise(Elaboratable):
    def __init__(self, sig=None):
        self.i = Signal() if sig is None else sig
        self.o = Signal()

    def elaborate(self, platform):
        m = Module()

        prev = Signal()
        m.d.sync += prev.eq(self.i)
        m.d.comb += self.o.eq(Cat(self.i, prev) == 0b01)

        return m

def rise(m, sig):
    r = Rise(sig)
    m.submodules += r

    return r.o


class SRAMs_test(Elaboratable):
    clk_name = "clk100"

    def elaborate(self, platform):
        m = Module()
        
        clk = platform.request(self.clk_name, 0)
        m.domains.sync = ClockDomain()
        m.d.comb += ClockSignal().eq(clk.i)

        signames = [
            "swi_clk", "dir", "oe", "we", "dio", "a",
            "user_ledr", "user_ledg", "user_ledo", "user_ledb",
            "user_btn",
            ("dbg", 0), ("dbg", 1),
        ]
        swi_sigs = types.SimpleNamespace()
        for name in signames:
            if type(name) == str:
                setattr(swi_sigs, name, platform.request(name, 0))
            else:
                name2 = name[0]
                num = name[1]
                setattr(swi_sigs, "{}_{}".format(name2, num), platform.request(name2, num))

        swi_clk = Signal(reset_less=True)
        we = Signal(2, reset_less=True)
        reading = Signal(reset_less=True)
        din = Signal(8, reset_less=True)
        dout = Signal(8, reset_less=True)
        oe = Signal(2, reset_less=True)
        a = Signal(8)
        red = Signal(reset_less=True)
        orange = Signal(reset_less=True)
        green = Signal(reset_less=True)
        blue = Signal()
        dbg = Signal(16)

        m.d.comb += [
            reading.eq(we == 0),
            swi_sigs.swi_clk.o.eq(swi_clk),
            swi_sigs.dir.o.eq(~reading), # dir is 0 when writing
            swi_sigs.oe.o.eq(oe),
            swi_sigs.we.o.eq(we),
            swi_sigs.dio.o.eq(dout),
            din.eq(swi_sigs.dio.i),
            swi_sigs.dio.oe.eq(~reading),
            swi_sigs.a.o.eq(a),
            swi_sigs.user_ledr.o.eq(red),
            swi_sigs.user_ledo.o.eq(orange),
            swi_sigs.user_ledg.o.eq(green),
            swi_sigs.user_ledb.o.eq(blue),
            swi_sigs.dbg_0.o.eq(dbg[0:8]),
            swi_sigs.dbg_1.o.eq(dbg[8:]),
        ]

        # Make button toggle push
        sw = debounce(m, swi_sigs.user_btn.i)
        push = rise(m, sw)
        with m.If(push == 1):
            m.d.sync += blue.eq(~blue)

        with m.FSM():
            SWI_DIVIDE2 = 20
            cnt = Signal(max=SWI_DIVIDE2-1)
            with m.State("START"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(0),
                    oe.eq(0b00),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                m.d.sync += [
                    a.eq(0xFF),
                    cnt.eq(SWI_DIVIDE2-1),
                ]
                m.next = "WRITE0"
            with m.State("WRITE0"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(0),
                    oe.eq(0b00),
                    we.eq(0b01),
                    dout.eq(a),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    m.next = "WRITE0B"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("WRITE0B"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(1),
                    oe.eq(0b00),
                    we.eq(0b01),
                    dout.eq(a),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    m.next = "WRITE1"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("WRITE1"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(0),
                    oe.eq(0b00),
                    we.eq(0b10),
                    dout.eq(~a),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    m.next = "WRITE1B"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("WRITE1B"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(1),
                    oe.eq(0b00),
                    we.eq(0b10),
                    dout.eq(~a),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    with m.If(a == 0):
                        m.d.sync += a.eq(0xFF)
                        m.next = "READ0"
                    with m.Else():
                        m.d.sync += a.eq(a - 1)
                        m.next = "WRITE0"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("READ0"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(0),
                    oe.eq(0b01),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    m.next = "READ0B"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("READ0B"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(1),
                    oe.eq(0b01),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    with m.If(din == a):
                        m.next = "READ1"
                    with m.Else():
                        m.next = "ERROR"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("READ1"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(0),
                    oe.eq(0b10),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    m.next = "READ1B"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("READ1B"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(1),
                    red.eq(0),
                    swi_clk.eq(1),
                    oe.eq(0b10),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                with m.If(cnt == 0):
                    m.d.sync += cnt.eq(SWI_DIVIDE2 - 1)
                    with m.If(din == ~a):
                        with m.If(a == 0):
                            m.next = "DONE"
                        with m.Else():
                            m.d.sync += a.eq(a - 1)
                            m.next = "READ0"
                    with m.Else():
                        m.next = "ERROR"
                with m.Else():
                    m.d.sync += cnt.eq(cnt - 1)
            with m.State("DONE"):
                m.d.comb += [
                    green.eq(1),
                    orange.eq(0),
                    red.eq(0),
                    swi_clk.eq(1),
                    oe.eq(0b01),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                # Button press toggles blue led and if on will contantly read the memmory
                with m.If(blue == 1):
                    m.d.sync += [
                        cnt.eq(SWI_DIVIDE2 - 1),
                        a.eq(0xFF),
                    ]
                    m.next = "READ0"
            with m.State("ERROR"):
                m.d.comb += [
                    green.eq(0),
                    orange.eq(0),
                    red.eq(1),
                    swi_clk.eq(1),
                    oe.eq(0b01),
                    we.eq(0b00),
                    dout.eq(0),
                ]
                # HALT

        # dbg output
        m.d.comb += [
            dbg[0:8].eq(din),
            dbg[8:].eq(a),
        ]
        return m


# The breadboard connection to the chip
# To reduce pins only 2 oe, 2 we and 8 a pins are used.
swI_res = [
    Resource("swi_clk", 0, Pins("1", dir="o", conn=("pmod", 0)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("dir", 0, Pins("2", dir="o", conn=("pmod", 0)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("oe", 0, PinsN("7 8", dir="o", conn=("pmod", 0)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("we", 0, PinsN("9 10", dir="o", conn=("pmod", 0)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("dio", 0, Pins("1 2 3 4 7 8 9 10", dir="io", conn=("pmod", 1)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("a", 0, Pins("1 2 3 4 7 8 9 10", dir="o", conn=("pmod", 2)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("dbg", 0, Pins("1 2 3 4 7 8 9 10", dir="o", conn=("pmod" ,4)), Attrs(IO_STANDARD="SB_LVCMOS33")),
    Resource("dbg", 1, Pins("1 2 3 4 7 8 9 10", dir="o", conn=("pmod" ,5)), Attrs(IO_STANDARD="SB_LVCMOS33")),
]


if __name__ == "__main__":
    p = BlackIcePlatform()
    p.add_resources(swI_res)

    test = SRAMs_test()

    #p.build(test, nextpnr_opts="", do_program=True)
    p.prepare(test, nextpnr_opts="")
